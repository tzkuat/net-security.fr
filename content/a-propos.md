+++
title = "A Propos"
description = "Présentation des auteurs de Net-Security"
date = "2021-11-14"
aliases = ["about-us","about"]
author = "Net-Security"
+++

Net-Security est né en 2018 grâce à 3  anciens étudiants en informatique : Adrien Pavone, Fabio Pace & Mickael Rigonnaux. Le but était de partager les connaissances acquises en entreprise et durant les études. Les sujets abordés sont la sécurité, le réseau, le système... Ces derniers temps le blog tant à se spécialiser vers les logiciels libres afin de les promouvoir.

Le blog est aujourd'hui maintenu seulement par Mickael Rigonnaux.

Aujourd'hui nous sommes tous les trois des professionnels de l'informatique avec 3 profils variés :
* RSSI / Ingénieur en sécurité
* Ingénieur DevOps
* Entrepeneur dans la prestation de service informatique

Si vous avez des questions vous pouvez nous joindre sur l'adresse suivante :
* contact[at]net-security.fr

### Fabio Pace

* Mail : fabio.pace[at]net-security.fr

Passionné par l’informatique depuis tout jeune, j’aime comprendre le fonctionnement d’un Système et le mettre en place. J’aime la domotique et les objets connectés mais j’aime également comprendre comment améliorer l’équipement d’un point de vue sécurité. 

La sécurité informatique est pour moi un aspect très important qu’il ne faut pas prendre à la légère.

Je suis également entrepreneur, si vous recherchez un prestataire pour un audit en sécurité et/ou une mise en place d’architecture réseau au sein de votre Système d’Information, n’hésitez pas à me contacter à l’adresse suivante : fabio.pace@datasys.fr ou en visitant le [site web](https://datasys.fr)

### Mickael Rigonnaux

* Mail : mickael.rigonnaux[at]net-security.fr

Eternel touche à tout & libriste, je vais essayer de partager avec vous ma passion pour les logiciels libres, le réseau & la sécurité. Passé par un BTS SIO & un Master en sécurité informatique, j'évolue actuellement dans l'univers du Datacenter. Btw i use Arch.

Lien : [Twitter](https://twitter.com/tzkuat) - [Linkedin](https://www.linkedin.com/in/mrigonnaux/) - [Site Perso](https://tzku.at)

### Adrien Pavone

* Mail : adrien.pavone[at]net-security.fr

Passionné d’informatique, j’appréciais déjà dès mon plus jeune âge monter/démonter des ordinateurs, les analyser, les défragmenter (quand tu « sais » faire ces choses à 12 ans tu deviens l’informaticien de la famille) et développer des sites internet.

J’apprécie tout ce qui tourne autour de l’automatisation, l’intégration continue, les tests de performance, le développement 🙂 Je rédigerai donc des articles sur ces différents domaines.

Entrepreneur depuis Février 2020, n’hésitez pas à me contacter pour toute demande sur des sujets DevOps, d’automatisation, d’intégration ou encore de performance sur hello@octocloudservices.com. Plus d’informations sur [OctoCloudServices](https://octocloudservices.com)

Lien : [Twitter](https://twitter.com/apavone5) - [Linkedin](https://www.linkedin.com/in/adrien-pavone/) - [Site Perso](https://octocloudservices.com/)

