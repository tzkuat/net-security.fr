---
title: Commandes GNU/Linux en vrac – Partie 3
author: Mickael Rigonnaux
type: post
date: 2022-02-02T22:20:44+00:00
url: /system/commandes-gnu-linux-en-vrac-partie-3/
thumbnail: /images/logo-3.png
featureImage: images/logo-3.png
shareImage: /images/logo-3.png
featured: true
tags: [
    "logiciellibre",
    "GNU/Linux"
]
categories: [
    "systeme",
    "logiciellibre"
]

---

Bonjour à tous ! Aujourd’hui le 3ème article de la série « Commandes GNU/Linux en vrac ». Les autres sont disponibles ici :

  * 1er : https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-1
  * 2ème : https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-2

Le but est de présenter et de vous faire découvrir des commandes et des outils qui pourraient être utiles pour vous.

## Commande wall

La commande `wall` est présente dans la quasi totalité des distributions GNU/Linux, son but est très simple, afficher un message dans le terminal pour l'ensemble des utilisateurs connectés. Imaginez que vous travaillez sur une machine et qu'un redémarrage soit nécessaire. En utilisant la commande `who` vous remarquez que plusieurs personnes sont connectées sur la fameuse machine. Vous pouvez alors les prévenir en utilisant la commande suivante :
* `wall "Attention, redémarrage de la machine prévue à 10h00. Merci de contacter XXX en cas de problème."`

Ce message va alors s'afficher sur tous les terminaux connectés à la machine. Cela générera une alerte via la GUI également.

Voici un exemple sur ma VM Manjaro avec deux sessions ouvertes :
![Illustration Wall](/images/gnulinux-vrac-3/gnulinux-vrac-3-1.png)

Il est également possible d'utiliser des options sur cet outil comme `-t` pour rajouter un timeout ou `-g` pour alerter seulement un groupe d'utilisateur.

## Commande chfn

Pour rejoindre la commande `wall`, la commande `chfn` (pour Change Your Finger iNformation) va permettre de modifier les informations personnelles des utilisateurs stockées dans `/etc/passwd`. Ces informations sont :
* Numéro de téléphone
* Numéro de bureau
* Nom / Prénom
* Commentaire

Par exemple si j'utilise mon utilisateur `tzkuat` :
* `chfn tzkuat`
```
[manjaro-vm ~]# chfn tzkuat
Modification des renseignements finger pour tzkuat.
Nom [tzkuat]: 
Bureau []: XXX
Téléphone bureau []: 0123456789
Téléphone domicile []: 0123456789

Les renseignements finger ont été modifiés.
```

Il est aussi possible de modifier une seule informatione en ligne de commande (sans interraction) :
```
chfn -o "Ceci est un commentaire" tzkuat
chfn -h "01.02.03.04.05" tzkuat
```

Voici les informations que vous pouvez modifier :
* `-f` : Nom complet
* `-r` : Numéro du bureau
* `-w` : Tel. bureau
* `-h` : Tel. perso
* `-o` : Commentaire

Les informations ont bien été ajoutées dans le fichier `/etc/passwd` :
* `tzkuat:x:1000:1000:tzkuat,Ceci est un commentaire,0123456789,0123456789:/home/tzkuat:/bin/bash`

Il est aussi possible de vérifier les informations d'un utilisateur à l'aide de la commande `finger`.

## Commandes zcat, zgrep & zless

Vous connaissez probablement les commandes `cat`, `grep` & `less` si vous utilisez régulièrement des machines GNU/Linux. Les commandes `zcat`, `zgrep` & `zless` sont en tout point similaires, la différence est qu'elles permettent d'intéragir directement avec des fichiers compressés.

Prenons par exemple un fichier `example.txt` contenant la liste suivante :
```
ajaccio 
bastia
calvi
bocognano
corte
venaco
porto
ota
evisa
```

Une fois compressé, ce fichier pourra être intérrogé via les commandes `zless`, `zcat` & `zgrep`, voic des exemples :

```
zgrep -e "ajaccio" -e "corte" example.txt.gz
zgrep "ajaccio" example.txt.gz
zcat example.txt.gz
```
![Illustration zcat & zgrep](/images/gnulinux-vrac-3/gnulinux-vrac-3-2.png)

La même chose est possible avec `less` il suffit de lancer `zless example.txt.gz`.

L'utilisation de ces commandes peut être très pratique pour analyser plusieurs fichiers de logs en même temps, voici des exemples :
```
zgrep -h "upgrade" /var/log/dpkg.log* | sort
zgrep -h "installed" /var/log/dpkg.log* | sort
```

## Commande tac

Pour le coup ça sera très rapide puisque `tac` c'est exactement la même chose que `cat` mais à l'envers ! Inutile donc indispensable :
* `tac example.txt`
* `cat example.txt`

![Illustration tac](/images/gnulinux-vrac-3/gnulinux-vrac-3-3.png)


## [Bonus] Rechercher dans l'historique

Pour terminer une astuce simple qui me facilite la vie régulièrement, à la place d'utiliser la commande `history` ou de taper durant 2 heures sur la flèche du haut (oui on le fait tous...), il est possible de faire une recherche dynamique dans l'historique des commandes, pour ça il suffit de faire la combinaison CTRL+R sur son clavier, ce menu devrait s'afficher, vous permettant de taper du texte afin de chercher dans vos commandes :
![Illustration recherche inverse](/images/gnulinux-vrac-3/gnulinux-vrac-3-4.png)


J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !