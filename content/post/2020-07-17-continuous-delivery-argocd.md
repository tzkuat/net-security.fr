---
title: Continuous Delivery avec Argo CD
author: Adrien Pavone
type: post
date: 2020-07-17T20:54:54+00:00
url: /cloud/continuous-delivery-argocd/
thumbnail: /images/argo-1.png
featured: true
tags: [
    "kubernetes",
    "GNU/Linux",
    "performance"
]
categories: [
    "systeme",
    "logiciellibre"
]

---

Bonjour à tous, dans cet article nous allons parler de continuous delivery, de livraison continue quoi.

**Disclaimer** : On va parler d'Argo CD, mais aussi de Prometheus, Helm, Grafana, HPA etc.

Avec la montée en puissance de la mouvance DevOps et de l'industrialisation des processus d'entreprise, certaines sociétés et grands groupes n'ont pas hésité à développer des solutions d'automatisation comme Amazon, Google ou encore JetBrains.

La communauté open source n'a pas hésité à développer et partager ces propres outils. Nous pensons à Gitlab CD/CD, CircleCI, Travis CI ou encore Jenkins.

Utilisateur quotidien de Jenkins ou encore Gitlab CI/CD pour mes différentes taches de tests unitaires, de performance ou encore de sécurité, plus orienté intégration continue donc, je me suis davantage intéressé à la livraison continue. Développeur de cœur, j'utilise tous les jours Gitlab pour son principal service de versioning, mais aussi de Continuous Integration. 

Je cherchais donc un outils GitOps.

C'est la semaine dernière en traînant sur le site de la Cloud Native Computing Foundation (CNCF pour les intimes) que je suis tombé sur leur [carte interactive][1] référençant un grand nombre de projets, chacun dans leur catégorie de la chaîne DevOps. En regardant de plus près la catégorie Continuous Integration & Delivery, je suis tombé sur le projet Argo, qui est un projet au stage de l'incubation (incubating) au sein de la CNCF.

![Projets & Niveaux de maturité à la CNCF](/images/cncf_2.jpg)


Pour rappel, Kubernetes, Prometheus, Helm, ou encore Harbor sont des projets open source au stade graduated, ou certifié et validé par la CNCF.

## Argo, une communauté au service de Kubernetes

Pour la faire rapide, [Argoproj][3] est un ensemble d'outils et de projets permettant de travailler avec Kubernetes.

Les principaux projets type Continuous Delivery que nous pouvons retrouver sur leur Github et qui vont nous intéresser sont les suivants :

  * [Argo Rollout][4] : grâce à l'utilisation de la ressource personnalisée Rollout, Argo fournit des stratégies de déploiement telles que le modèle Blue Green et Canary

  * [Argo CD][5] : un outil GitOps / CD pour Kubernetes.

Pour le reste, je vous recommande d'aller voir leurs repos très intéressant sur Github.

Vous l'aurez compris, le projet dont nous allons parler est Argo CD.

## Prérequis pour le lab

Dans cet atelier, nous allons mettre en place une chaîne automatique de livraison continue. Nous passerons rapidement sur le côté intégration, tests unitaires, E2E, de sécurité ou autre. Nous allons axer ce lab sur la partie CD.

Pour ce faire, vous allez avoir besoin des prérequis suivants :

  * un cluster Kubernetes (je sais&#8230;)
  * Helm v3

L'objectif est de mettre en place :

  * une stack avec un site web statique basé sur nginx
  * une stack de monitoring de vos pods avec prometheus-operator et prometheus-adapter
  * une façon de récupérer les métriques des pods avec metrics-server
  * un autoscaler horizontal de pods (HPA)
  * Argo CD (évidemment)

Pour ma part, mon cluster est composé d'un nœud master et de 3 workers.

### Versions de kubectl client / serveur et d'Helm

Versions de kubectl client / serveur :

```
root@lab-kubernetes-1:~# kubectl version
Client Version: version.Info{Major:"1", Minor:"18", GitVersion:"v1.18.5", GitCommit:"e6503f8d8f769ace2f338794c914a96fc335df0f", GitTreeState:"clean", BuildDate:"2020-06-26T03:47:41Z", GoVersion:"go1.13.9", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"18", GitVersion:"v1.18.3", GitCommit:"2e7996e3e2712684bc73f0dec0200d64eec7fe40", GitTreeState:"clean", BuildDate:"2020-05-20T12:43:34Z", GoVersion:"go1.13.9", Compiler:"gc", Platform:"linux/amd64"}
```

Version d'Helm :

```
root@lab-kubernetes-1:~# helm version
version.BuildInfo{Version:"v3.2.3", GitCommit:"8f832046e258e2cb800894579b1b3b50c2d83492", GitTreeState:"clean", GoVersion:"go1.13.12"}
```

## Installation des outils de monitoring

### Metrics Server

Metrics Server est un service efficace de métrologie des ressources en conteneurs dans K8S.

#### Pourquoi utiliser Metrics Server ?

Comme rappelé dans la [documentation officielle][6], vous aurez besoin de déployer Metrics Server si :

  * Vous avez besoin d'un système de scalabilité horizontal basé sur la CPU / Mémoire (HPA), ce qui est notre cas,
  * Ajuster / suggérer automatiquement les ressources nécessaires aux conteneurs (VPA), ce qui n'est pas notre cas.

Vous n'avez cependant pas besoin de Metrics Server si vous avez :

  * Des environnements non Kubernetes,
  * Besoin d'une source performante et précise de mesures de l'utilisation des ressources,
  * Un système HPA basé sur des ressources autres que la CPU / Mémoire.

Pour installer Metrics Server, rien de plus simple : 


```
root@lab-kubernetes-1:~# kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml
clusterrole.rbac.authorization.k8s.io/system:aggregated-metrics-reader created
clusterrolebinding.rbac.authorization.k8s.io/metrics-server:system:auth-delegator created
rolebinding.rbac.authorization.k8s.io/metrics-server-auth-reader created
apiservice.apiregistration.k8s.io/v1beta1.metrics.k8s.io created
serviceaccount/metrics-server created
deployment.apps/metrics-server configured
service/metrics-server created
clusterrole.rbac.authorization.k8s.io/system:metrics-server created
clusterrolebinding.rbac.authorization.k8s.io/system:metrics-server created
```


Vérification de Metrics Server :


```
root@lab-kubernetes-1:~# kubectl get pods -n kube-system -l k8s-app=metrics-server
NAME                              READY   STATUS    RESTARTS   AGE
metrics-server-5f956b6d5f-dp9vp   1/1     Running   0          52s
```


Pour vérifier que tout fonctionne correctement, nous pouvons faire un top sur nos nœuds par exemple :


```
root@lab-kubernetes-1:~# kubectl top node
error: metrics not available yet
```


Si comme moi vous rencontrez cette erreur, c'est parce que par défaut, Metrics Server communique au travers d'un canal sécurisé, avec le certificat auto-signé de votre cluster Kubernetes.

Pour outrepasser cette contrainte, nous pouvons éditer le déploiement de Metrics Server de la façon suivante :


```
root@lab-kubernetes-1:~# kubectl edit deployment metrics-server -n kube-system
```


Modifier les arguments du container metrics-server :


```
...
    spec:
      containers:
      - args:
        - --cert-dir=/tmp
        - --secure-port=4443
        - --kubelet-insecure-tls
...
```


Attention, comme le rappelle la documentation officielle, cet argument permet de : _**Do not verify the CA of serving certificates presented by Kubelets. For testing purposes only.**_

Enregistrer, et faire à nouveau un top node pour récupérer les métriques :


```
root@lab-kubernetes-1:~# kubectl top node
NAME               CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
lab-kubernetes-1   305m         15%    2838Mi          35%
lab-kubernetes-2   348m         8%     5490Mi          46%
lab-kubernetes-3   554m         13%    6250Mi          52%
lab-kubernetes-4   427m         10%    6295Mi          52%
```


### Prometheus Operator

Pourquoi Prometheus Operator ? Si vous avez déjà manipulé Prometheus, vous savez que c'est une plateforme de monitoring qui collecte les métriques des applications cibles sur les endpoints HTTP exposés des dites applications. Ce qu'il faut savoir c'est qu'à chaque fois que vous ajoutez / supprimez une application, il faut modifier le fichier de configuration de Prometheus. 

![Schéma Scraping de Prometheus](/images/prometheus-scrape.png)

Afin d’accélérer et faciliter cette étape chronophage, nous allons utiliser Prometheus Operator.

Prometheus Operator permet à Kubernetes de gérer de façon native les déploiements, le management de Prometheus et de ses composants. L'objectif de ce projet est de simplifier et d'automatiser la configuration de Prometheus pour des clusters Kubernetes.

#### Ajout du repository Helm stable et mise à jour

```
root@lab-kubernetes-1:~# helm repo add stable https://kubernetes-charts.storage.googleapis.com
"stable" has been added to your repositories
root@lab-kubernetes-1:~# helm repo update
Hang tight while we grab the latest from your chart repositories…
…Successfully got an update from the "stable" chart repository
Update Complete. ⎈ Happy Helming!⎈
```

#### Installation de Prometheus Operator

Une bonne habitude (je crois) que j'ai prise est de toujours inspecter les valeurs de la chart Helm à installer. Par exemple si vous voulez tuner votre installation, il est toujours intéressant de connaître les valeurs par défaut et mises à disposition dans le Chart.


```
root@lab-kubernetes-1:/opt/prometheus-grafana# helm inspect values stable/prometheus-operator > prometheus-operator.yaml
```

N'hésitez pas à regarder en détail ce fichier pour qu'il corresponde le plus à vos besoins, comme l'ajout d'ingress ou la modification du type des services. Pour notre lab, je ne modifierai pas ce fichier, et j'exposerai les différents services en CLI.

Si vous voulez davantage d'informations concernant l'architecture de Prometheus Operator ainsi que sur ses différents CRDs (Custom Resource Definition), vous trouverez ici un article très intéressant écrit par [Germain Lefebvre][7] et [Antoine Leteneur][8] via ce [lien][9].

Maintenant que le fichier est adapté à nos besoins, nous pouvons déployer la stack. Pour ce faire, nous allons d'abord créer le namespace **metrics** dans lequel l'ensemble de nos services de monitoring seront déployés :


```
root@lab-kubernetes-1:~# kubectl create namespace metrics
namespace/metrics created
```

Déploiement de la stack :

```
root@lab-kubernetes-1:/opt/prometheus-grafana# helm upgrade --install -f prometheus-operator.yaml prometheus-operator stable/prometheus-operator --namespace metrics
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
Release "prometheus-operator" has been upgraded. Happy Helming!
NAME: prometheus-operator
LAST DEPLOYED: Fri Jul 17 11:44:59 2020
NAMESPACE: metrics
STATUS: deployed
REVISION: 1
NOTES:
The Prometheus Operator has been installed. Check its status by running:
  kubectl --namespace metrics get pods -l "release=prometheus-operator"

Visit https://github.com/coreos/prometheus-operator for instructions on how
to create & configure Alertmanager and Prometheus instances using the Operator.
```


Vérification :


```
root@lab-kubernetes-1:~# kubectl --namespace metrics get pods
NAME                                                      READY   STATUS    RESTARTS   AGE
alertmanager-prometheus-operator-alertmanager-0           2/2     Running   0          97s
prometheus-operator-grafana-75554df8c9-qg64r              2/2     Running   0          97s
prometheus-operator-kube-state-metrics-69fcc8d48c-ddfrk   1/1     Running   0          97s
prometheus-operator-operator-6bbc476477-82ddc             2/2     Running   0          97s
prometheus-operator-prometheus-node-exporter-7hrgm        1/1     Running   0          97s
prometheus-operator-prometheus-node-exporter-nfdmz        1/1     Running   0          97s
prometheus-operator-prometheus-node-exporter-twcsc        1/1     Running   0          97s
prometheus-operator-prometheus-node-exporter-vcrfh        1/1     Running   0          97s
prometheus-prometheus-operator-prometheus-0               3/3     Running   1          97s

```


Nous constatons que le Chart a déployé un Node-exporter pour chacun de nos nœuds K8S, un Alertmanager, un Grafana et le serveur Prometheus.

Nous allons exposer Grafana ainsi que Prometheus afin d'y avoir accès au travers du navigateur web :


```
root@lab-kubernetes-1:~# kubectl expose deployment prometheus-operator-grafana --type=NodePort --name=grafana-nodeport -n metrics
service/grafana-nodeport exposed
```



```
root@lab-kubernetes-1:~# kubectl expose service prometheus-operator-prometheus --type=NodePort --name=prometheus-service-nodeport -n metrics
service/prometheus-service-nodeport exposed
```



```
root@lab-kubernetes-1:~# kubectl get svc -n metrics
NAME                                           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                       AGE
....
grafana-nodeport                               NodePort    10.110.121.212   <none>        80:32259/TCP,3000:32752/TCP   36s
prometheus-service-nodeport                    NodePort    10.96.198.206    <none>        9090:31112/TCP                6s
...
```


Nous pouvons maintenant accéder à Prometheus et Grafana à partir de l'IP d'un de nos nœuds Kubernetes, sur le port 32752 pour Grafana et sur le port 31112 pour Prometheus.

Les identifiants de connexion par défaut pour Grafana sont : admin / prom-operator.

Vous pourrez constater que Grafana vient avec une batterie de tableaux de bord déjà prêt à l&#8217;emploi, ce qui est appréciable.

### Création de notre application web et création d'un Chart

Notre application web va être un simple site web statique basé sur Nginx. Afin de simplifier la continue avec Argo CD, nous allons choisir le déploiement basé sur Helm.

Pour la création de mon Chart, je me suis fortement basé sur celui de [Bitnami][10]. Effectivement, afin de monitorer notre application grâce à Prometheus Operator, il va falloir déployer un container side-car au sein de notre pod afin de scraper les métriques de notre container Nginx.

![Schéma d'architecture SideCar](/images/conteneur-sidecar.png)

De cette façon, nous n'exposons pas nos métriques au monde entier, seulement à Prometheus.

Pour commencer, voici la commande pour créer le Chart : 


```
helm create helm-argocd-website
```


Il ne reste plus qu'à modifier les éléments dont nous avons besoin pour notre déploiement dans le dossier **templates**, soit :

  * un HPA, pour la scalabilité horizontale de nos pods,
  * un Service Monitor, pour que Prometheus Operator puisse modifier à la volée sa configuration en ajoutant le scraping de ce service monitor

Les modifications que j'ai apportées sont les suivantes :

  * Ajout d'une entrée sous metrics.autoscaling dans le fichier **values.yaml** : **targetRequestPerSecond**: 50, qui va définir la valeur par défaut à partir de laquelle l'autoscaling va se mettre en place (si le nombre de requêtes par seconde dépasse 50)
  * Ajout dans le fichier **templates/hpa.yaml** la métrique qui a pour nom **my\_nginx\_http\_requests\_total**, de type Pods, sur laquelle notre Prometheus va scraper
  * Modification du fichier **templates/deployment.yaml** pour ajouter le pod side-car qui va requêter le endpoint **127.0.0.1:8080/status** quand le Service Monitor va requêter sur /metrics
  * Modification du fichier Nginx **default.conf** pour que seulement 127.0.0.1 soit autorisé à requêter l'endpoint /status

Et d'autres modifications dont je vous laisserai le soin de vous approprier et prendre connaissance sur mon GitLab [ici][11], sinon l'article va être trop long et je vais finir par faire des podcasts. 

Maintenant que votre Chart est ok et que votre fichier .gitlab-ci.yml l'est aussi, vous n'avez plus qu'à tout pousser sur votre Git. Nous allons maintenant (enfin) parler de Argo CD, le déployer et l'utiliser.

### Déploiement d'Argo CD

Maintenant que votre environnement de monitoring est ok, nous allons déployer Argo CD. Pour ce faire, j'ai suivi la documentation officielle [ici][12]. Rien de compliquer, en 2 lignes c'est réglé pour du QuickStart. Je vous recommande tout de même de prendre connaissance du fichier install.yml et de le modifier si nécessaire :


```
root@lab-kubernetes-1:~# kubectl create namespace argocd
namespace/argocd created
root@lab-kubernetes-1:~# kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
customresourcedefinition.apiextensions.k8s.io/applications.argoproj.io created
customresourcedefinition.apiextensions.k8s.io/appprojects.argoproj.io created
serviceaccount/argocd-application-controller created
serviceaccount/argocd-dex-server created
serviceaccount/argocd-server created
role.rbac.authorization.k8s.io/argocd-application-controller created
role.rbac.authorization.k8s.io/argocd-dex-server created
role.rbac.authorization.k8s.io/argocd-server created
clusterrole.rbac.authorization.k8s.io/argocd-application-controller created
clusterrole.rbac.authorization.k8s.io/argocd-server created
rolebinding.rbac.authorization.k8s.io/argocd-application-controller created
rolebinding.rbac.authorization.k8s.io/argocd-dex-server created
rolebinding.rbac.authorization.k8s.io/argocd-server created
clusterrolebinding.rbac.authorization.k8s.io/argocd-application-controller created
clusterrolebinding.rbac.authorization.k8s.io/argocd-server created
configmap/argocd-cm created
configmap/argocd-rbac-cm created
configmap/argocd-ssh-known-hosts-cm created
configmap/argocd-tls-certs-cm created
secret/argocd-secret created
service/argocd-dex-server created
service/argocd-metrics created
service/argocd-redis created
service/argocd-repo-server created
service/argocd-server-metrics created
service/argocd-server created
deployment.apps/argocd-application-controller created
deployment.apps/argocd-dex-server created
deployment.apps/argocd-redis created
deployment.apps/argocd-repo-server created
deployment.apps/argocd-server created
```



```
root@lab-kubernetes-1:~# kubectl get pod -n argocd
NAME                                             READY   STATUS    RESTARTS   AGE
argocd-application-controller-7684cfcc66-q8krm   1/1     Running   0          59s
argocd-dex-server-675c85b57b-8v782               1/1     Running   0          59s
argocd-redis-6d7f9df848-mn5hw                    1/1     Running   0          59s
argocd-repo-server-66784b696b-vkldr              1/1     Running   0          58s
argocd-server-86cf69886-cvf56                    1/1     Running   0          58s
```


On expose le déploiement afin d'avoir accès à l'interface web :


```
root@lab-kubernetes-1:~# kubectl expose deployment argocd-server --type=NodePort --name=argocd-nodeport -n argocd
service/argocd-nodeport exposed
root@lab-kubernetes-1:~# kubectl get svc -n argocd
NAME                    TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                         AGE
...
argocd-nodeport         NodePort    10.110.131.45    <none>        8080:31278/TCP,8083:32691/TCP   6s
...
```


Ici, nous pourrons y avoir accès au travers du port 31278.

Normalement vous devriez obtenir cette interface, via le protocole HTTPS :

![Argo CD Page de login](/images/image-1024x485.png)


Pour récupérer le mot de passe, voici la commande à saisir :


```
root@lab-kubernetes-1:~# kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2
argocd-server-86cf69886-cvf56
```

Les identifiants ici sont : admin / argocd-server-86cf69886-cvf56

![Argo CD Page d'accueil](/images/image-1-1024x485.png)


Ouf ! Enfin nous y sommes, nous allons pouvoir déployer notre site web avec notre Chart Helm créé précédemment !

### Déploiement de notre Chart avec Argo CD

Maintenant que tout est en place, nous allons pouvoir créer une nouvelle application au sein d'Argo CD. Notre projet est publique et sur GitLab. Si jamais votre projet est privé, sachez que vous avez la possibilité d'ajouter vos repositories avec vos credentials sous Settings - Repositories. 

![Argo CD Settings - Repositories](/images/image-2-1024x485.png)

Si votre registry Docker est privée, pensez à ajouter le nom de votre secret dans votre Chart sous **imagePullSecrets**.

Pour notre lab, nous n'aurons pas besoin de l'ensemble de ces credentials, nous allons directement passer à la création de l'application.

Rendez vous sur la page d'accueil, **New App**.

Sous Général, nous allons renseigner le nom de l'application argocd, le type de Project à default, ainsi que la Sync Policy à Automatic avec PRUNE RESOURCES de selectionné. De ce fait, Argo CD va synchroniser et détecter automatiquement les changements entre sa révision en local et le repository distant. De plus, il supprimera les ressources en local qui ne seront plus présents dans votre repository.

![Argo CD Nouvelle App - General](/images/image-3-1024x399.png)

Sous Source, nous allons saisir notre projet GitLab. Pour moi ce sera **https://gitlab.com/adrienpavone/argocd-static-website.git**. Il faudra renseigner la révision que devra suivre Argo CD, sous **HEAD**. Enfin, le chemin sous lequel se trouve notre Chart, ici **helm-argocd-website**.

![Argo CD Nouvelle App - Source](/images/image-4-1024x295.png)

Concernant la partie Destination, je vais laisser le Cluster par défaut, et renseigner mon namespace **personal**. Ce sera le namespace au sein duquel Argo CD va déployer le Chart.

![Argo CD Nouvelle App - Destination](images/image-5-1024x211.png)

Pour terminer, Argo CD détecte automatique que sous le dossier renseigné précédemment se trouve un Chart Helm. Il va automatiquement créer les inputs et renseigner les valeurs par défaut que vous avez saisies au sein de votre fichier **values.yaml**.

Ici je vais juste modifier le champ **image.tag** pour enlever latest et mettre v0.0.1. Ensuite nous pouvons appuyer sur le bouton **Create**.

![Argo CD Nouvelle App - Helm](/images/image-6-1024x485.png)
![](/images/image-7.png)

Pour tester, j'ai décidé d'apporter quelques modifications et d'ajouter d'autres tags à mon projet, jusqu'à la version v0.0.6. Mon application s'est automatiquement synchronisée et a d'ailleurs ajouter des révisions à chaque fois que j'ai modifié le champs **image.tag**.

![Argo CD architecture d'un déploiement d'une application](/images/image-8-1024x488.png)

Si je vais sur <https://argocd.apav.one/>, j'ai bien accès à ma ressource.

Cependant, comme nous pouvons le constater, nous rencontrons un problème de synchronisation avec le HPA. Si nous regardons plus en détails sur notre nœud Kubernetes, nous constatons qu'il n'arrive pas à collecter les métriques **my\_nginx\_http\_requests\_total**.


```
root@lab-kubernetes-1:~# kubectl get hpa -n personal
NAME              REFERENCE                    TARGETS                              MINPODS   MAXPODS   REPLICAS   AGE
argocd-apav-one   Deployment/argocd-apav-one   3%/50%, <unknown>/50 + 1 more...     3         10        3          101s
```



```
root@lab-kubernetes-1:~# kubectl describe hpa argocd-apav-one -n personal
Name:                                                     argocd-apav-one
Namespace:                                                personal
Labels:                                                   app.kubernetes.io/instance=argocd
                                                          app.kubernetes.io/managed-by=Helm
                                                          app.kubernetes.io/name=argocd-apav-one-app
                                                          app.kubernetes.io/version=1.0.12
                                                          helm.sh/chart=helm-argocd-website-0.1.0
Annotations:                                              CreationTimestamp:  Fri, 17 Jul 2020 18:29:26 +0200
Reference:                                                Deployment/argocd-apav-one
Metrics:                                                  ( current / target )
  resource memory on pods  (as a percentage of request):  3% (9885696) / 50%
  "my_nginx_http_requests_total" on pods:                 <unknown> / 50
  resource cpu on pods  (as a percentage of request):     1% (2m) / 50%
Min replicas:                                             3
Max replicas:                                             10
Deployment pods:                                          3 current / 3 desired
Conditions:
  Type            Status  Reason              Message
  ----            ------  ------              -------
  AbleToScale     True    ReadyForNewScale    recommended size matches current size
  ScalingActive   True    ValidMetricFound    the HPA was able to successfully calculate a replica count from memory resource utilization (percentage of request)
  ScalingLimited  False   DesiredWithinRange  the desired count is within the acceptable range
Events:
  Type     Reason               Age                From                       Message
  ----     ------               ----               ----                       -------
  Warning  FailedGetPodsMetric  14s (x2 over 30s)  horizontal-pod-autoscaler  unable to get metric my_nginx_http_requests_total: unable to fetch metrics from custom metrics API: no custom metrics API (custom.metrics.k8s.io) registered

```


A vrai dire, c’est tout à fait normal puisque nous ne lui avons pas dit à quoi correspondait cette métrique, et comment la récupérer. Pour cela, nous allons avoir besoin d'un composant supplémentaire, Prometheus Adapter !

#### Déploiement de Prometheus Adapter

Prometheus Adapter va nous permettre d'écrire nos propres queries à destination du serveur Prometheus et donc de collecter les métriques dont nous avons besoin pour HPA.

Nous pourrions supprimer Metrics Server, et laisser en place seulement Prometheus Adapter en écrivant nos requêtes pour collecter la CPU et la Mémoire de nos containers. Pour ce lab, je vous montre qu'il est possible d'avoir les deux en place.

D'ailleurs Prometheus Adapter vous donne la possibilité de se synchroniser avec votre serveur Prometheus afin de récupérer l'ensemble des queries de base qui peuvent être lancées, pour cela il faudra laisser à **true** le paramètre **rules.default**, ce que nous allons faire ici.

Pour ce faire, vous allez avoir besoin d'une nouvelle fois d'Helm pour installer Prometheus Adapter (lien [ici][21]). Comme depuis le début de l'article, je vais récupérer les valeurs du fichier **values.yaml** afin de les adapter à mon besoin.


```
root@lab-kubernetes-1:/opt/prometheus-grafana# helm inspect values stable/prometheus-adapter > prometheus-adapter.yaml
```


Pour notre besoin, nous allons spécifier à Prometheus Adapter l'url du serveur Prometheus :


```
# Url to access prometheus
prometheus:
  url: http://prometheus-operator-prometheus
  port: 9090
  path: ""
```


Nous allons de même écrire notre query **my\_nginx\_http\_requests\_total** afin d'avoir les informations coté HPA. Ajoutez la requête au sein de **rules**, sous **custom** :


```
rules:
  default: true
  custom:
    - seriesQuery: '{__name__=~"nginx_http_requests_total",container!="POD",namespace!="",pod!=""}'
      seriesFilters: []
      resources:
        overrides:
          namespace:
            resource: namespace
          pod:
            resource: pod
      name:
        matches:
        as: "my_nginx_http_requests_total"
      metricsQuery: irate(nginx_http_requests_total{<<.LabelMatchers>>,container!="POD"}[5m])
```


Il ne reste plus qu'à déployer :


```
root@lab-kubernetes-1:/opt/prometheus-grafana# helm upgrade --install -f prometheus-adapter.yaml prometheus-adapter stable/prometheus-adapter -n metrics
Release "prometheus-adapter" does not exist. Installing it now.
NAME: prometheus-adapter
LAST DEPLOYED: Fri Jul 17 18:45:16 2020
NAMESPACE: metrics
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
prometheus-adapter has been deployed.
In a few minutes you should be able to list metrics using the following command(s):

  kubectl get --raw /apis/custom.metrics.k8s.io/v1beta1
```


Vous pouvez vérifier l'ensemble de vos requêtes de type custom de cette façon (n'oubliez pas d'installer jq si vous voulez un joli rendu) :


```
root@lab-kubernetes-1:~# kubectl get --raw /apis/custom.metrics.k8s.io/v1beta1 | jq .
{
  "kind": "APIResourceList",
  "apiVersion": "v1",
  "groupVersion": "custom.metrics.k8s.io/v1beta1",
  "resources": [
    {
      "name": "jobs.batch/prometheus_tsdb_wal_truncate_duration_seconds_sum",
      "singularName": "",
      "namespaced": true,
      "kind": "MetricValueList",
      "verbs": [
        "get"
      ]
    },
...
    {
      "name": "jobs.batch/process_max_fds",
      "singularName": "",
      "namespaced": true,
      "kind": "MetricValueList",
      "verbs": [
        "get"
      ]
    }
  ]
}
```


Si nous faisons un describe sur notre HPA pour vérifier que notre requête remonte correctement, voici le résultat :


```
....
Metrics:                                                  ( current / target )
  resource memory on pods  (as a percentage of request):  4% (10815488) / 50%
  "my_nginx_http_requests_total" on pods:                 500m / 50
  resource cpu on pods  (as a percentage of request):     1% (2m) / 50%
....
```


Vous devriez constater que Prometheus a mis à jour sa configuration en scrapant automatiquement le Service Monitor déployé grâce au Chart Helm :

![](/images/image-10.png)


De plus, si vous ajoutez [ce dashboard][22] à Grafana, vous pouvez monitorer vos containers Nginx, voir le nombre de requêtes etc.

#### Test de performance - test du HPA

Afin de tester l'HPA, nous allons appliquer une forte charge sur le site. Nous appliquons une volumétrie de 200 requêtes par seconde.

Nous pouvons constater que l'HPA s'est enclenché et a déployé 2 pods supplémentaires pour encaisser la charge. 

![](/images/image-12-1024x431.png)

### Conclusion

Voilà, c'est « tout » pour aujourd'hui. J'apprécie grandement Argo CD, mais n'en suis qu'au début de l'utilisation, j'aimerais élargir l'implication d'Argo dans mon environnement Kubernetes, avec des applications Statefuls, des bases de données, voir son comportement. Aujourd'hui j’apprécie pouvoir modifier mon code source et le voir se mettre à jour automatiquement dans mon environnement, avec une stratégie RollingUpdate.

Ce que je regrette à première vue aujourd'hui, c'est d'être OutOfSync sur le HPA, ce qui est dommage parce que ce n'est pas le cas et j'ai l'impression qu'il n'arrive pas à se synchroniser au niveau d'Argo, malgré le fait qu'il soit réellement UpToDate dans Kubernetes.

![Différence de configuration HPA sur ArgoCD](/images/image-11-1024x322.png)


Ce qui serait intéressant ici serait par exemple d'avoir une ImagePullPollicy à Always, et mettre le paramètre **image.tag** en latest, pour des environnements de Dev ou de Qualif. Comme ça nous aurions un vrai Continuous Deployment. (je préfère malgré tout avoir des versions fixes 🙂 ) 

Si vous le souhaitez, vous pouvez retrouver l’ensemble des sources utilisées pour cet article ici : <a rel="noreferrer noopener" href="https://gitlab.com/adrienpavone/argocd-static-website" target="_blank">https://gitlab.com/adrienpavone/argocd-static-website</a>[][11].

Si vous avez des questions ou des améliorations à apporter concernant mon déploiement, des best practices, ou simplement réduire la taille de mes articles je suis preneur.

Merci pour votre lecture et à bientôt !

APavone

 [1]: https://landscape.cncf.io/
 [2]: images/cncf_2.jpg
 [3]: https://github.com/argoproj
 [4]: https://github.com/argoproj/argo-rollouts
 [5]: https://github.com/argoproj/argo-cd
 [6]: https://github.com/kubernetes-sigs/metrics-server
 [7]: https://blog.ineat-group.com/author/germainlef/
 [8]: https://blog.ineat-group.com/author/antoinelet/
 [9]: https://blog.ineat-group.com/2020/05/prometheus-operator-dans-kubernetes/
 [10]: https://github.com/bitnami/charts/tree/master/bitnami/nginx
 [11]: https://gitlab.com/adrienpavone/argocd-static-website
 [12]: https://argoproj.github.io/argo-cd/
 [13]: images/image.png
 [14]: images/image-1.png
 [15]: images/image-2.png
 [16]: images/image-3.png
 [17]: images/image-4.png
 [18]: images/image-5.png
 [19]: images/image-6.png
 [20]: images/image-8.png
 [21]: https://github.com/DirectXMan12/k8s-prometheus-adapter
 [22]: https://raw.githubusercontent.com/nginxinc/nginx-prometheus-exporter/master/grafana/dashboard.json