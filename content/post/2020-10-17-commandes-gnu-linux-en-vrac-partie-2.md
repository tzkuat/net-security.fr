---
title: Commandes GNU/Linux en vrac – Partie 2
author: Mickael Rigonnaux
type: post
date: 2020-10-17T10:05:44+00:00
url: /system/commandes-gnu-linux-en-vrac-partie-2/
thumbnail: /images/logo-3.png
featured: true
tags: [
    "logiciellibre",
    "GNU/Linux"
]
categories: [
    "systeme",
    "logiciellibre"
]

---
Bonjour à tous ! Aujourd’hui le 2ème article de la série « Commandes GNU/Linux en vrac ». Le 1er est disponible ici :

  * 1er : <https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-1/>

Le but est de présenter et de vous faire découvrir des commandes et des outils qui pourraient être utiles pour vous.

## Commande wc

La commande `wc` ou word count va permettre au système de compter plusieurs éléments comme le nombre de lignes, de mots, de caractères, etc.

Je m'en sers quotidiennement et son utilisation est très simple.

Pour les exemples de cet article le fichier suivant est utilisé :

```
> cat pays.txt
France
Espagne
Etats Unis
Chine
Italie
Ecosse
Irlande
Allemagne
Thailande
```

Dans son utilisation la plus simple, c'est à dire sans argument les résultats renvoyés sont les suivants :

```wc pays.txt```

![Utilisation de WC](/images/image-22.png)


Dans l'ordre nous retrouvons :

  * 9, le nombre de lignes du fichier
  * 10, le nombre de mots du fichier
  * 73, la taille en octets (bytes) du fichier

Il est aussi possible de filtrer les résultats avec des arguments, par exemple :

```
# Le nombre de lignes
wc -l pays.txt

# Le nombre de mots
wc -w pays.txt

# Le nombre de caractères
wc -c pays.txt

# Le nombre d'octets
wc -m pays.txt

# Afficher le plus grand nombre de caractères sur une ligne (Allemagne)
wc -L pays.txt
```

![Utilisation de WC](/images/image-23.png)

Vous pouvez également utiliser `wc -h` pour afficher toutes les options de cet outil, qui j'espère vous servira.

## Commande chage

De son côté la commande `chage` est une commande système qui va permettre de gérer la validité des mots de passe et des comptes utilisateurs sur les systèmes GNU/Linux.

Elle permet également de récupérer des informations sur un compte, par exmeple sur mon Arch Linux :

![Utilisation de chage](/images/image-24.png)

La commande va permettre de configurer les différentes informations de la capture ci-dessus. Il sera alors possible de forcer le changement de mots de passe ou encore de désactiver le compte en question.

Un exemple avec un compte de test : net-security.

Vous pouvez modifier ces informations de manière interactive de la façon suivante :

```sudo chage net-security```

Les lignes suivantes seront à valider :

![Utilisation de chage](/images/image-25.png)


Nous avons dans l'ordre :

  * Durée de vie minimum du mot de passe, si vous mettez la valeur 5 par exemple l'utilisateur ne pourra changer son mot de passe durant 5 jours.
  * Durée de vie maximum du mot de passe en jour.
  * Modification de la date du dernier changement de mot de passe.
  * Configuration des alertes avant X jours.
  * Ajout du nombre de jour ou le compte reste inactif après l'expiration. Après ce nombre de jours le compte devient verrouillé.
  * Configuration de la date d'expiration du compte.

Il est également possible d'utiliser des arguments pour ces différentes options et donc de les utiliser indépendamment les unes des autres.

  * Pour changer la date du dernier changement de mot de passe :

```chage -d 2020-11-10 net-security```

  * Date d'expiration du compte :

```chage -E 2020-31-10 net-security```

  * Configuration des alertes :

```chage -W 10 net-security```

  * Configuration du nombre de jours ou le compte restera inactif :

```chage -I 14 net-security```

  * Durée de vie minimale d'un mot de passe :

```chage -m 2 net-security```

  * Durée de vie maximale d'un mot de passe :

```chage -M 365 net-security```

Le résultat après ces différentes commandes :

![Résultat commande chage](/images/image-26.png)

Vous pouvez également utiliser plusieurs paramètres sur la même commande, par exemple :

```chage -m 2 -M 100 -I 10 -E 2020-31-12 net-security```

Personnellement c'est une commande que je ne connaissais pas mais qui peut-être très intéressante dans le cadre d'une politique de gestion de mots de passe sur les systèmes GNU/Linux (hors LDAP/AD). Il est possible de l'automatiser et de le coupler à des outils comme Ansible par exemple.

## Commande tr

La commande `tr` pour translate va permettre des opérations simples comme supprimer ou transformer des chaînes de caractères.

C'est également que j'utilise régulièrement sur mes différents systèmes. Voici quelques exemples d'utilisation.

Le fonctionnement est suivant, à la suite de la commande `tr` vous allez entrer les informations à remplacer suivi de la valeur à utiliser.

Pour transformer un texte minuscule en majuscule vous pouvez utiliser ces commandes :

```
tr [:lower:] [:upper:]
tr [a-z] [A-Z]
```

![Utilisation commande tr(anslate)](/images/image-27.png)



Ou encore pour supprimer des espaces :

```tr -d ' '```

![Suppression des espaces avec chage](images/image-28.png)

Et même supprimer les suites de caractères :

```
tr -s 'e'
tr -s 'u'
tr -s 'eu'
```

![Suppression des caractères avec chage](/images/image-29.png)


Beaucoup d'autres utilisations sont possibles avec cet outil puissant, n'hésitez pas à faire des recherches pour trouver votre bonheur !

## Bonus : neofetch

Un outil un peu particulier, tout simplement car à ma connaissance il ne sert à rien à part afficher joliment les informations relatives à votre machine/système avec le logo de votre distribution préférée.

Il vous permettra cependant d'ajouter un terminal avec `neofetch` ouvert sur vos plus captures d'écrans ! Que vous posterez par la suite sur les meilleurs sub reddit :

  * [Arch Linux][1]
  * [Unix Porn][2]

Il n'existe pas nativement sur les distributions GNU/Linux mais vous pouvez l'installer facilement sur tous les systèmes.

De mon côté sur mon Arch c'était très facile :

```sudo pacman -S neofetch```

Le rendu est le suivant :

![Utilisation de neofetch](/images/image-30.png)

Si vous souhaitez le personnaliser, il existe un grand nombre d'article et d'options sur cet outil. Vous n'avez qu'à rajouter -help ou chercher sur DuckDuckGo !

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

 [1]: https://www.reddit.com/r/archlinux/
 [2]: https://www.reddit.com/r/unixporn/