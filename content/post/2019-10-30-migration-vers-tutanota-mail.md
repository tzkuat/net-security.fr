---
title: Migration vers Tutanota Mail
author: Mickael Rigonnaux
type: post
date: 2019-10-30T15:05:39+00:00
url: /security/migration-vers-tutanota-mail/
thumbnail: /images/Tutanota/tutanota_logo.jpg
tags: [
    "opensource",
    "vieprivee"
]
categories: [
    "securite"
]
---
![](/images/Tutanota/tutanota_banner.png)

Bonjour à tous ! Aujourd'hui un rapide article vers la nouvelle solution de mail que j'ai choisie pour remplacer mon Mail Pro de chez OVH, [Tutanota][1]. 

## Introduction

Avant de présenter la solution je vais présenter mes besoins. Depuis un petit moment maintenant, j'essaie de m'affranchir des solutions grand public et propriétaire dans un but purement éthique. Par exemple, j'utilise [Ubuntu][2] à la place de Windows sur mon poste, [LibreOffice][3] à la place d'Office, [Qwant][4] à la place de Google, etc. Je peux vous faire un article sur ce sujet si ça vous intéresse. 

Dans la même optique, j'utilisais un service mail propre à mon nom de domaine et non ma simple adresse Gmail. Par facilité je me suis tourné vers l'offre Mail Pro d'OVH (1.19€ par mois) vu que j'avais déjà enregistré mon domaine chez cet hébergeur. Après presque un an d'utilisation, j'ai voulu changer parce que de un l'offre est basée sur Exchange et qu'en plus de ça j'ai eu plusieurs problèmes notamment des déconnexions de mes clients mails ou encore les mails qui arrivent dans les indésirables.

Ayant un serveur en interne j'ai même pensé à héberger ma propre solution de messagerie, mais ça m'a semblé très lourd et pour un service aussi critique, j'ai préféré faire appel à des solutions externes. 

Je suis parti à la recherche d'une solution de messagerie avec les critères suivants : 

  * Chiffrée de bout en bout (si possible)
  * Hébergée en Europe
  * Open Source
  * Possibilité d'utiliser mon nom de domaine avec des alias
  * Facile d'utilisation / d'installation
  * Avec une application mobile/bureau
  * Ayant une politique respectueuse de la vie privée
  * Gestion d'un calendrier

J'ai donc dans un premier temps utilisé [ProtonMail][5] pour essayer mais je ne suis pas passé à l'option payante pour les raisons suivantes : 

  * Le prix : 5€ par mois pour le premium sans calendrier
  * Même si une partie l'est, ProtonMail n'est pas Open Source

C'est à ce moment là que j'ai découvert Tutanota, et ça ma semblé être la meilleure alternative. 

## Présentation de Tutanota

Tutanota est un service de mail allemand chiffré de bout en bout et Open Source sur la partie client de messagerie, il est proposé sous Licence GPL-3.0. Le lien du projet Github [est disponible ici][6]. Ce service est accessible via mobile (iOS/Android), Webmail et directement avec un client lourd (en bêta actuellement). 

La société Tutanota GmbH basée à Hamburg a été créé par 3 anciens étudiants en 2011. Le projet initial était de défendre la vie privée des utilisateurs en ligne, en 2017 l'entreprise comptait 2 millions d'utilisateur. 

Sur la partie chiffrement, les algorithmes utilisés sont AES-128 & RSA 2048 avec bCrypt pour les mots de passe. Vous trouverez également beaucoup d'information directement sur [leur FAQ][7]. Le déchiffrement se fait seulement à la connexion donc côté client avec votre mot de passe. 

Et contrairement à ProtonMail l'ensemble des données sont chiffrées et pas seulement le body du mail. 

Pour ce faire le logiciel permet d'envoyer des mails en mode « Confidentiel » c'est à dire que la personne qui recevra le mail recevra un lien vers un temporaire Tutanota afin d'accéder au mail. La page ne sera accessible qu'après avoir entré un mot défini à l'avance. Ce mode est disponible pour les personnes n'utilisant pas Tutanota. 

Maintenant sur la partie plus pratique, voici [les offres][8] de la société pour les particuliers : 

![](/images/Tutanota/tutanota_price.png)

Sur l'offre gratuite, c'est très similaire à ProtonMail, c'est à dire utilisation d'un domaine Tutanota avec des limites. 

De mon côté, je suis parti sur l'offre Premium à 12€ par an ou 1,20 par mois. Cette offre répond totalement à la liste de mes besoins, de plus il a été très facile de migrer de mon compte Mail Pro OVH. 

### Migration

Au niveau de la configuration DNS, j'ai dû modifier les entrées pour enlever les redirections d'OVH et les remplacer par Tutanota. Voici le résultat : 

||||
|--- |--- |--- |
|Adresse|Enregistrement|Données|
|example.com|TXT|« t-verify=xxxxx »|
|example.com|TXT|v=spf1 include:spf.tutanota.de -all|
|example.com|MX|100 mail.tutanota.de.|
|s1._domainkey.example.com|CNAME|s1._domainkey.tutanota.de.|
|s2._domainkey.example.com|CNAMZ|s2._domainkey.tutanota.de.|

L'ensemble de ces entrées sont indiquées lors de l'ajout de votre domaine personnel à votre compte Tutanota, tout comme les anciennes entrées à supprimer ce qui rend le changement très facile. De plus il est possible de tester en direct les changements depuis l'interface. 

Le seul défaut que je lui trouve pour l'instant c'est qu'il n'est pas possible d'importer des mails existants, comme mon fichier « pst » par exemple, mais c'est fonctionnalité est prévue dans leur roadmap. 

### Offre marque blanche

Il est aussi à noter que dans l'offre Pro il est également possible de créer directement un sous domaine pour votre société et de modifier l'interface (couleurs, logo) pour ajuster en fonction de votre charte.

Ce service est proposé en marque blanche, ce qui est très pratique pour les entreprises ou les associations par exemple. Il est aussi à noter que pour les associations à but non lucratif l'offre Premium est offerte. L'option marque blanche est également disponible pour les particuliers avec un compte premium pour 12€ par an. 

Pour configurer la redirection vers votre domaine il faut une nouvelle fois utiliser une entrée CNAME : 

||||
|--- |--- |--- |
|webmail.example.com.|CNAME|login.tutanota.com|


Voici également l'interface disponible pour ce menu, avec comme le voyez la possibilité de créer également un certificat Let's Encrypt ou d'utiliser un certificat externe :

![](/images/Tutanota/tutanota_marqueb.png)

### Interface et fonctionnalités

Au niveau de l'interface, c'est un client plutôt classique : 

![](/images/Tutanota/tutanota_interface.png)


Au niveau des fonctionnalités nous avons une liste de contact, le service de mail et plus récemment un calendrier et le tout chiffré. 

Comme vu précédemment, lors d'un envoi de mail à un utilisateur n'utilisant pas Tutanota, il est possible d'utiliser le mode « Confidentiel » en cochant la case suivante : 

![](/images/Tutanota/tutanota_mail1.png)

Et du coup l'utilisateur du mail recevra un mail de ce type pour accéder à son interface temporaire :

![](/images/Tutanota/tutanota_mail2.png)

Une fois sur la page vous devrez rentrer le mot de passe défini à l'avance :

![](/images/Tutanota/tutanota_mail3.png)

Vous pouvez maintenant accéder à votre message et même y répondre :

![](/images/Tutanota/tutanota_mail4.png)


## Conclusion

Donc, pour l'instant ce service répond à l'ensemble de mes besoins, à voir sur une utilisation à long terme si ça se confirme. De plus, avec les prises de conscience actuelle il semble être une très bonne alternative aux autres offres du marché. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://tutanota.com
 [2]: https://ubuntu.com/
 [3]: https://fr.libreoffice.org/
 [4]: https://www.qwant.com/
 [5]: https://protonmail.com/
 [6]: https://github.com/tutao/tutanota
 [7]: https://www.tutanota.com/fr/faq
 [8]: https://tutanota.com/pricing/
 [9]: images/Tutanota/tutanota_price.png
 [10]: images/Tutanota/tutanota_marqueb.png
 [11]: images/Tutanota/tutanota_interface.png
 [12]: images/Tutanota/tutanota_mail1.png
 [13]: images/Tutanota/tutanota_mail2.png
 [14]: images/Tutanota/tutanota_mail3.png
 [15]: images/Tutanota/tutanota_mail4.png