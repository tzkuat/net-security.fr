---
title: 'Copier un badge NFC avec Kali Linux & Android'
author: Mickael Rigonnaux
type: post
date: 2019-10-13T19:05:52+00:00
url: /security/copier-un-badge-nfc-avec-kali-linux-android/
thumbnail: /images/NFC/logo.png
featureImage: images/NFC/logo.png
shareImage: /images/NFC/logo.png
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "Android",
    "NFC",
    "Hack"
]
categories: [
    "securite"
]

---
Bonjour à tous ! Aujourd'hui nous allons voir ensemble comment copier un badge NFC avec un ARC122U sur [Kali Linux][1] ou avec un téléphone Android. 

## Introduction

Depuis maintenant quelques années la radio-identification (RFID) est utilisée dans beaucoup de domaine comme les titres de transport où encore pour l'accès à des bâtiments. Ces systèmes sont très pratiques et facile à mettre en œuvre mais sont bien souvent peu sécurisé. Cela peut-être dû à une mauvaise implémentation ou tout simplement à des faiblesses connues dans le système. 

La RFID est basée sur les ondes radio. Il permet d'échanger des données sans avoir pour autant un contact physique comme pour une bande magnétique. 

Un système RFID est forcément composé d'un « tag » et d'un « lecteur ». Lors d'une utilisation, à l'approche du tag le lecteur envoie un signal radio au tag. Ce dernier renvoi alors ses informations. 

Il existe également des « tags » qui peuvent être actifs, ce qui rallonge le champ d'action du tag, comme pour les badges des télépéages. Ces tags utilisent une source d'alimentation. 

Les fréquences utilisées par la RFID sont les suivants : 

  * Basse : 125kHz
  * Haute : 13.56MHz
  * Ultra Haute : 856Mhz

## Pourquoi ? 

On peut également se demander pourquoi copier un badge NFC est intéressant. Cela peut l'être pour plusieurs cas, dans mon cas c'est pour avoir un second VIGIK pour l'entrée de mon immeuble par exemple, car la co-propriété m'a dit que c'était impossible et que ça coutait très chère. 

Il peut-être aussi important d'essayer de copier votre badge d'accès pour votre entreprise par exemple afin de tester la sécurité du système. Et vous risquez d'être surpris. J'ai pu tester plusieurs systèmes, et un grand nombre d'entre eux ne prennent même pas la peine de changer les clés par défaut pour compliquer la copie. Que ça soit pour des accès à des bâtiments scolaires, à des hôtels, etc. 

Et pour finir, ça peut-être très intéressant dans le cadre d'un audit ou d'un red team ! Surtout que la copie est possible rapidement avec un simple téléphone Android. Selon le badge copié vous pourrez avoir accès à des bâtiments ou des salles spécifiques (serveurs, secrets, etc.). 

## NFC

De son côté le NFC (Near Field Communication) reprend les mêmes bases que la RFID en apportant des améliorations, comme la communication entre deux lecteurs. Le NFC lui fonctionne essentiellement sur la haute fréquence de 13.56MHz. 

Il existe plusieurs types de tag NFC avec plus ou moins de sécurité comme le Mifare Classic, celui que nous allons copier dans cet article ou encore les tags DES Fire vX. Ces derniers comportent un niveau de sécurité supérieur. 

## Mifare Classic

Les cartes Mifare Classic sont les plus répandues dans le monde du NFC même si elles sont vulnérables à plusieurs attaques. Ces failles connues depuis 2008 sont basées sur le hardware et sur la cryptographie appliquée à ces tags. L'algorithme CRYPTO1 est en effet obsolète aujourd'hui. 

Les cartes Mifare Classic existent en version 1ko et 4ko. Le fonctionnement est exactement le même, seul l'espace de stockage change. 

Voici le schéma du stockage d'une carte Mifare Classic 1K : 

![](/images/NFC/Mifare.png)

Vous remarquerez que le bloc 0 est normalement défini par le constructeur et n'est disponible qu'en lecture seule. Cependant, il est possible de se procurer des cartes avec le bloc 0 en écriture pour réaliser des copies complètes. 

Le bloc 0 correspond à l'UID qui est basé sur 4 ou 7 octets. 

Le stockage est divisé en secteurs et ces mêmes secteurs sont divisés en blocs. 

Pour un tag 1ko ça fait 16 secteurs avec 16 secteurs de 4 blocs de 16 octets. 

On remarque également la présence des clés dans chaque secteur pour accéder à la mémoire. 

Avant de passer à la partie pratique, je tenais à préciser qu'en dehors de l'aspect technique, le problème avec le NFC vient souvent de l'implémentation. Dans la plupart des cas, les clés utilisées sont celle par défaut et la partie utilisée du tag se résume seulement à l'UID. 

## Copier avec Kali Linux

Dans mon cas j'utilise Kali Linux car il englobe déjà tous les outils nécessaires à la duplication de notre tag NFC. 

J'utilise pour copier mon badge un ACR122U et des badges avec le bloc 0 accessible en écriture. Vous pouvez trouver ces outils directement en ligne. 

Tout d'abord il faut lancer la commande suivante pour charger le module et rendre notre ACR122U fonctionnel :

```
modprobe -r pn533_usb
```

Après avoir placé le tag NFC que vous voulez copier vous pouvez maintenant lancer la commande `nfc-list` pour vérifier qu'il est bien pris en compte : 

```
nfc-list -v
```

![](/images/NFC/Kali_1.png)

Vous pouvez maintenant lancer la copie du badge avec cette commande qui permet de créer un dump complet de votre tag : 

```
mfoc -O net-security.dmp
```

![](/images/NFC/Kali_2.png)

Si tout se déroule comme prévu vous devriez avoir un retour comme ci-dessous. Si ce n'est pas le cas, c'est que votre badge utilise des clés qui ne sont pas utilisés par défaut. Vous pouvez alors récupérer un fichier contenant d'autres clés et lancer un dump en utilisant ce fichier : 

```
wget https://github.com/ikarus23/MifareClassicTool/blob/master/Mifare%20Classic%20Tool/app/src/main/assets/key-files/extended-std.keys
mfoc -f extended-std.keys -O net-security.dmp
```

![](/images/NFC/Kali_3.png)

Et pour finir, vous pouvez écrire directement votre dump sur votre badge vierge avec l'UID 0 disponible en écriture :

```
nfc-mfclassic W a net-security.dmp
```

Vous avez maintenant votre nouveau badge copié, vous pouvez vérifier que l'UID à lui aussi été copié avec la commande : 

```
nfc-list -v 
```

Si vous rencontrez une erreur du type « unlock failure! » c'est que votre carte/tag n'est malheureusement pas disponible en écriture sur le bloc 0.

![](/images/NFC/Kali_4.png)

Il est peut-être possible que cela fonctionne avec la copie par téléphone que nous allons voir maintenant par contre. 

## Copier un badge avec un Smartphone Android

Dans mon cas je vais utiliser un Xiaomi Mi 9T Pro qui n'est pas rooter pour copier mon badge. Selon votre téléphone, vous aurez peut-être besoin qu'il le soit. 

Pour ce faire, je vais simplement utiliser l'application Mifare Classic Tool disponible [directement sur le Play Store][7]. 

Elle va nous permettre de manipuler et de modifier directement nos dump et de les écrire directement sur des badges vierges. 

La procédure est plus simple vu qu'elle ne nécessite pas d'utiliser la ligne de commande. 

Tout d'abord ouvrez l'application et rapprocher le tag que vous voulez copier du dos de votre téléphone. Vous devriez voir son UID s'afficher sur l'écran. Maintenant rendez vous dans « READ TAG » : 

![](/images/NFC/Android_1.png)

Une fois dans ce menu, cochez l'utilisation des deux listes de clé et lancer la lecture du tag : 

![](/images/NFC/Android_2.png)

Vous avez maintenant accès à l'ensemble des blocs de votre tag. Enregistrez le avec le logo de la disquette en haut. De mon côté je l'ai nommé « net-security ». 

![](/images/NFC/Android_3.png)

Vous pouvez maintenant vous rendre dans le menu principal et sélectionner « WRITE TAG ». 

Une fois dans ce menu il faut choisir « WRITE DUMP » et cochez les 2 options : 

![](/images/NFC/Android_8.png)

Il suffit maintenant de choisir le dump à écrire : 

![](/images/NFC/Android_7.png)

Et de lancer l'écriture en sélectionnant les deux fichiers de clés en approchant le tag disponible en écriture sur le bloc 0 du téléphone : 

![](/images/NFC/Android_5.png)

Si tout ce passe bien, vous aurez le message suivant : 

![](/images/NFC/Android_6.png)

Et voilà vous avez maintenant copié votre badge. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Sur le même principe n’hésitez pas à me dire si des articles sur d’autres outils de ce type vous intéresse. 

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.kali.org/
 [2]: images/NFC/Mifare.png
 [3]: images/NFC/Kali_1.png
 [4]: images/NFC/Kali_2.png
 [5]: images/NFC/Kali_3.png
 [6]: images/NFC/Kali_4.png
 [7]: https://play.google.com/store/apps/details?id=de.syss.MifareClassicTool&hl=fr
 [8]: images/NFC/Android_7.png
 [9]: images/NFC/Android_5.png
 [10]: images/NFC/Android_6.png