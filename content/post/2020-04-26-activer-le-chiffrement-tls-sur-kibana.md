---
title: Activer le chiffrement TLS sur Kibana
author: Mickael Rigonnaux
type: post
date: 2020-04-26T11:41:52+00:00
url: /security/activer-le-chiffrement-tls-sur-kibana/
thumbnail: /images/kibana_logo.png
featured: true
tags: [
    "cryptographie",
    "GNU/Linux",
    "Elastic",
    "Kibana"
]
categories: [
    "systeme",
    "securite"
]

---
Bonjour à tous ! Aujourd'hui un article assez court concernant l'activation du protocole TLS sur l'interface de [Kibana][1].

## Introduction

J'ai commencé il y a peu à travailler sur ELK et je pense donc proposer plusieurs articles dans ce style sur le blog.

Par défaut, lorsque vous faite une installation de Kibana, HTTP est utilisé pour les communications et aucune authentification n'est activée. Nous allons voir dans cet article comment activer rapidement le chiffrement des flux avec TLS et vérifier les configurations.

## Configuration

Je pars du principe que vous avez déjà monté votre [suite Elastic][2] et que vous voulez simplement activer TLS. Dans mon cas j'utilise un mono-noeud sous Centos 8 minimale avec la suite Elastic (Elasticsearch, Kibana) en version 7.6.2. La mise en place du chiffrement sur Kibana ne nécessite pas l'utilisation d'une licence commerciale, l'activation des fonctionnalités de sécurité d'Elasticsearch n'est pas obligatoire non plus.

Toutes les commandes sont lancées avec l'utilisateur root.

Il faut commencer par créer le certificat qui sera utilisé. Je génère un certificat auto-signé avec OpenSSL pour l'article, n'hésitez pas à utiliser le vôtre si vous en avez un. 


```
cd /etc/kibana/

openssl req -x509 -newkey rsa:2048 -nodes -keyout kibana.key -out kibana.crt -days 365
```


Le certificat en question sera valable 1 an et utilise une clé de 2048 bits avec le protocole RSA.

Je modifie en suite les droits des fichiers, pour que les utilisateurs du groupe « kibana » puisse les utiliser :


```
chown root:kibana kibana.crt
chown root:kibana kibana.key

chmod 644 kibana.crt
chmod 640 kibana.key
```


Les droits sur la clé sont plus restrictifs que sur le certificat, seul root et les personnes du groupe « kibana » peuvent récupérer la clé privée du certificat.

Il faut en suite ajouter les lignes suivantes dans le fichier de configuration /etc/kibana/kibana.yml :


```
server.ssl.enabled: true
server.ssl.certificate: /etc/kibana/kibana.crt
server.ssl.key: /etc/kibana/kibana.key
server.ssl.certificateAuthorities: /etc/kibana/kibana.crt
server.ssl.supportedProtocols: ["TLSv1.2"]
```


Dans mon cas j'active seulement TLS 1.2 car c'est la dernière version supportée par Kibana, contrairement à Elasticsearch il ne supporte pas encore TLS 1.3. Les protocoles TLS 1.0, TLS 1.1 & TLS 1.2 sont disponibles.

Le chemin vers l'autorité de certification redirige volontairement vers le certificat, vu que c'est un certificat auto-signé dans mon cas. Remplacez par votre autorité si vous en avez une.

Vous pouvez maintenant redémarrer Kibana pour terminer cette configuration :


```
systemctl restart kibana
```


Vous devriez maintenant avoir une interface de Kibana qui utilise le chiffrement TLS :

![](/images/kibana_1.png)

Si vous désirez aller plus loin dans la configuration de TLS, vous pouvez le faire en filtrant les suites de chiffrement que vous voulez utiliser. Par exemple en rajoutant cette ligne :


```
server.ssl.cipherSuites: ["ECDHE-RSA-AES128-GCM-SHA256", "ECDHE-ECDSA-AES128-GCM-SHA256", "ECDHE-RSA-AES256-GCM-SHA384"]
```


Cela permet d'utiliser seulement les suites que vous voulez. La liste des suites disponibles est disponible sur [ce lien][3]. 

Il faut cependant faire attention aux suites que vous activez, cela peut impacter le niveau de sécurité de la configuration, mais également poser des problèmes de compatibilité avec les clients que vous utilisez.

## Tester la configuration

Pour vérifier que vos configurations sont bien prises en compte vous pouvez utiliser l'outil disponible en ligne de commande [testssl][4]. J'ai déjà présenté cet outil dans [un article dédié][5].

Il suffit de le télécharger et de l'exécuter avec l'URL que vous voulez tester :


```
git clone --depth 1 https://github.com/drwetter/testssl.sh.git
cd testssl.sh
./testssh.sh 192.168.3.168:5601 # (443 si non renseigné)
```


Et voici le résultat sur mon instance de Kibana :

![](/images/kibana_2.png)

Nous voyons bien que seul TLS 1.2 est activé et que le nombre de cipher est réduit. La configuration est bien prise en compte.

## Sources

  * <https://www.elastic.co/guide/en/kibana/current/configuring-tls.html>
  * <https://www.elastic.co/guide/en/kibana/current/settings.html>

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.elastic.co/fr/kibana
 [2]: https://www.elastic.co/fr/products/
 [3]: https://www.elastic.co/guide/en/kibana/current/settings.html
 [4]: https://testssl.sh/
 [5]: https://net-security.fr/security/testez-votre-configuration-ssl/