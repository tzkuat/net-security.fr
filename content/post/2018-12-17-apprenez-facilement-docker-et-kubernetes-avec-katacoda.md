---
title: Apprenez facilement Docker et Kubernetes avec Katacoda !
author: Mickael Rigonnaux
type: post
date: 2018-12-17T15:41:40+00:00
url: /system/apprenez-facilement-docker-et-kubernetes-avec-katacoda/
thumbnail: /images/Katacoda/Kata_Present.png
featureImage: images/NFC/Kata_Present.png
shareImage: /images/NFC/Kata_Present.png
featured: true
tags: [
    "Docker",
    "Kubernetes",
    "DevOps"
]
categories: [
    "system"
]

---
Bonjour à tous, aujourd’hui un très rapide article sur [Katacoda](https://www.katacoda.com/), une plateforme interactive qui permet d’apprendre et de tester facilement des outils comme Docker Swarm ou Kubernetes directement depuis votre navigateur. Ce site a été créé par [Ben Hall](https://twitter.com/Ben_Hall).

Ce site Web propose différents tutoriels sur des technologies comme Docker Swarm, Kubernetes, CoreOS avec en supplément des environnements en ligne de commande pour les tester. Ils fournissent donc un ensemble complet pour apprendre même si vous n’avez pas de ressource sous la main.

Vous avez notamment la possibilité d’installer un cluster Kubernetes avec plusieurs ou un seul nœud, d’effectuer des configurations pour mettre la technologie docker en production, etc.

Les tutoriels durent en général entre 10 et 15 minutes et sont accompagnés d’explication en anglais ainsi que des commandes avec la possibilité de les entrer directement et d’observer le résultat.

Je trouve le principe et les cours très intéressants, à noter que tous les cours sont gratuits et que certains sont Open Source, vous avez même la possibilité d’en ajouter vous-même. Si vous êtes une entreprise, vous avez la possibilité d’adapter le contenu des cours en passant par l’offre payante de Katacoda.

Personnellement je suis tombé par hasard sur ce site en cherchant des tutoriels pour Kubernetes et ça m’a permis de mieux comprendre le fonctionnement de cet outil, qui n’est vraiment pas évident à installer. De plus les cours sont très rapides et simples à comprendre, l’idéal si vous débutez.

Voici un exemple des cours disponibles sur cet outil :

![](/images/Katacoda/Kata_2.png)

Les différents cours sont découpés en scénario, voici un exemple pour Kubernetes :

![](/images/Katacoda/Kata_1.png)

Et voici un exemple d’interface lors d’un cours, avec les instructions et les commandes sur la gauche et les environnements de test sur la droite :

![](/images/Katacoda/Kata_3.png)

Ce site propose également une partie sur la sécurité des conteneurs très complète qui devrait vous intéresser :

![](/images/Katacoda/Kata_5.png)

Cette partie contient les éléments de base de la sécurité sous Docker, comme l’utilisation des CGroups et de Seccomp.

Pour conclure sur cet article, je vous conseille vraiment de jeter un œil à cette plateforme si vous vous intéressez aux conteneurs, les cours sont très variés et simples à suivre.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat