---
title: Retour sur un exercice de phishing
author: Mickael Rigonnaux
type: post
date: 2022-06-29T10:05:44+00:00
url: /securite/rex-phishing
thumbnail: /images/phishing-logo.png
shareImage: /images/phishing-logo.png
featured: true
toc: true
tags: [
    "phishing",
    "zphisher",
    "vps",
    "domaine",
    "OSINT"
]
categories: [
    "securite"
]

---

Bonjour à tous ! Aujourd'hui un article qui va parler de sensibilisation à la sécurité. On a beau faire de la sensibilisation et communiquer régulièrement, rien ne vaut un exercice grandeur nature lorsque l'on parle de phishing. De mon côté c'est la 1ère fois que j'ai pu en mettre un en place. Je vais essayer de vous présenter dans cet article comment j'ai fait et ce que j'ai pu apprendre.

## Disclaimer

Cet article est proposé à titre pédagogique/professionnel uniquement. Je ne pourrais en aucun cas être tenu responsable des mauvaises utilisations des différents logiciels.

Dans tous les cas, ne débutez une campagne de phishing qu'avec l'accord de votre direction ou de votre responsable.

## Prise d'informations

Afin de coller le plus possible à la réalité, j'ai tout d'abord fait des recherches afin de trouver les différentes personnes à cibler lors du ou des tests.

### Organigramme

Pour ça, rien de bien compliqué : Linkedin. Peu importe la boite dans laquelle vous êtes (c'est vrai dans 99% du temps, surtout dans la tech) vous trouverez une grande partie de l'organigramme de la boite. Afin de ne pas éveiller de soupçons j'ai utilisé un faux compte, au cas où.

Ce qui est important c'est d'identifier les personnes qui vont être clés pour votre test. Par exemple :
* Le ou les directeurs
* Les personnes en charge du support
* Les administrateurs (système, réseau, sécurité)
* Les personnes en charge de la comptabilité, de la paie
* Toute autre personne capable de vous intéresser 

Personnellement, j'ai essayé de recréer l'organigramme de l'entreprise.

En dehors de Linkedin, vous pouvez utiliser des sites comme societe.com afin de trouver les dirigeants, ou tout simplement Google.

Des recherches comme `directeur <société A>` fonctionne généralement bien.

Sur Linkedin par exemple vous n'avez pas toujours accès aux noms de famille, il suffit alors de taper le nom du poste et celui de l'entreprise sur Google pour le récupérer.

### Les mails

Une fois que vous avez plus ou moins votre organigramme, nous pouvons nous intéresser aux mails. Afin de faire du phishing il est important de voir comment sont formés les mails de la société.

Pour cela, j'ai utilisé le site [hunter.io](https://hunter.io).

Ce site est accessible en partie gratuitement, il permet en 2 clics de récupérer le format de ces mails, voici un exemple simple avec le domaine `linux.org` :
![](/images/phishing-1.png)

Il permet également de trouver l'ensemble des mails que ce site a pu trouver en cherchant sur les différentes pages d'Internet. Cela permet de récupérer facilement les mailing lists comme : `communication@societe.com`.

En plus d'hunter.io, vous pouvez faire une Google Dork simple afin de trouver les mails :
* `intext:"@societe.com"`

Cela permet de remonter des mails génériques et éventuellement des mails appartenant à des personnes. Cela permet aussi de vérifier le format :
![](/images/phishing-2.png)

Pour terminer, vous pouvez aussi vérifier les différents mails avec un outil qui permet de voir si un mail existe ou non. Personnellement j'ai utilisé celui-ci :
* https://wiza.co/verify-email-free

Je ne sais pas vraiment ce que ça vaut, mais dans mon cas il fonctionnait correctement. Cela permet notamment de vérifier l'existence de mail comme :
* `communication@example.com`
* `exploitation@example.com`
* `reseau@example.com`
* `direction@example.com`

Encore une fois, tout dépendra de ce que vous cherchez. De mon côté je connaissais déjà la réponse à ces questions avant de commencer, le but était vraiment de me placer comme un attaquant.

J'avais donc en ma possession un organigramme, le format des mails ainsi qu'une partie des mails génériques, il ne restait qu'à créer des scénarios.

Je n'ai pas été plus loin dans la prise d'informations, mais vous pouvez aller aussi loin que vous le souhaitez : 
* Récupérer les range d'IP publiques
* Les systèmes utilisés

Afin de pouvoir faire du phishing en fonction de ces applications/outils.

Une information intéressante et facile à récupérer est le système de mail, c'est généralement disponible via une simple commande `dig` en récupérant les enregistrements DNS relatifs aux mails. Un exemple avec le domaine `atos.com` ou le peut voir qu'Office 365 est probablement utilisé via l'enregistremnt SPF :
```
tzkuat@tzkuat-pc:~$ dig atos.com txt | grep spf
atos.com.               841     IN      TXT     "v=spf1 mx include:spf.protection.outlook.com include:_spf.prod.hydra.sophos.com -all"
```

De même en récupérant l'enregistrement MX :
```
root@tzkuat-pc:~# dig microsoft.com mx

; <<>> DiG 9.18.1-1ubuntu1-Ubuntu <<>> microsoft.com mx
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 38696
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 7

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;microsoft.com.                 IN      MX

;; ANSWER SECTION:
microsoft.com.          3600    IN      MX      10 microsoft-com.mail.protection.outlook.com.

;; ADDITIONAL SECTION:
microsoft-com.mail.protection.outlook.com. 9 IN A 40.93.212.0
microsoft-com.mail.protection.outlook.com. 9 IN A 52.101.24.0
microsoft-com.mail.protection.outlook.com. 9 IN A 104.47.53.36
microsoft-com.mail.protection.outlook.com. 9 IN A 104.47.54.36
microsoft-com.mail.protection.outlook.com. 9 IN A 40.93.207.0
microsoft-com.mail.protection.outlook.com. 9 IN A 40.93.207.1
```

## Les scénarios

C'est je le pense la partie la plus importante et la plus difficile. Car il faut réussir à faire un scénario plausible avec **seulement** les informations récoltées durant la phase de prise d'informations.

Sinon, cela n'a pas de sens, il faut rester dans la position de l'attaquant et ne pas utiliser les informations que vous avez dans le cadre de votre travail. Envoyez un faux mail à votre collègue pour lui demander des informations/documents sur un dossier dont vous venez de discuter dans le couloir n'aura pas d'intérêt, car à moins d'une attaque très sophistiquée, cela n'est pas possible/plausible.

C'est donc là que la prise d'informations était importante : 
* Qui cibler ?
* Quelle identité faut-il usurper ?
* Quelles sont les personnes à éviter ?
* Que voulez-vous récupérer/faire ? Faire exécuter un fichier ? Récupérer des identifiants ?

Dans les grandes lignes, ce qui fonctionne :
* Se faire passer pour le directeur en étant pressé (un bon coup de pression)
* Se faire passer pour un administrateur réseau/système
* Se faire passer pour quelqu'un du support

Ce qu'il ne faut pas faire :
* Envoyer un mail de phishing à un trop grand nombre de personne
* Inclure les personnes s'occupant de la sécurité
* Envoyé un mail flou (il faut être précis et attendre une action concrète)

Voici des exemples de scénarios :
* Demander à des collègues (listes précises des administrateurs par exemple) d'ouvrir une pièce jointe en disant qu'elle vient d'un client (surtout avec la dernière vulnérabilité Follina)
* Demander aux équipes de support de réinitialiser + d'envoyer un mot de passe en se faisant passer pour un des directeurs
* Demander à l'ensemble de la société de réinitialiser des accès (Office 365 par exemple) en prétextant un problème de sécurité en renvoyant vers une fausse page d'authentification

Le plus important selon ma petite expérience est de ne pas viser trop large, si une boite de 400 personnes reçoit le même mail, au même moment... Cela va se voir.

Encore une fois, plus vous aurez pu récupérer d'informations, meilleur sera l'exercice et plus vous serez proche d'un attaquant.

## Les domaines

J'ai maintenant : les scénarios, les personnes à viser, leurs mails, leurs postes, etc.

Il faut maintenant choisir un ou plusieurs domaines afin de réaliser l'attaque plus concrètement.

De mon côté j'ai découvert quelque chose (qui était possible depuis un moment apparemment) : il est possible d'enregistrer des domaines avec des caractères spéciaux, et : ça fonctionne super bien.

Cela est possible grâce aux IDN ou Internationalized Domain Names.

Voici deux articles rédigé par Stéphane Bortzmeyer (également un [lien vers son blog](https://www.bortzmeyer.org/)) :
* [Peut-on avoir des caractères composés dans un nom de domaine ?](https://www.afnic.fr/observatoire-ressources/papier-expert/peut-on-avoir-des-caracteres-composes-dans-un-nom-de-domaine/)
* [Héberger un nom de domaine avec caractères composés](https://www.afnic.fr/observatoire-ressources/papier-expert/heberger-un-nom-de-domaine-avec-caracteres-composes/)

Ce qu'il faut retenir, c'est qu'il a toujours été possible de faire ça, mais que ça ne fonctionnait pas avant les IDN. Cela est possible grâce à l'encodage punycode, certains protocoles/applications permettent l'utilisation des IDN alors que d'autres non. Cela va changer l'affichage des caractères.

Par exemple (il est possible de tester ça grâce à la commande `idn2` sur un système GNU/Linux) : 
```
root@tzkuat-pc:~# echo société.com | idn2
xn--socit-esab.com
root@tzkuat-pc:~# echo âpple.com | idn2
xn--pple-9na.com
```

Le domaine `société.com` en punycode deviendra `xn--socit-esab.com` au format unicode, un autre exemple est disponible pour `âpple.com`. L'affichage changera en fonction de la prise en charge des IDN. Par exemple, sur les webmail comme Office 365 les domaines au format `xn--socit-esab.com` sont utilisés, alors que dans les clients Outlook, aucun problème avec les caractères spéciaux. Pour les navigateurs, aucun souci également, je ne pensais pas mais j'ai même réussi à générer des certificats avec ces domaines. La configuration mail (envoi/réception) est également parfaitement fonctionnelle.

Cela vous donne une idée de ce qu'il est possible de faire avec ces domaines. De ce que j'ai pu tester, cela fonctionne avec l'extension `.fr`. Et cela fonctionne également parfaitement pour les mails.

Evoluant dans la société SITEC, j'ai pu acheter par exemple le domaine `sítec.fr` en lieu et place de `sitec.fr`.

De mon côté pour mes exercices j'ai enregistré deux domaines un premier pour usurper les mails :
* `sítec.fr`

Ainsi qu'un second pour faire une fausse page de connexion Office 365 :
* `microsoft-online.fr` (en utilisant le sous domaine `login.microsoft-online.fr`) en sachant que la vraie page d'authentification pour Office 365 est `login.microsoftonline.com`

### Boites mails

La plupart des domaines sont fournis avec des boites mails gratuites. C'est personnellement ce que j'ai utilisé (via OVH) pour envoyer les mails de phishing.

Voici un exemple de mail reçu affiché sur Outlook via Office 365 :
![](/images/phishing-3.png)

Un autre depuis le Webmail Outlook :
![](/images/phishing-4.png)

Si vous devez faire des tests en usurpant plusieurs personnes, il suffit de supprimer la boite et d'en recréer une avec un nouveau nom. 

**Attention** : j'ai dû réaliser plusieurs tests afin d'avoir le bon format prénom/nom affiché lors de l'arrivée du mail.

Les mails en question passent parfaitement les anti-spam.

## Outils de phishing

De mon côté j'ai fait le choix de créer une fausse page de phishing basée sur Office 365. Pour cela il faut donc :
* Un hébergement
* Un certificat pour le domaine
* Un outil permettant de créer une fausse page

Pour l'ensemble des éléments le choix est vaste. Concernant l'outil, j'ai pu en tester plusieurs mais j'ai utilisé [zphisher](https://github.com/htr-tech/zphisher) dans la finalité. C'est un outil libre qui permet de créer des fausses pages pour plusieurs sites comme : Office 365, Twitter, Steam...

Toutes les informations sont disponibles sur le repo Github :
* https://github.com/htr-tech/zphisher

### Hébergement

Pour héberger ce faux site j'ai utilisé un VPS, qui m'a couté à peu près 4€/TTC pour le mois. La machine en question est sur Ubuntu.

L'outil `zphisher` propose plusieurs fonctionnalités, de mon côté j'ai utilisé l'option qui permet de faire écouter l'application sur `localhost` et j'ai utilisé un reverse proxy HAProxy afin de rendre l'application disponible publiquement.

J'ai tout d'abord fait pointer le domaine voulu (dans mon cas `login.microsoft-online.fr`) vers l'IP publique de mon VPS. Une fois fait j'ai pu générer un certificat Let's encrypt avec l'outil `certbot` : 
* `sudo certbot certonly --manual --preferred-challenges dns -d "login.microsoft-online.fr"`

Après avoir ajouté l'enregistrement le certificat est généré et il est possible de l'utiliser.

Il est également possible de générer un certificat avec un IDN, voici mon exemple avec le domaine `sítec.fr` :
* `sudo certbot certonly --manual --preferred-challenges dns -d "xn--stec-vpa.fr" -d "exchange.xn--stec-vpa.fr" -d "www.xn--stec-vpa.fr"`

Dans cet exemple le certificat sera valide pour :
* `sítec.fr`
* `www.sítec.fr`
* `exchange.sítec.fr`

J'ai pensé dans un premier temps créer un lien de phishing avec le domaine contenant des caractères spéciaux, mais cela ne fonctionnait pas bien sur les mobiles par exemple. Le domaine au format unicode était affiché dans certains cas. J'ai donc préféré garder ce domaine pour les mails exclusivement et acheter un second domaine pour le site de phishing.

Pour utiliser le certificat, j'ai utilisé `haproxy` (celui directement disponible dans ma distribution Ubuntu) avec la configuration suivante qui se veut très simple :
```
##### FRONTEND #####

frontend http-in
        bind *:80
        bind *:443 ssl crt /etc/haproxy/certs/
        mode http
        option httplog

## ACL SECTION ##

        #acl acl_phishing_1 hdr(host) exchange.xn--stec-vpa.fr
        acl acl_microsoft hdr(host) login.microsoft-online.fr

## BACKEND DECLARATION ##

#use_backend fish if acl_phishing_1
use_backend fish if acl_microsoft

######## BACKEND 80 443 ########

backend fish
         redirect scheme https if !{ ssl_fc }
         http-request add-header X-Forwarded-Proto https if { ssl_fc }
         server login.microsoft-online.fr 127.0.0.1:8080 check maxconn 50
```

J'ai également rajouté l'option `forwardfor` afin de récupérer les vraies IP publiques et non 127.0.0.1.

La configuration en place est faite pour le domaine `login.microsoft-online.fr` mais j'ai laissé en commentaire les tests avec l'autre domaine également. HAProxy ne prend pas en compte les caractères spéciaux, vous devrez donc mettre le domaine au format unicode. Pour la partie certificat, il suffit de regrouper la clé, le certificat ainsi que l'autorité dans un seul fichier et le placer dans l'arborescence `/etc/haproxy/certs/`.

HAProxy est configuré pour aller récupérer le faux site sur `127.0.0.1:8080`, c'est la configuration par défaut de `zphisher` si vous n'utilisez pas les autres options disponibles.

Je ne l'ai pas précisé, mais j'ai ouvert les ports 80 & 443 TCP depuis internet depuis le pare-feu applicatif de la machine.

Une fois fait, j'avais donc accès à ces ports mais sans la page de phishing :
![](/images/phishing-5.png)

### zphisher

Déjà évoqué, `zphisher` est un outil qui permet de créer facilement des sites de phishing, il est très simple d'utilisation et d'installation. Je rajoute encore [le lien](https://github.com/htr-tech/zphisher).

Pour l'installer il suffit de lancer 3 commandes :
```
git clone https://github.com/htr-tech/zphisher.git
cd zphisher
bash zphisher.sh
```

Pour mon cas il suffisait de l'exécuter la commande `./zphisher` et de choisir les options 4 puis 1. A ce moment-là, le service est lancé, et est exposé grâce au HAProxy :
![](/images/phishing-6.png)
![](/images/phishing-7.png)
![](/images/phishing-8.png)

Il est aussi possible de lancer la commande en fond (c'est pas très beau je suis d'accord) en utilisant la commande `screen`.

Voici un exemple de la page depuis un navigateur :
![](/images/phishing-9.png)

La page est maintenant prête.

## Rédaction des mails

C'est un point essentiel, il faut s'adapter à la personne que vous usurpez. Un directeur ne s'adresse pas aux équipes support de la même façon que le manager de l'équipe.

Le plus important selon moi est de faire un mail "propre", aussi bien que la forme que sur le fond. Si possible, sans faute d'orthographe et en utilisant une signature de mail (à voir si vous avez réussi à en récupérer une lors de la phase de prise d'information).

Il est également important de mettre la pression aux personnes qui vont recevoir le mail, en précisant que c'est pressé et que vous attendez une réponse rapidement.

Par exemple, pour forcer les personnes à cliquer à entrer leurs logins vous pouvez trouver un prétexte comme une fuite de données et la compromission de leurs identifiants.

Une autre astuce pour que le lien paraisse plus "authentique" : créer un lien au format html. Ce qui permet de tromper plus facilement les personnes qui vont recevoir ce lien.

Un exemple avec le domaine récemment créé `https://login.microsoft-online.fr` qui devient `https://login.micrososftonline.com` grâce à cette balise :
* `<a  href="https://login.microsoft-online.fr" target="_blank">https://login.microsoftonline.com</a>`

Voici ce que cela donne une fois envoyé via Outlook par exemple :
![](/images/phishing-10.png)

## L'exercice en lui-même et l'après

Sur les différents exercices que j'ai pu mener, j'aurais pu mieux faire certaines choses et voici ce que je peux préciser.

Enregistrer un domaine avec Microsoft dedans n'a pas fonctionné longtemps. J'ai toujours la main sur le domaine mais il n'est plus résolu par les serveurs DNS à travers le monde.

Concernant la publication des pages de phishing, j'ai pu tester avec plusieurs domaines et au bout de 2 à 3 heures en ligne la page est vue comme du phishing par les différents navigateurs :
![](/images/phishing-11.png)

Lorsque j'ai envoyé le mail à un nombre de personne important, il n'est pas arrivé dans les spams mais dans la boîte principale, mais a été redirigé vers les spams un peu moins d'une heure après la réception.

Certains anti-virus ont pu bloquer l'attaque (Avast notamment) mais Sentinel One et Windows Defender n'ont rien remonté sur l'opération.

Il faut donc être réactif car l'exercice dure très peu de temps lorsque l'on fait du phishing avec pour but de récupérer des logins. Aucun problème cependant pour les autres exercices.

Dans les exercices que j'ai pu faire, le résultat est de 100%. A chaque fois j'ai pu récupérer ce que je voulais et peu importe le niveau de sensibilisation et de compétence.

### L'après

Faire un exercice de phishing, c'est bien, qu'il serve à quelque chose : c'est mieux.

Le but ici n'est clairement pas d'humilier ni d'afficher les personnes qui se sont fait avoir. Le but est de faire de la pédagogie et de faire progresser toute l'entreprise.

Des règles simples :
* Ne stockez pas les mots de passe récupérés
* Ne publiez pas la liste et gardez-la pour vous
* Supprimez les différents résultats une fois analysé et validé

Profitez en pour faire des sessions de sensibilisation globale (avec toute l'entreprise et non pas que les personnes concernées par l'exercice).

Communiquez les résultats (sans les noms bien entendu) sur un intranet par exemple en expliquant votre réflexion et en montrant que cela peut être facile que prévu initialement.

Je pense personnellement que c'est le meilleur moyen de s'en prémunir. Et pour terminer, il est important de répéter l'exercice régulièrement !

## Conclusion

Si on devait faire une conclusion à ces exercices et à cet article : le phishing ciblé ça fonctionne, c'est simple (techniquement) et ça coûte rien.

En tout, cela a dû me prendre 4h pour mettre le tout en place et moins d'une vingtaine d'euros pour les deux domaines (avec mails) et l'hébergement (VPS).

Si vous en avez la possibilité, je vous conseille fortement de faire des exercices régulièrement pour vos collaborateurs, car le jour ou quelqu'un de motivé le fera : ça fonctionnera forcément.

Personnellement, j'ai appris beaucoup de chose, dont l'accessibilité de la chose. Je ne pensais pas par exemple que les domaines avec des caractères spéciaux fonctionnait aussi bien.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !