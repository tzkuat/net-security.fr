---
title: 'OpenSSL – Formats & Cheat-Sheet'
author: Mickael Rigonnaux
type: post
date: 2019-06-12T13:58:37+00:00
url: /security/openssl-formats-cheat-sheet/
thumbnail: /images/OpenSSL-1/openssl-present.png
featureImage: images/OpenSSL-1/openssl-present.png
shareImage: /images/OpenSSL-1/openssl-present.png
tags: [
    "GNU/Linux",
    "OpenSSL",
    "cryptographie",
    "chiffrement"
]
categories: [
    "securite",
    "logiciellibre"
]

---

Bonjour à tous ! Aujourd'hui nous allons voir ensemble les principales commandes OpenSSL sur la manipulation des certificats X509, que ça soit générer, convertir, signer, etc. 

**EDIT :** vous trouverez une version plus à jour des commandes [sur ce lien.][1] 

## Les principaux formats

Tout d'abord nous allons aborder les formats les plus utilisés dans le monde des certificats, c'est à dire : 

### PEM Format

Le format le plus commun, il est utilisé dans la plupart des cas. Les certificats PEM sont en B64 et les extensions sont généralements .crt, .cer, .pem, .key

Ce format la est lisible avec un éditeur et comporte les indications « --- BEGIN XXX --- » & « --- END XXX --- » et est valable pour les certificats et les clés privées. 

### DER Format

Le format DER est le format binaire des certificats. Tous les types de certificats et clés privées peuvent être convertis en DER. Les fichiers ne contiennent donc pas les indications « BEGIN » & « END » et les fichiers sont difficilement lisibles avec un éditeur comme vi par exemple. Ils sont principalement utilisés sur des plateformes Java. 

Les extensions pour les fichiers sont généralement .cer .der & .key 

### P7B / PKCS7

Le format P7B est également un format basé sur le B64 et possède généralement les extensions .p7b & .p7c

Ce format n'est possible que pour les parties publiques des certificats et les autorités. Il n'est donc pas possible de mettre une clé privée au format p7b. Ce format est utilisé principalement sur des solutions Microsoft et Java.

### P12 / PFX

Le format p12/pfx quant à lui permet de stocker tous les éléments en même temps, c'est à dire les autorités, le certificat et la clé privée dans un fichier au format binaire chiffré. 

Il est utilisé sur les environnements Microsoft et Java et utilise les extensions : .p12 & .pfx

### Java Key Store

Les Java Key Store ou JKS fonctionne presque comme les fichiers p12 à la différence que dans ce dernier vous ne pouvez pas inclure les autorités ou CA. Il y a donc deux fichiers distants, le JKS contenant la clé privée et le certificat et le fichier cacert (truststore) contenant les différentes autorités. 

Pour en savoir plus vous pouvez vous rendre [ici][2] et [ici][3]. 

L'extension utilisée pour le JKS est .jks et ce type de certificat ce manipule avec l'outil Keytool contrairement aux autres. Cependant, attention il n'est pas possible à ma connaissance d'extraire de clé privée d'un JKS, si vous en avez besoin pensez à stocker vos clés privées en lieu sûr. 

## Les commandes principales

### Générales

```
### Générer une nouvelle clé RSA
openssl genrsa -out www.exemple.com.key 2048

### Générer une demande de certificat (CSR) sans clé privée
openssl req -sha256 -nodes -newkey rsa:2048 -keyout www.exemple.com.key -out www.exemple.com.csr

### Générer une nouvelle demande de certificat à base d'une clé existante
openssl req -new -sha256 -key www.exemple.com.key -out www.exemple.com.csr

### Générer une demande de certificat à base d'un certificat existant
openssl x509 -x509toreq -in www.exemple.com.crt -out www.exemple.com.csr -signkey www.exemple.com.key

### Générer un certificat auto-signé pour des tests
openssl req -x509 -newkey rsa:2048 -nodes -keyout www.exemple.com.key -out www.exemple.com.crt -days 365

### Afficher les informations d'une CSR
openssl req -text -noout -verify -in CSR.csr

### Afficher les informations d'une clé privée
openssl rsa -noout -text -check -in www.exemple.com.key

### Afficher les informations d'un certificat
openssl x509 -in certificate.crt -text -noout

### Afficher les informations d'un P12
openssl pkcs12 -info -in KEYSTORE.p12

### Afficher et tester les certificats sur un serveur
openssl s_client -connect www.exemple.com:443

### Vérification de l'appartenance CSR/clé/certificat avec le hash du modulus
openssl x509 -noout -modulus -in www.exemple.com.crt | openssl sha256
openssl req -noout -modulus -in www.exemple.com.csr | openssl sha256
openssl rsa -noout -modulus -in www.exemple.com.key | openssl sha256

### Converstion PEM vers P12
openssl pkcs12 -export -inkey fichier_clé_privée -in fichier_certificat -certfile fichier_chaine_certification -out nom_du_fichier_voulu.pfx
### Un mot de passe vous sera demandé pour créer le fichier p12.

### Clé et certificat PEM en un seul fichier
cat votre_cert.crt votre_key.key > nom_du_pem.pem

### Extraction de données d’un p12/pfx
##### La passphrase du PFX sera demandée pour chaque opération.
##### Export certificat
openssl pkcs12 -in votre_fichier.pfx -out certificate.crt –nokeys
##### Extraction d’une clé en PKCS8
openssl pkcs12 –in votre_fichier.pfx -out key.key -nocerts –nodes

### Clé privée PKCS8 vers PKCS1 (RSA)
openssl rsa -in key.key -out key2.key

### Clé privée PKCS1 vers PKCS8 
openssl pkcs8 -topk8 -inform PEM -outform PEM -nocrypt -in pkcs1.key -out pkcs8.key

### Conversion d'une clé privée DER vers PEM
openssl rsa -inform der -in votrecle_der.der -out nomdecle_pem.key

### Conversion d'une clé privée PEM vers DER
openssl rsa -inform PEM -outform der -in votrecle.key -out nomdecle.der

### Conversion d'un certificat DER vers PEM
openssl x509 -inform der -in certder.cer -out certificatename.crt

### Conversion d'un certificat PEM to DER
openssl x509 -outform der -in certificatpem.crt -out certificatder.cer

```

Pour la partie P7B personnellement j'utilise l'outil en ligne disponible [ici][4], ce n'est pas gênant car le P7B ne contient que des fichiers publiques. 

### Certificats ECC (Elliptic Curve)

Voir également [l'article sur ce sujet][5]. 

```
### Générer clé :
openssl ecparam -genkey -name prime256v1 -out key.pem

### Ajouter passphrase à la clé ECC :
openssl ec -in example.key -des3 -out example.key

### Générer CSR :
openssl req -new -sha256 -key example.key -nodes -out example.csr

### Générer certificat :
openssl req -x509 -sha256 -days 365 -key key.pem -in csr.csr -out certificate.pem

### Lister paramètres ECC :
openssl ecparam -list_curves

### Afficher la clé publique de la clé privée :
openssl ec -in example.key -pubout

### Afficher la clé publique de la CSR :
openssl req -in example.csr -pubkey -noout

### Afficher la clé publique du certificat :
openssl x509 -in example.crt -pubkey -noout
```

### 

## KeyTool (Java Key Store)


```
### Importation/Création du JKS
keytool -importkeystore -destkeystore nomdevotrejks.jks -srckeystore votrep12.p12 -srcstoretype pkcs12

### Créer/Importer un certificat et une clé privée au format pem (.crt/.key) dans un JKS
##### Création d'un fichier .pem contenant la clé privée et le certificat
cat votre_cert.cert votre_key.key > nom_du_pem.pem
##### Ajout du fichier pem dans un keystore
keytool -import -trustcacerts -alias votre_alias -file votre_pem.pem -keystore nomdevotrejks.jks

### Créer/Importer une CA dans un truststore
keytool -import -alias donnez_un_alias_a_votre_cert -keystore votretruststore.jks -file CA_ROOT.crt
keytool -import -alias donnez_un_alias_a_votre_cert -keystore votretruststore.jks -file CA_INT.crt

### Afficher les certificats publiques présent dans un JKS :
keytool -list -rfc -keystore votrejks.jks -storepass mdp_du_jks
```


J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://github.com/tzkuat/Ressources/blob/master/openssl-cheatsheet.md
 [2]: http://javarevisited.blogspot.com/2012/09/difference-between-truststore-vs-keyStore-Java-SSL.html
 [3]: http://www.java67.com/2012/12/difference-between-truststore-vs.html
 [4]: https://www.sslshopper.com/ssl-converter.html
 [5]: https://net-security.fr/security/openssl-ecc/