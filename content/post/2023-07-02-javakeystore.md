---
title: Créer un Java Key Store (JKS) en ligne de commande
author: Mickael Rigonnaux
type: post
date: 2023-02-06T10:05:44+00:00
url: /securite/creer-javakeystore
thumbnail: /images/certificate-tls.png
featureImage: images/certificate-tls.png
shareImage: /images/certificate-tls.png
featured: true
toc: true
tags: [
    "openssl",
    "cryptographie",
    "javakeystore",
    "certificat"
]
categories: [
    "securite"
]

---
Bonjour à tous ! Un rapide article pour débuter 2023, comment créer facilement un Java Key Store (ou JKS) en ligne de commande & facilement sur une machine GNU/Linux.

Je sais que le sujet des certificats est toujours compliqué à aborder, encore plus dans les environnements Java. C'est très souvent une cause de problème et de blocage.

Dans cet article nous allons donc voir comment générer un Java Key Store depuis une clé et un certificat au format B64.

Vous pouvez consulter mon article sur le sujet des extensions/formats pour les certificats si ça vous intéresse :
* https://net-security.fr/security/openssl-formats-cheat-sheet/

## Java Key Store ?

Un Java Keystore (ou JKS) est un format spécifique à Java utilisé pour stocker des clés privées et des certificats. Il est géré par la bibliothèque Java Cryptography Extension (JCE) et est utilisé par de nombreuses applications Java pour gérer les clés et les certificats. Pour manipuler un JKS il faudra passer par la commande `keytool` qui un peu particulière. Vous trouverez généralement deux fichiers dans les environnements Java (qui sont deux Keystore en soit) :
* Un Java Key Store : qui va contenir vos clés privées + vos certificats
* Un Trust Store : qui va contenir les autorités de confiance

## PKCS12 / P12 / PFX ?

Le format de fichier PKCS12 permet de stocker plusieurs éléments : certificats, autorités et clés privées. Il est défini par le `Public-Key Cryptography Standards` et est un format portable et reconnu par de nombreuses applications. Ils sont très utilisés par Microsoft par exemple mais il est tout à fait possible de les gérer/manipuler via `openssl`. Les extensions pour ce format de fichier sont généralement : `.pfx` & `.p12`.

## Convertir une clé et un certificat en Java Key Store

Dans mon cas je commence par générer une clé privée et un certificat. Dans votre cas, utilisez vos propres clés/certificats. Ceux générés sont au format PKCS8 ou base64. Il est facile de le vérifier, il suffit de voir si les mentions `BEGIN CERTIFICATE` ou `BEGIN PRIVATE KEY` sont présentes en ouvrant le fichier.

* Génération d'une clée privée et d'un certificat : `openssl req -x509 -newkey rsa:2048 -nodes -keyout test-java.key -out test-java.crt -days 365`

Après avoir lancé la commande j'ai bien deux fichiers : une clé privée et un certificat dans le bon format.
```
tzkuat@pop-os ~/test-java> ls -l
total 8
-rw-rw-r-- 1 tzkuat tzkuat 1367 févr.  7 21:37 test-java.crt
-rw------- 1 tzkuat tzkuat 1704 févr.  7 21:37 test-java.key
tzkuat@pop-os ~/test-java> grep BEGIN *
test-java.crt:-----BEGIN CERTIFICATE-----
test-java.key:-----BEGIN PRIVATE KEY-----
```

Une fois que vous avez ces fichiers, il faut passer par un format de fichier transitoire : le format PKCS12. De tout ce que j'ai vu/lu, il est obligatoire de convertir d'abord vos éléments en PKCS12 avant de pouvoir générer un Java Key Store.

Pour cela, il suffit d'utiliser la commande `openssl` suivante :
* `openssl pkcs12 -export -inkey test-java.key -in test-java.crt -out test-java.p12`

Un mot de passe vous sera demandé pour la génération du `p12`. C'est obligatoire dans ce format pour protéger votre clé privée.

Après avoir lancé la commande nous avons 3 fichiers :
* La clé privée de départ 
* Le certificat de départ
* Un fichier au format p12 contenant notre clé privée et notre certificat

En sachant qu'il est aussi possible d'ajouter notre autorité au `p12` en ajoutant une option `-certfile`:
* `openssl pkcs12 -export -inkey test-java.key -in test-java.crt -certfile <fichier-autorite> -out test-java.p12`

```
tzkuat@pop-os ~/test-java> openssl pkcs12 -export -inkey test-java.key -in test-java.crt -out test-java.p12
Enter Export Password:
Verifying - Enter Export Password:
tzkuat@pop-os ~/test-java> ls -l
total 12
-rw-rw-r-- 1 tzkuat tzkuat 1367 févr.  7 21:37 test-java.crt
-rw------- 1 tzkuat tzkuat 1704 févr.  7 21:37 test-java.key
-rw------- 1 tzkuat tzkuat 2707 févr.  7 21:46 test-java.p12
tzkuat@pop-os ~/test-java> 
```

Nous pouvons maintenant vérifier les informations présentes dans notre fichier `p12` avec la commande suivante :
* `openssl pkcs12 -info -in test-java.p12`

Et on peut voir qu'il y a bien un certificat et une clé privée :
```
tzkuat@pop-os ~/test-java> openssl pkcs12 -info -in test-java.p12
Enter Import Password:
MAC: sha256, Iteration 2048
MAC length: 32, salt length: 8
PKCS7 Encrypted data: PBES2, PBKDF2, AES-256-CBC, Iteration 2048, PRF hmacWithSHA256
Certificate bag
Bag Attributes
    localKeyID: 96 C8 EF 6B 83 D1 AE 37 1F EA 2F 76 5D E0 1A 96 5A FD 4C 6B 
subject=C = FR, ST = Corsica, L = Ajaccio, O = Net-Security, OU = Example, CN = Example JAVA
issuer=C = FR, ST = Corsica, L = Ajaccio, O = Net-Security, OU = Example, CN = Example JAVA
-----BEGIN CERTIFICATE-----
MIIDwzCCAqugAwIBAgIUF7ynsl7wEnmqqHsLm95LVcGlmA0wDQYJKoZIhvcNAQEL
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
fLB10ycHLw==
-----END CERTIFICATE-----
PKCS7 Data
Shrouded Keybag: PBES2, PBKDF2, AES-256-CBC, Iteration 2048, PRF hmacWithSHA256
Bag Attributes
    localKeyID: 96 C8 EF 6B 83 D1 AE 37 1F EA 2F 76 5D E0 1A 96 5A FD 4C 6B 
Key Attributes: <No Attributes>
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----BEGIN ENCRYPTED PRIVATE KEY-----
MIIFLTBXBgkqhkiG9w0BBQ0wSjApBgkqhkiG9w0BBQwwHAQIFHMMYxBi32QCAggA
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
/xU1IkuqlVzUxml7hW3Efuzs7TMMoPAY6lfPxNLWhr/u
-----END ENCRYPTED PRIVATE KEY-----
```

Nous pouvons maintenant générer notre Java KeyStore grâce à la commande `keytool` suivante. Nous voyons bien que le certificat `p12` est utilisé comme source afin de créer notre JKS.
* `keytool -importkeystore -destkeystore test-java.jks -srckeystore test-java.p12 -srcstoretype pkcs12`

Deux mots de passe vont être demandés : le 1er sera celui de votre Java KeyStore que vous créez et le second sera celui du `p12` que vous importez.

Après avoir lancé la commande nous avons finalement 4 fichiers :
* La clé privée de départ 
* Le certificat de départ
* Un fichier au format p12 contenant notre clé privée et notre certificat
* Notre Java KeyStore qui contient notre clé privée et notre certificat

```
tzkuat@pop-os ~/test-java> keytool -importkeystore -destkeystore test-java.jks -srckeystore test-java.p12 -srcstoretype pkcs12
Import du fichier de clés test-java.p12 vers test-java.jks...
Entrez le mot de passe du fichier de clés de destination :  
Ressaisissez le nouveau mot de passe : 
Entrez le mot de passe du fichier de clés source :  
L'entrée de l'alias 1 a été importée.
Commande d'import exécutée : 1 entrées importées, échec ou annulation de 0 entrées

Warning:
Le fichier de clés JKS utilise un format propriétaire. Il est recommandé de migrer vers PKCS12, qui est un format standard de l'industrie en utilisant "keytool -importkeystore -srckeystore test-java.jks -destkeystore test-java.jks -deststoretype pkcs12".
tzkuat@pop-os ~/test-java> ls -l
total 16
-rw-rw-r-- 1 tzkuat tzkuat 1367 févr.  7 21:37 test-java.crt
-rw-rw-r-- 1 tzkuat tzkuat 2313 févr.  7 21:52 test-java.jks
-rw------- 1 tzkuat tzkuat 1704 févr.  7 21:37 test-java.key
-rw------- 1 tzkuat tzkuat 2707 févr.  7 21:46 test-java.p12
tzkuat@pop-os ~/test-java> 
```

Nous avons donc (enfin?) notre JKS prêt à être utilisé !

Si vous avez une autorité à importer dans le trust-store également vous pouvez utiliser la commande suivante :
* `keytool -import -alias donnez_un_alias_a_votre_cert -keystore votretruststore.jks -file CA_ROOT.crt`

Et pour terminer, on peut afficher les informations de notre JKS avec cette commande :
* `keytool -list -rfc -keystore test-java.jks`

Et nous pouvons voir que nous avons bien une entrée de type `PrivateKeyEntry` :
```
zkuat@pop-os ~/test-java> keytool -list -rfc -keystore test-java.jks
Entrez le mot de passe du fichier de clés :  
Type de fichier de clés : jks
Fournisseur de fichier de clés : SUN

Votre fichier de clés d'accès contient 1 entrée

Nom d'alias : 1
Date de création : 7 févr. 2023
Type d'entrée : PrivateKeyEntry
Longueur de chaîne du certificat : 1
Certificat[1]:
-----BEGIN CERTIFICATE-----
MIIDwzCCAqugAwIBAgIUF7ynsl7wEnmqqHsLm95LVcGlmA0wDQYJKoZIhvcNAQEL
xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
fLB10ycHLw==
-----END CERTIFICATE-----

*******************************************
*******************************************
```

Et voilà, c'est la fin de cet article sur les certificats !

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat
