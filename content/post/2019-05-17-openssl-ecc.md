---
title: 'OpenSSL & ECC'
author: Mickael Rigonnaux
type: post
date: 2019-05-17T10:04:33+00:00
url: /security/openssl-ecc/
thumbnail: /images/OpenSSL-ECC/ecc_logo.png
featureImage: images/OpenSSL-ECC/ecc_logo.png
shareImage: /images/OpenSSL-ECC/ecc_logo.png
tags: [
    "GNU/Linux",
    "OpenSSL",
    "cryptographie",
    "chiffrement"
]
categories: [
    "securite",
    "logiciellibre"
]

---

Bonjour à tous ! Aujourd'hui nous allons aborder ensemble un sujet assez spécifique, les certificats avec le système de clés ECC (Elliptic Curve Cryptography). Il est notamment utilisé dans le cadre d'SSL/TLS avec les algorithmes [ECDSA][1] & [ECDH][2]. 

## Introduction

J'imagine que vous avez déjà tous manipulé des certificats générés avec le protocole RSA, c'est actuellement le plus répandu dans le monde SSL/TLS. Mais personnellement, je vois passer de plus en plus de demande concernant ECC.

Il faut aussi savoir que les algorithmes ECC et RSA sont fondamentalement différents : 

  * La sécurité de RSA dépend de la difficulté de factoriser un grand nombre entier.
  * ECDSA repose sur la difficulté de calculer le «logarithme» discret d’un grand nombre entier.

Si vous n'avez rien compris, c'est normal moi non plus au début. Si ça vous intéresse je vous invite à lire les pages Wikipédia des 2 systèmes, [ECC][3] & [RSA][4]. 

## Pourquoi les courbes elliptiques ?

La 1ère raison est que ces derniers temps les tailles des clés RSA sont en augmentation constante, 2048 et bientôt j'imagine 3072 bits (l'ANSSI recommande 3072 bits en 2030). L'utilisation des courbes elliptiques permet de réduire radicalement la taille des clés tout en gardant un niveau de sécurité similaire ou supérieur. Par exemple : 

Clés RSA (bits) : 1024 / 2048 / 3072 / 7680
Clés ECC (bits) : 163 / 224 / 256 / 384

On arrive donc à diviser par plus de 10 la taille des clés dans certains cas tout en gardant un niveau de sécurité fort. 

Pour information, le Bitcoin est lui même basé sur un système de courbes elliptiques, pour plus d'informations vous pouvez vous [rendre ici][5]. 

L'utilisation de ce système permet également de réduire la puissance de calcul et le temps nécessaire pour générer une clé. Et également de réduire le besoin en stockage, ce qui est très intéressant dans le monde de l'IOT ou l'Internet des Objets. 

L'utilisation de ce système rend aussi l'ensemble des interactions plus rapide et améliore donc les performances. 

Il reste cependant tout à fait possible d'utiliser des certificats ECC pour des utilisations classiques comme dans le cadre d'un serveur Web, si ce dernier est compatible. Les autorités publiques proposent également des certificats de ce type. 

### Les inconvénients 

Et oui, l'utilisation de cette méthode asymétrique n'a pas que des avantages. 

Elle est beaucoup plus complexe que le système RSA, et dépend fortement des courbes que nous utilisons. Plusieurs chercheurs sont en désaccord sur les courbes à utiliser, notamment celles proposées par la NSA. 

Certaines courbes peuvent inclure des faiblesses et donc compromettre l'ensemble du système. 

De plus, certaines actions très simples avec OpenSSL ne fonctionnent plus avec ce système, comme la comparaison des modulus pour vérifier l'appartenance d'un certificat/clé. 

Ces technologies ne sont pas supportées par l'ensemble des systèmes, contrairement à RSA qui lui est reconnu partout. Et pour finir certaines actions sont plus lentes avec ECC, notamment la vérification des signatures. 

## OpenSSL

Nous allons donc maintenant manipuler des certificats en ECC avec l'outil [OpenSSL][6]. Cet outil supporte cet algorithme depuis la version 0.9.8 sortie en 2005.

Dans mon cas j'utilise OpenSSL sur Ubuntu 18.04 dans sa version 1.1.0g.

Voyons d'abord comment lister la liste des courbes disponibles dans notre outil : 


```
openssl ecparam -list_curves
```

![](/images/OpenSSL-ECC/ecc_1.png) 

La liste est normalement plus longue, dans notre cas nous utiliserons la courbes « primes256v1 ». 

Génération de la clé privée : 


```
openssl ecparam -genkey -name prime256v1 -out net-sec.key
```

On voit bien dans notre clé que nous avons une partie paramètres qui correspond à la courbe utilisée et également que la clé est beaucoup plus courte et rapide à générer : 

![](/images/images/OpenSSL-ECC/ecc_2.png)

Génération de la CSR (Certificate Signing Request), c'est dans cette partie que nous renseignerons les champs de notre certificat (CN, O, OU, etc.) : 


```
openssl req -new -sha256 -key net-sec.key -nodes -out net-sec.csr
```

Vous avez également la possibilité d'ajouter un mot de passe sur la CSR.

![](/images/OpenSSL-ECC/ecc_3.png)

Vous pouvez maintenant transmettre votre CSR à l'organisme qui doit le signer. Ou vous pouvez le signer vous même si besoin, dans notre cas il sera donc auto-signé : 

```
openssl req -x509 -sha256 -days 365 -key net-sec.key -in net-sec.csr -out net-sec.crt
```

Il faut aussi noter que dans le cas de la transmission de la CSR à un tiers il faut fournir seulement le CSR, la clé n'est pas nécessaire pour la signature.

![](/images/OpenSSL-ECC/ecc_4.png) 

Une des problématiques d'ECC est qu'il n'est pas possible d'utiliser la méthode de vérification basée sur le modulo du fichier pour vérifier si un certificat est bien issu d'une CSR ou d'une clé privée.

![](/images/OpenSSL-ECC/ecc_5.png)

Pour vérifier ce point nous allons donc utiliser la clé publique des différentes parties : 

```
# Clé privée 
openssl ec -in net-sec.key -pubout

# CSR
openssl req -in net-sec.csr -pubkey -noout

# Certificat
openssl x509 -in net-sec.crt -pubkey -noout
```

Vous devriez avoir un résultat comme celui-ci, avec 3 fois la même clé si les 3 fichiers sont issus de la même clé privée : 

![](/images/OpenSSL-ECC/ecc_6.png)

Voici d'autres commandes qui pourrait vous intéresser : 

```
# Vérifier un fichier CSR 
openssl req -text -noout -verify -in CSR.csr

# Vérifier une clé privée
openssl ec -in privateKey.key -check

# Vérifier un certificat
openssl x509 -in certificate.crt -text -noout

# Ajouter une passphrase à une clé privée ECC
openssl ec -in www.exemple.com.key -des3 -out www.exemple.com.key
```

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://fr.wikipedia.org/wiki/Elliptic_curve_digital_signature_algorithm
 [2]: https://en.wikipedia.org/wiki/Elliptic-curve_Diffie%E2%80%93Hellman
 [3]: https://fr.wikipedia.org/wiki/Cryptographie_sur_les_courbes_elliptiques
 [4]: https://fr.wikipedia.org/wiki/Chiffrement_RSA
 [5]: http://e-ducat.fr/links/ecdsa/
 [6]: https://www.openssl.org/