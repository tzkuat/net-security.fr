---
title: "Installation et configuration d'Observium"
author: Fabio Pace
type: post
date: 2019-05-14T09:45:17+00:00
url: /system/installation-et-configuration-dobservium/
thumbnail: /images/Observium/observium0.png

---

Bonjour à tous, dans cet article nous allons voir comment installer Observium et comment le configurer.

<u>Tout d'abord, qu'est-ce que Observium ?</u>

[Observium][1] est un logiciel de supervision réseau permettant de monitorer son infrastructure en partant du serveur (ou client) avec sa consommation (CPU, Disques, etc.) jusqu'au débit de chaque des ports d'un switch. 

Observium utilise la remonté d'information via le protocole SNMP.

Pour la mise place d'Observium, nous utiliserons la distribution CentOS7 (serveur et client).

## Installation

### Le serveur

Tout d'abord, il faut ajouter les répos REMI, OpenNMS et EPEL pour avoir les bons paquets (avec la bonne version) : 

```
yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install http://yum.opennms.org/repofiles/opennms-repo-stable-rhel7.noarch.rpm
yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
```

Il faut installer les packages yum-utils : 


```
yum install yum-utils
```


Nous utiliserons la version PHP 7.2 pour le serveur web, donc il faut activer le repo de REMI : 


```
yum-config-manager --enable remi-php72
```


Puis il suffit de faire une mise à jour du système :


```
yum update
```


Nous pouvons désormais installer les paquets pour le bon fonctionnement dObservium (HTTPD, PHP, SNMP(pour la remonté d'information)&#8230;) 


```
yum install wget.x86_64 httpd.x86_64 php.x86_64 php-opcache.x86_64 php-mysql.x86_64 php-gd.x86_64 php-posix php-pear.noarch cronie.x86_64 net-snmp.x86_64 net-snmp-utils.x86_64 fping.x86_64 mariadb-server.x86_64 mariadb.x86_64 MySQL-python.x86_64 rrdtool.x86_64 subversion.x86_64  jwhois.x86_64 ipmitool.x86_64 graphviz.x86_64 ImageMagick.x86_64 php-sodium.x86_64
```

NB : Nous installons également rdd-tool car nous allons devoir l'utiliser pour créer des graphiques et sauvegarder les données critiques.

Puis nous devons créer le répertoire où sera contenu la configuration d'Observium et télécharger le package :

```
cd /opt
wget http://www.observium.org/observium-community-latest.tar.gz
tar zxvf observium-community-latest.tar.gz
```


Nous devons maintenant configurer la base de donnée : MariaDB

NB : Pour la première utilisation de MySQL, vous pouvez modifier le mot de passe root :

```
/usr/bin/mysqladmin -u root password '<mysql root password>'
```


Nous allons créer la base de donnée « observium » et ajouter l'utilisateur « observium » en tant administrateur de la BDD.

```
mysql -u root -p
<mysql root password>
CREATE DATABASE observium DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON observium.* TO 'observium'@'localhost' IDENTIFIED BY '<observium db password>';
exit;
```


NB : N'oubliez pas de modifier le mot de passe du compte utilisateur « observium » pour plus de sécurité.

Il faut activer le service mariadb pour qu'il se lance automatiquement au démarrage du serveur, et on le lance manuellement : 

```
systemctl enable mariadb
systemctl start mariadb
```

On initialise le schéma de la base de donnée avec le script fournis : 


```
./discovery.php -u
```


Nous allons récupérer l&#8217;emplacement de l'outil « fping » pour que l'application puisse l'utiliser : /usr/sbin/fping.


```
which fping
```


Au tour du fichier de configuration d'Observium à modifier, n'oubliez pas de créer un backup du fichier avant de le modifier : 

```
cp /opt/observium/config.php /opt/observium/config.php.old
vi /opt/observium/config.php
```


```
<?php
## Check http://www.observium.org/docs/config_options/ for documentation of possible settings
## It's recommended that settings are edited in the web interface at /settings/ on your observium installation.
## Authentication and Database settings must be hardcoded here because they need to work before you can reach the web-based configuration interface
 
// Database config ---  This MUST be configured
 $config['db_extension'] = 'mysqli';
 $config['db_host']      = 'localhost';
 $config['db_user']      = 'observium';
 $config['db_pass']      = 'password';
 $config['db_name']      = 'observium';
 // Base directory
 $config['install_dir'] = "/opt/observium";
 // Default community list to use when adding/discovering
 $config['snmp']['community'] = array("public");
 // Authentication Model
 $config['auth_mechanism'] = "mysql";    // default, other options: ldap, http-auth, please see documentation for config help
 $config['fping'] = "/usr/sbin/fping";

 $config['enable_billing']       = 1;
 $config['collectd_dir']         = "/var/lib/collectd/rrd/";
 // End config.php
```

NB : Veuillez modifier la communauté SNMP public pour augmenter le niveau de sécurité

* ```$config['snmp']['community'] = array("TaCommunautéPrivée"); ```

Par défaut sur CentOS (sur RedHat plus particulièrement), SELinux est activé. SELinux est un module qui permet de définir une politique d'accès obligatoire. Il va falloir donc dans notre cas le désactiver complètement pour éviter des problèmes au niveau du module. Nous verrons dans un prochain article comment fonctionne SELinux.


```
vi /etc/selinux/config
SELINUX=permissive
```

Il faut maintenant autoriser le port 80 (HTTP) pour que les clients puissent accéder à la page web du serveur (et 161 pour le SNMP) : 


```
firewall-cmd --add-port=80/tcp,161/udp --permanent
firewall-cmd --reload
```


Il faut maintenant créer le répertoire qui contiendra les données des différents équipements : 


```
mkdir -p /opt/observium/rrd
```


Et on donne à l'utilisateur et au groupe apache la propriété du dossier : 


```
chown apache:apache /opt/observium/rrd
```


Nous allons ajouter un virtualhost dans la configuration d'apache : 


```
vi /etc/httpd/conf.d/observium.conf
```


```
<VirtualHost *>
   DocumentRoot /opt/observium/html/
   #ServerName  observium.domain.com
   CustomLog /opt/observium/logs/access_log combined
   ErrorLog /opt/observium/logs/error_log
   <Directory "/opt/observium/html/">
      AllowOverride All
      Options FollowSymLinks MultiViews
      Require all granted
   </Directory>
</VirutalHost>
```

Il faut désormais créer le répertoire qui contiendra les logs de l'application (et attribuer apache en tant que propriétaire du dossier)


```
mkdir -p /opt/observium/logs
chown apache:apache /opt/observium/logs
```


Enfin, nous créons un utilisateur (administrateur) pour l'accès à l'interface graphique. Nous utilisons le « level » 10 pour lui donner les droits adminitrateur :


```
## ./opt/observium/adduser.php <username> <password> <level> ##
./opt/observium.adduser.php admin admin 10
```


Vous devez relancer le service httpd et l'activer au démarrage du serveur 


```
systemctl restart httpd
systemctl enable httpd
```


Vous pouvez désormais vous rendre sur la page d'observium via l'adresse IP du serveur : pour ma part ca sera http://observium.lab.local

![](/images/Observium/observium1.jpg)

### Le client

Tout d'abord, vous devez installer le paquet SNMP pour la communication avec le serveur :


```
yum -y install net-snmp net-snmp-utils
```


On sauvegarde le fichier de configuration d'origine de SNMP pour y placer notre configuration : 


```
mv /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.orig
vi /etc/snmp/snmpd.conf
    #sec.name source community
    com2sec notConfigUser default Lab2Snmp

    #groupName securityModel securityName
    group notConfigGroup v2c notConfigUser

    #name incl/excl subtree mask(optional)
    view systemview included .1.3.6.1.2.1.1
    view systemview included .1.3.6.1.2.1.25.1.1

    #group context sec.model sec.level prefix read write notif
    access notConfigGroup «  » any noauth exact systemview none none

    syslocation Unknown (edit /etc/snmp/snmpd.conf)
    syscontact Root (configure /etc/snmp/snmp.local.conf)

    #We do not want annoying « Connection from UDP:  » messages in syslog.
    #If the following option is commented out, snmpd will print each incoming connection, which can be useful for debugging.

    dontLogTCPWrappersConnects yes
```


NB :  L'option « v2c » permet de définir le SNMP Version 2.  
Lab2Snmp équivaut au nom de votre communauté privée. 

Puis on active le service au démarrage et on le lance : 


```
systemctl enable snmpd
systemctl start snmpd
```


On autorise le flux SNMP à circuler :


```
firewall-cmd --permanent --zone=public --add-service=snmp
firewall-cmd --reload
```


Pour tester si le SNMP est fonctionnel au niveau du client : 


```
snmpwalk -v 2c -c Lab2Snmp -O e 127.0.0.1
```


```
SNMPv2-MIB::sysDescr.0 = STRING: Linux client-centos 3.10.0-957.el7.x86_64 #1 SMP Thu Nov 8 23:39:32 UTC 2018 x86_64
SNMPv2-MIB::sysObjectID.0 = OID: NET-SNMP-MIB::netSnmpAgentOIDs.10 DISMAN-EVENT-MIB::sysUpTimeInstance = Timeticks: (51956375) 6 days, 0:19:23.75
SNMPv2-MIB::sysContact.0 = STRING: Root  (configure /etc/snmp/snmp.local.conf)
SNMPv2-MIB::sysName.0 = STRING: client-centos
SNMPv2-MIB::sysLocation.0 = STRING: Unknown (edit /etc/snmp/snmpd.conf)
...
```

Votre client est désormais opérationnel pour communiquer avec le serveur.

## Configuration

Rendez-vous sur le serveur Observium. Pour ajouter un hôte, il suffit de saisir la commande suivante : 


```
php /opt/observium/add_device.php <HostnameOrIPAdress> <Community> v2c
php /opt/observium/add_device.php client-centos Lab2Snmp v2c
```


Pour commencer à récuperer les informaitons concernant le nouvel hôte : 


```
php /opt/observium/discovery.php -h all
php /opt/observium/poller.php -h all
```


Enfin, il est possible d'automatiser la remontée d'information via le cron du serveur :


```
vi /etc/cron.d/observium
 Découverte de tous les hôtes tous les 6 heures (mise à jour du schéma de la bdd) #
33 */6 * * * root /opt/observium/discovery.php -h all >> /dev/null 2>&1
# Vérification des nouveaux hôtes tous les 5 minutes #
*/5 * * * * root /opt/observium/discovery.php -h new >> /dev/null 2>&1
# Tous les 5 minutes, demande au poller de récupérer les informations de l’hôte #
*/5 * * * * root /opt/observium/poller-wrapper.py >> /dev/null 2>&1
# Tous les jours, récupération du syslog, eventlog et log des hôtes #
2 1 * * * root /opt/observium/housekeeping.php -ysel
# Tous les jours, récupération des informations concernant rrd, ports… #
2 2 * * * root /opt/observium/housekeeping.php -yrptb
```


Vous devez relancer crond : 


```
systemctl reload crond
```


Vous pouvez désormais vous connecter sur l'interface d'observium et vérifier que votre hôte a bien été ajouté : 

![](/images/Observium/observium1.png)
Et après quelques jours de remontés de données, vous obtenez des résultats (par exemple la disponibilité de la machine) : 

![](/images/Observium/observium3.png)

Si vous souhaitez avoir plus d'informations, vous pouvez également ajouter Collectd sur le serveur : 


```
yum install collectd
```


Et configurer le fichier de collectd en décommentant les lignes suivantes : 


```
vi /etc/collectd.conf
```


![](/images/Observium/observium4.png)

NB :  Vous pouvez ajouter autant de module que vous souhaitez monitorer.

On ajoute l'entrée de collectd au firewall : 


```
firewall-cmd --add-port=25826/udp --permanent
firewall-cmd --reload
```


Et on active et lance le service :


```
systemctl enable collectd
systemctl start collectd
```


On configure également collectd au niveau client : 


```
yum install collectd
vi /etc/collectd.conf
```


![](/images/Observium/observium6.png)

NB : La balise server est à configurer en fonction de l'IP et du port du serveur Observium.

On ajoute collectd au firewall du client : 


```
firewall-cmd --add-port=25826/udp --permanent
firewall-cmd --reload
```


Et on active et démarre le service : 


```
systemctl enable collectd
systemctl start collectd
```


Après quelques minutes, le dossier /var/lib/collectd/rrd/ doit contenir un dossier par client avec à l'intérieur toutes les données des différents plugins que vous avez configuré : 

![](/images/Observium/observium7.png)

Et vous pouvez vérifier sur l'interface web, l'onglet « collectd » qui est apparu avec les différentes données :

![](/images/Observium/observium5.png)

Vous pouvez ajouter des machines clients, serveurs, des switchs, des routeurs...et les répertorier dans Observium. 

Pour pouvez également ajouter des alertes pour être informer d'une latence élevée d'une machine par exemple.

J'espère que l'article vous aura plu.

N'hésitez pas à commenter si vous avez la moindre remarque.

A bientôt, 

Fabio Pace

 [1]: https://docs.observium.org/
 [2]: https://net-security.fr/wp-content/uploads/Observium/observium1.jpg
 [3]: https://net-security.fr/wp-content/uploads/Observium/observium1.png
 [4]: https://net-security.fr/wp-content/uploads/Observium/observium2.png
 [5]: https://net-security.fr/wp-content/uploads/Observium/observium3.png