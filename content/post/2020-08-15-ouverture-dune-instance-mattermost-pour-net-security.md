---
title: "Ouverture d'une instance Mattermost pour Net-Security"
author: Mickael Rigonnaux
type: post
date: 2020-08-15T11:31:20+00:00
url: /logiciel-libre/ouverture-dune-instance-mattermost-pour-net-security/
thumbnail: /images/Logo_mattermost.png
featured: true
tags: [
    "logiciellibre",
    "mattermost"
]
categories: [
    "logiciellibre"
]

---
Bonjour à tous ! Aujourd'hui un article très court pour vous présenter une amélioration sur le blog net-security.fr.

**EDIT** : l'instance a été fermée en septembre 2023.

## L'idée

De mon côté je fais partie de plusieurs communautés ou groupes sur diverses applications (Slack, Discord, etc.). Les sujets peuvent être divers et variés comme l'OSINT avec la communauté très active OSINT-FR que je vous invite à rejoindre si ça vous intéresse : [lien ici.][1]

De notre côté nous voulions proposer une communauté sur le même principe mais axée autour de l'administration système, du réseau, de la sécurité et principalement des logiciels libres. Ce dernier sujet nous tient à cœur et nous essayons de le mettre en avant le plus possible à travers ce blog.

En gardant la même idée nous avons donc déployé une instance de [Mattermost][2] qui est un logiciel libre proposé sous licence MIT pour la partie serveur.

Le but principal de ce service est de pouvoir échanger et de partager sur les sujets qui font que le blog existe aujourd'hui. L'idée reste proche [du journal du hacker][3] mais intègre une plus grande interactivité entre les différents membres.

Pour terminer, notre but n'est pas de créer une communauté gigantesque avec plusieurs milliers de membres, mais une communauté de gens investis et passionnés par l'informatique et les logiciels libres.

## Informations

Mattermost a donc été déployé sur un VPS de chez [Scaleway][4]. Le serveur en question est hébergé en France et utilise Debian 10. Sur la partie applicative, Mattermost tourne sur Docker, je pourrais faire un article sur le déploiement si ça vous intéresse.

Nous avons tout de même écrit des règles pour le serveur, elles sont présentes principalement pour que ce service reste un endroit d'échange :

  1. Se présenter succinctement est recommandé dans le channel General avant de rejoindre les autres channels.
  2. Respecter les membres de la communauté, partager dans la convivialité et la bonne humeur.
  3. Le serveur est mis à disposition gracieusement et est hébergé chez Scaleway (PARIS), il est susceptible d'être indisponible.
  4. Merci de contacter un administrateur avant de créer un channel.
  5. Ne pas abuser des mentions @all ou @here.
  6. Ne pas modifier le nom des channels existants sans l'accord des administrateurs.
  7. Respecter au maximum les sujets des différents channels.

## Comment rejoindre ?

C'est très simple, il suffit de se rendre [sur l'URL de l'instance][5] et de s'inscrire ! 

Vous aurez simplement besoin d'une adresse mail (elle sert pour valider votre compte et pour réinitialiser votre mot de passe) et d'un pseudo ! Vous aurez ensuite accès à toutes les fonctionnalités du serveur.

## Conclusion

Pour conclure sur ce rapide article, n'hésitez pas à rejoindre, à faire vivre et à partager ce serveur !

Adresse de l'instance : <https://mattermost.net-security.fr>

N'hésitez pas à revenir vers moi par mail ou en commentaire si vous avez des questions,

Mickael Rigonnaux @tzkuat

 [1]: https://osint-fr.net/
 [2]: https://mattermost.com/
 [3]: https://www.journalduhacker.net/
 [4]: https://www.scaleway.com/fr/
 [5]: https://mattermost.net-security.fr