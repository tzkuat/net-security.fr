---
title: "Durcissement d'un système GNU/Linux : Gestion des utilisateurs &  des connexions"
author: Mickael Rigonnaux
type: post
date: 2022-10-09T23:09:00+00:00
url: /securite/durcissement-gnulinux-1
thumbnail: /images/tux-1.png
featureImage: images/tux-1.png
shareImage: /images/tux-1.png
featured: true
toc: true
tags: [
    "gnu",
    "linux",
    "hardenning",
    "durcissement"
]
categories: [
    "securite",
    "systeme"
]

---
Bonjour à tous ! Aujourd'hui nous allons voir ensemble comment durcir rapidement et simplement un système GNU/Linux ! Plusieurs autres articles s'ajouteront afin de compléter la liste des configurations à faire.

## Introduction

Lorsque l'on déploie des machines GNU/Linux en entreprise il est nécessaire d'avoir des modèles standardisé et idéalement "durcis". Cela permet d'avoir une sécurité renforcée sur tous les environnements (dev, recette, production). En entreprise il n'est pas possible de simplement faire suivant -> suivant dans l'installeur et de livrer la machine.

Pour cela, il est possible d'appliquer plusieurs standards, voici des exemples :
* ANSSI : https://www.ssi.gouv.fr/guide/recommandations-de-securite-relatives-a-un-systeme-gnulinux/
* CIS : https://www.cisecurity.org/cis-benchmarks/
* Red Hat : https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/pdf/security_hardening/red_hat_enterprise_linux-8-security_hardening-en-us.pdf

Le but ici n'est pas d'appliquer toutes les recommandations, mais simplement de décrire les plus "simples" à mettre en œuvre pour avoir un "bon" niveau de sécurité. Le but va être d'étoffer les configurations à travers les articles. Cela sera à vous de voir ce que vous souhaitez utiliser ou non ou de voir si c'est suffisant pour votre cas d'utilisation.

Dans mon cas les configurations s'appliquent sur des systèmes GNU/Linux Debian/Ubuntu. J'ai aussi pu l'appliquer sur des machines SLES mais je n'ai pas fait ces modifications sur des machines RHEL/Centos like.

Dans cette première partie nous allons voir :
* Le durcissement basique d'un serveur SSH
* Le filtrage des utilisateurs par IP via PAM
* La gestion des utilisateurs
* La gestion des mots de passe

Cette 1ère partie est concentrée sur l'authentification car c'est pour moi le point le plus important.

## Durcissement du serveur SSH

Sans rentrer dans les détails, pour un serveur SSH, les configurations les plus basiques à mettre en place sont :
* Empêcher l'utilisateur `root` de se connecter
* Empêcher les mots de passe vide
* Désactiver les connexions en SSH par mot de passe
* Mettre un timeout
* Limiter le nombre de tentatives de connexion

Ce qui donne dans la configuration `/etc/ssh/sshd_config` :
```
PasswordAuthentication no
PermitRootLogin no
PermitEmptyPasswords no
```

En plus de cela, je rajoute une timeout sur les connexions SSH :
```
ClientAliveInterval 1200
ClientAliveCountMax 3
```

Et pour finir, rajouter une limite pour les tentatives de connexion :
```
MaxAuthTries 3 (6 par défaut)
```

Il est aussi possible de créer un nouveau fichier pour surcharger la configuration SSH, par exemple `/etc/ssh/sshd_config.d/security.conf` qui donnerait :
```
PasswordAuthentication no
PermitRootLogin no
PermitEmptyPasswords no
ClientAliveInterval 1200
ClientAliveCountMax 3
MaxAuthTries 3
```

Après tout cela, il est nécessaire de relancer le service :
* `systemctl restart sshd`

## Gestion des utilisateurs et filtrage avec PAM

Selon votre utilisation, il peut être nécessaire de créer un groupe ou des utilisateurs spécifiques comme :
* Des administrateurs locaux (connexion via un bastion ?)
* Des comptes de services (ansible par exemple)
* Des simples comptes utilisateurs

Dans mon cas, je vais créer un utilisateur spécifique à ansible ainsi qu'un groupe d'utilisateur administrateur avec des droits sudo qui va s'appeler `admtest`.

```
groupadd admtest
adduser ansible
```

Une fois fait, j'adapte mon fichier `/etc/sudoers` en fonction :
```
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL

# Ajout de l'utilisateur Ansible en NOPASSWD
ansible ALL=(ALL) NOPASSWD:ALL

# Ajout du groupe admtest pour les comptes admin.xxx 
%admtest ALL=(ALL:ALL) ALL
```

Dans cette configuration nous voyons que nous avons :
* Le groupe sudo (avec demande de mot de passe)
* L'utilisateur ansible autorisé à faire du sudo sans mot de passe
* Le groupe admtest autorisé à faire du sudo (avec demande de mot de passe)

Afin de bloquer le fichier en écriture j'utilise la commande `chattr` déjà évoqué dans [un article](https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-1/), elle permet de bloquer un fichier en rajoutant un attribut, même pour un super-utilisateur.

* `sudo chattr +i /etc/sudoers`

### Filtrage par IP source avec PAM

Nous avons donc plusieurs groupes / utilisateurs capables de se connecter avec des droits administrateurs sur notre machine. Afin de limiter ces connexions, il est possible via un module PAM, de filtrer par IP source ou réseau les utilisateurs.

Par exemple, il est possible de dire que les connexions des utilisateurs du groupe admtest sont limités à l'IP d'un bastion, ou que les connexions Ansible ne peuvent se faire que via les serveurs dédiés.

Pour ce faire il est nécessaire de modifier le fichier `/etc/pam.d/sshd` pour décommenter la ligne suivante :
```
# Uncomment and edit /etc/security/access.conf if you need to set complex
# access limits that are hard to express in sshd_config.
account  required     pam_access.so
```

Comme indiqué dans le fichier, la suite de la configuration se fait dans le fichier `/etc/security/access.conf`, c'est dans ce fichier que vous allez pouvoir écrire les règles d'accès.

Voici un exemple de fichier avec nos groupes et utilisateurs :
```
## Accès ansible

+:ansible:192.168.1.150
-:ansible:ALL

## Accès groupe admtest

+:@admtest:192.168.2.100
-:@admtest:ALL

## Accès supervision

+:nagios:192.168.1.100 192.168.4.200
-:nagios:ALL

## Desactivation root
-:root:ALL
```

Cela fonctionne avec des `+` et des `-`. Toute la documentation est disponible ici :
* https://linux.die.net/man/5/access.conf

Dans ce cas nous voyons que : 
* `ansible` peut se connecter seulement depuis l'IP `192.168.1.150`
* Les utilisateurs du groupe `admtest` peuvent se connecter depuis l'IP `192.168.2.200` qui peut être un bastion par exemple
* Les utilisateurs de supervision peuvent se connecter depuis les IPs `192.168.1.100` & `192.168.4.200`
* L'utilisateur `root` ne peut pas se connecter (doublons avec la conf SSH)
* Même si ce n'est pas écrit explicitement, tous les autres utilisateurs (hors des `-`) sont autorisés à se connecter

Ce n'est qu'un exemple, il est aussi possible d'être plus restrictif :
Voici un exemple de fichier avec nos groupes et utilisateurs :
```
## Accès ansible

+:ansible:192.168.1.150

## Accès groupe admtest

+:@admtest:192.168.2.100

## Accès supervision

+:nagios:192.168.1.100 192.168.4.200

## Desactivation de tous les autres utilisateurs
-:ALL:ALL
```

Dans ce cas, seuls les utilisateurs définit dans ce fichier pourront se connecter.

A vous maintenant d'adapter ce fichier à vos besoins. En sachant qu'il est aussi possible d'autoriser des réseaux, etc.

Il est aussi possible de bloquer ce fichier en écriture avec la commande suivante :
* `sudo chattr +i /etc/security/access.conf`

### Gestion des mots de passe

Si des utilisateurs doivent accéder à la machine, il est important de mettre en place une politique rattachée. Même si la recommandation est d'utiliser des clés SSH ce n'est pas toujours possible (il faudra adapter la configuration du serveur SSH en fonction).

Pour cela, il existe un autre module PAM qui nécessite l'installation du paquet `libpam-cracklib`.

Après l'installation il suffit de  modifier le fichier `/etc/pam.d/common-password` et d'ajouter les éléments suivants sur cette ligne :
```
password requisite pam_cracklib.so retry=3 minlen=15 difok=3 ocredit=1 lcredit=1 ucredit=1 dcredit=1
```

En sachant que :
* `minlen` : taille minimum du mot de passe
* `dcredit` : nombre de chiffres minimum
* `ucredit` : nombre de majuscules minimum
* `lcredit` : nombre de minuscules minimum
* `ocredit` : nombre de caractères spéciaux minimum

La politique est donc la suivante :
* 15 caractères minimum 
* 1 chiffre
* 1 majuscule
* 1 minuscule
* 1 caractère spécial

La documentation est disponible ici :
* https://linux.die.net/man/8/pam_cracklib

#### Empêcher la réutilisation des mots de passe

Il faut vérifier la présence du fichier `/etc/security/opasswd` et ajouter la configuration `remember=x` sur la ligne du fichier `/etc/pam.d/common-password` (ou X est l'historique à stocker) :
* `password[success=1 default=ignore]      pam_unix.so obscure use_authtok try_first_pass yescrypt remember=5`

Dans ce cas nous avons donc un historique de 5 mots de passe par utilisateur.

## Conclusion

Nous avons donc sur notre machine :
* Durcis un minimum le service SSH
* Limiter les connexions des utilisateurs par IP source
* Mis en place une politique pour les mots de passe

Encore une fois, le but ici n'est pas d'être exhaustif mais de montrer que l'on peut durcir un système facilement avec quelques configurations natives dans les distributions. D'autres articles viendront compléter celui-ci, avec notamment la possibilité d'automatiser ce durcissement et de le vérifier via Ansible.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !