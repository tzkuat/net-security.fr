---
title: "PrivateBin : un pastebin chiffré & libre"
author: "Mickael Rigonnaux"
type: post
date: 2022-09-14T13:45:44+00:00
url: /securite/privatebin
thumbnail: /images/privatebin_logo_1.png
shareImage: /images/privatebin_logo_1.png
featured: true
toc: false
tags: [
    "pastebin",
    "privatebin",
    "chiffrement",
    "cryptographie",
    "logiciellibre"
]
categories: [
    "securite",
    "logiciellibre"
]

---

![](/images/banner-privatebin.png)


Bonjour à tous ! Aujourd'hui un article sur un outil que j'ai découvert il y a plusieurs années et que j'utilise maintenant quotidiennement que ça soit en pro ou en perso : [PrivateBin](https://privatebin.info).

## Introduction

PrivateBin est un 'pastebin' libre et chiffré qui permet d'échanger à la fois du texte (texte, code source, markdown) ainsi que des fichiers si vous l'autorisez dans la configuration. L'outil est un fork de ZeroBin et est développé initialement par [SebSauvage](https://sebsauvage.net/) et repris par la communauté, il est développé en PHP et est proposé sous [licence ZLib](https://opensource.org/licenses/Zlib).

Voici le lien du repo Github :
* https://github.com/PrivateBin/PrivateBin

Le but est de pouvoir échanger des données sans que le serveur hébergeant l'application ne puisse avoir connaissance de ces données. Toutes les données sont chiffrées avec le protocole AES256 et sont déchiffrées dans le navigateur, elles ne sont jamais stockées en "clair" sur le serveur. La dernière version lorsque j'écris ses lignes est

Si vous voulez plus d'informations, vous pouvez vous rendre sur le [site officiel](https://privatebin.info) du logiciel.

Une liste d'instances de PrivateBin est également accessible publiquement [ici](https://privatebin.info/directory/).

## Les fonctionnalités / le fonctionnement

Pour l'utiliser, il suffit de taper son texte (ou déposer son fichier), sélectionner le format si besoin et de choisir les options voulues, puis d'envoyer le paste grâce au bouton dédié. Et c'est tout !

Un lien sera alors généré et vous pouvez l'envoyer à la personne cible.

Parmi les fonctionnalités, vous pouvez :
* Limiter la durée de validité du lien
* Préciser si ce lien expire après lecture
* Mettre un mot de passe pour accéder à ce lien
* Envoyer un fichier
* Autoriser la discussion

Il existe un grand nombre d'option sur l'outil, elles sont toujours paramétrables via un fichier de configuration dédié.

Voici un exemple de l'interface de PrivateBin :
![](/images/privatebin-1.png)

## Mes utilisations

J'imagine que je ne suis pas le seul à devoir fournir à des clients des informations comme :
* Des IPs de machines
* Des identifiants (VPN, accès machines)
* Des configurations
* [...]

De mon côté je trouve que PrivateBin s'y prête très bien. Généralement j'applique la configuration suivante :
* Mot de passe (ou phrase de passe pour que ça soit plus facile à taper)
* Effacé après lecture
* Envoi du mot de passe par SMS

Et le tour est joué, les données ne sont pas stockées et le client/l'autre partie peut récupérer les informations.

De plus c'est très rapide pour envoyer rapidement une conf dans un format cohérent, jamais vraiment pratique dans un mail ou autre.

## Installation & configuration

Pour l'installation, toutes les informations sont disponibles sur le site officiel, ça ne sert à rien de le recopier : 
* https://github.com/PrivateBin/PrivateBin/blob/master/INSTALL.md

Il en est de même pour la configuration :
* https://github.com/PrivateBin/PrivateBin/wiki/Configuration

De mon côté, j'utilise l'image docker proposé par PrivateBin, elle est disponible ici :
* https://hub.docker.com/r/privatebin/nginx-fpm-alpine

J'utilise personnellement l'image en version `1.4.0`. Cette dernière est fournie avec nginx & php en version 8.

Afin d'autoriser l'upload des fichiers il faut d'abord créer un fichier de configuration, qui sera monté dans le conteneur via un volume. Avant de lancer le conteneur j'ai créé le fichier `conf.php` dans l'arborescence `/data/privatebin/privatebin-cfg/` avec la configuration suivante :
```
[model]
class = Filesystem
[model_options]
dir = PATH "data"
[main]
fileupload = true
sizelimit = 10485760
name = "XXX PrivateBin"
sizelimit = 52428800
```

La configuration est très simple, le but est simplement d'autoriser l'upload de fichier et de mettre en place une limite (50Mo ici).

Pour lancer le conteneur, j'utilise après la commande suivante :
* `docker run -d --restart="always" --read-only -p 8080:8080 -v /data/privatebin/privatebin-cfg/conf.php:/srv/cfg/conf.php:ro -v /data/privatebin/privatebin-data:/srv/data privatebin/nginx-fpm-alpine:1.4.0`

Pour mon utilisation, c'est suffisant, en utilisant un reverse proxy pour exposer le service. Il est également possible de l'installer directement sur un serveur Web ou encore de le déployer par Ansible.

## Conclusion

L'article était rapide, mais je pense que c'est important de mettre en avant ce genre d'outil, qui peut aider dans la vie de tous les jours. Personnellement, je m'en sers tous les jours, j'espère que ça sera pareil pour vous !

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

Sources :
* https://linuxfr.org/news/privatebin-securise-vos-partages-de-texte-en-version-1-1
* https://privatebin.info/
* https://github.com/PrivateBin/PrivateBin/wiki