---
title: ESET Endpoint Encryption
author: Fabio Pace
type: post
date: 2019-10-17T13:00:41+00:00
url: /security/eset-endpoint-encryption/
thumbnail: /images/Eset-Encryption/encryption_eset_0-1.png

---
![](/)
  <figure class="aligncenter size-large is-resized"><img loading="lazy" src="images/Eset-Encryption/encryption_eset_0-1.png" alt="" class="wp-image-1397" width="588" height="370" srcset="images/Eset-Encryption/encryption_eset_0-1.png 500w, images/Eset-Encryption/encryption_eset_0-1-300x189.png 300w" sizes="(max-width: 588px) 100vw, 588px" /></figure>
</div>

Bonjour à tous, après plusieurs mois d'absence, nous allons voir dans cet article l'installation et le fonctionement d'un produit que j'utilise dans le cadre professionnel : ESET Endpoint Encryption.

## Introduction

L’utilitaire ESET Endpoint Encryption permet de chiffrer un (ou plusieurs) disque(s). 

Vous pouvez donc chiffrer&nbsp;: 

  * Une partition (ce que nous allons faire)
  * Un disque entier (non recommandé pour les dualboot)
  * Une clé USB
  * Un CD / DVD
  * Un dossier
  * Un texte
  * Un mail…

## Préparation

Tout d’abord, il faut récupérer l’utilitaire sur la page officiel d’ESET&nbsp;: 

<https://www.eset.com/fr/business/endpoint-security/encryption/dowload/#client>

Attention, pour un particulier il faudra suivre le lien suivant&nbsp;: <https://www.eset.com/fr/home/products/smart-security-premium/> &nbsp;l’utilitaire est inclut dans le pack antivirus Endpoint Protection.

Vous suivez l’installation de l’utilitaire, puis vous devez redémarrer votre PC pour terminer l’installation.

## Configuration

Au redémarrage, la fenêtre pour configurer l’utilitaire apparaît, il suffit de faire «&nbsp;_Installer_&nbsp;»&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="581" height="320" src="images/Eset-Encryption/encryption_eset_1.png" alt="" class="wp-image-1273" srcset="images/Eset-Encryption/encryption_eset_1.png 581w, images/Eset-Encryption/encryption_eset_1-300x165.png 300w" sizes="(max-width: 581px) 100vw, 581px" /></figure>
</div>

Puis, vous devez saisir votre clé de licence&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="499" height="388" src="images/Eset-Encryption/encryption_eset_2.png" alt="" class="wp-image-1274" srcset="images/Eset-Encryption/encryption_eset_2.png 499w, images/Eset-Encryption/encryption_eset_2-300x233.png 300w" sizes="(max-width: 499px) 100vw, 499px" /></figure>
</div>

Un bref descriptif des fonctionnalités comprises dans votre licence apparaît&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="499" height="388" src="images/Eset-Encryption/encryption_eset_3.png" alt="" class="wp-image-1275" srcset="images/Eset-Encryption/encryption_eset_3.png 499w, images/Eset-Encryption/encryption_eset_3-300x233.png 300w" sizes="(max-width: 499px) 100vw, 499px" /></figure>
</div>

  * _Core_&nbsp;: correspond à l’utilitaire pour manager 
  * _Removable_&nbsp;: permet de chiffrer les clés USB et CD/DVD
  * _Full_ _Disk_&nbsp;: permet de chiffrer les disques internes (ou partitions)

Puis, vous devez saisir un mot de passe qui permet d’accéder au fichier de clé. Le fichier de clé contient les éléments de configuration du logiciels (licences, fonctionnalités activées…)&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="499" height="388" src="images/Eset-Encryption/encryption_eset_4.png" alt="" class="wp-image-1276" srcset="images/Eset-Encryption/encryption_eset_4.png 499w, images/Eset-Encryption/encryption_eset_4-300x233.png 300w" sizes="(max-width: 499px) 100vw, 499px" /></figure>
</div>

A la suite de configuration, ESET vous propose de créer une clé de chiffrement qui permettra de chiffrer le contenu. Nous choisissons de générer une clé de chiffrement qui sera utilisé par défaut, et l’algorithme choisi est AES. Nous devons lui donner un nom pour la reconnaitre au cas où nous souhaitons générer plusieurs clés. Vous pouvez également choisir de créer la clé de chiffrement plus 

**NB&nbsp;:** _Si vous choisissez AES, ESET vous crée une clé de chiffrement de 128 bits. Si vous la créer plus tard, vous pouvez choisir la longueur de clé._ _Une longueur de 128 bits suffit amplement, aucun processus d’aujourd’hui ne permet de «&nbsp;casser&nbsp;» une clé de 128 bits. Cela permettra également de gagner en performance, à la place d’utiliser une clé de 256 bits._

![](/)
  <figure class="aligncenter"><img loading="lazy" width="499" height="388" src="images/Eset-Encryption/encryption_eset_5.png" alt="" class="wp-image-1277" srcset="images/Eset-Encryption/encryption_eset_5.png 499w, images/Eset-Encryption/encryption_eset_5-300x233.png 300w" sizes="(max-width: 499px) 100vw, 499px" /></figure>
</div>

Vous devez avoir un message vous indiquant que la configuration a été effectuée avec succès&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="499" height="388" src="images/Eset-Encryption/encryption_eset_6.png" alt="" class="wp-image-1278" srcset="images/Eset-Encryption/encryption_eset_6.png 499w, images/Eset-Encryption/encryption_eset_6-300x233.png 300w" sizes="(max-width: 499px) 100vw, 499px" /></figure>
</div>

ESET vous propose de sauvegarder votre fichier de clé. Je vous conseil d'enregistrer votre fichier sur un serveur de fichier ou dans un endroit sécurisé :

![](/)
  <figure class="aligncenter"><img loading="lazy" width="503" height="342" src="images/Eset-Encryption/encryption_eset_7.png" alt="" class="wp-image-1279" srcset="images/Eset-Encryption/encryption_eset_7.png 503w, images/Eset-Encryption/encryption_eset_7-300x204.png 300w" sizes="(max-width: 503px) 100vw, 503px" /></figure>
</div>

Lorsque la configuration de base est terminée, vous devez avoir le logo de l’utilitaire dans votre barre de tache en bas à droite&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="274" height="346" src="images/Eset-Encryption/encryption_eset_8.png" alt="" class="wp-image-1280" srcset="images/Eset-Encryption/encryption_eset_8.png 274w, images/Eset-Encryption/encryption_eset_8-238x300.png 238w" sizes="(max-width: 274px) 100vw, 274px" /></figure>
</div>

Vous pouvez vérifier et gérer vos clés de chiffrement dans la partie «&nbsp;_Gestionnaire de clés_&nbsp;»&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="810" height="420" src="images/Eset-Encryption/encryption_eset_9.png" alt="" class="wp-image-1281" srcset="images/Eset-Encryption/encryption_eset_9.png 810w, images/Eset-Encryption/encryption_eset_9-300x156.png 300w, images/Eset-Encryption/encryption_eset_9-768x398.png 768w" sizes="(max-width: 810px) 100vw, 810px" /></figure>
</div>

## Chiffrement du disque

Lorsque vous choisissez l’option «&nbsp;_Chiffrement de disque complet_&nbsp;», vous devez choisir «&nbsp;_Gérer les disques_&nbsp;»&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="512" height="346" src="images/Eset-Encryption/encryption_eset_10.png" alt="" class="wp-image-1282" srcset="images/Eset-Encryption/encryption_eset_10.png 512w, images/Eset-Encryption/encryption_eset_10-300x203.png 300w" sizes="(max-width: 512px) 100vw, 512px" /></figure>
</div>

Un message va vous proposer de redémarrer votre poste pour effectuer une analyse du disque avant de procéder au chiffrement (et pour éviter ainsi un problème de boot).

Si le PC redémarre correctement sans message d’erreur, une fenêtre va apparaitre vous invitant si vous souhaitez chiffrer tout le disque ou une partition. Dans notre cas nous choisirons la partition contenant Windows car j’ai un dualboot linux et ESET ne gère pas le multiboot (ni les partitions EXT).

![](/)
  <figure class="aligncenter"><img loading="lazy" width="533" height="399" src="images/Eset-Encryption/encryption_eset_11.png" alt="" class="wp-image-1283" srcset="images/Eset-Encryption/encryption_eset_11.png 533w, images/Eset-Encryption/encryption_eset_11-300x225.png 300w" sizes="(max-width: 533px) 100vw, 533px" /></figure>
</div>

Vous devez confirmer le fait que vous allez chiffrer votre disque&nbsp;:

![](/)
  <figure class="aligncenter"><img loading="lazy" width="533" height="399" src="images/Eset-Encryption/encryption_eset_12.png" alt="" class="wp-image-1284" srcset="images/Eset-Encryption/encryption_eset_12.png 533w, images/Eset-Encryption/encryption_eset_12-300x225.png 300w" sizes="(max-width: 533px) 100vw, 533px" /></figure>
</div>

Un utilisateur «&nbsp;_admin_&nbsp;» va être créer par l’utilitaire afin de pouvoir accéder au PC chiffrer&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="533" height="399" src="images/Eset-Encryption/encryption_eset_13.png" alt="" class="wp-image-1285" srcset="images/Eset-Encryption/encryption_eset_13.png 533w, images/Eset-Encryption/encryption_eset_13-300x225.png 300w" sizes="(max-width: 533px) 100vw, 533px" /></figure>
</div>

Veuillez enregistrer le fichier dans votre répertoire que vous avez créé précédemment.

C’est ce compte que vous devez utiliser au démarrage de votre PC avant de vous authentifier comme d’habitude.

Vous pouvez créer jusqu’à 125 utilisateurs pour accéder au PC, dans notre cas nous allons créer un utilisateur avec un mot de passe plus simple à retenir pour accéder au PC (cela peut être le même que celui du domaine ou de l'utilisateur local)

**NB&nbsp;:**_Un problème avec le «&nbsp;**@**&nbsp;» lors de l’ouverture de session. Ce symbole ne s’affiche pas, veuillez donc choisir un mot de passe sans ce symbole. Vous pouvez utiliser les symboles suivants&nbsp;: «&nbsp;**$, £, %…&nbsp;**»_

![](/)
  <figure class="aligncenter"><img loading="lazy" width="533" height="399" src="images/Eset-Encryption/encryption_eset_14.png" alt="" class="wp-image-1286" srcset="images/Eset-Encryption/encryption_eset_14.png 533w, images/Eset-Encryption/encryption_eset_14-300x225.png 300w" sizes="(max-width: 533px) 100vw, 533px" /></figure>
</div>

Veuillez vérifier les informations et confirmer via la coche en bas de l’utilitaire&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="533" height="399" src="images/Eset-Encryption/encryption_eset_15.png" alt="" class="wp-image-1287" srcset="images/Eset-Encryption/encryption_eset_15.png 533w, images/Eset-Encryption/encryption_eset_15-300x225.png 300w" sizes="(max-width: 533px) 100vw, 533px" /></figure>
</div>

L’utilitaire va commencer à chiffrer le disque en arrière-plan, il se peut que l’utilisation du PC soit ralentie&nbsp;:

![](/)
  <figure class="aligncenter"><img loading="lazy" width="557" height="199" src="images/Eset-Encryption/encryption_eset_16.png" alt="" class="wp-image-1288" srcset="images/Eset-Encryption/encryption_eset_16.png 557w, images/Eset-Encryption/encryption_eset_16-300x107.png 300w" sizes="(max-width: 557px) 100vw, 557px" /></figure>
</div>

## Impact de performance lié au chiffrement

Le disque doit normalement être sollicité et légèrement le processeur (c’est lui qui chiffre les données)&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="666" height="593" src="images/Eset-Encryption/encryption_eset_17.png" alt="" class="wp-image-1289" srcset="images/Eset-Encryption/encryption_eset_17.png 666w, images/Eset-Encryption/encryption_eset_17-300x267.png 300w" sizes="(max-width: 666px) 100vw, 666px" /></figure>
</div>

Si vous vérifiez les processus, c’est le processus «&nbsp;System&nbsp;» qui va chiffrer les données&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="664" height="541" src="images/Eset-Encryption/encryption_eset_18.png" alt="" class="wp-image-1290" srcset="images/Eset-Encryption/encryption_eset_18.png 664w, images/Eset-Encryption/encryption_eset_18-300x244.png 300w" sizes="(max-width: 664px) 100vw, 664px" /></figure>
</div>

Dans le moniteur de ressources, on peut voir les performances de votre disque sollicité par le chiffrement&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="1024" height="922" src="images/Eset-Encryption/encryption_eset_19-1024x922.png" alt="" class="wp-image-1299" srcset="images/Eset-Encryption/encryption_eset_19-1024x922.png 1024w, images/Eset-Encryption/encryption_eset_19-300x270.png 300w, images/Eset-Encryption/encryption_eset_19-768x692.png 768w, images/Eset-Encryption/encryption_eset_19.png 1155w" sizes="(max-width: 1024px) 100vw, 1024px" /></figure>
</div>

A la fin du chiffrement, vous avez un message indiquant que le disque a été chiffré.

Vous ne devez plus avoir de sollicitation niveau disque et processeur&nbsp;: 

![](/)
  <figure class="aligncenter"><img loading="lazy" width="666" height="593" src="images/Eset-Encryption/encryption_eset_20.png" alt="" class="wp-image-1292" srcset="images/Eset-Encryption/encryption_eset_20.png 666w, images/Eset-Encryption/encryption_eset_20-300x267.png 300w" sizes="(max-width: 666px) 100vw, 666px" /></figure>
</div>

J’espère que l’article vous aura plu. N’hésitez pas à commenter si vous avez la moindre remarque.

A bientôt, 

Fabio Pace.