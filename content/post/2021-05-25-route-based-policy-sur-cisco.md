---
title: Route Based Policy sur Cisco
author: Mickael Rigonnaux
type: post
date: 2021-05-25T11:47:51+00:00
url: /reseau/route-based-policy-sur-cisco/
thumbnail: /images/Untitled-2021-05-18-0921-1.png
featured: true
tags: [
    "cisco",
    "routage"
]
categories: [
    "reseau"
]
---
## Introduction

Dans cet article nous allons aborder une notion de réseau, les `route based policy`, c'est-à-dire du routage en fonction d'un réseau source. De mon côté j'ai utilisé cette fonctionnalité au cours d'une migration, cela est très utile lorsque vous avez plusieurs réseaux/interfaces pour sortir. Cela permet de dire spécifiquement à un réseau d'utiliser une passerelle ou `next-hop` particulier.

## Schéma

Vu qu'un schéma vaut mieux que 1000 explications : 

![Route Based Policy](/images/route-based-policy-1.png)


Dans ce schéma, vu que la route par défaut pointe vers `Router-1`, les réseaux 10 & 20 vont passer par ce routeur pour sortir sur Internet. Le but de la configuration dans notre exemple est de faire sortir le réseau 20 par le `Router-2` sans modifier la route par défaut.

## Explication

Dans mon exemple j'ai utilisé un Cisco 3750G avec la version `15.0(2)SE5`. Je vais passer la configuration par défaut ainsi que le routage, tout cela doit déjà être fait avant de faire les configurations qui vont suivre. Pour information le réseau 10 correspond au VLAN 10 et le réseau 20 au VLAN 20.

Dans un premier temps j'ai dû vérifier et changer le profil SDM de mon switch de niveau 3. Si vous réalisez l'opération sur un routeur, vous n'aurez probablement pas besoin de changer ce mode. Le mode SDM ou Switching Database Manager est un mode qui permet de changer les fonctionnalités de l'équipement par rapport à son emplacement ainsi qu'à sa fonction, car un le but est qu'un switch puisse être utilisé de plusieurs manières. 

Par exemple sur les switchs Cisco comme les 3750, il permet de changer de modifier les fonctionnalités disponibles, il en existe 4 sur mon modèle. Le mode `vlan` , qui va permettre seulement la commutation des paquets, c'est à dire transformer mon switch de niveau 3 en niveau 2. Le mode `default,` qui est activé par défaut sur vos équipements et qui permet les fonctionnalités classiques d'un switch de niveau 3 (le mode `access` est très proche du `default`, c'est pour cela qu'il est ignoré), pour finir le mode `routing` qui lui permet d'utiliser des fonctionnalités avancées au niveau du routage.

Dans notre cas nous allons utiliser le mode `routing`.

Sources : <https://www.fingerinthenet.com/cisco-sdm/>

## Configuration

Tout d'abord, vous pouvez vérifier le mode de votre équipement avec la commande `show sdm prefer`, voici le résultat de mon côté avant le changement, le switch est en mode `default` :

```
ROUTER-3# show sdm prefer 
 The current template is "desktop default" template.
 The selected template optimizes the resources in
 the switch to support this level of features for
 8 routed interfaces and 1024 VLANs. 

  number of unicast mac addresses:                  6K
  number of IPv4 IGMP groups + multicast routes:    1K
  number of IPv4 unicast routes:                    8K
    number of directly-connected IPv4 hosts:        6K
    number of indirect IPv4 routes:                 2K
  number of IPv6 multicast groups:                  0
  number of directly-connected IPv6 addresses:      0
  number of indirect IPv6 unicast routes:           0
  number of IPv4 policy based routing aces:         0
  number of IPv4/MAC qos aces:                      0.5K
  number of IPv4/MAC security aces:                 1K
  number of IPv6 policy based routing aces:         0
  number of IPv6 qos aces:                          20
  number of IPv6 security aces:                     25
  ```

Attention, le changement de ce mode va également modifier beaucoup de choses sur votre équipement ! Comme le nombre d'adresses MAC utilisable ou encore le nombre d'hôte directement connecté. De mon côté j'ai réalisé des études avant de faire de changement. Je précise également qu'il faudra `reload` l'équipement, et que de mon côté je n'ai eu aucune perte de configuration.

Il faut maintenant passer en mode configuration sur l'équipement et lancer le changement :

```
configure terminal
sdm prefer routing
exit
reload
```

Après le `reload`, vérifiez que vous avez bien accès à votre équipement et relancez la commande `show sdm prefer`, le résultat devrait être différent de la première fois :

```
ROUTER-3# show sdm prefer  
 The current template is "desktop routing" template.
 The selected template optimizes the resources in
 the switch to support this level of features for
 8 routed interfaces and 1024 VLANs. 

  number of unicast mac addresses:                  3K
  number of IPv4 IGMP groups + multicast routes:    1K
  number of IPv4 unicast routes:                    11K
    number of directly-connected IPv4 hosts:        3K
    number of indirect IPv4 routes:                 8K
  number of IPv6 multicast groups:                  0
  number of directly-connected IPv6 addresses:      0
  number of indirect IPv6 unicast routes:           0
  number of IPv4 policy based routing aces:         0.5K
  number of IPv4/MAC qos aces:                      0.5K
  number of IPv4/MAC security aces:                 1K
  number of IPv6 policy based routing aces:         0
  number of IPv6 qos aces:                          20
  number of IPv6 security aces:                     25
```

On voit bien maintenant que nous pouvons réaliser des `policy based routing`.

Vu que cette politique est basée sur une ACL, on va commencer par la créer :

```
configure terminal
ip access-list extended ACL_TEST
permit ip 192.168.20.0 0.0.0.255 any
```

Maintenant il faut vérifier que l'ACL est bien présente :

```
do sh access-list
    Extended IP access list ACL_TEST
    10 permit ip 192.168.20.0 0.0.0.255
```

On va maintenant créer la politique et attribuer le prochain saut :

```
route-map TEST permit 10
match ip address ACL_TEST
set ip next-hop 10.2.0.2 (IP du Routeur-2)
```

Il suffit maintenant d'attribuer cette politique à notre VLAN 20, et c'est la fin de la configuration :

```
interface vlan 20
ip policy route-map TEST
```

On peut maintenant sauvegarder la configuration avec commande `copy running-config startup-config` (en dehors du monde configuration).

## Les tests

On peut maintenant tester l'accès depuis le réseau 10 avec un `traceroute` :

```
  1     1 ms     1 ms     1 ms  192.168.10.254
  2    <1 ms    <1 ms    <1 ms  10.1.0.2
  3    14 ms    <1 ms    <1 ms  X.X.X.X
```

Et par le réseau 20 : 

```
  1     1 ms     1 ms     1 ms  192.168.20.254
  2    <1 ms    <1 ms    <1 ms  10.2.0.2
  3    14 ms    <1 ms    <1 ms  X.X.X.X
```

## Sources

  * <https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/iproute_pi/configuration/15-mt/iri-15-mt-book/iri-pbr.html>
  * <https://chinnychukwudozie.com/2013/09/08/configuring-policy-based-routing-on-cisco-ios-12/>

## Conclusion

Et voilà, la politique est en place et fonctionne de mon côté ! Je dois dire que cela m'a beaucoup aidé dans le cadre d'une migration, et que ça fonctionne très bien sur mon infrastructure.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire, surtout que je ne suis vraiment pas expert en réseau ! N’hésitez pas à me dire également si ce genre d’article vous plaît ! 

Merci pour votre lecture et à bientôt !