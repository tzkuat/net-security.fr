---
title: Convertir un disque VMWARE vers HYPERV
author: Fabio Pace
type: post
date: 2019-12-04T08:45:00+00:00
url: /system/convertir-un-disque-vmware-vers-hyperv/
thumbnail: /images/vmware-to-hyperv/vmware_to_hyperv0.png
tags: [
    "Virtualisation",
    "Windows",
    "VMWare",
    "HyperV"
]
categories: [
    "systeme"
]
---
![](/images/vmware-to-hyperv/vmware_to_hyperv0.png)

Bonjour à tous, aujourd'hui nous allons voir les différentes étapes pour convertir un disque Vmware vers Hyper-V. 

## Le besoin

Je travaille avec Vmware sur mon LAB. Or, un client m'a demandé de préparer une machine virtuelle pour son infra, et le prérequis est qu'elle soit compatible avec leur système de virtualisation : Hyper-V.

Pour éviter d'installer Hyper-V sur mon poste, j'ai donc créé la VM sur VMware, puis converti le disque. 

## Rappels

Lorsque vous accédez aux fichiers de configuration de la VM, vous devez apercevoir différentes extensions : 

  * _.vmx_ : Fichier de configuration de la machine virtuelle
  * _.vmwf_ : FIchier de configuration de machine virtuelle supplémentaires
  * _.vmsd_ : Description des snapshots de machine virtuelle
  * _.nvram_ : Configuration du BIOS ou EFI de machine virtuelle
  * _.vmdk_ : Disque virtuel

C'est donc les fichiers correspondant à l'extension _.vmdk_ sur lesquels nous allons travailler.

## La conversion

Tout d'abord, il faut éteindre la VM pour éviter d'avoir des erreurs d'utilisation du disque.

Puis, il faut se placer dans le répertoire d'installation de Vmware afin de pouvoir utiliser leurs outils, dans mon cas c'est : _C:\Program Files (x86)\VMware\VMware Workstation_

Sur Wokstation, Vmware propose de créer soit un simple disque pour la VM, soit des multiples disques qui faciliteront l'utilisateur en cas de déplacement des disques. 

Lorsque j'ai créé la VM, j'avais choisi cette option, il va donc falloir réunir les disques en un seul : 


```
./vmware-vdiskmanager.exe -r "C:\Users\f.pace\Documents\Virtual Machines\Unifi Controller\Unifi Controller.vmdk" -t 0 "C:\Users\f.pace\Documents\Virtual Machines\Unifi Controller\Unifi_Controller_VMWARE.vmdk"
```
![](/images/vmware-to-hyperv/vmware_to_hyperv1-1.png)

Il se peut que l'utilitaire ne souhaite pas réunir les disques car ils sont corrompus : 

![](/images/vmware-to-hyperv/vmware_to_hyperv1.2-1.png)

Vous devez donc réparer votre disque via la commande suivante : 


```
./vmware-vdiskmanager.exe -R "C:\Users\f.pace\Documents\Virtual Machines\Unifi Controller\Unifi Controller.vmdk"
```

![](/images/vmware-to-hyperv/vmware_to_hyperv1.3.png)


Vous relancez la commande de « réunification ».

Le disque a bien été créé, et est égal à la somme de chaque disque de la VM :

![](/images/vmware-to-hyperv/vmware_to_hyperv2.png)

Lorsque vous avez votre disque VMware pret, il faut télécharger le logiciel WinImage : <http://www.winimage.com/download.htm> afin de convertir vers Hyper-V.

**<U style="color:red;">NB :</U>** _Vous pouvez également faire l'inverse : convertir un disque .VHD (Hyper-V) vers .VMDK (VMware)._

![](/images/vmware-to-hyperv/vmware_to_hyperv3.png)

Il faut choisir le disque préparé précédement, puis deux options s'offrent à vous : 

  * Soit il convertit en créant un VHD de taille fixe (non recommandé)
  * Soit il convertit en créant un VHD dynamique

Il est préférable de choisir la deuxième option pour éviter de prendre de l'éspace inutilement. 

![](/images/vmware-to-hyperv/vmware_to_hyperv4.png)

Au bout de quelques minutes (ou quelques heures si votre disque est important), vous obtenez votre disque pour Hyper-V : 

![](/images/vmware-to-hyperv/vmware_to_hyperv5.png)

J’espère que l’article vous aura plu et qu'il sera vous sera utile. N’hésitez pas à commenter si vous avez la moindre remarque.

A bientôt, 

Fabio Pace.

 [1]: images/vmware-to-hyperv/vmware_to_hyperv1-1.png
 [2]: images/vmware-to-hyperv/vmware_to_hyperv1.2-1.png
 [3]: images/vmware-to-hyperv/vmware_to_hyperv1.3.png