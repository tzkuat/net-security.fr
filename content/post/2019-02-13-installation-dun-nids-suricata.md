---
title: "Installation d'un NIDS : Suricata"
author: Fabio Pace
type: post
date: 2019-02-13T14:00:05+00:00
url: /security/installation-dun-nids-suricata/
thumbnail: /images/suricata/NIDS_suricata_0.png
featureImage: images/suricata/NIDS_suricata_0.png
shareImage: /images/suricata/NIDS_suricata_0.png
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "IDS",
    "NIDS",
    "Suricata"
]
categories: [
    "logiciellibre",
    "securite"
]
---
![](/images/suricata/NIDS_suricata_0.png)

Bonjour à tous, dans cet article nous allons voir comment installer et configurer l'IDS Suricata (qui est pour un être plus précis un NIDS).

Tout d'abord, qu'est ce qu'un **IDS** (_Intrusion Detection System_) ?  
C'est un outil qui permet d'analyser les flux (provenant du réseau ou d'une machine (système d'exploitation par exemple)), et va permettre de faire une remonter d'alerte en fonction de son fichier de signature.  
Suricata est un NIDS (_Network Intrusion Detection System_) : un IDS qui est basé sur le réseau. 

En parallèle, il y a également des **HIDS** qui sont orientés « machines » (_Host Intrusion Detection System_).  
Vous pouvez visiter cet excellent article écrit par M.Rigonnaux : https://homputersecurity.com/2018/06/10/wazuh-hids-presentation-installation/>

## Préparation

Pour l’installation et la configuration de Suricata, nous avons choisi CentOS connu pour sa fiabilité et sa robustesse.

Avant de pouvoir l'installer, il faut installer toutes ses dépendances :

```
yum -y install gcc libpcap-devel pcre-devel libyaml-devel \ 
file-devel zlib-devel jansson-devel nss-devel libcap-ng-devel \ 
libnet-devel tar make  libnetfilter_queue-devel lua-devel \ 
python-yaml epel-release lz4-devel cargo
```

## Installation

Afin d’installer Suricata, il faut se rendre sur la page officielle afin de le télécharger: <https://openinfosecfoundation.org/download/>

![](/images/suricata/NIDS_suricata_1.png)

On récupère le fichier avec la dernière version stable.

**NB** : En date du 13/02/2019, la dernière version stable est : *suricata-4.1.2*.

Puis nous effectuons un téléchargement directement à partir de notre serveur: 


```
wget https://openinfosecfoundation.org/download/suricata-4.1.2.tar.gz
```

On décompresse le fichier: 


```
tar -xvzf suricata-4.1.2.tar.gz
```

On se rend à l’intérieur du dossier: 


```
cd suricata-4.1.2
```

Et on lance l’installation du logiciel: 

```
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var --enable-nfqueue --enable-lua
```

* `–prefix=/usr` : installation des binaires de Suricata dans /usr/bin/suricata
* `–sysconfdir=/etc` : installation des fichiers de configuration de suricata dans /etc/suricata
* `–localstatedir=/var` : configuration de suricata pour placer les logs dans /var/log/suricata
* `–enable-nfqueue` : activation du module IPS (pour une future utilisation)
* `–enable-lua` : activation du module Lua pour la détection et l’affichage en fonction de différent filtre. 

Il faut vérifier si les options LRO et GRO de vos cartes réseaux sont désactivées: 


```
ethtool -k enp2s0 | grep "large-receive-offload"
ethtool -k enp2s0 | grep "generic-receive-offload"
```


Si les deux modules sont à «Off», vous pouvez poursuivre, sinon je vous invite à les désactiver: 


```
ethtool -K enp2s0 gro off lro off
```


**NB** : L’argument **-k** de la commande «**ethtool**» permet de spécifier une carte réseau: attention au nom de votre carte réseau.

**NB** 2 :  

* LRO (Large Receive Offload) permet de réduire le traitement des paquets réseau par le CPU (c’est le processeur de la carte réseau qui va travailler). Si la carte reçoit 5 paquets provenant de la même source avec des entêtes similaires (entête MAC), l’hôte va les stocker dans un tampon et va transférer un seul paquet en les regroupant.
* GRO (Generic Receive Offload) : permet de former un seul paquet (regroupant une multitude paquet) qui possèdent tous une entête MAC commune (et des données TCP / IP différentes).

On lance la compilation et l’installation de Suricata en lui demandant de récupérer les règles de Suricata mais également la configuration de base: 

```
make install-full
```

Si vous souhaitez installer seulement la configuration de Suricata: 

```
make install-conf
```

Enfin, si vous souhaitez installer seulement les règles de Suricata: 

```
make install-rules
```

A la fin de l’installation, nous pouvons vérifier si Suricata est bien installé: 

```
suricata -h
```

![](/suricata/NIDS_suricata_2.png)

## Configuration

### La configuration de base

Nous devons maintenant éditer le fichier de configuration de Suricata: 


```
vi /etc/suricata/suricata.yaml
```

Nous devons modifier la variable «HOME_NET» afin qu’il y figure votre réseau local: 

* **HOME_NET: « [10.13.0.0/16] »**

Nous avons trois interfaces réseaux sur le poste: 

  * **enp4s0** est l'interface de management: 10.13.9.19
  * **enp2s0** est l'interface miroir des bornes wifi: 10.13.9.17 
  * **enp3s0** est l'interface miroir des prises brassés (prise rj-45 murale): 10.13.9.18

L’interface miroir va nous permettre de récupérer une copie de tout le trafic sortant des bornes wifi et des prises brassés sur les différents switch.  
La configuration du switch est à faire en amont (pas traité dans cet article).

A l’installation de Suricata, il copie un fichier contenant toutes les règles dans /var/lib/suricata/rules et se nomme «suricata.rules». C’est se fichier de règle que nous allons utiliser (en écrasant les autres fichiers de règles).  
Pour lancer suricata sur les interfaces ci-dessus: 


```
suricata -c /etc/suricata/suricata.yaml -i enp2s0 -i enp3s0 -S /var/lib/suricata/rules/suricata.rules -D
```

* -c : pour indiquer le fichier de configuration
* -i : pour indiquer sur quelle interface suricata va écouter
* -S : pour spécifier le fichier de règle pour Suricata (et ne pas récupérer les règles indiquées dans le fichier de configuration)
* -D : lance suricata en mode démon 

Nous pouvons tester notre configuration en rajoutant l’argument «**-T**» à la fin de notre commande pour éviter de faire une fausse manipulation:


```
suricata -c /etc/suricata/suricata.yaml -i enp2s0 -i enp3s0 -S /var/lib/suricata/rules/suricata.rules -T
```

![](/images/suricata/NIDS_suricata_3.png)

Deux fichiers vont se créer: 
* eve.json : fichier structuré et très verbeux, qui va être envoyé à un SIEM (Splunk dans notre cas) 

![](/images/suricata/NIDS_suricata_4.png)

* fast.log : c’est un fichier où toutes les alertes seront notifiés, il est plus facile à lire car une ligne correspond à une informatio

### Le mode service

Nous allons maintenant ajouter Suricata en tant que service. 

Il faut commencer par créer un fichier `/etc/systemd/system/suricata.service`: 

```
[Unit]
Description=Suricata IDPS Daemon
Wants=network.target syslog.target
After=network.target syslog.target
Restart=on-failure
 
[Service]
Type=forking
PIDFile=/var/run/suricata.pid
ExecStart=/usr/bin/suricata -c /etc/suricata/suricata.yaml -i enp2s0 -i enp3s0 -S /var/lib/suricata/rules/suricata.rules -D
ExecReload=/bin/kill -HUP $MAINPID
ExecStopPost=/bin/kill $MAINPID
Restart=on-failure
 
[Install]
WantedBy=multi-user.target 
```

**Attention:** il faut modifier la valeur de «ExecStart» en fonction de vos cartes réseaux.

Puis il faut activer le service : 

```
systemctl enable suricata.service
```

Enfin, nous pouvons démarrer Suricata: 

```
systemctl start suricata.service
```

Ou le stopper: 

```
systemctl stop suricata.service
```

Ou le redémarrer: 

```
systemctl restart suricata.service
```

Ou vérifier son statut:


```
systemctl status suricata.service
```


<strong style="color:red;">NB :</strong> _Nous pouvons appeler directement «suricata» au lieu de «suricata.service» pour gagner du temps._

Au prochain redémarrage de votre serveur, suricata se lancera automatiquement. 

### La récupération des règles

Nous allons maintenant créer un script qui va gérer la récupération des règles tous les jours. 

Nous créons donc un script `/home/rotateRulesSuricata.sh`:
```
#!/bin/sh
 
# Téléchargement du fichier contenant toutes les règles dans /root #
wget http://rules.emergingthreats.net/open/suricata/emerging-all.rules -P /root
 
# On déplace le fichier que l’on vient de télécharger dans le dossier des règles #
mv /root/emerging-all.rules /var/lib/suricata/rules/suricata.rules
 
# On redémarre suricata pour prendre en compte la modification #
systemctl restart suricata 
```

Nous donnons le droit à root de l’exécuter: 

```
crontab -e

# Rotation des règles tous les jours à 2h00 du matin #
00 02 * * * /home/rotateRulesSuricata.sh 
```

### La gestion des logs

Nous allons voir maintenant l’installation et la configuration de LogRotate (pour la rotation des logs).  
Le paquet «LogRotate» est présent par défaut dans CentOS 7, si ce n’est pas le cas:


```
apt install logrotate
```


On va créer le fichier `/etc/logrotate.d/suricata` :
```
/var/log/suricata/*.log /var/log/suricata/*.json
{
    daily
    rotate 45
    missingok
    nocompress
    create
    sharedscripts
    dateext
    dateformat -%d%m%Y
    postrotate
            systemctl restart suricata
    endscript
} 
```

Nous planifions donc une rotation de logs sur 45 jours, sans compression, en incluant la date du jour dans le nom du fichier. 

Nous vérifions à quelle heure se lance le «cron.daily»: 

```
grep run-parts /etc/anacrontab
```

Pour ma part, il se lance à 1h05 du matin:

![](/images/suricata/NIDS_suricata_6.png)

**NB** : Pour une machine serveur, il est préférable d’utiliser «cron» qui est un démon (contrairement à anacron qui est plus approprié pour des ordinateurs fixes ou portables).  
Dans mon cas, le «cron.daily» était déjà configuré dans anacron, c’est la raison pour laquelle j’ai laissé cela.

Votre IDS Suricata est maintenant configuré et opérationnel.

J'espère que cet article vous aura plu.  
N'hésitez pas à me faire un retour pour tout problème rencontré.

A bientôt,

Fabio Pace.