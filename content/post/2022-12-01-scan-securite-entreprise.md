---
title: Gérer les scans de sécurité en entreprise
author: Mickael Rigonnaux
type: post
date: 2022-05-15T10:05:44+00:00
url: /security/scan-securite-entreprise
thumbnail: /images/scan-entreprise/logo.png
shareImage: /images/scan-entreprise/logo.png
featured: true
tags: [
    "scanner",
    "cve",
    "vulnérabilité"
]
categories: [
    "securite"
]

---
Bonjour à tous, aujourd'hui un article un peu plus organisationnel que technique : comment gérer les scans de sécurité sur un réseau d'entreprise ?

## Introduction

Dans presque toutes les entreprises qui ont pour cœur de métier l'informatique, des scans de sécurité sont réalisés. Je vais essayer dans cet article de vous présenter la manière dont je vois les choses pour gérer au mieux les différents scans et leurs résultats avec ma petite expérience sur le sujet.

## Scan de sécurité ?

Les scans de sécurité sont des analyses actives des différentes informations et vulnérabilités présentes sur les différents équipements & machines. Grâce à des scanners, vous allez pouvoir voir quelles sont les machines vulnérables sur le réseau, quelles sont les informations que l'on peut récupérer, etc.

Généralement, ils sont réalisés avec des outils commerciaux comme : Nessus (Tenable), Qualys, Nexpose... Le but ici n'est pas d'en préférer un, je n'ai pas testé toutes les solutions. Personnellement j'ai l'habitude de travailler avec Qualys/Nessus qui ont tous les deux des avantages et des incovénients.

## Quoi scanner et ou placer la/les sondes ?

C'est la 1ère question qu'il faut se poser : que veut-on scanner ?

Dans mon cas, j'ai toujours été contraint à une forte exposition sur internet (des /21 ou /22 d'adresse IP publiques) avec au moins deux sites. Il est donc nécessaire de scanner l'interne et l'externe.

### Réseaux locaux

Pour l'interne, je suis partisan de placer une sonde par site, pour scanner les réseaux locaux.

Ce que je fais généralement c'est :
* Whitelister les IPs des scanners (dans l'IPS)
* Ouvrir l'ensemble des flux à ces scanners

De mon point de vue je pense que les scanners de sécurité doivent absolument voir tout le réseau. Cela permet d'avoir une vision complète et de remonter toutes les vulnérabilités possibles afin de les corriger.

Il est également possible de multiplier les scanners (ou les interfaces réseaux sur ce dernier) et de réaliser des tests en simulant un utilisateur.

Pour le découpage des scans, il est important de ne pas simplement lancer un scan avec seulement 192.168.0.0/16, 10.0.0.0/8, si vous avez un nombre de machine important, les résultats seront trop difficiles à traiter et à prioriser.

Pour l'avoir vécu récemment : il faut créer des groupes afin de traiter par famille ou métier si vous le pouvez. Par exemple :
* Un scan dédié aux outils bureautiques
* Un scan dédié aux serveurs de production du client A
* Un scan dédié aux postes de travail et aux imprimantes
* Un scan dédié aux interfaces d'administration
* Un scan dédié aux équipements réseaux
* Etc.

Il ne faut pas hésiter à multiplier le nombre de scan. De mon côté j'ajoute directement les réseaux et non les hôtes, cela permet de trouver automatiquement les nouveaux hôtes. Cela demande une attention particulière et un processus d'ajout en fonction des modifications apportées sur le réseau (création, suppression, etc.)

En plus d'une meilleure gestion, cela permet de mieux repérer les effets de bord. Si vous lancez un scan sur tout le SI d'un coup, il va être difficile d'identifier si l'effet peut venir du scanner ou non.

### Réseaux publics

Pour les réseaux publics il y a plusieurs choix possibles :
* Soit vous avez une sonde directement sur la plateforme de scan (Tenable/Qualys) qui peut scanner les IPs publiques (en mode "cloud")
* Soit vous avez plusieurs sites et vous réalisez des scans croisés

Dans mon cas le site A scan les IP publiques du site B et inversement. Cela permet de ne pas avoir d'effet de bord avec les règles permissives déclarer sur chaque site.

Cependant : pas d'ouverture spécifique pour les sondes vers les différentes IP publiques. Je pense que le plus opportun est d'avoir la même vision que les attaquants présents sur Internet. Cela permet de recouper avec les informations des scans internes et de prioriser.

## Quand scanner les différents réseaux ?

Que cela soit pour les réseaux privés ou publiques il est très important de réaliser un planning des différents scans. C'est une conséquence du découpage abordé plus haut : plus vous découpez, plus c'est compliqué à gérer.

De mon côté j'ai toujours travaillé comme ça et je pense que c'est la meilleure solution.

Après avoir découpé vos différents réseaux en groupes, vous réalisez un planning mensuel, cela permet de suivre les évolutions des vulnérabilités et repasser régulièrement sur les réseaux afin de ne pas passer à côté de quelque chose.

Par exemple :
* Le 1er lundi du mois à 19h : début du scan bureautique
* Le 1er mardi du mois à 19h : début du scan production client A
* Etc.

Ce planning doit ensuite être partagé à toutes les personnes concernées avec les IPs utilisées, cela permet de mieux identifier les problèmes liés au scan de sécurité parce qu'il y en a toujours. Chaque responsable peut à tout moment savoir si un scan est lancé sur sa plateforme à un instant T, c'est aussi le but du planning.

Il faut cependant que votre solution de scan puisse gérer la planification des scans.

Si possible, il faut éviter que plusieurs scans tournent en même temps afin de ne pas créer de confusion, certains scans peuvent durer plusieurs heures/jours, il faut adapter le planning en fonction et ajouter la durée des scans si c'est possible.

Pour les résultats des scans, il est aussi préférable de récupérer les différents scans et de les stocker dans un autre endroit : une instance Nextcloud/Owncloud par exemple. Cela permet d'avoir des formats PDF par exemple, facile à montrer à un auditeur avec le suivi mensuel des vulnérabilités trouvées.

Nessus par exemple envoi un rapport PDF synthétique à la fin de chaque scan si on le configure.

## Gérer les vulnérabilités

Qui dit scan de vulnérabilités, dit vulnérabilités. Un SI sans machine ancienne ou vulnérabilité présentes, pour moi, ça n'existe pas (du moins je n'ai pas eu la chance d'en voir...).

Il est important de mettre en place un amont un processus pour prioriser et traiter les différentes vulnérabilités avec un suivi.

Cela permet de ne pas laisser de vulnérabilités trainer et d'être transparent sur le traitement.

Généralement, c'est une section spécifique dans l'outil de ticketing.

Si c'est les premiers temps que votre scanner tourne : il va avoir beaucoup d'informations à traiter :
* Les faux positifs 
* Les choses que vous ne pouvez plus patcher
* Les choses que vous découvrez
* Etc.

Il va être important de prioriser, de mon côté cela passe généralement par :
* La gravité de la vulnérabilité
* L'exposition ou non sur internet

Cela sera à adapter en fonction de vos besoins, mais je traite en priorité les vulnérabilités présentes sur les IP publiques.

Vous pouvez par exemple dire :
* Les vulnérabilités critiques et hautes sont traités directement par l'équipe sécurité
* Les vulnérabilités moyennes et basses sont traités selon le planning de mise à jour classique (pas d'action supplémentaire)

## Allouer du temps

Nous avons tous des contraintes de temps, de production, de projet et nous n'avons pas tous la chance d'avoir une équipe de sécurité dédié. C'est pour cela qu'il faut consacrer du temps à la gestion des scanners de sécurité et aux informations qu'ils remontent. De mon côté j'ai bloqué 3h tous les lundi afin de traiter cela.

La gestion va prendre beaucoup plus de temps que la mise en place car il va falloir des interactions avec toutes les parties : la production, le réseau, les utilisateurs... C'est avant tout un travail de sensibilisation et de gestion du changement.

## Autres

En plus de tout cela je vais rajouter des informations un peu en vrac :
* Pensez à bien déclarer les IPs des scanners aux différents clients/membres des équipes afin de ne pas avoir de surprise
* Ne vous amusez pas à scanner les machines de vos clients sans documents signés de leur part
* Les scanners ont tendances à couter cher, mais c'est selon moi le 1er pas à franchir pour la sécurité de l'entreprise
* Attention lorsque vous scannez les imprimantes... Elles ont tendances à lancer des impressions en boucle (parole de quelqu'un qui a vidé toutes les imprimantes sur 3 sites différents).

## Conclusion

Encore une fois, c'est ma vision des choses sur le sujet, je pense qu'il est possible de faire mieux (comme toujours) mais c'est déjà un bon début si vous arrivez à appliquer ça. Le but ici est de ne pas simplement acheter un scanner, s'en servir 3 fois et le laisser mourir. C'est un travail sur le long terme si vous voulez que cela serve à quelque chose.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !