---
title: "Présentation & Installation d'Harbor, une registry sécurisée pour vos conteneurs"
author: Mickael Rigonnaux
type: post
date: 2019-09-29T18:52:56+00:00
url: /security/presentation-installation-dharbor-une-registry-securisee-pour-vos-conteneurs/
thumbnail: /images/Harbor/harbor.png
featureImage: images/Harbor/harbor.png
shareImage: /images/Harbor/harbor.png
tags: [
    "GNU/Linux",
    "Docker",
    "Conteneur"
]
categories: [
    "systeme",
    "logiciellibre"
]

---

Bonjour à tous, aujourd'hui nous allons aborder un point et un outil important concernant la sécurité des conteneurs. 

## Introduction

En effet, dans le monde des conteneurs avec [Docker][1], [Kubernetes][2] ou autres, un point primordial et très compliqué est la sécurité. Que ça soit au niveau de la gestion des flux, des traces, de la détection des attaques ou dans notre cas du niveau de sécurité du conteneur directement. 

## Harbor

[Harbor][3] est une registry privée qui permet de scanner les images poussées sur cette dernière. C'est un logiciel Open Source avec un licence Apache 2.0 créé et maintenu par [VMWare][4], ce projet est aussi soutenu par la Cloud Native Computing Foundation. 

Cette registry permet donc d'avoir un endroit en interne pour stocker vos propres images ou directement les images issues de Docker Hub par exemple. De plus, cette registry vous permets également de stocker vos charts en plus des conteneurs associés. Tout en proposant une interface graphique plaisante. 

Plus concrètement, Harbor utilise le scanner très connu de [CoreOS][5] pour réaliser ses tests, c'est à dire [Clair][6] ainsi qu'une base CVE disponible localement ou directement en ligne selon les installations. 

D'autres fonctionnalités existent sur cette registry, comme la possibilité de bloquer une image si elle comporte des CVE critiques ou encore de créer plusieurs projets afin de séparer les environnements ou de mieux gérer les droits. Il est également possible d'ajouter une authentification AD/LDAP pour une utilisation en entreprise.

Pour finir, d'autres options existent et sont disponibles sur ce logiciel comme l'API ou encore l'intégration de Notary pour la signature des images. 

## Installation 

Plusieurs méthodes d'installations sont disponibles sur ce logiciel : 

  * [Sur Docker][7] avec Docker 17.06+ et docker-compose 1.18+
  * [Sur Kubernetes][8] avec Helm 2.8+ et Kubernetes Cluster 1.10+

Dans cet article je vais utiliser la version pour Docker avec docker-compose. 

### Pré-requis

#### Matériel

Voici les ressources minimums/recommandées :
* CPU : 2/4
* RAM : 4GB/8GB
* Disque : 40GB/160GB

#### Logiciel

Voici les logiciels recommandés :
* Docker engine : 17.06+
* Docker-compose : 1.18+
* OpenSSL : la dernière pour votre système.

Dans la configuration par défaut, Habor utilise les ports 80/443 pour les requêtes de l'API et pour l'interface web, c'est à dire également pour la registry. Le port 4443 peut-être utilisé si vous utilisez Notary. 

Vous avez également le choix entre l'installation hors-ligne ou en-ligne, la différence se situe au niveau de la base CVE. Dans le mode hors-ligne elle est directement sur vos machines alors qu'avec la version en ligne votre registry accède à une base de données en ligne. 

Dans cet article nous déploierons la version en ligne sur Ubuntu 18.04.3 Server. 

### Déploiement

Avant tout, vérifions les versions des logiciels installés avec les commandes : 

```
docker -v 
docker-compose -v 
```

![](/images/Harbor/harbor_1-1.png)

Si comme moi votre version d'Ubuntu utilisait une version de docker-compose non supportée vous pouvez installer la dernière avec ces commandes : 

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose
```

L'installation d'Harbor est basée sur un fichier de configuration « harbor.yml » et sur un script « install.sh »

Avant d'accéder à ces fichiers il faut télécharger l'installeur et le décompresser : 

```
wget https://storage.googleapis.com/harbor-releases/release-1.9.0/harbor-online-installer-v1.9.0.tgz
tar xvf harbor-online-installer-v1.9.0.tgz
```

Lorsque j'écris ces lignes, la version 1.9.0 est la dernière stable, lorsque vous faites votre installation vérifiez les versions disponibles [sur ce lien][10]. 

C'est à ce moment là ou vous pouvez choisir l'installation en ligne ou hors ligne. Si vous choisissez la version hors ligne il faut remplacer « online » par « offline » dans les commandes. 

Vous avez maintenant accès aux fichiers : 

![](/images/Harbor/harbor_2.png)

Vous pouvez maintenant réaliser la configuration du déploiement de votre registry avec le fichier harbor.yml. Notamment au niveau du nom de domaine à utiliser, si vous voulez utiliser des certificats, les ports à utiliser, les mots de passe à utiliser, etc. 

De mon côté je vais seulement modifier les mots de passe par défaut et activer le chiffrement avec HTTPS. J'utilise mon cas un certificat Let's Encrypt, si de votre côté vous utilisez un certificat auto-signé, vous pouvez voir [mon article sur OpenSSL][12]. Si vous utiliser un certificat auto-signé ou généré via une autorité non reconnue faite bien attention à ajouter la chaîne ou directement le certificat dans les keystore des différentes machines (client/serveur). 

Vous avez des informations sur l'ensemble des paramètres disponibles dans [la documentation officielle.][13]

Après avoir configuré votre fichier, vous pouvez lancer l'installation d'Harbor avec la commande suivante : 

```
sudo ./install.sh --with-clair --with-chartmuseum

```
Dans ce cas, Harbor sera installé avec Clair pour les scans et le repo de Chart Helm. 

L'installeur va donc télécharger toutes les images nécessaires et charger les configurations. 

![](/images/Harbor/harbor_3.png)

Si l'installation se déroule bien, vous aurez un message vous indiquant que votre registry est disponible à l'adresse que vous avez indiquée. Dans mon cas j'ai rajouté une entrée dans mon fichier host pour que ça fonctionne.

![](/images/Harbor/harbor_4.png)

Vous avez maintenant accès à l'interface d'Harbor avec les logins entrés dans le fichier de configuration : 

![](/Harbor/harbor_5.png)

Harbor est maintenant installé. Nous allons voir maintenant comment pousser une image sur cette registry et la scanner avec l'outil Clair. 

### Pousser et scanner une image

Nous allons maintenant voir comment pousser une image dans notre registry. L'image « ubuntu:latest » dans mon cas. Pour ce test personnellement j'ai utilisé une machine différente. Tout d'abord il faut la télécharger et lui ajouter un tag : 

```
docker pull ubuntu:latest
docker tag ubuntu:latest harbor.rm-it.fr/library/ubuntu:dev1
```

![](/images/Harbor/harbor_6.png)

Et en suite se connecter à la registry et pousser l'image : 

```
docker login harbor.rm-it.fr
docker push harbor.rm-it.fr/library/ubuntu:dev1
```

![](§images/Harbor/harbor_7.png)

Si durant l'une de ces étapes vous rencontrez une erreur liée à votre certificat SSL vous pouvez procéder comme ceci : 

```
mkdir /etc/docker/certs.d/
mkdir /etc/docker/certs.d/harbor.rm-it.fr (à remplacer par votre nom de domaine) 
```

Et ajouter dans ce dossier l'autorité ou le certificat installé sur votre registry. 

Nous pouvons maintenant voir quand dans le projet par défaut « library » nous avons bien notre image Ubuntu avec le tag dev1 : 

![](/images/Harbor/harbor_8.png)

Pour lancer le scan il suffit de la cocher de cliquer sur « Scan » : 

![](/images/Harbor/harbor_9.png)

Une fois terminé vous aurez accès aux différentes CVE/Informations de votre image :

![](/images/Harbor/harbor_10.png)

De plus, si vous voulez automatiser les scans, vous pouvez vous rendre dans « Vulnerability » pour automatiser le scan de vos images :

![](/images/Harbor/harbor_11.png)


## Conclusion

Voilà, nous avons maintenant une registry capable de scanner nos images. Ceci dit, l'outil n'est pas parfait car avec certaines images il n'arrive pas à réaliser de scan complet comme avec les versions les plus réduites d'alpine.

Il est tout de même un outil agréable à utiliser et très intéressant pour débuter l'aspect sécurité sur la partie DevOps. 

La partie repo de charts sera abordé dans prochain article avec plus de détail sur Helm également. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Sur le même principe n'hésitez pas à me dire si des articles sur d'autres outils de ce type vous intéresse. 

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.docker.com/
 [2]: https://kubernetes.io/
 [3]: https://goharbor.io/
 [4]: https://www.vmware.com/
 [5]: https://coreos.com/
 [6]: https://coreos.com/clair/docs/latest/
 [7]: https://github.com/goharbor/harbor
 [8]: https://github.com/goharbor/harbor-helm
 [10]: https://github.com/goharbor/harbor/releases
 [12]: https://net-security.fr/security/openssl-formats-cheat-sheet/
 [13]: https://github.com/goharbor/harbor/blob/master/docs/installation_guide.md