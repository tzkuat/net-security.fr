---
title: "Présentation & Installation de phpIPAM, un gestionnaire d'adresses IP libre"
author: Mickael Rigonnaux
type: post
date: 2020-03-21T21:54:47+00:00
url: /system/presentation-installation-de-phpipam/
thumbnail: /images/phpipam_logo.png
tags: [
    "GNU/Linux",
    "Docker",
    "Management"
]
categories: [
    "systeme",
    "logiciellibre"
]

---

Bonjour à tous ! Aujourd'hui un rapide article pour vous présenter un outil que j'utilise maintenant depuis plus d'un an que ça soit sur le plan personnel ou professionnel, j'ai nommé [phpIPAM][1].

## Introduction & Présentation

PhpIPAM est une application web de gestion d'adresses IP libre et proposée sous licence GPLv3. Elle est développée en PHP et en Javascript. Elle utilise également de l'Ajax avec la bibliothèque jQuery ainsi qu'une base de données SQL. 

C'est une alternative libre à IPAM de Microsoft. Je trouve que cette application est peu connue, c'est pour ça que j'ai choisi de la mettre en avant dans un article dédié. 

Les sources de l'application sont [disponibles ici][2]. 

Concrètement, phpIPAM permet de référencer les adresses IP que vous utilisez, tout ça dans une jolie application Web. Il permet également de faire de la découverte réseau pour découvrir les IP déjà utilisées par exemple. Je l'utilise pour mon réseau personnel par exemple, je pourrais difficilement m'en passer aujourd'hui. Vous pouvez ajouter manuellement ou automatiquement les différents hôtes dans cette interface. 

C'est une application très pratique peu importe la taille de votre infrastructure. Cela devient même indispensable lorsque vous avez un SI conséquent, histoire de savoir quelles sont les IP utilisées ou non mais également de savoir quelle machine utilise tel IP, etc. 

Cela permet d'avoir sur une seule interface, la liste de tous les réseaux utilisés avec toutes les IP utilisées/disponibles dans ce réseau, tout en rajoutant les noms de domaine ou de machine rattachée. Il est également possible de rajouter une description par exemple. 

Il est possible de gérer les IPv4 & IPv6 avec phpIPAM. L'outil propose plusieurs autres fonctionnalités comme : 

  * La connexion via LDAP/AD, adapté pour une utilisation en entreprise
  * La découverte des réseaux
  * La possibilité d'importer des réseaux depuis des fichiers (CSV, XLS)
  * La gestion des VLAN
  * Les alertes par mail

Cela permettra de remplacer votre vieux tableau excel sans problème. Et vu qu'une démo vaut toutes les explications du monde, vous pouvez tester l'outil directement sur l'instance de démo publique proposée par le développeur : 

  * <https://demo.phpipam.net/login/>

## Installation

De mon côté j'ai déployé phpIPAM à l'aide de docker, pour cela j'utilise une machine Ubuntu Server 18.04. Si vous ne voulez pas utiliser docker, des documentations pour l'installation sont [disponibles sur le site officiel][3]. 

Le logiciel est composé de 3 conteneurs : 

  * phpipam-www qui permet de fournir l'interface graphique grâce au serveur web Apache2
  * phpipam-cron qui permet de planifier la découverte de réseau à l'aide de cron
  * mariaDB comme système de base de données SQL

Toutes les commandes lancées dans ce tuto sont exécutées avec l'utilisateur root. 

Tout d'abord, il faut commencer par mettre à jour la machine : 


```
apt update
apt upgrade
```


Et maintenant l'installation de docker et ses dépendances : 


```
apt-get remove docker docker-engine docker.io containerd runc
apt-get update
apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Vérification de la clé ajoutée 
apt-key fingerprint 0EBFCD88

# Ajout du repo 
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Installation de docker
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io
```


Vous pouvez maintenant vérifier votre installation de docker avec la commande suivante : 


```
docker -v
```


![](/images/phpipam_1.png)

Il faut maintenant installer docker-compose et vérifier son installation également : 

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

docker-compose -v
```


![](/images/phpipam_2.png)

Vous devez en suite récupérer le [docker-compose fourni][4] pour l'installation et l'adapter à vos besoins. Voici le mien : 


```
version: '3'

services:
  phpipam-web:
    image: phpipam/phpipam-www:1.4x
    ports:
      - "8082:80"
    environment:
      - TZ=Europe/London
      - IPAM_DATABASE_HOST=phpipam-mariadb
      - IPAM_DATABASE_PASS=votre_mdp
      - IPAM_DATABASE_WEBHOST=%
    restart: unless-stopped
    volumes:
      - phpipam-logo:/phpipam/css/images/logo
    depends_on:
      - phpipam-mariadb

  phpipam-cron:
    image: phpipam/phpipam-cron:1.4x
    environment:
      - TZ=Europe/Paris
      - IPAM_DATABASE_HOST=phpipam-mariadb
      - IPAM_DATABASE_PASS=votre_mdp
      - SCAN_INTERVAL=1h
    restart: unless-stopped
    depends_on:
      - phpipam-mariadb

  phpipam-mariadb:
    image: mariadb:10.5.1
    environment:
      - MYSQL_ROOT_PASSWORD=votre_mdp
    restart: unless-stopped
    volumes:
     - phpipam-db-data:/var/lib/mysql

volumes:
  phpipam-db-data:
  phpipam-logo:
```


Dans le docker-compose proposé par défaut, les tags d'images proposées sont des « latest », je vous déconseille d'utiliser des images avec le tag latest, il vaut mieux choisir des versions spécifiques dans les tags pour connaître exactement la version que vous utilisez. Cela vous aidera fortement au moment de mettre à jour votre stack. 

Vous pouvez en suite récupérer ce docker compose sur votre serveur et le lancer. Dans mon cas il se trouve dans /opt/phpipam/docker-compose.yaml : 

```
mkdir /opt/phpipam 
cd /opt/phpipam
vi /opt/phpipam/docker-compose.yaml
docker-compose up -d
```

![](/images/phpipam_3.png)

Si tout se déroule correctement, vous pouvez vous rendre sur l'interface de phpIPAM depuis votre navigateur :

![](/images/phpipam_4.png)

Sélectionnez en suite « New phpipam installation ». Dans mon cas, je laisse phpipam générer la base de données pour moi avec la 1ère option : 

![](/images/phpipam_5.png)

Il faudra renseigner le compte root ainsi que le mot de passe que vous avez indiqué dans le docker-compose : 

![](/images/phpipam_6.png)

Pour terminer l'installation vous allez devoir définir le compte administrateur de votre instance phpIPAM : 

![](/images/phpipam_7.png)

L'instance est maintenant prête, vous n'avez plus qu'à vous connecter et ajouter vos réseaux : 

![](/images/phpipam_8.png)

Dans le cadre d'une utilisation en production ou en entreprise je vous conseille de mettre en place du chiffrement HTTPS sur ce site. Dans mon cas j'utilise un reverse proxy HAProxy pour faire de la décharge SSL vers mon phpIPAM. 

## Conclusion 

Pour faire une rapide conclusion, phpIPAM est pour moi une solution très facile à mettre en place et qui permet d'avoir une grosse valeur ajoutée, et ça quelle que soit la taille de votre SI. 

Que vous soyez administrateur système/réseau, développeur ou encore architecte, je pense que cet outil vous permettra de gagner un temps précieux ! 

N'hésitez pas à contribuer ou à faire des dons sur ce projet si vous l'utilisez ! 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://phpipam.net/
 [2]: https://github.com/phpipam/phpipam
 [3]: https://phpipam.net/documents/installation/
 [4]: https://hub.docker.com/r/phpipam/phpipam-www