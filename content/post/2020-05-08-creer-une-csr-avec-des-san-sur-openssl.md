---
title: Créer une CSR avec des SAN sur OpenSSL
author: Mickael Rigonnaux
type: post
date: 2020-05-08T15:59:50+00:00
url: /security/creer-une-csr-avec-des-san-sur-openssl/
thumbnail: /images/opensslsan-logo-1.png
featured: true
tags: [
    "cryptographie",
    "GNU/Linux",
    "GnuPG"
]
categories: [
    "systeme",
    "logiciellibre",
    "securite"
]

---
Bonjour à tous ! Aujourd'hui un article sur [OpenSSL][1] pour savoir comment créer une [CSR][2] avec des [SAN][3].

**MàJ du 30/03/2024 : il est maintenant possible de créer des CSR avec SAN directement en CLI, c'est expliqué sur le blog [sur ce lien.](https://net-security.fr/securite/openssl-csr-san-en-cli/)**

## CSR ?

Tout d'abord une petite explication, une CSR ou Certificate Signing Request est tout simplement une demande de certificat. Ce fichier issu de votre clé privée va contenir toutes les informations nécessaires à la signature de votre certificat, le CN, les SAN, etc. C'est ce fichier que vous devez transmettre à votre autorité de certification pour demander un certificat. Une CSR contiendra donc une clé publique et les différentes informations, la clé privée n'est heureusement pas incluse et permet juste de signer ce fichier.

Les CSR sont définies dans le standard PKCS8 et son but principal est de ne pas faire transiter les clés privées.

Ces demandes sont généralement sous cette forme :


```
-----BEGIN CERTIFICATE REQUEST-----
MIIBMzCB3gIBADB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEW
MBQGA1UEBxMNU2FuIEZyYW5jaXNjbzEjMCEGA1UEChMaV2lraW1lZGlhIEZvdW5k
YXRpb24sIEluYy4xGDAWBgNVBAMUDyoud2lraXBlZGlhLm9yZzBcMA0GCSqGSIb3
DQEBAQUAA0sAMEgCQQC+ogxM6T9HwhzBufBTxEFKYLhaiNRUw+8+KP8V4FTO9my7
5JklrwSpa4ympAMMpTyK9cY4HIaJOXZ21om85c0vAgMBAAGgADANBgkqhkiG9w0B
AQUFAANBAAf4t0A3SQjEE4LLH1fpANv8tKV+Uz/i856ZH1KRMZZZ4Y/hmTu0iHgU
9XMnXQI0uwUgK/66Mv4gOM2NLtwx6kM=
-----END CERTIFICATE REQUEST-----
```


## SAN ?

Un Subject Alternative Name est une extension de la norme X509, cela permet d'ajouter des informations additionnelles dans un certificat. Ca permet par exemple de créer un certificat valable pour plusieurs domaines, par exemple :

  * www.net-security.fr
  * demo.net-security.fr
  * test.net-security.fr

Un SAN peut contenir plusieurs informations comme des adresses mails, des adresses IP ou des noms de domaine.

## Créer une CSR avec des SAN

Les commandes suivantes sont lancées depuis la distribution GNU/Linux Arch en utilisant OpenSSL 1.1.1g.

Tout d'abord il faut créer une clé privée :


```
## RSA
openssl genrsa -out exemple.key 2048

# ECC
openssl ecparam -genkey -name prime256v1 -out exemple.key
```


Si vous voulez créer simplement une CSR sans SAN, vous pouvez utiliser les commandes suivantes :


```
## RSA
openssl req -new -sha256 -key exemple.key -out exemple.csr

## ECC
openssl req -new -sha256 -key exemple.key -nodes -out exemple.csr
```


Pour générer une CSR avec des SANs, le plus simple est selon moi d'utiliser un fichier de configuration comme celui-ci :


```
[ req ]
prompt = no
distinguished_name = dn
req_extensions = req_ext

[ dn ]
CN = exemple.com
emailAddress = ssl@exemple.com
O = Societe
OU = Departement
L = Ville
ST = Etat
C = FR

[ req_ext ]
subjectAltName = DNS: www.exemple.com, DNS: mail.exemple.com, IP: 192.168.1.1
```


En utilisant ce fichier nous allons créer une CSR pour exemple.com, www.exemple.com, mail.exemple.com et 192.168.1.1. Dans mon cas le fichier est nommé exemple.conf.

Une fois le fichier en place il suffit de lancer la commande suivante en utilisant votre clé générée dans la partie précédente :


```
openssl req -new -config exemple.conf -key exemple.key -out exemple.csr
```


La CSR est générée automatiquement vu que l'option « prompt » est désactivée. Vous pouvez vérifier les informations de votre CSR avec la commande suivante :


```
openssl req -text -noout -verify -in exemple.csr
```
![](/images/openssl_san-1.png)>

On voit bien les différentes informations présentes dans notre fichier de configuration. Il ne reste qu'à transmettre cette CSR à une autorité de certification pour signature.

## Sources

  * [https://ethitter.com/2016/05/generating-a-csr-with-san-at-the-command-line/]
  * [https://fr.wikipedia.org/wiki/Subject\_Alternative\_Name][3]
  * [https://fr.wikipedia.org/wiki/Certificate\_Signing\_Request][2]

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.openssl.org/
 [2]: https://fr.wikipedia.org/wiki/Certificate_Signing_Request
 [3]: https://fr.wikipedia.org/wiki/Subject_Alternative_Name
 [4]: images/openssl_san-1.png