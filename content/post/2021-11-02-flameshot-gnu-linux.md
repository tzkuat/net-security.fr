---
title: "Capture d'écran sous GNU/Linux avec Flameshot"
author: Mickael Rigonnaux
type: post
date: 2021-11-02T00:19:35+00:00
url: /system/capture-ecran-gnu-linux/
thumbnail: /images/flameshot.png
featureImage: images/flameshot.png
shareImage: /images/flameshot.png
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "LogicielLibre"
]
categories: [
    "systeme"
]
---

Bonjour à tous, aujourd'hui un article après un long moment d'absence, comme vous pouvez le voir le blog a été migré vers Hugo, un article sera dédié à la migration. Nous allons aborder aujourd'hui l'outil de capture d'écran [Flameshot](https://github.com/flameshot-org/flameshot) sous GNU/Linux !

## Pourquoi ?

On réalise tous un grand nombre de capture d'écran, pour diverses raisons : articles, documentations, etc. J'utilise un système GNU/Linux quotidiennement et ça depuis 5 ans (Arch/Manjaro/PopOS) et j'ai eu du mal à trouver un outil pour réaliser des captures d'écran correctement. Celui par défaut de KDE fait l'affaire par exemple, mais quand il faut identifier des zones, rajouter du texte... Ce n'était pas très pratique de copier/coller puis de modifier tout ça sur KolourPaint. C'est la que j'ai découvert Flameshot, que j'utilise maintenant depuis 2 ans quotidiennement.

## Flameshot ?

Flameshot est un logiciel libre développé en C++ et proposé sous licence GPLv3, c'est pour moi le logiciel libre le plus abouti et le plus simple à utiliser pour faire des captures d'écran. Il est toujours activement maintenu par sa communauté, vous pouvez trouver toutes les informations sur son [repository Github](https://github.com/flameshot-org/flameshot).

Il est possible d'installer sur la plupart des systèmes comme MacOS, Windows et GNU/Linux. Dans cet article nous n'allons traiter que la partie GNU/Linux et personnellement c'est une machine virtuelle Manjaro que je vais utiliser.

### Installation

Pour l'installation, vous pouvez une nouvelle fois vous référer à la page Github, vous pouvez même le compiler vous même si vous voulez. Dans mon cas j'ai simplement exécuté :
```
sudo pacman -Syu flameshot
```

Pour les autres distributions c'est tout aussi simple :
```
apt install flameshot
zypper install flameshot
dnf install flameshot
```

Flameshot est maintenant installé.

### Configuration

Au niveau configuration, personnellement je n'ai pas touché grand chose, j'ai changé les couleurs pour que ce soit bleu et plus mauve et j'ai rajouté un raccourci clavier.

Pour le raccourci clavier sous Manjaro, voici la configuration :

![Raccourci clavier Flameshot Manjaro](/images/flameshot-1.png)

Personnellement j'utilise le raccourci "Windows + C" pour capturer seulement.

Afin d'ouvrir la console de configuration de l'outil vous devez lancer la commande suivante dans votre terminal :
```
flameshot config
```

![Menu configuration de Flameshot](/images/flameshot-2.png)

Dans ce menu vous pourrez configurer les paramètres du logiciel comme la couleur, l'opacité ainsi que l'ensemble des options que vous souhaitez utiliser.

### Utilisation

Maintenant que Flameshot est installé et configuré, voici un exemple des captures qu'il est possible de réaliser avec cet outil :

![Exemple de capture Flameshot](/images/flameshot-3.png)

L'ensemble de la capture a été réalisé directement depuis le menu ouvert par flameshot lors du lancement de la capture : 

![Menu capture de Flameshot](/images/flameshot-4.png)

On peut donc (liste non exhaustive) :
* Flouter
* Encadrer
* Cacher
* Faire des flèches
* Faire des traits
* Numéroter
* Ajouter du texte
* ...

Et tout ça sans avoir à ouvrir un outil tier. On peut également copier, enregistrer, ouvrir avec un autre outil...

Tout ça pour dire que je suis vraiment fan de ce logiciel et que je l'installe aujourd'hui sur l'ensemble de mes machines.

L'article n'est pas incroyablement technique, le but était simplement de présenter l'outil. J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !




