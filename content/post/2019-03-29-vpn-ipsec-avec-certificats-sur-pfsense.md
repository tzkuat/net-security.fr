---
title: VPN IPSec avec certificats sur PFSense
author: Mickael Rigonnaux
type: post
date: 2019-03-28T23:36:05+00:00
url: /system/vpn-ipsec-avec-certificats-sur-pfsense/
thumbnail: /images/pfsense-site_to_site-ipsec/pfsense-logo.png
featureImage: images/pfsense-site_to_site-ipsec/pfsense-logo.png
shareImage: /images/pfsense-site_to_site-ipsec/pfsense-logo.png
tags: [
    "PFSense",
    "VPN",
    "cryptographie",
    "chiffrement"
]
categories: [
    "securite",
    "logiciellibre",
    "reseau"
]

---
![](/images/pfsense-site_to_site-ipsec/bannierez.png)

Bonjour à tous ! Aujourd'hui nous allons découvrir ensemble comment installer un VPN IPSec avec des certificats entre deux routeurs [PFSense][1] ! 

## Présentation

Tout d'abord une rapide présentation de PFSense, c'est un routeur/firewall Open Source (licence Apache 2.0) développé par la société [Netgate](https://www.netgate.com/). Ce dernier est basé sur FreeBSD & est un fork du logiciel m0n0wall. Plus d'informations [ici](https://www.pfsense.org/).

Présentons maintenant rapidement le [VPN][2] ou Virtual Private Network. C'est un système permettant de créer un lien direct entre deux réseaux ou deux équipements sur le Internet. Plus concrètement, cela permet de créer un « tunnel » entre deux réseaux privés en passant par Internet, il doit donc assurer plusieurs fonctions comme la confidentialité, l'intégrité et l'authentification. Voici un schéma : 

![](/images/pfsense-site_to_site-ipsec/vpn-1.png)

Il existe plusieurs types de VPN, mais dans cet article nous allons seulement aborder le VPN site à site.

## Présentation de la plateforme 

Voici un schéma de la plateforme mise en place pour cet article : 

![](/images/pfsense-site_to_site-ipsec/schema_infra-1.png)

Nous pouvons donc voir les différents réseaux : 

||||
|--- |--- |--- |
|Réseau|Adressage|Passerelle|
|LAN PFSense-22|172.22.0.0/24|172.22.0.254|
|LAN PFSense-18|172.18.0.0/24|172.18.0.254|
|WAN (simulé)|10.0.0.0/24|——————|

Voici également les différents équipements de l'infrastructure : 

||||||
|--- |--- |--- |--- |--- |
|Nom|Interface|IP|Passerelle|OS|
|Ubuntu01|LAN|172.22.0.1|172.22.0.254|Ubuntu 18.04 LTS|
|Ubuntu02|LAN|172.18.0.1|172.18.0.254|Ubuntu 18.04 LTS|
|PFSense-22|LAN/WAN|172.22.0.254/10.0.0.1|—————|PFSense 2.4.4|
|PFSense-18|LAN/WAN|172.18.0.254/10.0.0.2|—————|PFSense 2.4.4|

L'ensemble de la plateforme a été réalisée avec VMWare Workstation 15 sur une machine Ubuntu 18.04 LTS. Les différents réseaux créés sont des réseaux NAT avec accès à l'hôte pour faciliter la configuration et les tests. 

## Mise en œuvre 

Tout d'abord il faut installer les 2 firewalls et configurer les cartes réseaux, vous aurez un résultat similaire à celui-ci :

![](/images/pfsense-site_to_site-ipsec/interfaces-22.png)

![](/images/pfsense-site_to_site-ipsec/interfaces-18.png)

### Création & installation des certificats

Nous allons maintenant générer les différents certificats nécessaires à la mise en place du VPN. Il faudra générer 3 certificats : 

  1. L'autorité de certification
  2. Le certificat serveur pour le PFSense-22
  3. Le certificat serveur pour le PFSense-18

Les certificats générés pour ce cas sont des certificats x509 basé sur le protocole RSA. Vu que nous n'avons pas d'autorité de certification nous allons en créer une sur le PFSense-22 et générer les certificats depuis ce dernier. 

Pour se faire il faut se rendre dans « System » -> « Cert. Manager » sur le PFSense-22. Dans l'onglet « CAs » nous allons nous rendre dans « Add » et créer une autorité de la manière suivante : 

![](/images/pfsense-site_to_site-ipsec/create_ca.png)

Nous avons renseignés ici seulement les paramètres « Descriptive name » et « Common Name ». Il faut savoir que nous créons une autorité racine dans notre cas, c'est pour cela qu'elle est nommée « root ». Les autres paramètres concernent, la durée de vie de la clé et les différents algorithmes comme la longueur de la clé RSA (2048) et l'algorithme de hashage (SHA256). Nous pouvons laisser les paramètres par défaut car les algorithmes sont toujours considérés comme sûrs. 

Après avoir créé la CA nous pouvons créer les certificats pour nos deux firewalls, tout d'abord le PFSense-22. Il faut se rendre dans l'onglet « Certificates » 

![](/images/pfsense-site_to_site-ipsec/create_certificate_22.png)

Nous voyons maintenant que notre certificat sera signé par l'autorité créée juste au-dessus. Nous renseignons le « Common Name », dans notre cas nous mettons simplement le nom du firewall PFSense-22 car nous n'avons pas de nom DNS pour ce dernier, si vous en avez un entrez-le. Comme pour l'autorité nous laissons les algorithmes par défaut (RSA 2048 & SHA256). Ce que nous changeons ici ce sont le type de certificat en mettant « Server Certificate » et on ajoute un également un « Alternative Names » aussi appelé SAN. Ce type d'enregistrement permet au certificat d'être valable pour plusieurs URLs/IPs. Nous renseignons donc l'IP du firewall PFSense-22 et nous créons le certificats. 

Il faut également réaliser l'opération pour le PFSense-18, mais attention tous les certificats sont générés sur le PFSense-22 :

![](/images/pfsense-site_to_site-ipsec/create_certificate_18.png)

Les certificats sont maintenant tous créés : 

![](/images/pfsense-site_to_site-ipsec/certificates.png)

Il faut maintenant exporter le certificat pour le PFSense-18 afin de l'uploader sur ce dernier, pour se faire il faut cliquer sur les boutons « Export Certificate » & « Export Key » : 

![](/images/pfsense-site_to_site-ipsec/export.png)

Vous aurez maintenant en votre possession la clé privée et le certificat pour le PFSense-18. Il faut également exporter la partie publique de l'autorité de certification afin de l'installer sur le PFSense-18 en utilisant le même principe. 

Vous aurez donc 3 fichiers à importer sur le second firewall, la clé privée du certificat, le certificat public et le certificat public de l'autorité ou CA. Les fichiers sont tous aux formats B64, c'est à dire qu'ils ont tous la même formes : 

```
----- BEGIN xxx -----
xxxxxxxxxxx
----- END xxx -----  
```

Pour les importer sur le PFSense-18 il faut également se rendre dans « System » -> « Cert. Manager ». 

Dans l'onglet CA il faut importer l'autorité de certification en copiant le texte du fichier dans la case dédiée : 

![](/images/pfsense-site_to_site-ipsec/import_ca.png)

Pour la CA il faut seulement renseigner le champs « Certificate Data » avec le contenu du fichier et le « Descriptive Name » et choisir l'option « Import an existing CA »

Il faut maintenant importer le certificat du routeur et sa clé privée dans l'onglet « Certificates » : 

![](/images/pfsense-site_to_site-ipsec/import_certificate.png)

Dans cet import la il faut seulement renseigner les champs du certificat et de la clé privée en ajoutant un descriptif.

Nous avons maintenant de chaque côté les bons certificats pour créer notre VPN. 

### Mise en place du VPN

Pour la mise en place du VPN, des configurations sont nécessaires sur les 2 machines, nous allons commencer par le PFSense-22. Le VPN mis en place est donc un VPN IPSec. 

Voici une présentation rapide d'IPSec : 

IPSec est un ensemble de protocoles permettant de fournir un service sécurisé grâce à la cryptographie. Il permet d'assurer l'authentification, la confidentialité & l'intégrité des données entre les 2 parties. 

#### Configuration PFSense-22

Pour la configuration du VPN il faut se rendre dans « VPN » -> « IPSec » & « Tunnels » :

![](/images/pfsense-site_to_site-ipsec/vpn_22_1.png)

La 1ère partie des configurations concerne le protocole IKE (Internet Key Exchange) qui permet l'échange des clés, dans notre cas IKEv2 est préféré pour plusieurs raisons qui sont disponibles [ici][4]. En bref, la version 2 est améliorée, elle permet un plus haut niveau de sécurité tout en étant plus rapide et moins gourmande en ressources. 

Nous choisissons également le protocole IPv4, l'interface où notre VPN sera utilisé c'est-à-dire la WAN. Nous renseignons également l'IP distante (l'IP WAN du PFSense-18). Le mode d'authentification choisi est « Mutual RSA » car il offre un meilleur niveau de sécurité que la méthode par clé partagéê (PSK). L'[ANSSI][5] préconise également la mise en place avec RSA [ici][6]. 

Il faut ensuite choisir les différents identifiants, c'est-à-dire l'IP du firewall PFSense-22 et l'IP distante (PFSense-18). Renseignez également le certificat du firewall en question ainsi que l’autorité de certification.

![](/images/pfsense-site_to_site-ipsec/vpn_22_2.png)

Dans la 2ème partie de la configuration il faut choisir les algorithmes de chiffrements et de hachage. Nous pouvons laisser les choix par défaut. 

Nous pouvons maintenant configurer la phase 2 du VPN de la manière suivante : 

![](/images/pfsense-site_to_site-ipsec/vpn_22_p2.png)

Dans cette partie il faut renseigner le réseau local « LAN » dans « Local Network » en source de notre VPN et ajouter le réseau de destination, c'est à dire le réseau 172.18.0.0/24 du PFSense-18.

Pour les algorithmes nous choisissons « ESP » (Encapsulation Security Payload » car ce dernier permet le chiffrement et l'authentification aec « AH » (Authentication Header) alors que ce dernier seul n'assure que l'authentification. 

#### Configuration PFSense-18

Pour la configuration du second la configuration est presque similaire, il suffit juste de refaire la même chose en adaptant les paramètres : 

![](/images/pfsense-site_to_site-ipsec/vpn_18_1.png)

Maintenant la partie 2 de la configuration : 

![](/images/pfsense-site_to_site-ipsec/vpn_18_2.png)

Nous pouvons maintenant faire la phase 2 du protocole IPSec : 

![](/images/pfsense-site_to_site-ipsec/vpn_18_p2.png)

La configuration du protocole est maintenant terminée. Nous pouvons passer aux règles de flux. 

### Règles de flux

Le sujet de cet article n'est pas la gestion des règles, nous allons juste vérifier que sur chacun de nos PFSense, les règles LAN sont les suivantes dans « Firewall » -> « Rules » -> « LAN » : 

![](/images/pfsense-site_to_site-ipsec/fw_rules.png)

Si c'est le cas de chaque côté, vous pouvez ajouter une règle any/any sur l'onglet « IPSec » de chaque côté. Cette règle est mise en place à des fins de test uniquement, ne pas reproduire en production ! 

![](/images/pfsense-site_to_site-ipsec/fw_ipsec.png)

## Activation & tests

Nous pouvons maintenant activer notre VPN, pour se faire il faut se rendre sur un des deux PFSense dans « Status » -> « IPSec » et vous pourrez activer votre connexion avec le bouton « Connect VPN » : 

![](/images/pfsense-site_to_site-ipsec/staus_vpn.png)

Si le VPN est fonctionnel vous devriez avoir une vue comme celle-ci sur chacun de vos PFSense après avoir cliqué sur le bouton « Connect VPN » :

![](/images/pfsense-site_to_site-ipsec/vpn_active.png)

Nous pouvons maintenant tester notre VPN en envoyant un ping depuis la machine Ubuntu01 vers la machine Ubuntu02. Cela peut-être un peu long, le temps que le tunnel soit monté : 

![](/images/pfsense-site_to_site-ipsec/vpn-ping-22.png)

Maintenant dans l'autre sens : 

![](/images/pfsense-site_to_site-ipsec/vpn-ping-18.png)

Afin de s'assurer que le trafic passe bien par le VPN et non pas par le réseau directement, vous pouvez lancer une capture de trames sur le réseau WAN que vous avez simulé. De mon côté, aucune trace des requêtes ICMP, ce qui veut dire que le VPN fonctionne bien : 

![](/images/pfsense-site_to_site-ipsec/wireshark-ping.png)

Et pour finir un dernier test avec l'outil netcat afin de simuler une connexion HTTPS de la machine Ubuntu01 vers la machine Ubuntu02. Dans un premier temps nous mettons la machine Ubuntu02 en écoute sur le port 443 :

![](/images/pfsense-site_to_site-ipsec/vpn-test-nc-1.png)

Nous lançons ensuite une connexion sur ce port depuis la machine Ubuntu01 et envoyons une donnée de test « test-vpn » : 

![](/images/pfsense-site_to_site-ipsec/vpn-test-nc2.png)

Et nous pouvons voir que cette donnée est bien reçu par le serveur Ubuntu02 : 

![](/images/pfsense-site_to_site-ipsec/vpn-test-nc3.png)

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.pfsense.org/
 [2]: https://fr.wikipedia.org/wiki/R%C3%A9seau_priv%C3%A9_virtuel
 [3]: images/pfsense-site_to_site-ipsec/vpn_22_1.png
 [4]: http://rockhoppervpn.sourceforge.net/techdoc_ikev1vsikev2.html
 [5]: https://www.ssi.gouv.fr/
 [6]: https://www.ssi.gouv.fr/uploads/2012/09/NT_IPsec.pdf