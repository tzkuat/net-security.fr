---
title: "Vérifier l'ouverture d'un port sous GNU/Linux"
author: Mickael Rigonnaux
type: post
date: 2021-07-11T00:14:38+00:00
url: /reseau/verifier-louverture-dun-port-sous-gnu-linux/
thumbnail: /images/tux-1.png
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "LogicielLibre"
]
categories: [
    "systeme",
    "reseau"
]
---

Aujourd'hui un article assez basique sur un sujet plutôt simple mais qui concerne tout le monde : vérifier si un port est bien ouvert sur des machines GNU/Linux.

## Introduction

Ce sujet est plutôt simple mais je pense qu'il concerne tout le monde, car c'est la base pour vérifier si un service est fonctionnel ou non. Cela permet de tester vos règles de pare-feu ainsi que les différents accès réseaux, ces tâches sont le quotidien de la plupart des admninistrateurs réseau/système. On verra d'abord comment vérifier directement sur la machine si le port est en écoute, s'il existe des connexions établies et pour finir on verra comment tester les flux vers des machines distantes. Nous n'aborderons pas le filtrage des flux sur les pare-feu car ce n'est pas le sujet de l'article.

Dans cet article nous allons utiliser deux machines sous GNU/Linux Red Hat Enterprise Linux 8 pour les tests :

  * Machine 1 : 192.168.3.1 - p-awx-01
  * Machine 2 : 192.168.3.4 - p-docker-01

## Vérification des ports en écoute

Tout d'abord nous allons vérifier les ports en écoute sur la machine 192.168.3.4, il y en a normalement plusieurs. Pour cela nous pouvons utiliser plusieurs commandes, dans mon cas j'utilise `ss` ou `netstat`. Selon votre distribution vous aurez au moins un des deux outils présent.

### netstat / ss

Vérification des ports TCP en écoute avec `netstat -lnt` ou `ss -lnt` : 

```
[root@p-docker-01 ~]# netstat -lnt
Connexions Internet actives (seulement serveurs)
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat
tcp        0      0 0.0.0.0:8081            0.0.0.0:*               LISTEN
tcp        0      0 192.168.3.4:8082        0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:8083            0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:8088            0.0.0.0:*               LISTEN
tcp6       0      0 :::22                   :::*                    LISTEN

[root@p-docker-01 ~]# ss -lnt
State                       Recv-Q                      Send-Q                                           Local Address:Port                                            Peer Address:Port
LISTEN                      0                           128                                                  127.0.0.1:199                                                  0.0.0.0:*
LISTEN                      0                           128                                                    0.0.0.0:8081                                                 0.0.0.0:*
LISTEN                      0                           128                                                192.168.3.4:8082                                                 0.0.0.0:*
LISTEN                      0                           128                                                    0.0.0.0:8083                                                 0.0.0.0:*
LISTEN                      0                           128                                                    0.0.0.0:22                                                   0.0.0.0:*
LISTEN                      0                           128                                                    0.0.0.0:8088                                                 0.0.0.0:*
LISTEN                      0                           128                                                       [::]:22                                                      [::]:*
```

Vous remarquerez que l'on retrouve les connexions IPv4 & IPv6. On ne parlera que de IPv4 dans l'article. Les ports suivants sont en écoute : 8081, 8082, 8083, 22 & 8088.

Au niveau des options nous avons :

  * `-l` pour listen 
  * `-n` pour ne pas résoudre le nom des ports
  * `-t` pour TCP

De mon côté j'ai tendance à utiliser les arguments `-lntp` pour afficher le PID et le nom du programme :

```
[root@p-docker-01 ~]# netstat -lntp
Connexions Internet actives (seulement serveurs)
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat        PID/Program name
tcp        0      0 0.0.0.0:8081            0.0.0.0:*               LISTEN      87115/docker-proxy
tcp        0      0 192.168.3.4:8082        0.0.0.0:*               LISTEN      197733/docker-proxy
tcp        0      0 0.0.0.0:8083            0.0.0.0:*               LISTEN      88320/docker-proxy
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      854/sshd
tcp        0      0 0.0.0.0:8088            0.0.0.0:*               LISTEN      28833/docker-proxy
tcp6       0      0 :::22                   :::*                    LISTEN      854/sshd

[root@p-docker-01 ~]# ss -lntp
State               Recv-Q              Send-Q                           Local Address:Port                             Peer Address:Port
LISTEN              0                   128                                  127.0.0.1:199                                   0.0.0.0:*                  users:(("snmpd",pid=853633,fd=10))
LISTEN              0                   128                                    0.0.0.0:8081                                  0.0.0.0:*                  users:(("docker-proxy",pid=87115,fd=4))
LISTEN              0                   128                                192.168.3.4:8082                                  0.0.0.0:*                  users:(("docker-proxy",pid=197733,fd=4))
LISTEN              0                   128                                    0.0.0.0:8083                                  0.0.0.0:*                  users:(("docker-proxy",pid=88320,fd=4))
LISTEN              0                   128                                    0.0.0.0:22                                    0.0.0.0:*                  users:(("sshd",pid=854,fd=5))
LISTEN              0                   128                                    0.0.0.0:8088                                  0.0.0.0:*                  users:(("docker-proxy",pid=28833,fd=4))
LISTEN              0                   128                                       [::]:22                                       [::]:*                  users:(("sshd",pid=854,fd=7))
```

Même principe avec UDP en utilisant `netstat -lnu` ou `ss -lnu` :

```
[root@p-docker-01 ~]# netstat -lnu
Connexions Internet actives (seulement serveurs)
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat
udp        0      0 0.0.0.0:53              0.0.0.0:*
udp        0      0 0.0.0.0:161             0.0.0.0:*
udp6       0      0 :::53                   :::*

[root@p-docker-01 ~]# ss -lnu
State                       Recv-Q                      Send-Q                                           Local Address:Port                                            Peer Address:Port
UNCONN                      0                           0                                                      0.0.0.0:53                                                   0.0.0.0:*
UNCONN                      0                           0                                                      0.0.0.0:161                                                  0.0.0.0:*
UNCONN                      0                           0                                                         [::]:53                                                      [::]:*
```

On voit bien les ports 161 (snmp) et 53 (DNS, mais ouvert avec nc pour le test). Avec les arguments `-lnup` :

```
[root@p-docker-01 ~]# netstat -lnup
Connexions Internet actives (seulement serveurs)
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat        PID/Program name
udp        0      0 0.0.0.0:53              0.0.0.0:*                           826544/nc
udp        0      0 0.0.0.0:161             0.0.0.0:*                           853633/snmpd
udp6       0      0 :::53                   :::*                                826544/nc

[root@p-docker-01 ~]# ss -lnup
State                Recv-Q               Send-Q                             Local Address:Port                              Peer Address:Port
UNCONN               0                    0                                        0.0.0.0:53                                     0.0.0.0:*                   users:(("nc",pid=826544,fd=4))
UNCONN               0                    0                                        0.0.0.0:161                                    0.0.0.0:*                   users:(("snmpd",pid=853633,fd=9))
UNCONN               0                    0                                           [::]:53                                        [::]:*                   users:(("nc",pid=826544,fd=3))
```

Pour terminer, on peut également regrouper l'ensemble des arguments pour avoir toutes les informations : `-lntup`

```
[root@p-docker-01 ~]# netstat -lntup
Connexions Internet actives (seulement serveurs)
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat        PID/Program name
tcp        0      0 127.0.0.1:199           0.0.0.0:*               LISTEN      853633/snmpd
tcp        0      0 0.0.0.0:8081            0.0.0.0:*               LISTEN      87115/docker-proxy
tcp        0      0 192.168.3.4:8082        0.0.0.0:*               LISTEN      197733/docker-proxy
tcp        0      0 0.0.0.0:8083            0.0.0.0:*               LISTEN      88320/docker-proxy
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      854/sshd
tcp        0      0 0.0.0.0:8088            0.0.0.0:*               LISTEN      28833/docker-proxy
tcp6       0      0 :::22                   :::*                    LISTEN      854/sshd
udp        0      0 0.0.0.0:53              0.0.0.0:*                           826544/nc
udp        0      0 0.0.0.0:161             0.0.0.0:*                           853633/snmpd
udp6       0      0 :::53                   :::*                                826544/nc

[root@p-docker-01 ~]# ss -lntup
Netid            State             Recv-Q            Send-Q                       Local Address:Port                        Peer Address:Port
udp              UNCONN            0                 0                                  0.0.0.0:53                               0.0.0.0:*                users:(("nc",pid=826544,fd=4))
udp              UNCONN            0                 0                                  0.0.0.0:161                              0.0.0.0:*                users:(("snmpd",pid=853633,fd=9))
udp              UNCONN            0                 0                                     [::]:53                                  [::]:*                users:(("nc",pid=826544,fd=3))
tcp              LISTEN            0                 128                              127.0.0.1:199                              0.0.0.0:*                users:(("snmpd",pid=853633,fd=10))
tcp              LISTEN            0                 128                                0.0.0.0:8081                             0.0.0.0:*                users:(("docker-proxy",pid=87115,fd=4))
tcp              LISTEN            0                 128                            192.168.3.4:8082                             0.0.0.0:*                users:(("docker-proxy",pid=197733,fd=4))
tcp              LISTEN            0                 128                                0.0.0.0:8083                             0.0.0.0:*                users:(("docker-proxy",pid=88320,fd=4))
tcp              LISTEN            0                 128                                0.0.0.0:22                               0.0.0.0:*                users:(("sshd",pid=854,fd=5))
tcp              LISTEN            0                 128                                0.0.0.0:8088                             0.0.0.0:*                users:(("docker-proxy",pid=28833,fd=4))
tcp              LISTEN            0                 128                                   [::]:22                                  [::]:*                users:(("sshd",pid=854,fd=7))
```



## Vérification des sessions établies

Cette section ne va concerner que les sessions TCP.

### netstat / ss

Comme tout à l'heure nous allons utiliser les commandes `netstat` & `ss`.

Tout d'abord avec les arguments `-natu` :

```
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat
tcp        0      0 127.0.0.1:199           0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:8081            0.0.0.0:*               LISTEN
tcp        0      0 192.168.3.4:8082        0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:8083            0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:8088            0.0.0.0:*               LISTEN
tcp        0      0 192.168.3.4:22          10.2.2.2:1609           ESTABLISHED
tcp        0     48 192.168.3.4:22          10.2.2.2:1046           ESTABLISHED
tcp        0      0 192.168.3.4:35646       172.65.251.78:443       ESTABLISHED
tcp        0      0 192.168.3.4:22          10.2.2.2:1610           ESTABLISHED
tcp        0      0 192.168.3.4:41542       172.65.251.78:443       ESTABLISHED
tcp        0      0 192.168.3.4:22          10.2.2.2:1054           ESTABLISHED
tcp6       0      0 :::22                   :::*                    LISTEN
udp        0      0 0.0.0.0:53              0.0.0.0:*
udp        0      0 0.0.0.0:161             0.0.0.0:*
udp6       0      0 :::53                   :::*

[root@p-docker-01 ~]# ss -natu
Netid                  State                   Recv-Q                  Send-Q                                     Local Address:Port                                       Peer Address:Port
udp                    UNCONN                  0                       0                                                0.0.0.0:53                                              0.0.0.0:*
udp                    UNCONN                  0                       0                                                0.0.0.0:161                                             0.0.0.0:*
udp                    UNCONN                  0                       0                                                   [::]:53                                                 [::]:*
tcp                    LISTEN                  0                       128                                            127.0.0.1:199                                             0.0.0.0:*
tcp                    LISTEN                  0                       128                                              0.0.0.0:8081                                            0.0.0.0:*
tcp                    LISTEN                  0                       128                                          192.168.3.4:8082                                            0.0.0.0:*
tcp                    LISTEN                  0                       128                                              0.0.0.0:8083                                            0.0.0.0:*
tcp                    LISTEN                  0                       128                                              0.0.0.0:22                                              0.0.0.0:*
tcp                    LISTEN                  0                       128                                              0.0.0.0:8088                                            0.0.0.0:*
tcp                    ESTAB                   0                       0                                            192.168.3.4:22                                             10.2.2.2:1609
tcp                    ESTAB                   0                       48                                           192.168.3.4:22                                             10.2.2.2:1046
tcp                    ESTAB                   0                       0                                            192.168.3.4:35646                                     172.65.251.78:443
tcp                    ESTAB                   0                       0                                            192.168.3.4:22                                             10.2.2.2:1610
tcp                    ESTAB                   0                       0                                            192.168.3.4:41542                                     172.65.251.78:443
tcp                    ESTAB                   0                       0                                            192.168.3.4:22                                             10.2.2.2:1054
tcp                    LISTEN                  0                       128                                                 [::]:22                                                 [::]:*
```

Cela permet de récupérer les ports en écoute ainsi que les connexions. Pour filtrer sur les connexions seulement il suffit d'utiliser `grep` : 

```
[root@p-docker-01 ~]# ss -natu | grep ESTAB
tcp    ESTAB    0        0            192.168.3.4:22             10.2.2.2:1609
tcp    ESTAB    0        48           192.168.3.4:22             10.2.2.2:1046
tcp    ESTAB    0        0            192.168.3.4:35646     172.65.251.78:443
tcp    ESTAB    0        0            192.168.3.4:22             10.2.2.2:1610
tcp    ESTAB    0        0            192.168.3.4:41542     172.65.251.78:443
tcp    ESTAB    0        0            192.168.3.4:22             10.2.2.2:1054

[root@p-docker-01 ~]# netstat -natu | grep ESTAB
tcp        0      0 192.168.3.4:22          10.2.2.2:1609           ESTABLISHED
tcp        0     48 192.168.3.4:22          10.2.2.2:1046           ESTABLISHED
tcp        0      0 192.168.3.4:35646       172.65.251.78:443       ESTABLISHED
tcp        0      0 192.168.3.4:22          10.2.2.2:1610           ESTABLISHED
tcp        0      0 192.168.3.4:41542       172.65.251.78:443       ESTABLISHED
tcp        0      0 192.168.3.4:22          10.2.2.2:1054           ESTABLISHED
```

Dans ces connexions, nous voyons bien plusieurs choses :

  * Des flux https sortants
  * Des flux ssh entrants (ma connexion VPN)

Cette partie a été présentée avant car elle permettra de vérifier vos connexions dans les tests à venir. Il est possible d'aller beaucoup plus loin au niveau des sessions TCP afin de voir les différents états mais ce n'est pas le but de cet article.

## Vérifier les flux

Une fois que vous avez vérifié que les ports étaient bien en écoute, vous pouvez tester directement une connexion sur l'un d'eux.

Tous les tests sont effectués dans le sens 192.168.3.1 -> 192.168.3.4.

### telnet

Attention cette méthode n'est valide que pour un port TCP.

Lorsque l'on parle de `telnet` nous pensons tous à un protocole d'administration non sécurisé, mais c'est aussi un excellent outil pour tester simplement si un port est accessible ou non.

Par exemple pour tester si le port 22 est ouvert sur la machine 192.168.3.4 vous pouvez simplement lancer `telnet 192.168.3.4 22` : 

```
[root@p-awx-01 tzkuat]# telnet 192.168.3.4 22
Trying 192.168.3.4...
Connected to 192.168.3.4.
Escape character is '^]'.
SSH-2.0-OpenSSH_8.0
```

On voit bien que la connexion est établie, et on la retrouve bien avec notre commande `ss` sur le serveur distant :

```
[root@p-docker-01 /]# ss -natu | grep ESTAB | grep 192.168.3.1
tcp    ESTAB   0        0            192.168.3.4:22          192.168.3.1:58738
```

On remarque également que le résultat est différent si la connexion n'aboutit pas :

```
[root@p-awx-01 ~]# telnet net-security.fr 2222
Trying 54.36.91.62...
```

### nc / ncat

Nous pouvons également utiliser l'outil netcat qui va permettre de tester à la fois les flux TCP & UDP.

#### Connexion en TCP

Comme pour telnet il suffit de lancer une commande suivie d'un port pour vérifier la connectivité. Vous pouvez lancer `nc -v <IP> <PORT>` pour tester simplement la connexion ou `nc -vz <IP> <PORT>` pour réaliser un scan de port :

```
[root@p-awx-01 ~]# nc -v 192.168.3.4 22
Ncat: Version 7.70 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.3.4:22.
SSH-2.0-OpenSSH_8.0
^C

[root@p-awx-01 ~]# nc -vz 192.168.3.4 22
Ncat: Version 7.70 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.3.4:22.
Ncat: 0 bytes sent, 0 bytes received in 0.01 seconds.
```

#### Connexion en UDP

Pour UDP, on pourrait penser qu'il suffit de rajouter l'argument `-u` et que le principe restera le même, mais non, car cela ne fonctionne pas et répond toujours que le port est en écoute, un exemple sur un DNS public :

```
[root@p-awx-01 ~]# nc -vzu 1.1.1.1 53
Ncat: Version 7.70 ( https://nmap.org/ncat )
Ncat: Connected to 1.1.1.1:53.
Ncat: UDP packet sent successfully
Ncat: 1 bytes sent, 0 bytes received in 2.01 seconds.

[root@p-awx-01 ~]# nc -vzu 1.1.1.1 55
Ncat: Version 7.70 ( https://nmap.org/ncat )
Ncat: Connected to 1.1.1.1:55.
Ncat: UDP packet sent successfully
Ncat: 1 bytes sent, 0 bytes received in 2.01 seconds.

[root@p-awx-01 ~]# nc -vzu 1.1.1.1 161
Ncat: Version 7.70 ( https://nmap.org/ncat )
Ncat: Connected to 1.1.1.1:161.
Ncat: UDP packet sent successfully
Ncat: 1 bytes sent, 0 bytes received in 2.01 seconds.

[root@p-awx-01 ~]# nc -vzu 1.1.1.1 1693
Ncat: Version 7.70 ( https://nmap.org/ncat )
Ncat: Connected to 1.1.1.1:1693.
Ncat: UDP packet sent successfully
Ncat: 1 bytes sent, 0 bytes received in 2.01 seconds.
```

Personnellement pour vérifier si un port UDP est ouvert, je stoppe le service en question (snmp par exemple) et j'utilise netcat des deux côtés : côté serveur pour mettre le port en écoute et côté client pour initier la connexion.

Tout d'abord on va mettre le port en écoute sur le serveur (192.168.3.4) avec la commande `nc -lu 192.168.3.4 161` ou `nc -lu 161` si vous ne spécifiez pas l'IP sur laquelle écouter. Les deux arguments vous permettent de mettre le port en écoute et d'utiliser UDP. Si on vérifie avec `netstat` depuis un autre terminal on voit bien le port en écoute : 

```
[root@p-docker-01 ~]# netstat -lnup
Connexions Internet actives (seulement serveurs)
Proto Recv-Q Send-Q Adresse locale          Adresse distante        Etat        PID/Program name
udp        0      0 192.168.3.4:161         0.0.0.0:*                           864800/nc
```

Pour terminer et vérifier la connexion, il suffit de lancer la connexion côté client et d'envoyer du texte avec la commande `nc -u 192.168.3.4 161` : 

```
[root@p-awx-01 ~]# ncat -u 192.168.3.4 161
test netcat UDP
test net-security.fr
```

Si vous retournez maintenant sur votre terminal ou le `netcat` serveur est lancé, vous devriez retrouver le texte envoyé (si la connexion est bonne bien entendu) : 

```
[root@p-docker-01 ~]# nc -lu 192.168.3.4 161
test netcat UDP
test net-security.fr
```

On voit bien que les échanges fonctionnent dans ce cas. Nous allons nous arrêter ici pour la partie netcat, on reviendra dessus dans un article dédié si cela vous intéresse.

### nmap

Il est difficile de parler d'ouverture de flux et de port sans mentionner `nmap`. Cette boite à outil permet très rapidement de vérifier si un port est ouvert. Nous n'allons pas aborder les scans de port dans cette section car ce n'est pas le but de l'article.

#### Connexion TCP

Pour vérifier si un port est ouvert il suffit de lancer la commande suivante `nmap -Pn <IP> -p <PORT>` :

```
[root@p-awx-01 ~]# nmap -Pn 192.168.3.4 -p 22
Starting Nmap 7.70 ( https://nmap.org ) at 2021-07-11 01:43 CEST
Nmap scan report for 192.168.3.4
Host is up (0.00025s latency).

PORT   STATE SERVICE
22/tcp open  ssh
MAC Address: 6A:1B:4C:97:64:C6 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.52 seconds
[root@p-awx-01 ~]# nmap -Pn 192.168.3.4 -p 2222
Starting Nmap 7.70 ( https://nmap.org ) at 2021-07-11 01:43 CEST
Nmap scan report for 192.168.3.4
Host is up (0.00026s latency).

PORT     STATE  SERVICE
2222/tcp closed EtherNetIP-1
MAC Address: 6A:1B:4C:97:64:C6 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.50 seconds
[root@p-awx-01 ~]# nmap -Pn 192.168.3.4 -p 8081
Starting Nmap 7.70 ( https://nmap.org ) at 2021-07-11 01:43 CEST
Nmap scan report for 192.168.3.4
Host is up (0.00024s latency).

PORT     STATE SERVICE
8081/tcp open  blackice-icecap
MAC Address: 6A:1B:4C:97:64:C6 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.50 seconds
```

Au niveau des arguments, on mentionne seulement de vérifier le port même si le ping ne répond pas (`-Pn`) et on spécifie le port avec `-p`. On voit bien dans les résultats que les ports 22 et 8081 sont ouverts et que le 2222 est fermé.

#### Connexion UDP

Pour UDP, le principe reste cette fois le même, on va seulement rajouter l'argument `-sU` pour spécifier qu'on cherche un port UDP. Cela donne `nmap -Pn <IP> -sU -p <PORT>` :

```
[root@p-awx-01 ~]# nmap -Pn 192.168.3.4 -sU -p 161
Starting Nmap 7.70 ( https://nmap.org ) at 2021-07-11 01:53 CEST
Nmap scan report for 192.168.3.4
Host is up (0.00030s latency).

PORT    STATE SERVICE
161/udp open  snmp
MAC Address: 6A:1B:4C:97:64:C6 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.50 seconds
[root@p-awx-01 ~]# nmap -Pn 192.168.3.4 -sU -p 22
Starting Nmap 7.70 ( https://nmap.org ) at 2021-07-11 01:53 CEST
Nmap scan report for 192.168.3.4
Host is up (0.00029s latency).

PORT   STATE  SERVICE
22/udp closed ssh
MAC Address: 6A:1B:4C:97:64:C6 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.50 seconds
[root@p-awx-01 ~]# nmap -Pn 192.168.3.4 -sU -p 2221
Starting Nmap 7.70 ( https://nmap.org ) at 2021-07-11 01:53 CEST
Nmap scan report for 192.168.3.4
Host is up (-0.15s latency).

PORT     STATE  SERVICE
2221/udp closed rockwell-csp1
MAC Address: 6A:1B:4C:97:64:C6 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.49 seconds
```

Et voilà ! On voit bien que le port 161 est ouvert et que les deux autres sont fermés.

On va s'arrêter là sur les commandes et les tests, vous pouvez maintenant voir si un port est en écoute et ouvert sur une machine.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !