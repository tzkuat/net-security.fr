---
title: "Dump de mémoire sur VMWare"
author: Mickael Rigonnaux
type: post
date: 2022-09-27T20:05:44+00:00
url: /security/dump-memoire-vmware
thumbnail: /images/DUMP-VMWARE/vmware-logo.png
shareImage: /images/DUMP-VMWARE/vmware-logo.png
featured: true
tags: [
    "vmware",
    "forensic",
    "volatility",
    "vm"
]
categories: [
    "systeme",
    "securite"
]

---

Bonjour à tous ! Aujourd'hui un article rapide qui va traiter un point simple : le dump de mémoire d'une machine virtuelle VMWare afin de l'analyser avec Volatility (ou autre) et de faire du forensic.

## Introduction

Pour tout un tas de raisons, il peut être nécessaire de récupérer la mémoire vive d'une machine virtuelle, notamment en cas de compromission car cela peut entrer dans votre procédure de "gestion des preuves" que vous devrez stocker, analyser et mettre à disposition des tiers selon la gravité de l'incident ainsi que sa nature.

Nous allons donc voir comment récupérer simplement cette mémoire afin de l'analyser.

## Dump de mémoire VMWare

Dans mon cas je vais faire ce dump depuis une "VCenter Server" dans sa version. Cela fonctionne de la même façon en 6.x et j'imagine que c'est sensiblement la même chose en version 8.

Tout d'abord il faut choisir la VM cible et la mettre en état de "pause" :
![](/images/DUMP-VMWARE/DUMP-VMWARE-1.png)


En sachant qu'il est aussi possible de réaliser l'opération sans mettre la machine en pause, il faudra alors faire un snapshot de cette dernière en ajoutant l'option pour inclure la mémoire vive.

Une fois la machine en pause, il faut maintenant se rendre dans la partie "Banque de données" et de cliquer sur la banque hébergeant la machine virtuelle.
![](/images/DUMP-VMWARE/DUMP-VMWARE-2.png)

Une fois fait il suffit de se rendre dans le dossier de votre VM et de selectionner le fichier au format `nom-vm-xxxxxx.vmem` :
![](/images/DUMP-VMWARE/DUMP-VMWARE-3.png)


Ce fichier correspond à la mémoire RAM de la machine, une fois téléchargé (il devrait faire la taille de la mémoire allouée à la machine), vous pourrez, par exemple, lancer l'outil `volatility` afin d'analyser en profondeur cette dernière, ce que nous ferons dans un prochain article !

Dans mon exemple c'est une machine Windows Server 2016.

![](/images/DUMP-VMWARE/DUMP-VMWARE-4.png)

Et voilà, vous avez maintenant la mémoire de votre machine virtuelle en un fichier, prête à être stockée ou analyser.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !
