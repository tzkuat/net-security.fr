---
title: Commandes GNU/Linux pour détecter une intrusion
author: Mickael Rigonnaux
type: post
date: 2020-09-19T13:29:58+00:00
url: /security/commandes-gnu-linux-pour-detecter-une-intrusion/
thumbnail: /images/tux-1.png
featured: true
tags: [
    "logiciellibre",
    "GNU/Linux",
    "intrusion"
]
categories: [
    "systeme",
    "logiciellibre",
    "securite"
]

---
Bonjour à tous ! Après un petit moment d'absence nous allons voir aujourd'hui comment essayer de détecter une intrusion sur un système GNU/Linux.

## Introduction

Aujourd'hui nous sommes beaucoup à rencontrer des tentatives d'intrusion sur nos SI, et ça peu importe le cœur de métier. Les intérêts peuvent être nombreux pour les attaquants, par exemple le vol de données, récupérer de la puissance de calcul, etc.

Il est important d'avoir une procédure pour décrire comment détecter et vérifier si la machine est intègre, au moins au niveau du système et des actions des utilisateurs.

De mon côté, j'ai pu utiliser les commandes suivantes, la liste n'est pas exhaustive et les différentes commandes sont sûrement améliorables avec des paramètres d'ailleurs.

## Les commandes et actions

Sauf indication contraire les commandes sont lancées avec un utilisateur ayant des droits super utilisateur.

### w & who

Les commandes `w` et `who` vont permettre dans un premier temps de voir si quelqu'un est connecté à la machine. Selon la commande lancée les informations sont différentes :

![](/images/w-who-intrusion.png)

Ici les IPs ne remontent pas car je suis en local sur ma machine.

### last & lastb

Les systèmes GNU/Linux gardent également des traces des connexions. Cela peut aller de l'utilisateur, l'IP et même la durée de la connexion.

Vous pouvez récupérer ces informations avec la commande `last` :

![](/images/last-intrusion.png)

Comme pour w & who les IPs ne remontent pas car je suis en local sur ma machine, mais en cas de connexion distante l'IP est affichée.

La commande `lastb` affiche de son côté les tentatives de connexion erronées avec les mêmes informations. Pour que cela soit plus exhaustif j'ai lancé la commande `lastb` sur le VPS hébergeant l'instance Mattermost de Net-Security :

![](/images/lastb-intrusion.png)


### Les utilisateurs et shells

#### history

Si un attaquant arrive à prendre la main sur un utilisateur de votre machine il a de grande chance qu'il laisse des traces.

Vous pouvez vérifier les commandes lancées par un utilisateur en utilisant la commande `history` depuis l'utilisateur en question.

Exemple avec mon utilisateur :

![](/images/image-13.png)

Pour le faire pour un utilisateur différent il suffit de se connecter en son nom et de lancer la même commande :


```
su user1
history
```


L'ensemble des commandes sont tracées dans un fichier qui se nomme `.bash_history` et qui se trouve à la racine du dossier de l'utilisateur. Vous pouvez également le consulter de la façon suivante :


```
cat /home/user/.bash_history
```


#### /etc/passwd & /etc/shells

Si un attaquant dispose de droit élevé il sera en capacité de créer son propre utilisateur. Nous allons donc vérifier les utilisateurs du système avec les commandes suivantes :


```
less /etc/passwd
cat /etc/passwd
```


![](/images/image-14.png)


Vous pouvez également filtrer seulement le nom des utilisateurs avec la commande suivante :


```
awk -F':' '{ print $1}' /etc/passwd
```


Ou bien afficher seulement les utilisateurs pouvant utiliser un shell :


```
cat /etc/passwd | grep /bin/bash (ou zsh, sh, etc.)
```


Vous pouvez récupérer la liste des shells disponibles sur votre machine de la façon suivante :


```
cat /etc/shells
```


![](/images/image-15.png)


### Les processus

Lorsqu'un attaquant prend la main sur votre machine son but va être de la garder ou bien d'installer un programme malveillant comme un mineur, un botnet, etc.

Pour vérifier cela vous pouvez utiliser les commandes permettant de lister les processus.

Comme la commande `ps` (process status) qui va permettre de lister l'ensemble des processus de la machine :


```
ps
ps aux
```


Vous pouvez également utiliser la commande `top` qui grâce à son interface permettre également de surveiller les consommations de la machine et d'afficher les processus les plus gourmands en ressources.


```
top
```


![](/images/image-16.png)


Pour finir vous pouvez également lancer la commande `lsof`. Cette dernière permet grâce à ses options de retrouver des informations sur les processus.

Afficher les connexions ouvertes par un processus :


```
lsof -i
lsof -i -p <pid-proc>
```


Afficher les fichiers ouverts par un processus :


```
lsof -l
lsof -p <pid-proc>
```


### ss & netstat

L'utilitaire `netstat` est présent sur presque toutes les distributions mais il est aujourd'hui déprécié. Il va permettre de vérifier des informations comme les ports exposés, les routes, etc.

Un attaquant pour garder accès à votre machine est capable d'exposer un port TCP ou UDP afin d'avoir un autre moyen de se connecter à la machine.


```
netstat -lntup (liste les ports TCP/UDP exposés avec les services)
netstat -r (liste les routes)
netstat -s (affiche les stats des cartes)
```

![](/images/image-17.png)

L'utilitaire `ss` de son côté reste très proche de `netstat` au niveau des fonctionnalités. Il est présent nativement sur les dernières distributions et est le remplaçant de `netstat`.

Il reste possible de récupérer les ports exposés avec cet outil :


```
ss -lntup
ss -s (affiche les stats de la carte)
```

![](/images/image-18.png)


### Les cartes réseau et les routes

Au niveau du réseau, le trafic peut être redirigé par l'attaquant et les cartes réseaux modifiées (ajout, suppression, etc.).

Vous pouvez vérifier les informations au niveau des cartes avec la commande `ip` :


```
ip address
ip a
```

![](/images/image-19.png)

Les routes peuvent également être affichées grâce à la même commande :


```
ip route
```

![](images/image-20.png)

### crontab

L'utilisation de la fonctionnalité `crontab` est fréquente dans le cas d'une intrusion. Personnellement j'ai déjà vu plusieurs serveurs utilisant un `crontab` pour relancer leurs mineurs ou leurs scanners.

Il faut donc vérifier les différents utilisateurs pour être sûr qu'aucune tâche malicieuse ne se relance automatiquement.

Pour cela il suffit d'utiliser la commande `crontab` avec les différentes options proposées.

Vérification de l'utilisateur courant :


```
crontab -l
```


Vérification d'un autre utilisateur :


```
crontab -u user -l
```


Il est aussi possible de voir les tâches planifiées par heure, jour ou semaine :


```
ls -la /etc/cron.daily
ls -la /etc/cron.hourly
ls -la /etc/cron.weekly
```


### Les fichiers modifiés

Si vous êtes victimes d'une intrusion des fichiers seront probablement modifiés sur votre système. Il est possible de les lister avec l'outil très puissant `find`.

Voici deux exemples à adapter selon vos besoins.

Retrouver tous les fichiers créés/modifiés les 5 derniers jours :


```
find / -mtime -5 -ctime -5
```


Retrouver les fichiers modifiés dans la dernière minute :

```
find / -mmin -1
```

La commande `find` permet également de récupérer les fichiers en fonction des UID des utilisateurs mais je ne maîtrise pas encore toutes ses subtilités.

Vous trouverez plus d'informations [ici][2]. 

### Les logs

Même si cela paraît évident, les logs doivent être analysées en cas d'intrusion ou de tentative d'intrusion.

Sauf indication contraire elles sont présentes dans `/var/log` et vous pouvez les analyser avec différents outils comme : `tail`, `cat`, `less`, `grep`&#8230;

L'analyse des logs dépendra forcément de votre système et de vos applicatifs mais voici quelques exemples :

```
cat /var/log/syslog
cat /var/log/syslog | less
tail -f -n 5 /var/log/syslog
cat /var/log/syslog | grep fail
tail -f /var/log/syslog
```

### Edit

Suite à un retour d'un ancien collègue Amine [@_1mean][3] les éléments suivants peuvent également être effectués.

#### Vérifier les clés SSH

Un attaquant pourra également ajouter sa clé SSH au niveau des utilisateurs de la machine histoire de garder un accès permanent.

Il faut donc vérifier `.ssh/authorized_keys` des utilisateurs ! Par exemple :

```
cat /root/.ssh/authorized_keys
cat /home/debian/.ssh/authorized_keys
cat /home/user1/.ssh/authorized_keys
```

#### Vérifier le contenu de /tmp

Une autre remarque pertinente est de vérifier le contenu du dossier `/tmp` car si un attaquant arrive à récupérer un shell ou un accès à la machine c'est le seul endroit ou il pourra uploader ses fichiers !

Pour vérifier son contenu il faut utiliser les commandes classiques :

```
ls /tmp
ls -la /tmp
ls -la /tmp | more
less /tmp
ls -la /tmp | grep xxx
```

### Les IPs publiques

Pour terminer avec cet article vous pouvez également analyser les IPs remontées dans vos systèmes ainsi que votre IP pour savoir si elles sont considérées comme malveillante.

Si une de vos machines est utilisée pour scanner internet ou émettre du spam cela peut être le cas et il faudra engager des changements et des démarches.

Voici quelques sites qui permettent de trouver ce genre d'information :

  * <https://whatismyipaddress.com/blacklist-check>
  * <https://shodan.io>
  * <http://www.blocklist.de/en/index.html>

## Sources

  * <https://homputersecurity.com/2018/03/10/detection-d-une-intrusion-systeme-avec-des-commandes-de-base-de-linux/>
  * <https://www.system-linux.eu/index.php?post/2009/03/18/La-commande-lsof>
  * [https://linuxhint.com/determine\_if\_linux\_is\_compromised/][4]
  * <https://www.tecmint.com/35-practical-examples-of-linux-find-command/>

## Conclusion

Voilà c'est la fin de cet article dédié à la détection d'intrusion sur les systèmes GNU/Linux ! Si ça vous intéresse tous les tests ont été réalisés sur ma machine ArchLinux ainsi que sur un VPS utilisant Debian 10 !

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

 [1]: images/image-13.png
 [2]: https://www.tecmint.com/35-practical-examples-of-linux-find-command/
 [3]: https://twitter.com/_1mean
 [4]: https://linuxhint.com/determine_if_linux_is_compromised/