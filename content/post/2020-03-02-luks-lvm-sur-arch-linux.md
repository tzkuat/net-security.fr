---
title: 'LUKS & LVM sur Arch Linux'
author: Mickael Rigonnaux
type: post
date: 2020-03-02T08:15:33+00:00
url: /system/luks-lvm-sur-arch-linux/
thumbnail: /images/arch_logo-1.png
tags: [
    "cryptographie",
    "GNU/Linux",
    "LUKS",
    "Arch"
]
categories: [
    "systeme",
    "securite",
    "logiciellibre"
]

---
Bonjour à tous ! Aujourd'hui un article sur un point qui m'a fait perdre une grosse partie de mon dimanche après midi, la mise en place du chiffrement avec LUKS sur mes partitions [Arch Linux][1]. 

Cette partie est un complément à [mon article récent][2] expliquant comment installer Arch Linux.

## Pourquoi ? 

La raison est simple, j'ai envie que mes données soient chiffrées et protégées sur mon poste. Après avoir regardé les différentes méthodes disponibles, celle présentée ici me paraît la plus adaptée à mon besoin. 

Elle permet de chiffrer plusieurs partitions LVM avec la même clé et de toutes les déchiffrer d'un coup au démarrage, il suffit de rentrer la passphrase de la clé lorsqu'elle est demandée. 

## LUKS ?

LUKS ou Linux Unified Key Setup est le standard du noyau Linux pour le chiffrement des disques. Il permet de chiffrer des disques et des partitions. Il supporte également la gestion de plusieurs mots de passe si la machine doit être utilisée par plusieurs personnes. 

Cryptsetup est le logiciel qui permet d'implémenter LUKS sous Linux, il sera utilisé dans cet article. 

## LVM ? 

LVM ou Logical Volume Manager est un système qui permet de créer et de gérer des volumes logiques. Cela permet de remplacer le partitionnement en le rendant beaucoup plus souple car il ne se soucie pas de l&#8217;emplacement du disque. 

Ce sujet a déjà été traité par Fabio sur ce blog, si ça vous intéresse l'article avec les commandes est [disponible ici][3].

## Installation

Pour cette partie, on va repartir de l'installation d'Arch et on va s'arrêter une fois que le chiffrement sera en place et fonctionnel, pour le reste je vous invite à voir mon article précédent. 

Tout d'abord, je précise que je suis toujours dans une configuration Legacy/BIOS, pour une mise en place avec EFI la configuration changera un petit peu. 

Dans mon cas je vais utiliser une machine virtuelle avec Virtual Box et un disque de 80Go qui sera nommé « /dev/sda ». 

Les caractéristiques de la machine : 

  * CPU : 1vCPU
  * RAM : 4 Go
  * DD : 80 Go

Le schéma de partitionnement est le suivant : <figure class="wp-block-table">

| Partition  | Taille | Utilisation    | Chiffré |
| :--------- |:-------|:---------------|:--------|
| /dev/sda1  | 1Go    | Boot ("/boot") | Non     |
| /dev/sda2  | 79Go   | LVM            | Oui     |


La partition /dev/sda2 sera elle-même découpée de la façon suivante avec LVM : <figure class="wp-block-table">

| Partition         | Taille | Utilisation    |
| :---------------- |:-------|:---------------|
| /dev/mapper/swap  | 4Go    | SWAP           |
| /dev/mapper/root  | 30Go   | Root "/"       |
| /dev/mapper/home  | %      | home "/home    |

Je pars donc du principe que vous avez déjà consulté mon article précédent, nous arrivons donc sur le shell d'Arch après avoir lancé le live CD : 

![](/images/luks_1.png)

On va commencer par charger le clavier français : 


```
loadkeys fr
```


Nous pouvons maintenant afficher les disques disponibles : 

```
fdisk -l
```

![](/images/luks_2.png)

Nous avons bien notre disque de 80 Go. Pour la partie partitionnement, nous allons utiliser l'outil « cfdisk » et pas « fdisk ». Cet outil permet d'avoir une sorte d'interface interactive pour le partitionnement, ce qui est très pratique. 

Lors du premier lancement il faut choisir « dos » et entrer : 

![](/images/luks_3.png)

L'interface a normalement cette forme : 

![](/images/luks_4.png)

Dans cette partie il faut créer seulement deux partitions, le résultat doit être le suivant : 

![](/images/luks_5.png)

Pour vérifier que les partitions sont bien présentes vous pouvez lancer une nouvelle fois la commande : 


```
fdisk -l
```


Après avoir créé les partitions, nous pouvons lancer la création le conteneur LUKS sur la partition /dev/sda2 :


```
cryptsetup luksFormat /dev/sda2
```

![](/images/luks_6.png)

**ATTENTION** : la passphrase que vous entrez ici est celle qui vous permettra de déchiffrer vos données, si vous la perdez vos données seront perdues. 

Les paramètres par défaut sont utilisés pour LUKS dans cet exemple. Les algorithmes utilisés sont les suivants : 

  * AES-XTS 256
  * SHA256

Vous pouvez sélectionner vous-mêmes les algorithmes si vous le souhaitez, les explications sont [disponibles ici][4]. 

Il faut maintenant ouvrir ce conteneur : 


```
cryptsetup open /dev/sda2 cryptlvm
```

La partition sera montée déchiffrée dans /dev/mapper/cryptlvm. 

Nous pouvons maintenant lancer le partitionnement avec LVM. Création du volume physique : 

```pvcreate /dev/mapper/cryptlvm ```

Puis le groupe de volumes, dans mon cas il s'appelle lvmGroup mais appelez le comme vous voulez : 


```
pvcreate lvmGroup /dev/mapper/cryptlvm
```


Pour terminer avec LVM, les volumes logiques : 

```
lvcreate -L 4G lvmGroup -n swap
lvcreate -L 30G lvmGroup -n root
lvcreate -l 100%FREE lvmGroup -n home
```

![](/images/luks_7.png)

Il faut maintenant formater les volumes : 

```
mkfs.ext4 /dev/lvmGroup/root
mkfs.ext4 /dev/lvmGroup/home 
mkswap /dev/lvmGroup/swap
```

Et les monter : 

```
mount /dev/lvmGroup/root /mnt
mkdir /mnt/home 
mount /dev/lvmGroup/home /mnt/home
swapon /dev/lvmGroup/swap 
```

Sur la partie « boot » il faut également préparer la partition /dev/sda1 : 

```
mkfs.ext4 /dev/sda1
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
```

Pour vérifier le partitionnement vous pouvez lancer la commande suivante : 


```
lsblk
```

![](/images/luks_14.png)

Pour continuer, il faut réaliser l'installation de base d'Arch présentée dans [mon autre article][2], je vous invite à le suivre en sautant la partie partitionnement et jusqu'à la commande :


```
pacman -S grub os-prober
```

A partir de cette commande vous allez lancer l'installation de grub : 

```
grub-install --target=i386-pc /dev/sda
```


Il faut en suite récupérer l'UUID de la partition /dev/sda2 : 


```
blkid
```


![](/images/luks_8.png)

Dans mon cas il correspond à « fb4bc0f4-1ede-46ac-96d4-8bd316ce271f ».

Il faut maintenant se rendre dans le fichier /etc/default/grub et ajouter la ligne suivante avec l'UUID récupéré dans la partie « GRUB\_CMDLINE\_LINUX » : 


```
cryptdevice=UUID=fb4bc0f4-1ede-46ac-96d4-8bd316ce271f:cryptlvm root=/dev/lvmGroup/root
```


![](/images/luks_9-1.png)

Il faut également dé-commenter la ligne « GRUB\_DISABLE\_LINUX_UUID=true ». 

Cette partie permettra de lancer le déchiffrement de la partition au démarrage de grub. 

Sur la partie kernel, il faut tout d'abord installer le paquet lvm2 : 

```
pacman -S lvm2
```

Il faut en suite rajouter les entrées suivantes dans la partie HOOKS du fichier /etc/mkinitcpio.conf : 

  * encrypt 
  * lvm2

Le résultat doit être le suivant : 

![](/images/luks_10.png)

Il reste maintenant à créer la configuration de grub et à installer les modules sur le kernel : 

```
grub-mkconfig -o /boot/grub/grub.cfg
```

![](/images/luks_11.png)

```
mkinitcpio -p linux
```


![](/images/luks_12.png)

Vous pouvez maintenant sortir du chroot et relancer la machine pour voir si la configuration est bonne : 

```
exit
umount -R /mnt
reboot
```

Vous devriez retrouver l'écran de grub : 

![](/images/arch_12.png)

Suivi de l'écran demendant la passphrase de votre clé : 

![](/images/luks_15.png)

Après avoir entré votre passphrase vous pourrez accéder normalement à votre instance Arch Linux ! 

**ATTENTION EDIT** : je viens de me rendre compte que l'écran demandant le mot de passe utilise le clavier QWERTY et pas AZERTY. Je regarde pour changer ça dès que je peux. 

Il existe beaucoup d'autres façons de chiffrer les disques ou les partitions, comme indiqué plus haut, cette méthode est la plus adaptée à mon besoin. Si ça vous intéresse, la documentation d'Arch Linux en présente plusieurs sur [ce lien][5]. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.archlinux.org/
 [2]: https://net-security.fr/system/arch-linux-pourquoi-et-comment/
 [3]: https://net-security.fr/system/les-commandes-lvm/
 [4]: https://wiki.archlinux.org/index.php/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode
 [5]: https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system