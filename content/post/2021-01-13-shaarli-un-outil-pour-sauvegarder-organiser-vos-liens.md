---
title: 'Shaarli, un outil pour sauvegarder & organiser vos liens'
author: Mickael Rigonnaux
type: post
date: 2021-01-13T18:59:24+00:00
url: /logiciel-libre/shaarli-un-outil-pour-sauvegarder-organiser-vos-liens/
thumbnail: /images/shaarli-2.png
featured: true
tags: [
    "logiciellibre",
    "GNU/Linux",
    "veille",
    "Shaarli",
    "Debian"
]
categories: [
    "systeme",
    "logiciellibre"
]

---

Bonjour à tous ! Aujourd'hui un article sur un outil très intéressant que j'utilise tous les jours depuis maintenant 2 ans. Il me sert essentiellement pour sauvegarder et partager des liens dans le cadre de ma veille technologique, il s'agit de [Shaarli][1].  


## Shaarli ?

Shaarli est un outil qui permet de sauvegarder, gérer, d'organiser et de partager ses marques pages. Il se veut minimaliste, simple et sans base de données pour fonctionner. Il est bien entendu proposé sous licence libre GPLv3 et a été créé par Seb Sauvage. Le logiciel est développé principalement en PHP/Javascript.

Le repository du projet est disponible sur Github à l'adresse suivante :

  * <https://github.com/shaarli/Shaarli>

Il permet plus précisément d'enregistrer des liens en ajoutant les éléments suivants :

  * Titre
  * Tag (pour les classer)
  * Des notes (au format markdown)

Vous pouvez enregistrer ces liens directement depuis votre navigateur préféré avec une l'extension dédiée. Le but de Shaarli est de partager, les liens que vous enregistrer sont donc publics (si votre instance est exposée bien entendu), mais vous avez également la possibilité d'ajouter des liens privés.

Pour vous donner une idée, une instance de démo est disponible à l'adresse suivante avec le login demo/demo :

  * <https://demo.shaarli.org/?>

Si vous cherchez des instances Shaarli plus fournies je rajoute l'instance de Sebsauvage, de Pofilo ainsi que la mienne :

  * <https://links.tzku.at>
  * <https://sebsauvage.net/links/>
  * <https://links.pofilo.fr/>

Comme vous pouvez le voir, il est très facile d'utiliser l'outil et de chercher des informations. De mon côté je ne me passe plus de cet outil, même si je n'ai pas encore totalement l'habitude de commenter mes liens.

C'est un des grands avantages de l'outil selon moi, de pouvoir rajouter des notes aux liens que l'on enregistre, car on ne va pas se mentir, sans prise de notes, ça devient vite compliqué de s'y retrouver.

La solution Shaarli est également proposée par Framasoft avec son service MyFrama. Ce service sera coupé prochainement, c'est pour cela que je met en avant ce service dans cet article.

## Pourquoi Shaarli ?

Pour vous parler de mon expérience personnel, j'ai d'abord commencé à enregistrer mes liens dans mes marques pages Firefox, avec un compte pour synchroniser le tout. Cela fonctionnait très bien, mais avec des dossiers et plus de 500 liens, ce n'était plus possible. J'ai donc migré vers Pocket car la solution était intégrée nativement dans Firefox et que j'ai pu exporter et importer mes URLs facilement. Mais la solution Pocket, bien que très fonctionnel ne répondait pas à mon besoin.

Mon besoin était simple, stocker des liens et des URLs avec des catégories ou tag, pouvoir prendre des notes et dans l'idéal sur une solution libre et publique. Pocket de son côté est une solution propriétaire, qui permet de lire, d'enregistrer et de synchroniser ses articles, avec un système d'article lu ou non. Il est également possible de télécharger l'application mobile et de disposer des articles en mode hors connexion.

Pour la partie lecture, cela ne m'intéresse pas et ce n'est pas mon besoin, tout d'abord car je ne sauvegarde pas que des articles. De plus, la solution est propriétaire, et même si elle appartient à Mozilla, cela me gêne un peu d'utiliser une solution de ce type.

Je me suis alors tourné vers Wallabag, avec de grande peine pour l'installer dans sa version docker officielle. Mais la finalité était la même, vu que c'est dans les grandes lignes la même chose que Pocket, en version libre.

C'est à ce moment que j'ai découvert Shaarli, par l'intermédiaire de Framasoft. Très simple d'installation et d'utilisation, c'était exactement l'outil que je voulais.

## Installation

De ce côté-là, rien d’exceptionnel, vu la simplicité de l'outil et l'absence de base de données l'installation est très simple; De mon côté j'ai choisi une installa via docker car il existe une image pour Shaarli, cela m'a permis d'installer le logiciel en 5 minutes, montre en main. Dans mon cas j'ai utilisé une machine Debian 10.

Pour cela il faut tout d'abord avoir docker installé, vous pouvez trouver tout ce qu'il faut ici :

  * <https://docs.docker.com/engine/install/>

Après avoir installé docker, il suffit de lancer la commande suivante :

```docker run -v data:/var/www/shaarli/data -d --restart always -p 80:80 shaarli/shaarli:master```

Dans l'ordre nous retrouvons les options :

  * `-v data:/var/www/shaarli/data` qui permet de créer un volume pour les données du conteneur et de les retrouver sur la machine
  * `-d` pour lancer le conteneur en mode détaché 
  * `--restart always` qui permet de relancer le conteneur
  * `-p 80:80` pour exposer le port 80 du conteneur sur le port 80 de la machine

Après tout cela, vous aurez accès à l'interface de Shaarli pour réaliser la configuration qui vous le verrez est très simple, en l'absence de base de données.

![Installation Shaarli 1](/images/image-45.png)

Vous pouvez en suite choisir d'installer Shaarli et vous arriverez à la page d'accueil après vous être authentifié :

![Présentation Shaarli](/images/image-46.png)

Il ne vous reste maintenant qu'à ajouter des liens dans votre instance Shaarli. De mon côté j'ai fait le choix d'utiliser un simple conteneur sur le port 80 que j'ai couplé avec HAProxy pour l'exposition et la gestion d'SSL/TLS. Si vous voulez faire différemment, toutes les méthodes d'installation sont disponibles sur le site officiel :

  * <https://shaarli.readthedocs.io/en/master/>

Après avoir configuré et exposé votre instance, vous pourrez enfin ajouter l'extension dans votre navigateur afin de pouvoir ajouter des liens facilement et simplement :

  * <https://addons.mozilla.org/fr/firefox/addon/shaarli/>

La configuration est très simple, vous n'avez qu'à modifier les paramètres de l'extension pour entrer le nom de votre instance :

![Extension Shaarli](/images/image-47.png)

Après cela, il vous suffira de vous authentifier dans l'extension et vous pourrez ajouter facilement des articles sur votre instance, en ajoutant des tags et des commentaires, par exemple :

![Ajout lien Shaarli](/images/image-48.png)

Si vous voulez une idée plus précise de ce que ça peut donner à l'utilisation, vous pouvez consulter les instances partagées plus haut dans l'article.

Et voilà Shaarli est installé et fonctionnel, encore une fois le but de cet article n'est pas de montrer techniquement comment cela fonctionne car la documentation le fait beaucoup mieux que moi, mais plus de vous faire découvrir cet outil que je trouve incroyable par les services qu'il rend et sa simplicité.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

 [1]: https://github.com/shaarli/Shaarli