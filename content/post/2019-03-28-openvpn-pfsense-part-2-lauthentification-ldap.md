---
title: "OpenVPN & PFSense – Part. 2 : L'authentification LDAP"
author: Fabio Pace
type: post
date: 2019-03-28T08:30:00+00:00
url: /system/openvpn-pfsense-part-2-lauthentification-ldap/
thumbnail: /images/pfsense-ldap/ad_pfsense0.png


---

Bonjour à tous, pour continuer l'article sur [OpenVPN & PFSense][1] qui traite la partie Haute Disponibilité, nous allons maintenant parler de l'authentification LDAP.

Nous gardons bien évidement la même architecture à savoir :

![](/images/pfsense-ha/openvpn_pfsense1.png)

## Qu'est-ce que LDAP ? 

LDAP (Lightweight Directory Access Protocol) est un protocole utilisant l'annuaire qui regroupe toutes les informations nécessaires de l'entreprise.

Il est beaucoup utilisé dans les entreprises pour permettre l'accès et la communication des employés aux services de l'entreprise. 

Nous allons utiliser « Active Directory » qui s'appuie sur le protocole LDAP et qui est propriétaire Microsoft. 

La connaissance des attribues liés à l'Active Directory est nécessaire, en voici un exemple :

![](/images/pfsense-ldap/ad_pfsense1.png)

Afin de simplifier l’authentification, nous allons donc utiliser l’authentification de l’Active Directory afin de connecter les utilisateurs au VPN. 

## Configuration

Il faut tout d'abord ajouter un serveur d'authentification en se rendant dans « System » -> « User Manager » -> « Authentification Server » :
![](/images/pfsense-ldap/ad_pfsense2.png)

Puis, il faut compléter les champs suivants : 

![](/images/pfsense-ldap/ad_pfsense3.png)

  1. Choisir un nom explicite de votre serveur d'authentification. Dans notre cas nous indiquons « STAFF » car il authentifiera les utilisateurs présent dans l'OU « STAFF »
  2. Le protocole choisie est donc LDAP. 
  3. Entrer l'adresse IP (ou le nom de domaine du serveur s'il vous avez de la résolution DNS)
  4. Laisser par défaut le port du protocole LDAP, à savoir 389. Si vous utilisez du LDAPS choisissez le port 636 et ajouter la CA de l'AD
  5. Laissez par défaut le transport TCP
  
  
![](/images/pfsense-ldap/ad_pfsense4.png)

  1. Veuillez choisir « Entire Subtree » pour rechercher dans toute l'organisation de votre AD
  2. Renseigner la base de votre AD (les DN) : DN=lab,DN.dom par exemple
  3. Soit vous renseigner le chemin complet de l'OU pour cibler les utilisateurs qui auront le droit de s'authentifier, soit vous renseigner l'utilisateur (étape 4, 5, 6) ayant le droit de requêter dans l'AD et vous choisissez « Select a Container ».
  4. Décocher la case Bind Anonymous car l'Active Directory est par défaut « non requêtable » sans être préalablement authentifié
  5. Renseigner l’utilisateur ayant droit de requêter dans l'AD
  6. Saisir le mot de passe de l’utilisateur précédemment renseigner

Votre authentification via Active Directory est désormais fonctionnelle.

Pour tester votre configuration, vous pouvez utiliser l'outils prévu à cet effet sur Pfsense : 

![](/images/pfsense-ldap/ad_pfsense5.png)

Nous pouvons voir que l'authentification a réussi car nous avons un message en vert _« User fabio.pace authentificated successfully. This user is a member of groups. »_

Je vous remercie pour votre lecture, et j'espère que l'article vous aura plu.

Vous pouvez continuer en lisant la 3ème partie : [les certificats sur PFSense][8] pour un serveur OpenVPN.

A bientôt.

Fabio Pace.

 [1]: https://net-security.fr/system/openvpn-et-pfsense-part-1-haute-disponibilite/
 [2]: https://net-security.fr/wp-content/uploads/pfsense-ha/openvpn_pfsense1.png
 [3]: https://net-security.fr/wp-content/uploads/pfsense-ldap/ad_pfsense1.png
 [4]: https://net-security.fr/wp-content/uploads/pfsense-ldap/ad_pfsense2.png
 [5]: https://net-security.fr/wp-content/uploads/pfsense-ldap/ad_pfsense3.png
 [6]: https://net-security.fr/wp-content/uploads/pfsense-ldap/ad_pfsense4.png
 [7]: https://net-security.fr/wp-content/uploads/pfsense-ldap/ad_pfsense5.png
 [8]: https://net-security.fr/system/openvpn-pfsense-part-3-les-certificats/