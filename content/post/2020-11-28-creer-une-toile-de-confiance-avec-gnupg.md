---
title: Créer une toile de confiance avec GnuPG
author: Mickael Rigonnaux
type: post
date: 2020-11-28T11:39:26+00:00
url: /security/creer-une-toile-de-confiance-avec-gnupg/
thumbnail: /images/GNUPG/Gnupg_banne.png
featured: true
tags: [
    "logiciellibre",
    "GNU/Linux",
    "cryptographie",
    "GnuPG"
]
categories: [
    "securite",
    "logiciellibre"
]

---
Bonjour à tous ! Aujourd'hui nous allons aborder ensemble un sujet que j'ai pu traiter dans le cadre de mon alternance, la mise en place d'une toile de confiance GnuPG.

## Introduction

PI : je parle souvent de « clé » GPG, il faut comprendre biclé car il y a toujours une clé publique et une clé privée.

Le chiffrement GnuPG a déjà été abordé [sur ce blog][1], mais moins précisément. La mise en place d'une toile de confiance peut se faire dans le cadre d'un chiffrement entre une entité A et une entité B, ce qui était mon cas.

La mise en place de cette procédure permettra de créer une zone de confiance entre les deux entités. Le principe reste le même qu'avec les autorités de certification pour SSL/TLS sauf que nous n'aurons pas la possibilité de passer par des autorités de confiance, cela se fera en direct. L'entité A devra signer la clé publique maître de l'entité B et inversement, tout cela en garantissant l'intégrité des données.

Dans mon article je parle plus au niveau des entreprises/entités, mais tout cela est également vrai pour les échanges entre personnes.

## Clé maître et clé applicative

Dans les différents exemples que j'ai pu traiter il y avait souvent des notions de clés maître et de clés applicatives. Il faut voir les clés maîtres comme des autorités de certification interne à l'entité, une fois validée et signée par l'entité adverse elle permettra de générer des clés applicatives qui seront de confiance.

Un schéma vaut mieux qu'une longue explication :

![Explication toile de confiance](/images/master-app-key-gpg.png)

Les biclés applicatives sont directement issues et signées des clés maîtres de chaque entité, ce qui permet d'avoir cette toile de confiance.

Le schéma peut également être simplifié en utilisant seulement les biclés « maître » si les échanges sont entre deux personnes par exemple.

L'utilisation de ce système permet cependant un meilleur niveau de sécurité, car les clés maîtres ne sont jamais utilisées pour chiffrer des données mais seulement pour signer des clés publiques.

## Procédure

Tout cela doit bien entendu être tracé dans une procédure pour garantir l'intégrité et le bon traitement des données et des clés.

Nous allons maintenant aborder les différentes étapes pour générer les clés maîtres et applicatives, ainsi que toutes les étapes nécessaires à la mise en place de cette toile de confiance.

Dans mon exemple je vais montrer les étapes des deux entités pour que vous puissiez suivre les étapes dans l'intégralité.

### Génération des biclés

Il faut dans un premier temps générer les bi-clés maîtres GPG de chaque côté :

```gpg --full-gen-key```

Cette commande permet de générer une clé GPG avec toutes les options :

![Génération GnuPG](/images/image-31.png)


Vous allez devoir entrer des caractéristiques comme :

  * Les protocoles à utiliser
  * La taille de la clé
  * La durée de validité (à définir selon vos besoins)
  * La passphrase pour protéger la clé privée

Vous pouvez ensuite vérifier la présence des clés publiques et privées :

```
gpg -k # Clé publique
gpg -K # Clé privée
```

![Verification UID](/images/image-32.png)

Les clés sont maintenant générées pour les deux entités.

### Partage des biclés

Les biclés générées il faut maintenant les partager entre les partager entre les entités. Vous pouvez pour cela définir de chaque côté des personnes responsables des clés si vous êtes un société. Cette personne devra être chargée de transférer la clé à l'entité adverse et de vérifier l'intégrité.

Il faut tout d'abord exporter les clés en utilisant l'ID récupéré avec les commandes `gpg -k` :

```gpg --export --armor ID_KEY > entite-x-master-pub.gpg```

![Export GnuPG](/images/image-34.png)

Il faut maintenant envoyer la clé publique exportée vers l'entité deux. Vu que la clé est publique vous pouvez le faire via le moyen de votre choix, le plus important est de valider l&#8217;empreinte de cette clé pour être sûr de son intégrité.

Cela doit être réalisé avant l'envoi par l'entité une et à la réception par l'entité deux : 

```sha256sum entite-x-master-pub.gpg```

![Vérification Hash](/images/image-35.png)

L'action doit être réalisée dans les deux sens.

### Import et signature des clés

Après avoir reçu et vérifier la clé, chaque entité devra importer la clé publique de l'entité distant dans son keystore GPG afin de la signer avec leur clé privée.

Pour cela il faut importer la clé de la façon suivante :

```gpg --import entite-x-master-pub.gpg```

![Import clé GnuPG](/images/image-36.png)


Vous pouvez vérifier la présence de la clé avec `gpg -k` après l'import.

Pour la signature, vous pouvez lancer les commandes suivantes :

```
gpg --sign-key nom_de_clé_à_signer
gpg --sign-key ID_clé_à_signer
gpg --sign-key mail_clé_à_signer
```

![Signature des clés GnuPG](/images/image-37.png)


Il faut une nouvelle fois réaliser l'export de la clé que vous venez de signer avant de la renvoyer vers la personne compétente :

```gpg --export --armor ID_KEY > entite-x-master-pub-sign.gpg```

La clé ainsi que le hash devraient être différents de la clé initiale car les informations ont changées. Il faut suivre la même procédure pour renvoyer la clé en vérifiant les empreintes à l'envoi et à la réception.

### Vérification de la signature

Une fois les clés publiques réceptionnées dans chaque entité, il faut vérifier si la signature est correcte. Il faut d'abord réimporter les clés dans les keystore :

```gpg --import entite-x-master-pub-sign.gpg```

![Import clé signée GnuPG](/images/image-38-1024x159.png)


Vous devriez voir un message vous indiquant que la clé importée est signée.

Pour vérifier plus concrètement il faut utiliser les commandes suivantes :

```
gpg --list-sig
gpg --list-sig ID_KEY
```

Par exemple pour la clé de l'entité 1 :

![Vérification des clés GnuPG](/images/image-39.png)

Nous avons maintenant des clés maîtres publiques signées de chaque côté ! La toile de confiance est créée entre les entités 1 & 2. Si vous n'avez pas besoin d'utiliser de biclé applicative, vous pouvez vous arrêter ici.

### Génération des biclés applicatives

Les clés applicatives sont des clés qui sont issues de la clé maître d'une entité. Elle sera forcément de confiance vu qu'elle sera signée avec clé publique maître validée par les deux entités.

Pour générer une biclé applicative, il suffit de réaliser les opérations suivantes, tout d'abord créer une nouvelle clé, sur l'entité 1 par exemple :

```gpg --full-gen-key```

![Génération clé applicative](/images/image-40.png)

Il suffit en suite de signer cette clé avec la commande utilisée précédemment :

```
gpg --sign-key nom_de_clé_à_signer
gpg --sign-key ID_clé_à_signer
gpg --sign-key mail_clé_à_signer
```

![Signature clé GnuPG](/images/image-41.png)

Et maintenant il suffit de vérifier la signature de la clé :

```
gpg --list-sig
gpg --list-sig ID_KEY
```

![Vérification signature clé applicative GnuPG](/images/image-42.png)

La clé applicative est bien signée, il suffit maintenant de l'exporter de la transmettre à l'entité 2.

### Envoi et vérification des biclés applicatives

La procédure est la même que précédemment, il faut d'abord exporter la clé dans un fichier :

```gpg --export --armor 025FBFC5BB23279452C8B3A0010B1FE19BB25100 > entite-1-app-key-sign.gpg```

![Export clé applicative GnuPG](/images/image-43.png)

Et il faut également vérifier l'empreinte du fichier à l'envoi ainsi qu'à la réception :

```sha256sum entite-1-app-key-sign.gpg```

![Vérification hash clé applicative GnuPG](/images/image-44.png)


Pour terminer, vérifier la signature une fois réceptionnée :

```
gpg --import entite-1-app-key-sign.gpg
gpg --list-sig 025FBFC5BB23279452C8B3A0010B1FE19BB25100
```

Voilà, nous avons maintenant généré des clés maîtres et applicatives pour former une toile de confiance entre les deux entités ! Si vous voulez aller plus loin vous pouvez utiliser la librairie Python de GnuPG pour automatiser tout cela.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

 [1]: https://net-security.fr/security/gnupg-introduction-cheat-sheet/
 [2]: images/master-app-key-gpg.png
 [3]: images/image-31.png
 [4]: images/image-34.png
 [5]: images/image-35.png
 [6]: images/image-36.png
 [7]: images/image-37.png
 [8]: images/image-38.png
 [9]: images/image-39.png
 [10]: images/image-40.png
 [11]: images/image-41.png
 [12]: images/image-42.png
 [13]: images/image-43.png
 [14]: images/image-44.png