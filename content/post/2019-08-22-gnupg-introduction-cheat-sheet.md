---
title: 'GnuPG – Introduction & Cheat-Sheet'
author: Mickael Rigonnaux
type: post
date: 2019-08-22T12:41:08+00:00
url: /security/gnupg-introduction-cheat-sheet/
thumbnail: /images/GNUPG/Gnupg_banne.png
featureImage: images/OpenSSL-1/openssl-present.png
shareImage: /images/OpenSSL-1/openssl-present.png
tags: [
    "GNU/Linux",
    "GNUPG",
    "cryptographie",
    "chiffrement"
]
categories: [
    "securite",
    "logiciellibre"
]

---

Bonjour à tous, aujourd'hui nous allons voir ensemble un outil très utilisé dans le monde de la cryptographie, [GnuPG (Gnu Privacy Guard)][1]. Ce dernier est l’implémentation sous licence GNU du standard [OpenPGP][2]. 

**EDIT :** vous trouverez une version plus à jour des commandes [sur ce lien.][3]

## A quoi ça sert ? 

GPG comme PGP, qui fonctionnent de la même façon et sont compatibles servent à échanger des messages/fichiers chiffrés et signés afin d'assurer l'authenticité, l'intégrité et la confidentialité des données. 

## Comment ça marche ? 

Ce logiciel est basé sur un système de clé asymétrique comme pour les certificats x509. Pour réaliser des échanges chiffrés et signés, chaque partie devra avoir une paire de clés, avec une clé privée et publique. 

Chaque partie doit échanger sa clé publique avec l'autre. Dans le cas d'un envoi la clé publique du **destinataire** servira à chiffrer le message et la clé privée de **l'expéditeur** servira à réaliser une signature. La signature est un condensat ou hash du message qui est chiffré avec la clé privée de l'expéditeur. 

Lors de la réception, le destinataire pourra alors déchiffrer le message avec **sa clé privée** et déchiffrer la signature avec la clé publique de **l'expéditeur**. Ce hash permet de vérifier l'intégrité des données en le comparant à celui du fichier reçu. 

![](/images/GNUPG/gpg_1.png)

L'algorithme asymétrique principalement utilisé par l'outil est RSA, même s'il est possible d'utiliser un système basé sur les courbes elliptiques. 

D'autres algorithmes sont utilisés pour les échanges avec GPG comme des fonctions de hachage comme SHA-256 ou SHA-1 pour les signatures ou des algorithmes symétriques comme AES128 ou encore 3DES. 

Les algorithmes symétriques sont utilisés pour réaliser les échanges une fois que la clé secrète est connue par les deux parties. La clé secrète est échangée avec l'aide du protocole asymétrique RSA. Ce mécanisme est en place car il serait trop lourd d'utiliser seulement le chiffrement asymétrique. Ce type de fonctionnalité est aussi implémenté dans les protocoles SSL/TLS par exemple. 

## L'utilisation

Le logiciel GnuPG est implémenté dans de nombreux systèmes et peut-être utilisé sur Windows, Mac et Linux. Sur Windows, vous pouvez utilser GPG4Win par exemple.  
Dans mon cas, j'utilise GPG sur Ubuntu 18.04 en ligne de commande directement.

Pour installer GnuPG sous Ubuntu : 

```
sudo apt update
sudo apt install gnupg2
gpg2
```

Avec la dernière commande votre keystore GPG sera automatiquement créé :

![](/images/GNUPG/GPG_2.png)

Avant de générer des clés, nous allons voir le fichier de configuration de GPG, par défaut il se trouve dans le dossier ..gnupg dans le dossier de l'utilisateur. Selon les versions, il se peut qu'il ne soit pas présent, vous pouvez alors le créer avec la commande suivante : 

```touch ~/./gnupg/gpg.conf```

Le fichier d'origine est également présent sur ce lien. 

Voilà maintenant la liste des options les plus communes et les plus importantes dans ce fichier : 

```
### Clé à utiliser par défaut pour différentes actions ###
default-key ID_KEY

### Liste des algorithmes symétriques à utiliser ###
personal-cipher-preferences AES256 AES 3DES

### Liste des algorithmes de hachage à utiliser ###
personal-digest-preferences SHA512 SHA384 SHA256

### Liste des méthodes de compression ###
personal-compress-preferences ZLIB BZIP2 ZIP Uncompressed

### Liste dans l'ordre des préférences ###
default-preference-list SHA512 SHA384 SHA256 AES256 AES 3DES ZLIB BZIP2 ZIP Uncompressed

```

Vous pouvez changer ces paramètres en fonction de vos préférences/besoins. Ils seront utilisés pour générer les clés GPG. 

Par défaut AES256 est utilisé pour générer des clés, mais cet algorithme est disproportionné comparé aux tailles de clés RSA utilisées. Dans certaines normes comme PCI CP, il est indiqué qu'il faut utiliser des clés de force égale ou supérieur à celle de toute clé qu'elle protège. 

La clé asymétrique doit-être égale ou supérieur à la clé symétrique. Or nous voyons bien dans ce tableau du [NIST][6] qu'AES256 est largement supérieur à RSA 2048 ou 3072. C'est une bonne pratique. 

![](/images/GNUPG/Key_size.jpg)

Voici maintenant la liste des principales commandes pour l'outil GPG. Il est aussi à noter que les clés privées GPG sont forcément protégée par une passphrase, qui vous sera demandé à chaque action avec cette dernière. 

Toute la gestion des clés se fait par l'ID des clés. 

```
1. Générer une paire de clés GPG
gpg2 --full-gen-key
-> Plusieurs questions seront posées comme la taille, algorithme à utiliser, la durée, la passphrase, etc.

2. Lister les clés GPG (et récupérer les ID)
gpg2 --list-keys

3. Lister les clés publiques
gpg2 -k

4. Lister les clés privées 
gpg2 -K

5. Récupérer le fingerprint de la clé
gpg2 --fingerprint ID_KEY

6. Exporter une clé publique dans un fichier
gpg2 --export --armor ID_KEY > sortie.key

7. Importer une clé publique depuis un fichier
gpg2 --import fichier_cle.key

8. Exporter une clé privée dans un fichier
gpg2 --export-secret-key -a > sortie.key

9. Importer une clé privée depuis un fichier
gpg2 --import --allow-secret-key-import fichier_cle.key

10. Signer un fichier
gpg2 -s -u ID_KEY fichier_à_signer
gpg2 -s fichier_à_signer (avec la clé par défaut)

11. Chiffrer un fichier
gpg2 -r ID_KEY -e -a fichier_à_chiffrer

12. Chiffrer et signer un fichier
gpg2 -s -a -e -u ID_KEY fichier_à_signer

13. Déchiffrer un fichier
gpg2 -r ID_KEY -d fichier_à_chiffrer

14. Vérifier la signature séparée
gpg2 --verify fichier_signature fichier_signé

15. Vérifier la signature incluse
gpg2 --verify fichier_signé

16. Supprimer une clé :
gpg2 --delete-secret-keys ID_KEY #privée
gpg2 --delete-key ID_KEY #publique

17. Informations plus poussées sur une paire de clé 
gpg2 --edit-key ID_KEY

   17.1 Vérifier signature de la clé
   > check
   17.2 Lister les préférences
   > pref
   > showpref

18. Changer la passphrase d'une clé privée 
gpg2 --edit-key ID_KEY
> passwd
> save 
```


Il est également possible de créer un système avec des clés maîtres afin d'avoir une toile de confiance dans la génération des clés. C'est à dire que des clés maîtres vont signer des clés applicatives par exemple, le principe est le même que pour les certificats et les autorités de certification. 

Si vous utilisez qu'une seule clé maître vous pouvez la définir comme clé par défaut. 


```
1. Signature de la clé 
gpg2 --sign-key ID_KEY_A_SIGNER

2. Vérification de la signature d'une clé 
gpg2 --edit-key ID_KEY_SIGNEE
> check 
```

Avant de terminer cet article, un exemple sur la génération d'une clé :

![](/images/GNUPG/GPG_3.png)

![](/images/GNUPG/GPG_4-1.png)

Voilà, vous connaissez maintenant les commandes de bases et les principes de GPG/PGP. Cette technologie peut également se rajouter dans des logiciels, comme dans Thunderbird par exemple pour le chiffrement des mails. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://gnupg.org/
 [2]: https://www.openpgp.org/
 [3]: https://github.com/tzkuat/Ressources/blob/master/gpg-cheatsheet.md
 [6]: https://www.nist.gov/