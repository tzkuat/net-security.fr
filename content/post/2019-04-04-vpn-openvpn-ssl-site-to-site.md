---
title: 'VPN OpenVPN & SSL – Site to Site'
author: Fabio Pace
type: post
date: 2019-04-04T20:42:09+00:00
url: /system/vpn-openvpn-ssl-site-to-site/
featured_image: /images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_0.png
shareImage: /images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_0.png
tags: [
    "pfsense",
    "openvpn"
]
categories: [
    "systeme",
    "reseau"
]

---
![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_0.png)

Bonjour à tous, aujourd'hui nous allons voir comment monter un VPN OpenVPN Site to Site basé sur SSL.

Utiliser un VPN SSL permet d'alléger les flux réseau et gagner en performance (contrairement à IPSec). De plus, SSL est omniprésent dans le monde d'Internet. Nous l'utilisons au quotidien sans se rendre compte de son efficacité. 

NB : Attention, dans un monde d'entreprise, il est judicieux de couplé l'utilisation d'un VPN avec des politiques de sécurité comme une analyse virale du poste de travail avant d'initier la connexion au site distant.

## L'architecture

Tout d'abord, voici l'architecture que nous allons mettre en place : ![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_1.png)

Les deux PFSenses ont donc minimum 2 pattes pour simuler le WAN et le LAN.  
Par défaut, le ping est autorisé partout afin de vérifier le fonctionnement.  
Le client-01 peut donc atteindre sa passerelle (pfsense01), le FAI, Internet, et le pfsense02 (coté WAN).

Le but du VPN va permettre donc pouvoir faire communiquer le client01 avec le client02 de façon sécurisé.

Afin de faciliter l'article, voici les règles firewall que nous avons mis en place en amont.

Sur l'interface WAN :![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_2.png)

NB : « Pfsense01 » et « Pfsense02 » sont des alias qui pointent tous les deux sur leur interface WAN.

NB : La règle NAT pour SSH est utilisé pour administrer les clients (qui sont des CentOS).

Sur l'interface LAN ![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_3.png)

## Les certificats

Tout d'abord, nous devons créer une CA pour certifier nos certificats (serveurs et clients).

Pour cela, rendez-vous dans **System -> Certificate Manager -> CAs** à partir du Pfsense01 pour créer la CA : 

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_5.png)

  1. Nommer la CA pour rendre explicite la configuration
  2. Choisir « Create an Internal CA » pour la créer. Nous choisirons de l'importer dans le deuxième Pfsense
  3. Choisir une clé au dessus de 1024 pour le chiffrement assymétrique. En effet, il est recommandé d'utiliser au minimum des clés de 2048 bits
  4. L'algorithme de hash que nous devons utiliser est au minimum SHA256. Les précédents (MD5, SHA, SHA1 ne sont plus recommandés dû à leur faiblesse)
  5. La durée de validité de la CA, nous vous recommandons 10 ans au minimum pour une CA pour une meilleure gestion
  6. Les champs concernant le ceritificat en lui-même. Ces champs sont facultatifs

Votre CA est maintenant créée :![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_6.png)

Nous allons d'ores et déjà importer la clé public dans le deuxième Pfsense afin de certifier les certificats que nous allons délivrer par la suite.  
  
Pour cela, vous devez choisir « Export CA » :![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_7.png)

Nous copions la CA depuis un éditeur de texte, et nous allons la coller dans le deuxième pfsense. Pour cela, depuis pfsense02, se rendre également dans  
**System -> Certificate Manager -> CAs** et ajouter une CA :

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_8.png)

  1. Nommer la CA (de préférence comme sur pfsense01 afin de s'y retrouver plus facilement si vous gérez divers CA)
  2. Choisir « Import an existing CA »
  3. Coller la CA qui vous avez récupéré.

**Attention à ne pas copier la clé de la CA qui elle reste privée (interne à pfsense01) !**

Votre CA est maintenant importée sur votre deuxième pfsense.![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_9.png)

NB : Vous devez avoir une croix noire dans la colonne « Internal » ce qui signifie que votre CA n'est pas interne à cet équipement (normal vu que nous venons de l'importer depuis le pfsense01).

Retournons sur le pfsense01 afin de créer les deux certificats : un pour le serveur vpn (pfsense01) et un pour le client vpn (pfsense02).

Dans **System -> Certificate Manager -> Certificates** : 

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_10.png)

  1. Choisir de créer un certificat interne
  2. Nommer le certificat. Je vous conseil d'insérer le type du certificat (« serveur » dans notre cas) afin de différencier avec le certificat client que nous générons plus tard
  3. Choisir sur quelle CA nous devons s'appuyer
  4. Les algorithmes par défauts sont préconisés (même remarque que précédement)
  5. La durée de vie d'un certificat serveur est en moyenne entre 1 et 3 ans. Pour faciliter la gestion du lab, nous laissons 10 ans
  6. Le CommonName est le nom du certificat (je vous rappelle que nous utilisons SSL pour la connexion VPN, donc il s'agit ici du nom de domaine de votre VPN)
  7. Les différents champs qui sont préremplis via la CA
  8. Choisir « **Server Certificate** » et ajouter un SAN du même nom que le CommonName au minimum (vous pouvez rajouter des alias à cet endroit)

Votre certificat serveur est maintenant créé : ![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_11.png)

Il ne reste plus que le certificat client, nous refaisons donc la même étape, avec une petite modification à deux endroits : 

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_12-1.png)

  1. Changer le nom du certificat avec cette fois-ci le mot clé « Client »
  2. Choisir « User Certtificate » au lieu de « Server Ceritifcate »

NB : Dans un cadre d'entreprise, vous devez modifier également le CommonName et le SAN rattachés au certificat client (qui est théoriquement différent à celui du serveur).

Votre certificat client est maintenant créé, il ne vous reste plus qu'a l'exporter du pfsense01 :![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_13.png)

Pour l'importer sur le pfsense02 :![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_14.png) 

  1. Vous copiez le certificat du client
  2. Vous copiez la clé privée du certificat car c'est le pfsense02 qui doit être le propriétaire.

Votre certificat client est maintenant importé dans le pfsense02 :![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_15.png)

## La configuration VPN

Rendez-vous dans la partie OpenVPN : **VPN -> OpenVPN -> Servers**

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_16.png)

  1. Choisir « Peer to Peer » pour un tunnel site à site
  2. UDP sera plus perfomant et plus robuste que TCP car il n'y aura pas de session TCP pour l'initialisation du tunnel mais également pour laisser le tunnel actif (donc réduction des requêtes)
  3. TUN est nécessaire si vous souhaitez faire du routage couche 3 (IP) (utilisé dans la plupart des cas). TAP quand à lui est rarement utilisé mais est spécifique pour du routage niveau 2 (ethernet)
  4. L'interface où les deux PFSense vont discuter
  5. Le port du serveur 
  6. La description pour rendre la configuration explicite
  
![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_17.png)

  1. Choisir d'utiliser une clé TLS pour authentifier le client
  2. Génerer aléatiorement une clé TLS sur le serveur
  3. Choisir sur quelle CA se repose les certificats que nous allons utiliser
  4. Choisir le certificat serveur que nous avons généré
  5. Une clé 2048 bits est recommandée pour du chiffrement asymétrique
  6. Pour le chiffrement symétrique, l'algorithme AES-CBC avec une clé de 128 bits est largement sufisant. Il n'est pas nécessaire de choisir une clé plus grande; attention la performance sera impactée
  7. Décocher la case NCP afin d'éviter une négociation de l'algorithme de chiffrement afin de gagner en perfomance. (Nous reseignerons directement le bon algorithme sur le client)
  8. Choisir SHA256 pour l'algorithme de hash (différent de SHA ou SHA1)
  9. Vérifier le certificat serveur et client seulement
  
![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_18.png)

  1. Renseigner la plage d'IP pour le tunnel VPN : un /30 est sufisant car le tunnel va se monter entre deux routeurs.
  2. Mettez la plage d'adresse du réseau distant (le LAN du pfsense02 dans notre cas)

Nous avons terminé la configuraiton du serveur : 

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_19.png)

Nous allons maintenant configurer le VPN coté client (pfsense02).  
Rendez-vous donc **VPN -> OpenVPN -> Clients** :

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_20.png)

  1. Choisir site à site « Peer to Peer »
  2. Choisir UDP comme du coté serveur
  3. TUN pour du routage niveau 3
  4. L'interface où pfsense02 peut comuniquer avec pfsense01 : WAN
  5. L'adresse IP du pfsense01
  6. Le port du serveur VPN: 1194
  7. Une description pour rendre plus explicite la configuration
  
![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_21.png)

  1. Choisir d'utiliser une clé TLS
  2. Coller la clé que le serveur VPN a généré (il suffit de configurer la partie serveur du pfsense01 pour retrouver la clé TLS générée)
  3. Choisir une authentification TLS seulement (et non un chiffrement)
  4. La CA à renseigner à celle que nous avons importé précedement
  5. Choisir le certificat client 
  6. L'algorithme de chiffrement symétrique est AES-128-CBC
  7. Désactiver NPC pour la négociation des algorithmes
  8. SHA256 pour l'algorithme de hash
  
![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_22.png)

  1. La plage d'adresse du tunnel VPN (comme configuré sur le serveur)
  2. L'adresse IP distante du serveur PVN (pfsense01)

Votre client VPN est maintenant configuré : 

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_23.png)

Vérifier si votre VPN est bien monté dans : **Status -> OpenVPN** :

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_24.png)

Nous pouvons voir que le VPN est « UP » (monté) est que la patte de l'interface du VPN sur le pfsense02 est 172.0.0.2.

Vous ne pouvez toujours pas faire de ping vers le réseau distant à partir d'un poste client, vous devez ajouter la règle autorisant les flux à circuler dans l'interface OpenVPN : 

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_25.png)

Vous pouvez vérifier par un test de ping : 

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_26.png)

Et si vous modifiez la règle ICMP sur l'interface LAN des OpenVPN, et que vous acceptez tout le traffic ipv4*, alors vous pourrez effectuer un traceroute pour visualiser le chemin du paquet : 

![](/images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_27.png)

La configuration du VPN site à site est donc terminé. 

J'espère que l'article vous aura plu, n'hésitez pas si vous souhaitez partager vos avis sur cet article.

A bientot, 

Fabio Pace.

 [1]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_1.png
 [2]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_2.png
 [3]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_3.png
 [4]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_5.png
 [5]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_6.png
 [6]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_7.png
 [7]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_8.png
 [8]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_9.png
 [9]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_10.png
 [10]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_11.png
 [11]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_12-1.png
 [12]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_13.png
 [13]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_14.png
 [14]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_15.png
 [15]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_16.png
 [16]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_17.png
 [17]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_18.png
 [18]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_19.png
 [19]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_20.png
 [20]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_21.png
 [21]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_22.png
 [22]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_23.png
 [23]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_24.png
 [24]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_25.png
 [25]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_26.png
 [26]: images/pfsense-site_to_site-openvpn&ssl/vpn_ssl_sts_27.png