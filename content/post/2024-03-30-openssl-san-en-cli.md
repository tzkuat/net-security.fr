---
title: "OpenSSL : Générer une CSR avec des SAN directement en CLI"
author: Mickael Rigonnaux
type: post
date: 2024-03-30T14:00:44+00:00
url: /securite/openssl-csr-san-en-cli
thumbnail: /images/csr.png
shareImage: /images/csr.png
featured: true
toc: true
tags: [
    "openssl",
    "cryptographie",
    "certificat",
    "csr"
]
categories: [
    "securite"
]

---

Bonjour à tous ! Aujourd'hui un rapide article sur OpenSSL (après un long moment d'absence) on va reparler CSR, SAN et OpenSSL pour voir comment créer une CSR avec des SAN directement en ligne de commande.

## Introduction

Ce sujet a déjà été abordé sur le blog en 2020 et à l'époque c'était à ma connaissance la seule méthode. Depuis la version OpenSSl 1.1.1> il est possible de créer une CSR avec des SAN sans passer par un fichier spécifique. On peut directement faire ça en ajoutant simplement une option à notre commande `-addext`.

Le lien vers l'article précédent :
* https://net-security.fr/security/creer-une-csr-avec-des-san-sur-openssl/

## CSR ? SAN ?

**Cette section est reprise de l'article précédent.**

### CSR ?

Tout d'abord une petite explication, une CSR ou Certificate Signing Request est tout simplement une demande de certificat. Ce fichier issu de votre clé privée va contenir toutes les informations nécessaires à la signature de votre certificat, le CN, les SAN, etc. C'est ce fichier que vous devez transmettre à votre autorité de certification pour demander un certificat. Une CSR contiendra donc une clé publique et les différentes informations, la clé privée n'est heureusement pas incluse et permet juste de signer ce fichier.

Les CSR sont définies dans le standard PKCS8 et son but principal est de ne pas faire transiter les clés privées.

Ces demandes sont généralement sous cette forme :


```
-----BEGIN CERTIFICATE REQUEST-----
MIIBMzCB3gIBADB5MQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEW
MBQGA1UEBxMNU2FuIEZyYW5jaXNjbzEjMCEGA1UEChMaV2lraW1lZGlhIEZvdW5k
YXRpb24sIEluYy4xGDAWBgNVBAMUDyoud2lraXBlZGlhLm9yZzBcMA0GCSqGSIb3
DQEBAQUAA0sAMEgCQQC+ogxM6T9HwhzBufBTxEFKYLhaiNRUw+8+KP8V4FTO9my7
5JklrwSpa4ympAMMpTyK9cY4HIaJOXZ21om85c0vAgMBAAGgADANBgkqhkiG9w0B
AQUFAANBAAf4t0A3SQjEE4LLH1fpANv8tKV+Uz/i856ZH1KRMZZZ4Y/hmTu0iHgU
9XMnXQI0uwUgK/66Mv4gOM2NLtwx6kM=
-----END CERTIFICATE REQUEST-----
```


### SAN ?

Un Subject Alternative Name est une extension de la norme X509, cela permet d'ajouter des informations additionnelles dans un certificat. Ca permet par exemple de créer un certificat valable pour plusieurs domaines, par exemple :

  * www.net-security.fr
  * demo.net-security.fr
  * test.net-security.fr

Un SAN peut contenir plusieurs informations comme des adresses mails, des adresses IP ou des noms de domaine.

## Créer une CSR avec des SANs en CLI

Nous allons maintenant voir comment créer une CSR avec des SANs directement en CLI.

Dans mes exemples, je vais ajouter tous les éléments nécessaires à la génération du certificat dans les arguments des commandes. Nous allons voir comment faire cela avec des clés privées RSA et ECC.

### RSA

#### Créer une clé privée RSA et une CSR avec SAN

```
openssl req -newkey rsa:2048 -sha256 \
  -nodes -keyout net-security-test.key -out net-security-test.csr \
  -subj "/C=FR/ST=Corsica/L=Ajaccio/O=Net-Security/OU=IT Dept/CN=net-security.fr" \
  -addext "subjectAltName=DNS:www.net-security.fr,DNS:*.example.com,IP:10.0.0.1"
```

Sur ces lignes nous voyons bien :
* 1ère ligne : la commande et les options classiques pour créer une clé/CSR : taille de clé, algorithme de hachage
* 2ème ligne : Les options : noms des fichiers, etc.
* 3ème ligne : Le subject complet du certificat C / ST / L / O / OU / CN
* 4ème ligne : l'ajout de l'extension `SubjectAltName` ainsi que les valeurs.

Et si on vérifie les informations de la CSR avec la commande suivante :
* `openssl req -text -noout -verify -in net-security-test.csr`
```
Certificate request self-signature verify OK
Certificate Request:
    Data:
        Version: 1 (0x0)
        Subject: C = FR, ST = Corsica, L = Ajaccio, O = Net-Security, OU = IT Dept, CN = net-security.fr
        Subject Public Key Info:
 [...]
        Attributes:
            Requested Extensions:
                X509v3 Subject Alternative Name: 
                    DNS:www.net-security.fr, DNS:*.example.com, IP Address:10.0.0.1
    Signature Algorithm: sha256WithRSAEncryption
```

Pour les autres commandes/exemples, les commandes seules seront données.


#### Créer un certificat auto-signé RSA avec des SAN

On reprend la même commande en ajoutant `-x509` et `-days 365`. La CSR n'est pas stockée ici.

```
openssl req -x509 -newkey rsa:2048 -sha256 -days 365 \
    -nodes -keyout net-security-test.key -out net-security-test.crt \
    -subj "/C=FR/ST=Corsica/L=Ajaccio/O=Net-Security/OU=IT Dept/CN=net-security.fr" \
    -addext "subjectAltName=DNS:www.net-security.fr,DNS:*.example.com,IP:10.0.0.1"
```

#### Créer une CSR à partir d'une clé existante

Attention, il faut déjà avoir généré la clé privée `net-security-test.key` avant de lancer la commande.

```
openssl req -new -sha256 -key net-security-test.key -out net-security-test.csr \
  -subj "/C=FR/ST=Corsica/L=Ajaccio/O=Net-Security/OU=IT Dept/CN=net-security.fr" \
  -addext "subjectAltName=DNS:www.net-security.fr,DNS:*.example.com,IP:10.0.0.1"
```

### ECC

#### Créer une clé privée ECC et une CSR avec SAN

```
openssl req -newkey ec -pkeyopt ec_paramgen_curve:prime256v1 -sha256 \
  -nodes -keyout net-security-test.key -out net-security-test.csr \
  -subj "/C=FR/ST=Corsica/L=Ajaccio/O=Net-Security/OU=IT Dept/CN=net-security.fr" \
  -addext "subjectAltName=DNS:www.net-security.fr,DNS:*.example.com,IP:10.0.0.1"
```

#### Créer un certificat auto-signé ECC avec des SAN

On reprend la même commande en ajoutant `-x509` et `-days 365`. La CSR n'est pas stockée ici.

```
openssl req -x509 -newkey ec -pkeyopt ec_paramgen_curve:prime256v1 -sha256 -days 365 \
    -nodes -keyout net-security-test.key -out net-security-test.crt \
    -subj "/C=FR/ST=Corsica/L=Ajaccio/O=Net-Security/OU=IT Dept/CN=net-security.fr" \
    -addext "subjectAltName=DNS:www.net-security.fr,DNS:*.example.com,IP:10.0.0.1"
```

#### Créer une CSR à partir d'une clé existante

Attention, il faut déjà avoir généré la clé privée `net-security-test.key` avant de lancer la commande.

```
openssl req -new -sha256 -key net-security-test.key -out net-security-test.csr \
  -subj "/C=FR/ST=Corsica/L=Ajaccio/O=Net-Security/OU=IT Dept/CN=net-security.fr" \
  -addext "subjectAltName=DNS:www.net-security.fr,DNS:*.example.com,IP:10.0.0.1"
```

## Conclusion

Et voilà, nous avons maintenant vu comment créer des CSR avec des SAN en CLI, que ça soit en ECC ou en RSA.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat