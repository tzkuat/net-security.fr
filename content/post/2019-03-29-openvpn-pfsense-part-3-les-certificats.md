---
title: 'OpenVPN & PFSense – Part. 3 : Les Certificats'
author: Fabio Pace
type: post
date: 2019-03-29T07:00:28+00:00
url: /system/openvpn-pfsense-part-3-les-certificats/
thumbnail: /images/pfsense-certificats/cert_pfsense0.png
shareImage: /images/pfsense-certificats/cert_pfsense0.png
tags: [
    "pfsense",
    "openvpn"
]
categories: [
    "systeme",
    "reseau"
]

---
![](/images/pfsense-certificats/cert_pfsense0.png)

Bonjour à tous, pour continuer l'article sur [OpenVPN & PFSense - Authentification LDAP][2], nous allons voir comment créer sa propre CA (_Certificate_ Authority) et le certificat serveur pour OpenVPN.

## Le certificat racine

Le certificat racine, appelé également CA (ou AC en français « Autorité de Certification ») permet de générer des certificats. L'autorité certifiera les certificats.

A savoir qu'il existe deux types d'autorité de certification : 

  * **Externe** : les CA qui se trouvent sur vos navigateurs (ceux de Google, Firefox&#8230;) qui permet de certifier tous les sites web publics (par exemple « DST Root CA X3 » pour la CA de Let's Encrypt Authority X3 afin de certifier le site https://net-security.fr)
  * **Interne** : les CA de votre entreprise qui permet de ceritfier les sites interne de l'entreprise, qui n'ont pas la vocation d'être publiés sur internet. 

_Mais pourquoi n'utilises-t'on pas de CA externe directement ?_ 

Car faire valider son certificat par une autorité de certification publique est payante, et dans un réseau d'entreprise, il n'est pas nécessaire que le certificats soit certifiés par Google sachant que Google n'aura jamais accès au site web (vu qu'il est interne).

![](/images/pfsense-certificats/cert_pfsense1.png)

  1. Se rendre dans la partie « System » -> « Certificate Manager » -> « CAs »
  2. Choisir un nom pour rendre la CA explicite
  3. Choisir « Create an Internal Ceritificate Authority » pour créer une CA

La CA est toujours générée avec une clé. En effet, celle-ci est gardé en toute sécurité sur le serveur, contrairement au certificat racine qui est lui donné au client afin d'avoir le bundle complet (la chaine complète : son certificat « client » et le certificat du serveur).

![](/images/pfsense-certificats/cert_pfsense2-1.png)


  1. La longueur de la clé doit au minimum être de 2048, au maximum 4096 car RSA ne peut gérer au maximum des clés de 4096 bits
  2. L'algorithme de hash doit appartenir à la famille SHA2 au minimum (MD5, SHA et SHA1 étant dépréciés) : nous choisissons SHA256
  3. La validité du certificat : une CA possède généralement une durée de validité longue. Notre choix est donc de la garder 10 ans
  4. Si vous avez une résolution DNS, choisissez le nom du serveur comme « Common Name » même si cela n'impacte pas la sécurité de votre certificat : nous laissons « internal-ca » qui est la valeur par défaut
  5. Remplir les différents champs qui concerne le sujet d'autorité du certificat. Par défaut tous les certificats émis par cette CA auront ces valeurs : FR pour le pays France
  6. PACA pour la région
  7. MARSEILLE pour la ville
  8. « Company Name » pour le nom de la société

Votre CA est maintenant créée. Vous pouvez la visualiser dans la page d'accueil des CA : 

![](/images/pfsense-certificats/cert_pfsense3.png)

## Le certificat Serveur

Pour authentifier le client au serveur, il faut bien évidement créé un certificat pour l'application souhaité : le serveur VPN.

Vous pouvez bien évidement utilisé le certificat généré précédement qui est la CA, mais ce n'est pas une bonne pratique et une bonne chose à faire en terme de sécurité. Cette CA est le garant des certificats et non pas le certificat pour une application donnée.

![](/images/pfsense-certificats/cert_pfsense4.png)

  1. Rendez-vous dans la partie « Certificates »
  2. Choisir un certificat interne « Create an internal Certificate »
  3. Saisir un nom pour rendre plus explicite le certificat que l'on va créer
  
![](/images/pfsense-certificats/cert_pfsense5.png)

  1. Choisir sur quelle CA sera certifié ce certificat : CA_VPN
  2. Choisir une clé RSA de 2048 pour le chiffrement asymétrique (le certificat en lui même). L'algorithme de hash sera du SHA256 comme pour la CA
  3. Une bonne pratique est de valider le certificat entre 1 an et 3 ans. Ici nous le validons pour 10 ans pour faciliter la gestion de celui-ci dans le lab
  4. Le Common Name sera le nom DNS pour atteindre la ressource : vpn.lab.dom par exemple
  5. Laisser par défaut les valeurs renseignées automatiquement : c'est celles que nous avons enregistré dans la CA

NB : Pourquoi valider un certificat sur une durée faible ? Pour mieux gérer les certificats, et de les régénérés souvent pour éviter qu'un ceritifcat soit compromis.

![](/images/pfsense-certificats/cert_pfsense6.png)

  1. Choisir le type du certificat : Server Certificate
  2. Rajouter l'Alternative Name afin qu'il correspond au DNS de l'application (nécéssaire pour les applications web, pas utile pour un VPN) : nous avons tout de même rajouté l'option : vpn.lab.dom

Votre certificat serveur est maitenant créé. Vous pouvez les visualiser dans la page dédiée : *

![](/images/pfsense-certificats/cert_pfsense7.png)

NB : Le premier certificat est le certificat par défaut de PFSense pour le mode HTTPS de la page d'administration. C'est un certificat auto-signé.

Votre certificat serveur est prêt pour être intégrer dans le serveur VPN. 

J'espère que l'article a été clair pour vous, n'hésitez pas de commenter si vous avez des questions.

Dernière étape, [la configuration du serveur VPN][10] : OpenVPN.

A bientôt

Fabio Pace

 [1]: images/pfsense-certificats/cert_pfsense0.png
 [2]: https://net-security.fr/system/openvpn-pfsense-part-2-lauthentification-ldap/
 [3]: images/pfsense-certificats/cert_pfsense1.png
 [4]: images/pfsense-certificats/cert_pfsense2-1.png
 [5]: images/pfsense-certificats/cert_pfsense3.png
 [6]: images/pfsense-certificats/cert_pfsense4.png
 [7]: images/pfsense-certificats/cert_pfsense5.png
 [8]: images/pfsense-certificats/cert_pfsense6.png
 [9]: images/pfsense-certificats/cert_pfsense7.png
 [10]: https://net-security.fr/system/openvpn-pfsense-part-4-openvpn-client-to-site/