---
title: "Installation d'un routeur Juniper depuis une clé USB"
author: Mickael Rigonnaux
type: post
date: 2021-12-28T00:12:35+00:00
url: /reseau/Installation-juniper-usb/
thumbnail: /images/juniper.png
featureImage: images/juniper.png
shareImage: /images/juniper.png
featured: true
tags: [
    "Juniper",
    "JunOS"
]
categories: [
    "reseau"
]
---

Bonjour ! Aujourd'hui un article rapide suite à la mise à jour d'un routeur. Nous avons remplacé la mémoire de la routing engine, qui est une carte flash "compact flash", une mise à jour de 2GB à 8GB.

Suite à ça, plus aucune version de JunOS n'était installée sur le routeur.

## Récupérer la bonne version de JunOS

De mon côté je n'avais pas accès à la version désirée (pour coller à la production) directement sur le site de Juniper. J'ai donc récupéré ma version sur Webarchive :
* https://archive.org/download/junos-srxsme

## Formater une clé en FAT32 & déposer le fichier

Après avoir récupéré la version voulue, il faut récupérer une clé USB, au moins de la même taille que la "compact flash" utilisée.

Il faut ensuite la formater en FAT32, si vous êtes sous Windows, ce n'est pas possible via la GUI sur les dernières versions.  Il est cependant possible de le faire via le cmd avec la commande `format /FS:FAT32 D:`. `D:` étant la clé USB.

Sous GNU/Linux, il existe plusieurs possibilités (Gparted, Nautilus, etc.). Vous pouvez aussi lancer la commande suivante : `mkfs.vfat -n CLE_VFAT /dev/sdc`

Une fois la clé formatée, il suffit de déposer le fichier télécharger directement à la racine sans créer de dossier.

## Installation de JunOS

Après avoir inséré la clé dans votre routeur, il faut se connecter en console et appyer sur entrée lorsque la ligne `Press Enter to stop auto bootsequencing and to enter loader prompt.` afin d'accéder au menu `loader>`. Après ça il suffit de lancer la commande `install file:///junos-xxx.tgz`. Si votre routeur le permet vous pouvez aussi lancer `install --format file:///junos-xxx.tgz`, cela permettra de corriger les problèmes de taille réduite sur la carte.

![Illustration installation Juniper](/images/juniper-1.png)

L'installation est maintenant lancée, il n'y a plus qu'à attendre et à redémarrer votre routeur.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !
