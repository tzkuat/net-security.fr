---
title: Bloquez les publicités avec Pi-Hole
author: Mickael Rigonnaux
type: post
date: 2019-03-21T20:25:33+00:00
url: /system/bloquez-les-publicites-avec-pi-hole/
featured_image: images/Pi-hole-vortex2.jpg
featureImage: images/Pi-hole-vortex2.jpg
shareImage: /images/Pi-hole-vortex2.jpg
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "Privacy",
    "Publicité"
]
categories: [
    "logiciellibre",
    "systeme"
]
---

Bonjour à tous ! Après un petit moment d'absence je vais vous présenter aujourd'hui un petit outil très pratique ! Je l'ai installé personnellement il y a plusieurs semaines chez moi, [Pi-Hole][1]. 

## Présentation

Pi-Hole qu'est-ce que c'est ? C'est un logiciel Open-Source développé par Jacob Selma qui permet de bloquer les publicités comme AdBlock par exemple. Le fonctionnement est par contre fondamentalement différent, car AdBlock lui charge quand même les publicités et utilise donc quand même votre bande passante. Alors que Pi-Hole lui permet de ne pas utiliser cette bande passante car il est basé sur le DNS. Ce qui permet grâce à des listes de noms définis de bloquer les publicités en filtrant les URLs. 

De plus, son utilisation permet de bloquer les publicités sur tous les appareils et avec toutes les applications ! Alors que les plugins Web permettent un blocage seulement au niveau du navigateur. 

Un des autres avantages est que le logiciel est très léger, il est possible soit de le conteneuriser soir de l'installer sur un Raspberry Pi. Dans mon cas il s'agit d'un Raspberry Pi 3B.

Au niveau configuration dans votre réseau vous devrez donc utiliser Pi-Hole comme serveur DNS, et aussi activer l'option DHCP si comme moi votre routeur FAI ne vous permet pas de modifier la configuration DHCP. 

Passons maintenant à l'installation ! 

## Installation 

Comme vu un peu plus haut de mon côté j'ai installé ce logiciel sur un Raspberry 3B sous [Raspbian][2]. 

J'ai tout d'abord configuré une IP fixe sur ce Raspberry dans le fichier /etc/dhcpcd.conf : 

![](/images/Pihole-static-ip.png)

Une fois modifié vous pouvez relancer le service réseau et vérifier la bonne configuration de votre Raspberry (vous serez sûrement déconnecté) avec les commandes suivantes : 

```
service networking restart
ip a
```

![](/images/Pihole-static-ip2.png)

Vous pouvez maintenant lancer l'installation de Pi-Hole sur votre Raspberry avec la commande suivante : 

```
curl -s « https://raw.githubusercontent.com/jacobsalmela/pi-hole/master/automated%20install/basic-install.sh » | bash
```


Vous pourrez suivre l'ensemble de l'installation : 

![](/images/Pihole-shell-install.png)

Plusieurs questions seront posées comme les serveurs DNS externes à utiliser, Cloudflare pour moi : 

![](/images/Pihole-config-dns.png)

Et à la fin vous devriez avoir un message comme celui ci : 

![](/images/Pihole-installed.png)

Une fois l'installation effectuée, vous pourrez vous rendre sur la magnifique interface d'administration de Pi-Hole basée sur AdminLTE. Et vu que j'ai littéralement détruit mon Pi-Hole pour réaliser ce magnifique tuto, toutes les stats sont à 0 (d'où le changement d'IP&#8230;) :  


![](/images/Pihole-admin-interface.png)

Et si comme moi vous n'avez pas la possibilité de modifier la configuration du DHCP de votre routeur, vous pouvez activer la fonction DHCP sur Pi-Hole dans « Settings » et « DHCP » : 

![](/images/Pihole-dhcp.png)

Vous pouvez maintenant désactiver le DHCP de votre router/box et profiter pleinement de votre Pi-Hole ! Personnellement pour parler des statistiques, cet outil permet de bloquer à peu près 10% de mon trafic à en croire l'interface d'administration après 2 mois d'utilisation. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://pi-hole.net/
 [2]: https://www.raspberrypi.org/downloads/raspbian/