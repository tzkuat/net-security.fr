---
title: Gérer les règles sur Suricata
author: Mickael Rigonnaux
type: post
date: 2019-11-21T16:23:10+00:00
url: /security/suricata-rules/
thumbnail: /images/suricata-logo.png
tags: [
    "logiciellibre",
    "Suricata",
    "IDS",
    "NIDS"
]
categories: [
    "securite"
]
---
![](/images/suricata-logo.png)

Bonjour à tous ! Aujourd'hui nous allons aborder la gestion des règles sur l'IDS/IPS Suricata. 

Je ne vais pas revenir sur la présentation de l'outil et son intérêt dans cet article car cela a [déjà été fait sur ce blog par mon collaborateur Fabio Pace.][1]

Pour information, Suricata est disponible [depuis peu en version 5.0][2]. 

## La problématique

Suricata c'est très bien, mais par défaut l'ensemble des règles sont activées, et cela engendre un nombre d'alerte important. Elles sont dans mon cas (plus de 1000 serveurs avec beaucoup de flux) :

  * Trop nombreuses
  * Pleine de faux positifs
  * Peu pertinentes

Il faut donc prendre en compte le contexte lors de l'installation et quels sont réellement vos besoins pour mieux cibler l'utilisation. Cela change énormément selon les cœurs de métiers et la taille de l'infrastructure à superviser. 

J'ai donc d'abord établi une liste simple des évènements que je voulais superviser avec mon IDS. En voici quelques exemples : 

  * Tentative de scan sur le réseau (Nessus, nmap, OpenVAS, etc.)
  * Certificat expiré ou auto-signé
  * Tentative d'exploit (BlueKeep, Wannacry, etc.)

En ciblant seulement sur ces évènements les règles à appliquer sont fortement réduites. Nous avons donc moins d'alertes sur le réseau et elles sont plus pertinentes. 

De plus, il est également possible lorsqu'une catégorie est maîtrisée (gestion des faux positifs, etc.) d'ajouter des règles pour rendre le système de plus en plus complet.

Le plus gros du travail donc à faire en amont de l'installation de l'IDS afin d'identifier les services à monitorer et de savoir comment vous allez gérer vos alertes. Il y aura également du boulot en avale pour le traitement et le filtrage des alertes. 

## La gestion des règles

Nous pouvons maintenant voir la gestion des règles sur Suricata. 

Tout d'abord, voici comment une règle est formée : 

```
  alert tcp any any -> any any (msg: « ATTACK [PTsecurity] Possible Bluekeep RDP exploit CVE-2019-0708 « ; flow: established, to_server; content: « |17 03 01 00 20| »; depth: 5; fast_pattern; content: « |17 03 01| »; distance: 32; within: 3; byte_test: 2, >, 450, 0, relative, big; flowbits: set, BlueKeep.pkt1; flowbits: noalert; reference: cve, 2019-0708; reference: url, github.com/Ekultek/BlueKeep; reference: url, github.com/ptresearch/AttackDetection; metadata: Open Ptsecurity.com ruleset; classtype: attempted-admin; sid: 10004861; rev: 1;)
```

La 1ère partie « alert » correspond à l'action à effectuer en cas de détection 

La 2ème partie « tcp any any -> any any » correspond à l'en-tête. C'est cette partie qui permet de définir le sens de l'alerte ainsi que les réseaux et protocoles. 

La dernière partie correspond aux options appliquées à la règle. C'est ici que vous pouvez configurer plus particulièrement la règle en fonction des informations. 

Vous pouvez [trouver ici][3] également toutes les informations sur les différentes configurations possibles. 

### Ajout des règles

Par défaut, lorsque vous tapez la commande « suricata-update » et que vous n'avez pas défini de source, Suricata tente de récupérer l'ensemble des règles de « Emerging Threats ». Ce qui fait plus de 20 000 règles ajoutées dans le fichier /var/lib/suricata/rules/suricata.rules : 


```
suricata-update
```


Résultat : ![](/images/suricata-rules-1.png)

De mon côté, j'utilise les règles présentes sur [ce lien][5], elles viennent de la même source que celles récupérées par défaut et permettent de les gérer plus finement. Pour ce faire il faut utiliser la commande suivante :


```
suricata-update add-source ID URL_RESOURCES
```


Un exemple avec l'ajout des règles relative aux exploits : 

```
suricata-update add-source exploit https://rules.emergingthreats.net/open/suricata/rules/emerging-exploit.rules
```


Résultat : ![](/images/suricata_rules_2.png)

Il est maintenant possible de faire la même chose pour les autres règles que vous implémentez dans votre IDS. De plus, avec la commande « suricata-update » il est possible de mettre à jour et de récupérer quotidiennement les règles.

### Ajout des règles custom

J'imagine que vous avez dans votre SI des spécificités, vous aurez donc besoin de créer des règles spécifiques, de les mettre à jour et les ajouter automatiquement. Pour ce faire j'utilise la même commande qu'au-dessus, mais en source j'ai ajouté un fichier git personnel. Par exemple avec mon git publique : 


```
suricata-update add-source custom https://raw.githubusercontent.com/tzkuat/suricata-resources/master/custom.rules
```

Dans le cadre d'une entreprise vous pouvez utiliser le repo interne de la société. 

Il faudra en suite lancer la commande « suricata-update » pour mettre à jour vos règles. 

## IP Reputation

Dans cette dernière partie nous allons voir comment installer la module IP Reputation de Suricata. Il permet de générer des alarmes si du traffic est généré vers des IPs spécifiques. Dans ce cas j'utilise une blacklist publique ainsi que la liste des IPs de Tor.

La configuration se fait dans le fichier /etc/suricata/suricata.yaml et il faut activer les configurations suivantes : 


```
# IP Reputation
reputation-categories-file: /etc/suricata/iprep/categories.txt
default-reputation-path: /etc/suricata/iprep
reputation-files:
- blacklist.list
- tor-nodes.list
```


Il faut en suite créer le dossier /etc/suricata/iprep/ et le fichier categories.txt. 

Ce dernier est formé de la manière suivante : 

  * ID,Nom,Description

Par exemple : 

  * 1,Tor-hosts,Tor nodes IPs
  * 2,Blocklist,FireHol, Blacklist IP

Et pour finir, les fichiers blacklist.list & tor-nodes.list sont configurés avec des IPs, l'ID que vous définissez dans les catégories et pour finir le score de l'IP. 

Par exemple : 

  * 1.2.3.4,1,100

Et pour finir il faut ajouter des règles pour activer vraiment ce module et récupérer les alertes. Elles [sont disponibles ici][7].

Vous avez toutes les ressources pour ce module ici : 

  * <https://suricata.readthedocs.io/en/suricata-5.0.0/rules/ip-reputation-rules.html>
  * <https://suricata.readthedocs.io/en/suricata-5.0.0/reputation/ipreputation/ip-reputation.html>

De mon côté personnellement j'utilise un script que j'ai développé pour installer le module IPREP. Je me base sur les IPs de Tor et sur la blacklist de FireHol. Le script est [disponible directement sur mon Git.][8]

Il s'utilise de la façon suivante : 


```
Installation 
./iprep.sh --install

MàJ de toutes les IPs
./iprep.sh --update-all

MàJ de Tor ou FireHol
./iprep.sh --update-tor ou --update-blacklist
```


Il ne reste plus qu'à ajouter les règles pour activer le module mais la configuration est faite. Le script est un simple script Bash, il est très facile à modifier et à comprendre. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://net-security.fr/security/installation-dun-nids-suricata/
 [2]: https://suricata-ids.org/2019/10/15/announcing-suricata-5-0-0/
 [3]: https://suricata.readthedocs.io/en/suricata-5.0.0/rules/index.html
 [4]: images/suricata-rules-1.png
 [5]: https://rules.emergingthreats.net/open/suricata/rules/
 [6]: images/suricata_rules_2.png
 [7]: https://suricata.readthedocs.io/en/suricata-4.1.4/rules/ip-reputation-rules.html
 [8]: https://github.com/tzkuat/suricata-resources/blob/master/iprep.sh