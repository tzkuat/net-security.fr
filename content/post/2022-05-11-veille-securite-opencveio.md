---
title: Veille en vulnérabilités avec OpenCVE.io
author: Mickael Rigonnaux
type: post
date: 2022-05-11T14:05:44+00:00
url: /security/veille-opencveio
thumbnail: /images/opencve/logo.png
featureImage: images/opencve/logo.png
shareImage: /images/opencve/logo.png
featured: true
tags: [
    "opensource",
    "cve",
    "vulnerabilité",
    "veille"
]
categories: [
    "securite"
]

---
Bonjour à tous ! Aujourd'hui un article rapide (et qui traîne depuis longtemps) sur un outil open-source que je suis depuis un petit moment : OpenCVE.io.

OpenCVE est un outil qui répond à un besoin que nous rencontrons tous (ou presque) dans la gestion de la sécurité et du patch-management : la gestion des failles de sécurité et la veille en vulnérabilités.

Nous utilisons tous divers systèmes, logiciels, matériels... Tous ces éléments peuvent être touchés par des failles de sécurité, OpenCVE.io permet d'être alerté en cas de vulnérabilité sur une des briques de l'infrastructure. Car il est aujourd'hui impossible sans un outil ou des filtres d'analyser toutes les vulnérabilités qui sortent chaque jour.

Pour faire énormément de veille dans le cadre pro/perso, c'est un outil très important sur cet aspect. Il faut néanmoins avoir connaissance des différentes solutions qui sont utilisées sur son infrastructure et ce n'est pas toujours facile.

Plus concrètement, c'est un outil Web qui permet de s'abonner à des vendeurs et/ou des logiciels : Apache2, Ubuntu, PHP, Microsoft Windows... Et d'être alerté en cas de vulnérabilité sur ces catégories. Il permet en plus de ça d'accéder à la liste complète des CVE et de les filtrer par vendeur, produit, etc.

Ce logiciel est développé initialement par [Nicolas Crocfer](https://twitter.com/ncrocfer) & [Laurent Durnez](https://twitter.com/LaurentDurnez), il est aujourd'hui maintenu par une petite communauté sur Github et a probablement de beaux jours devant lui. Il est proposé sous licence open-source "[Business Source Licence](https://github.com/opencve/opencve/blob/master/LICENSE)" mais devrait passer sous licence libre Apache v2 le 22/01/2025 si l'on en croit la licence du projet sur Github.

Le projet est presque entièrement développé en python, toutes les sources sont disponibles sur le projet Github :
* https://github.com/opencve/opencve

Il dispose d'une interface Web ainsi que d'une API avec de multiples fonctionnalités.

## Installation

Vous avec deux choix pour utiliser ce produit, soit utiliser la version SaaS disponible à l'adresse [opencve.io](https://opencve.io/login) (vous pouvez créer un compte, elle est accessible gratuitement) soit installer et configurer l'outil selon vos envies en suivant la documentation. Deux installations sont disponibles, en conteneur ou en direct sur la machine :
* Docker : https://docs.opencve.io/installation/docker/
* Installation simple : https://docs.opencve.io/installation/manual/

L'installation ne sera pas détaillée dans cet article, cela n'a pas d'intérêt je n'aurais que copier/coller la documentation officielle.

De mon côté à titre personnel j'utilise la version SaaS.

## A l'utilisation

A l'utilisation l'outil se veut très simple, une fois connecté vous avez accès aux dernières vulnérabilités de votre environnement :
![](/images/opencve/opencve-1.jpg)

Pour gérer vos différentes sources (logiciel/vendeur/produit) vous devez vous rendre dans l'onglet "Vendors & Product" ou CPE :
![](/images/opencve/opencve-2.jpg)

Depuis ce menu vous aller pouvoir ajouter les différentes sources que vous voulez suivre, de mon côté j'ai ajouté plusieurs vendeurs et produits comme vous avez pu le voir sur la page d'accueil :
![](/images/opencve/opencve-3.jpg)

Le fait d'ajouter ces éléments va permettre plusieurs choses comme :
* Le suivi des vulnérabilités sur la page d'accueil
* Les alertes par mail en cas de nouvelle vulnérabilité

La gestion des différentes notifications est possible directement dans les paramètres de votre utilisateur en fonction de vos préférences :
![](/images/opencve/opencve-4.jpg)

Si vous voulez en recevoir par mail, cela aura cette forme la :
![](/images/opencve/opencve-5.jpg)

Vous pouvez également utiliser les autres onglets pour chercher les différentes vulnérabilités, que ça soit par produit, vendeur, catégorie... L'outil est complet en ce sens.

## Conclusion

Personnellement c'est un outil que je suis depuis un moment et même si cet article n'a rien de révolutionnaire je voulais le présenter et lui donner un peu de visibilité à mon humble niveau. C'est également une idée que j'avais en tête depuis un moment (je l'avais d'ailleurs donné en Workshop à des étudiants) car avant ça je n'avais trouvé aucun logiciel capable de filtrer les (très nombreuses) vulnérabilités qui sortent chaque jour et d'avoir des alertes seulement sur ce que j'utilise moi.


J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !