---
title: "OpenSSL : Analyser les informations d'un certificat X.509"
author: Mickael Rigonnaux
type: post
date: 2024-05-28T08:00:44+00:00
url: /securite/openssl-analyse-certs-x509
thumbnail: /images/tls-infos.png
shareImage: /images/tls-infos.png
featured: true
toc: true
tags: [
    "openssl",
    "cryptographie",
    "certificat",
    "csr"
]
categories: [
    "securite"
]

---

Bonjour à tous ! Aujourd'hui un autre article sur OpenSSL, nous allons voir ensemble comment récupérer et analyser toutes les informations d'un certificat x509 : numéro de série, DN du l'autorité, DN du certificat, etc.

## Introduction

Dernièrement nous avons beaucoup parlé des certificats sur ce blog : générer des certificats/CSR avec des SANs, vérifier l'appartenance d'une clé à un certificat... Mais nous n'avons jamais abordé la partie la plus basique : quelles sont les informations contenues par un certificat et surtout, comment les lire et les comprendre.

J'avais déjà réalisé cet exercice mais dans une présentation, qui est [disponible ici](https://repo.tzku.at/presentation/CRYPTO-tzkuat.pdf).

## Certificat x509 ?

Lorsque l'on aborde les certificats SSL/TLS, le format utilisé est X.509 en version 3. x509 est une norme qui permet de spécifier les formats pour les certificats à clé publique. Si cela vous intéresse, vous trouverez toutes les informations concernant ce format dans les [RFC5280](https://datatracker.ietf.org/doc/html/rfc5280) et [RFC2459](https://datatracker.ietf.org/doc/html/rfc2459).

Tout ce que vous devez savoir c'est que cette norme X.509 qui donne la structure des certificats que nous utilisons tous les jours.

## Structure d'un certificat X.509

La structure d'un certificat est disponible dans la [RFC2459](https://datatracker.ietf.org/doc/html/rfc2459#section-4) et nous retrouvons les éléments suivants :
* Version
* Numéro de série
* Algorithme de signature du certificat
* DN : Distinguished name de l'autorité ou du délivreur
* Validité
  * Pas avant
  * Pas après
* DN de l'objet du certificat
* Informations sur la clé publique
  * Algorithme de la clé publique
  * Clé publique
* Extensions
  * Liste des extensions (SAN, etc.)
* Signature des informations par l'autorité de certification

Nous allons maintenant voir comment se traduisent ses informations dans la pratique.

## Analyse des informations d'un certificat

Comme exemple je vais prendre le certificat utilisé pour ce blog, qui utilise des SAN et d'autres extensions.

Pour cela, on va récupérer toutes les informations avec la commande suivante :
* `openssl x509 -in certificate.crt -text -noout`

Ce qui nous donne comme résultat :
```
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            03:aa:0a:46:5d:c8:67:1c:3c:8d:f8:9e:b6:fc:c5:52:3e:e0
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = US, O = Let's Encrypt, CN = R3
        Validity
            Not Before: May  6 18:03:27 2024 GMT
            Not After : Aug  4 18:03:26 2024 GMT
        Subject: CN = net-security.fr
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:ae:81:84:f8:e1:7f:e0:83:34:cf:0f:06:f5:37:
                    5a:6b:35:cd:20:7b:43:dc:26:9b:60:75:27:b4:40:
                    ea:85:29:94:89:93:8d:95:ff:b0:a6:0a:22:25:b2:
                    ef:e2:ae:2d:5e:bf:22:62:15:50:2b:3b:5f:9c:73:
                    34:3f:0d:ee:55:1c:66:62:c7:d3:57:e6:c9:cd:85:
                    1f:2d:41:6d:67:c1:b7:2a:eb:cf:9f:7d:17:e2:30:
                    49:df:54:67:04:41:e4:2b:8a:63:9e:dc:a6:d7:65:
                    8c:54:1a:40:df:6d:7a:64:2d:43:77:af:e1:5c:14:
                    ed:b7:94:ff:ce:6b:db:a6:15:91:9d:e1:64:a2:94:
                    29:f2:06:49:00:e8:95:66:f2:f9:1f:2d:91:63:32:
                    fe:08:f3:9b:e7:f0:e3:4b:fb:46:4b:f7:1f:42:05:
                    18:6a:29:e0:40:00:1b:ef:09:7b:7a:94:30:fd:ab:
                    79:6c:7e:22:b8:c3:4c:b4:bd:24:b7:24:37:fa:99:
                    7d:28:de:3d:33:ed:a8:12:11:43:02:f7:cc:a5:51:
                    4e:be:ca:5a:44:3e:79:bf:cf:cf:51:1e:fd:cd:ee:
                    99:c9:0c:c0:b2:81:ef:df:1b:d4:b2:6a:e3:bf:c9:
                    6f:c1:99:54:fa:58:e6:58:b6:6b:d4:3a:e2:4d:b7:
                    16:1f
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment
            X509v3 Extended Key Usage:
                TLS Web Server Authentication, TLS Web Client Authentication
            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Subject Key Identifier:
                CE:22:C3:D8:02:10:09:AD:C4:3F:18:75:FD:C6:8E:22:0F:12:FD:89
            X509v3 Authority Key Identifier:
                14:2E:B3:17:B7:58:56:CB:AE:50:09:40:E6:1F:AF:9D:8B:14:C2:C6
            Authority Information Access:
                OCSP - URI:http://r3.o.lencr.org
                CA Issuers - URI:http://r3.i.lencr.org/
            X509v3 Subject Alternative Name:
                DNS:links.tzku.at, DNS:net-security.fr, DNS:tzku.at, DNS:www.net-security.fr, DNS:www.tzku.at
            X509v3 Certificate Policies:
                Policy: 2.23.140.1.2.1
            CT Precertificate SCTs:
                Signed Certificate Timestamp:
                    Version   : v1 (0x0)
                    Log ID    : 19:98:10:71:09:F0:D6:52:2E:30:80:D2:9E:3F:64:BB:
                                83:6E:28:CC:F9:0F:52:8E:EE:DF:CE:4A:3F:16:B4:CA
                    Timestamp : May  6 19:03:27.604 2024 GMT
                    Extensions: none
                    Signature : ecdsa-with-SHA256
                                30:45:02:21:00:D7:0A:0C:7D:AB:33:2D:50:A2:12:79:
                                CF:67:06:09:91:13:6C:EF:6A:7D:10:51:9C:95:EB:CC:
                                CD:C4:03:0C:16:02:20:6E:C3:C2:E6:E5:D8:F9:FE:03:
                                7B:3B:B3:FF:9C:3D:83:57:0C:4C:2E:99:D6:92:AD:0E:
                                C6:44:61:E7:2E:EA:BF
                Signed Certificate Timestamp:
                    Version   : v1 (0x0)
                    Log ID    : 76:FF:88:3F:0A:B6:FB:95:51:C2:61:CC:F5:87:BA:34:
                                B4:A4:CD:BB:29:DC:68:42:0A:9F:E6:67:4C:5A:3A:74
                    Timestamp : May  6 19:03:27.669 2024 GMT
                    Extensions: none
                    Signature : ecdsa-with-SHA256
                                30:45:02:20:32:A1:D9:FB:21:1E:C0:1F:43:B8:21:33:
                                AA:9E:63:64:3B:20:EF:4C:21:94:88:42:B0:70:28:A7:
                                44:47:9B:FD:02:21:00:D7:17:F9:02:C7:C8:0A:F7:ED:
                                2F:B1:1F:F8:8C:41:A1:4B:DF:80:14:B0:BF:7D:F7:8B:
                                F9:C2:65:77:85:53:82
    Signature Algorithm: sha256WithRSAEncryption
    Signature Value:
        29:70:b0:0a:11:f4:96:6e:63:be:d5:c6:a7:15:41:f6:bd:08:
        ac:ad:49:22:c1:7f:8d:8e:49:aa:76:d6:18:e9:67:cd:76:c8:
        32:71:eb:29:b2:4b:cb:f9:07:3b:36:c9:99:a9:d2:ca:ba:5b:
        06:0f:62:af:7f:c9:72:28:09:9f:8c:68:d6:6e:1a:97:87:fc:
        15:c3:19:33:d6:c1:cb:b5:b8:dc:f5:30:85:48:47:3e:ce:f4:
        9a:63:91:af:fd:67:6b:74:ad:da:d9:29:2d:b2:d3:68:01:da:
        df:74:13:74:1b:a0:cf:f8:06:c3:0b:52:fa:f6:29:f4:2d:d8:
        78:c9:fb:f4:15:b5:6a:83:5b:3e:1d:df:d0:15:ba:1c:a3:a8:
        94:28:ff:d8:8f:92:bf:98:3f:40:a3:10:0a:11:92:5e:82:d5:
        b9:04:04:d9:ce:64:fd:44:c5:a6:9f:cf:8b:06:04:07:aa:45:
        f0:ae:c6:65:e0:36:9c:39:b2:d4:d8:97:c9:e4:7e:18:dc:19:
        1d:29:76:31:8a:53:bc:b4:97:00:ff:f6:16:f6:8b:e0:ae:97:
        70:40:81:47:22:b0:3f:cf:50:3d:06:90:82:97:d7:9b:df:e4:
        d9:8a:18:fd:93:f8:f5:00:bd:60:09:6a:5e:3c:09:7a:10:4e:
        4d:1b:1e:bd
```

Nous retrouvons toutes les informations listées dans la structure juste au dessus. Nous allons les passer en revue une par une.

### Version

Cette section comporte seulement la version de la norme X.509 utilisée. Globalement, c'est la version 3 qui est largement utilisée aujourd'hui.

Cela se traduit par la ligne suivante :
* `Version: 3 (0x2)`


### Numéro de série

Le numéro de série est donné directement par l'autorité de certification. De son côté c'est un numéro unique qui permet d'identifier le certificat. Si vous générez des certificats auto-signé, ce champ n'a pas d'importance.

Cela se traduit par la ligne suivante :
```
Serial Number:
            03:aa:0a:46:5d:c8:67:1c:3c:8d:f8:9e:b6:fc:c5:52:3e:e0
```

### Algorithme de signature du certificat

Cette section va contenir les algorithmes utilisés pour signer notre certificat. Cela va dépendre de la méthode de signature utilisé par l'autorité de certification. Dans mon cas, via Let's Encrypt c'est sha256 avec RSA qui est utilisé.

Nous retrouvons la ligne suivante :
* `Signature Algorithm: sha256WithRSAEncryption`

### DN : Distinguished name de l'autorité de certification

Comme nous avons un DN sur notre certificat, l'autorité en possède un également. Il permet de donner plusieurs informations : le pays (C), l'organisation (O), le common name (CN). Dans cette section nous retrouvons toutes les informations relatives à l'autorité qui a signé notre certificat. C'est un élément important pour vérifier d'où vient notre certificat.

La ligne concernée est la suivante :
* `Issuer: C = US, O = Let's Encrypt, CN = R3`

### Validité

Cette section est très importante car c'est ici que nous retrouvons la durée de vie de notre certificat. Qui se traduit toujours avec 2 éléments : pas avant et pas après.

Car il est possible de générer un certificat qui n'est pas encore valide (mais qui le sera demain).

Voici les lignes :
```
Validity
    Not Before: May  6 18:03:27 2024 GMT
    Not After : Aug  4 18:03:26 2024 GMT
```

### DN de l'object du certificat

C'est la section la plus commune ou vous allez retrouver les informations de votre certificat : CN, O, L, C. Et c'est un bon exemple ici car cela permet de montrer que tous ces éléments sont **optionnels** ils sont recommandés pour la traçabilité et l'identification mais dans mon cas, nous avons seulement l'object CN.

Voici la ligne dans mon exemple :
* `Subject: CN = net-security.fr` 

### Informations sur la clé publique

Ici on retrouve plusieurs informations relatives à la clé publique de notre certificat. Par exemple :
* La taille de la clé
* La méthode de chiffrement
* Le modulo (dans le cadre de RSA)

Voici les lignes concernées :
```
Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:ae:81:84:f8:e1:7f:e0:83:34:cf:0f:06:f5:37:
                    5a:6b:35:cd:20:7b:43:dc:26:9b:60:75:27:b4:40:
                    ea:85:29:94:89:93:8d:95:ff:b0:a6:0a:22:25:b2:
                    ef:e2:ae:2d:5e:bf:22:62:15:50:2b:3b:5f:9c:73:
                    34:3f:0d:ee:55:1c:66:62:c7:d3:57:e6:c9:cd:85:
                    1f:2d:41:6d:67:c1:b7:2a:eb:cf:9f:7d:17:e2:30:
                    49:df:54:67:04:41:e4:2b:8a:63:9e:dc:a6:d7:65:
                    8c:54:1a:40:df:6d:7a:64:2d:43:77:af:e1:5c:14:
                    ed:b7:94:ff:ce:6b:db:a6:15:91:9d:e1:64:a2:94:
                    29:f2:06:49:00:e8:95:66:f2:f9:1f:2d:91:63:32:
                    fe:08:f3:9b:e7:f0:e3:4b:fb:46:4b:f7:1f:42:05:
                    18:6a:29:e0:40:00:1b:ef:09:7b:7a:94:30:fd:ab:
                    79:6c:7e:22:b8:c3:4c:b4:bd:24:b7:24:37:fa:99:
                    7d:28:de:3d:33:ed:a8:12:11:43:02:f7:cc:a5:51:
                    4e:be:ca:5a:44:3e:79:bf:cf:cf:51:1e:fd:cd:ee:
                    99:c9:0c:c0:b2:81:ef:df:1b:d4:b2:6a:e3:bf:c9:
                    6f:c1:99:54:fa:58:e6:58:b6:6b:d4:3a:e2:4d:b7:
                    16:1f
                Exponent: 65537 (0x10001)
```


### Extensions

Dans cette partie nous retrouvons toutes les extensions X.509 qui sont utilisées par notre certificat. Il en existe un très grand nombre mais parmi les plus courantes :
* Utilisation de SAN (Subject Alternative Name)
* Utilisation de certificat "client"
* Utilisation de certificat "serveur"
* Extension pour utiliser le certificat pour en signer d'autre
* Etc.

Ces lignes sont concernées :
```
        X509v3 extensions:
            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment
            X509v3 Extended Key Usage:
                TLS Web Server Authentication, TLS Web Client Authentication
            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Subject Key Identifier:
                CE:22:C3:D8:02:10:09:AD:C4:3F:18:75:FD:C6:8E:22:0F:12:FD:89
            X509v3 Authority Key Identifier:
                14:2E:B3:17:B7:58:56:CB:AE:50:09:40:E6:1F:AF:9D:8B:14:C2:C6
            Authority Information Access:
                OCSP - URI:http://r3.o.lencr.org
                CA Issuers - URI:http://r3.i.lencr.org/
            X509v3 Subject Alternative Name:
                DNS:links.tzku.at, DNS:net-security.fr, DNS:tzku.at, DNS:www.net-security.fr, DNS:www.tzku.at
            X509v3 Certificate Policies:
                Policy: 2.23.140.1.2.1
            CT Precertificate SCTs:
                Signed Certificate Timestamp:
                    Version   : v1 (0x0)
                    Log ID    : 19:98:10:71:09:F0:D6:52:2E:30:80:D2:9E:3F:64:BB:
                                83:6E:28:CC:F9:0F:52:8E:EE:DF:CE:4A:3F:16:B4:CA
                    Timestamp : May  6 19:03:27.604 2024 GMT
                    Extensions: none
                    Signature : ecdsa-with-SHA256
                                30:45:02:21:00:D7:0A:0C:7D:AB:33:2D:50:A2:12:79:
                                CF:67:06:09:91:13:6C:EF:6A:7D:10:51:9C:95:EB:CC:
                                CD:C4:03:0C:16:02:20:6E:C3:C2:E6:E5:D8:F9:FE:03:
                                7B:3B:B3:FF:9C:3D:83:57:0C:4C:2E:99:D6:92:AD:0E:
                                C6:44:61:E7:2E:EA:BF
                Signed Certificate Timestamp:
                    Version   : v1 (0x0)
                    Log ID    : 76:FF:88:3F:0A:B6:FB:95:51:C2:61:CC:F5:87:BA:34:
                                B4:A4:CD:BB:29:DC:68:42:0A:9F:E6:67:4C:5A:3A:74
                    Timestamp : May  6 19:03:27.669 2024 GMT
                    Extensions: none
                    Signature : ecdsa-with-SHA256
                                30:45:02:20:32:A1:D9:FB:21:1E:C0:1F:43:B8:21:33:
                                AA:9E:63:64:3B:20:EF:4C:21:94:88:42:B0:70:28:A7:
                                44:47:9B:FD:02:21:00:D7:17:F9:02:C7:C8:0A:F7:ED:
                                2F:B1:1F:F8:8C:41:A1:4B:DF:80:14:B0:BF:7D:F7:8B:
                                F9:C2:65:77:85:53:82
```

On retrouve bien les SANs par exemple. On voit également la partie liée à la Certificate Transparency avec la section `CT Precertificate SCTs`.

### Signature des informations par l'autorité de certification

Pour terminer, nous retrouvons la dernière partie relative à la signature des différentes informations de notre certificat par l'autorité de certification.

On retrouve donc l'algorithme de signature ainsi que la signature en elle même :
```
    Signature Algorithm: sha256WithRSAEncryption
    Signature Value:
        29:70:b0:0a:11:f4:96:6e:63:be:d5:c6:a7:15:41:f6:bd:08:
        ac:ad:49:22:c1:7f:8d:8e:49:aa:76:d6:18:e9:67:cd:76:c8:
        32:71:eb:29:b2:4b:cb:f9:07:3b:36:c9:99:a9:d2:ca:ba:5b:
        06:0f:62:af:7f:c9:72:28:09:9f:8c:68:d6:6e:1a:97:87:fc:
        15:c3:19:33:d6:c1:cb:b5:b8:dc:f5:30:85:48:47:3e:ce:f4:
        9a:63:91:af:fd:67:6b:74:ad:da:d9:29:2d:b2:d3:68:01:da:
        df:74:13:74:1b:a0:cf:f8:06:c3:0b:52:fa:f6:29:f4:2d:d8:
        78:c9:fb:f4:15:b5:6a:83:5b:3e:1d:df:d0:15:ba:1c:a3:a8:
        94:28:ff:d8:8f:92:bf:98:3f:40:a3:10:0a:11:92:5e:82:d5:
        b9:04:04:d9:ce:64:fd:44:c5:a6:9f:cf:8b:06:04:07:aa:45:
        f0:ae:c6:65:e0:36:9c:39:b2:d4:d8:97:c9:e4:7e:18:dc:19:
        1d:29:76:31:8a:53:bc:b4:97:00:ff:f6:16:f6:8b:e0:ae:97:
        70:40:81:47:22:b0:3f:cf:50:3d:06:90:82:97:d7:9b:df:e4:
        d9:8a:18:fd:93:f8:f5:00:bd:60:09:6a:5e:3c:09:7a:10:4e:
        4d:1b:1e:bd
```

## Conclusion

Et voilà, nous venons de voir rapidement tous les éléments/champs d'un certificat X.509v3.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat