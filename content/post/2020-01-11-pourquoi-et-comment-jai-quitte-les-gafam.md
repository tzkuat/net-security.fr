---
title: "Pourquoi et comment j'ai quitté les GAFAM"
author: Mickael Rigonnaux
type: post
date: 2020-01-11T11:20:29+00:00
url: /security/privacy/pourquoi-et-comment-jai-quitte-les-gafam/
thumbnail: /images/gafam_logo-1.png
tags: [
    "GNU/Linux",
    "gafam",
    "vieprivee"
]
categories: [
    "systeme",
    "securite",
    "logiciellibre"
]

---

Bonjour à tous, aujourd'hui un article non technique mais qui porte sur un sujet très abordé en ce moment, les [GAFAM][1] et la protection des données. 

## Introduction

Comme indiqué dans plusieurs articles, j'ai changé depuis à peu près un an ma façon de consommer des services sur Internet, et ça pour plusieurs raisons. 

Avant ça, j'étais comme « tout le monde ». J'utilisais Google avec mon adresse Gmail, Windows 10 sur mon ordinateur de tous les jours, Facebook comme réseau principal, etc. 

J'ai pris conscience récemment de plusieurs choses. Comme de la collecte énorme de données par les différents services. Notamment depuis l'affaire Snowden, d'ailleurs je vous invite à lire [son livre][2] si ce n'est pas déjà fait. 

J'ai également changé ma vision sur Internet, et son fonctionnement actuel, totalement à l'inverse de ce pourquoi il était prévu. 

## Ma vision d'internet

Pour moi internet est un outil merveilleux pour le partage de connaissance et les échanges entre les différentes personnes. Ce blog a été créé sur cette idée.

C'est également une plateforme ou tout le monde devrait pouvoir s'exprimer librement, sans être censuré ou sans subir aucune oppression.

C'est une source de savoir sans fin, ou il est possible d'apprendre sur presque tous les sujets avec des projets communautaires énormes comme Wikipédia par exemple.

Et pour passer rapidement, aucune organisation privée ou publique ne devrait avoir le droit de le contrôler.

Je sais, ça serait trop beau et on ne peut pas faire confiance à l'homme pour se réguler. Cela engendrerait sûrement énormément de dérives.  


## Internet et ses dérives

Nous vivons aujourd'hui dans un Internet « centralisé » ou la plupart des services sont gérés par des grosses sociétés comme Google, Facebook, Microsoft & Apple. La plupart des utilisateurs de ces différents services ne sont pas conscients de la collecte de masse qui est faite. Car aujourd'hui si on se demande qui en sait le plus sur nous, la réponse est évidente. Notre téléphone. Il est avec nous tout le temps, il sait à quelle heure on se lève, on se couche, ou nous allons, avec qui nous parlons, etc.

Et quand nous leur parlons de cette collecte et de l'importance, nous avons souvent droit à « Je m'en fiche qu'ils prennent mes informations. » ou encore « Je n'ai rien à cacher. ». 

Pour illustrer cette bêtise nous pouvons reprendre l'analogie de d'Edward Snowden : 

> Dire que votre droit à la vie privée importe peu, car vous n’avez rien à cacher revient à dire que votre liberté d’expression importe peu, car vous n’avez rien à dire. Car même si vous n’utilisez pas vos droits aujourd’hui, d’autres en ont besoin. Cela revient à dire : les autres ne m’intéressent pas. »
> -- Edward Snowden

Et pour continuer sur la lancée, l'utilisation d'internet ne rapproche plus les personnes. Elles les éloignent encore plus. La plupart des gens utilisent les réseaux sociaux seulement avec des gens qu'ils connaissent déjà. Ils n’interagissent que par des « likes » ou pour identifier leurs amis sur des photos de chat ou des sujets encore plus débile les uns que les autres (faux concours, fake news, etc.). Et plus récemment pour se mettre en avant et montrer une vie qui n'est pas la leur, tout n'est qu'apparence. 

Et avec l'arrivée des « influenceurs » et des nouvelles générations. Mise en avant de produit contrefait, drop-shipping, diffusion de fausses informations, etc.

On est bien loin de l'idée de départ d'Internet et des merveilles promises...

Mais c'est difficile d'en vouloir à ces personnes, car il est très difficile de sortir de ce « confort » et il n'existe, malheureusement aucune sensibilisation. 

Et je ne parle pas seulement des réseaux sociaux, ne plus utiliser les principaux services disponibles demande une grosse remise en question et des efforts considérables.

Maintenant un autre sujet encore plus important pour moi, la censure et l'impact des états sur internet. Par exemple, l'impact des réseaux sociaux sur la campagne de Donald Trump. Ou encore de la grande mode pour les états totalitaires de « couper » internet dans tout un pays.

Sans parler des états, il y a également Github qui coupe ses services dans certains pays.

Encore une fois, nous sommes bien loin des grandes idées fondatrices de l'Internet. Je vais m'arrêter la, je me suis assez apitoyé sur mon sort et lister toutes les dérives prendrais beaucoup trop de temps. 

## Ma contribution

De mon côté, ma petite contribution est d'utiliser des solutions respectueuses de la vie privée des utilisateurs et de les promouvoir. Dans l'idéal des solutions libres et Open Source. 

En plus de ça, j'essaie de préparer des sessions de sensibilisations, de répandre mes idées autour de moi (et ça fonctionne sur certaines personnes). J'essaie également d'héberger des services et de les proposer comme [Nextcloud][3], un [nœud Tor][4] ou encore [draw.io][5]. 

Et pour finir, je réalise des dons à des services que je trouve important comme [Wikipédia][6] ou encore [Framasoft][7]. 

### Ressources

Une petite liste de plusieurs ressources pour vous faire comprendre un peu mieux cette collecte : 

  * [Mémoires Vives - Edward Snowden][2]
  * [Nulle part ou se cacher - Glenn Greenwald][8]
  * [Nothing to hide - Marc Meillassoux][9]
  * [Citizenfour - Laura Poitras][10]
  * [La Quadrature du Net][11]
  * [Snowden - Oliver Stone][12]

## Les alternatives que j'utilise

Dans cette partie je ne vais pas lister toutes les alternatives, de un parce que ce n'est clairement pas le but de l'article et de deux parce que ça serait beaucoup trop long.

Je vais essayer de vous présenter les différents services/logiciels que j'utilise maintenant et ceux que j'ai remplacé. 

### Moteur de recherche

![](/images/gafam_search.png)

À la place du moteur de recherche Google j'alterne entre deux services : 

  * [Qwant][13]
  * [Searx][14]

Qwant est un moteur de recherche français qui se veut respectueux de la vie privée. Il est en ligne en 2013 et est aujourd'hui utilisé par l'administration française par exemple, ainsi que plusieurs autres administrations ou organismes. 

Même s'il est sujet à des scandales en ce moment, c'est celui que j'utilise principalement que ça soit sur mobile ou sur PC directement. 

J'utilise également depuis peu searx qui est un [métamoteur][15] de recherche qui permet d'agréger les résultats de plus de 70 moteurs de recherche. Ce moteur de recherche est sous licence libre et son développement est disponible sur Github. Cet outil s'utilise via des instances, il est donc possible d'héberger sa propre instance, je pense d'ailleurs que j'en mettrais une à disposition d'ici peu. 

J'ai également utilisé le service [DuckDuckGo][16] avant de passer sur Qwant, au niveau des résultats c'est l'un des meilleurs. 

### Service de mail

![](/images/tutanota_banner.png)

Pour remplacer mon classique Gmail j'ai longtemps hésite entre [Protonmail][17], une solution chiffrée de bout en bout hébergée en Suisse et [Tutanota][18], un service de mail allemand open-source et également chiffré de bout en bout. 

J'ai choisi Tutanota, les différentes raisons sont listées dans l'article dédié [disponible ici.][19]

Protonmail & Tutanota sont des services accessibles gratuitement.

Au niveau du client de mail j'ai préféré également Thunderbird de Mozilla à Outlook.

### Système d'exploitation pour PC

![](/images/gafam_os.png)

De ce côté-la, le choix était plutôt évident avec les systèmes GNU/Linux. J'en ai utilisé plusieurs avant de trouver ce qui me convenait vraiment (Kali Linux, Ubuntu, BackBox, Kubuntu, Arch Linux, etc.) 

Aujourd'hui concrètement j'utilise 2 systèmes : 

  * [PopOS][20] par System76 basé initialement sur Ubuntu
  * Arch Linux a qui j'ai consacré [l'article précédent][21]. 

J'ai dans un premier temps utilisé PopOS qui est un OS accessible à tous et très plaisant graphiquement, avant de passer vers Arch Linux pour une utilisation plus poussée. 

Sur la partie serveur, j'utilise des OS Linux essentiellement (Debian, Ubuntu, Centos, etc.). 

### Réseaux Sociaux

De ce côté-la, je n'utilise pas d'outil libre ou Open Source, j'ai simplement supprimé mes comptes Instagram & Facebook. 

J'utilise principalement Twitter ainsi que Linkedin (oui je sais ça appartient à Microsoft) pour un aspect purement professionnel. 

Il existe cependant des alternatives comme [Mastodon][22], mais je ne peux pas m'avancer sur ces outils car je ne les ai pas testés. 

### Suite Bureautique

![](/images/gafam_lo.png)

À la place du très connu « Pack Office » j'utilise maintenant le très célèbre également [Libre Office][23], qui est la branche la plus à jour et la plus récente du projet Open Office. 

Cette suite qui à ma grande surprise est aujourd'hui très évoluée répond parfaitement à mes besoins côté bureautique, notamment avec son interface avec des « onglets » dans le style de Microsoft Office pour une transition plus douce. 

Ce logiciel libre est compatible avec la plupart des distributions Linux, de plus la communauté [LibreOfficeFR][24] est très active sur Twitter. 

### Navigateur Web

![](/images/gafam_nav.png)

Pour la navigation, j'ai longtemps utilisé Chrome comme tout le monde. Je suis aujourd'hui un adepte de [Firefox][25], je l'affectionne particulièrement avec sa fonction « Sync ». Ce logiciel est libre, il est proposé par la [Fondation Mozilla][26]. 

J'ai également pour projet de tester le navigateur [Brave][27]. Ce dernier est basé sur [Chromium][28] et a été créé par l'inventeur du Javascript et le co-fondateur de Mozilla. Il se veut 40% plus rapide que Google Chrome.

### Solution de stockage en ligne

![](/images/gafam_nextcloud.png)

Au niveau du stockage en ligne, j'utilisais 2 solutions : Google Drive pour mes documents et iCloud pour la sauvegarde de mon iPhone. 

Ces 2 services sont aujourd'hui assurés par une instance [Nextcloud][3] sur mon serveur. 

Nextcloud est un logiciel libre, issu du fork d'[OwnCloud][29]. C'est un service très utilisé par des professionnels et des particuliers. Il intègre énormément de fonctionnalités grâce à des plug-ins. 

Si vous êtes un adepte de l'édition de document en ligne, [CollaboraOnline][30] est une version de LibreOffice en ligne qui s'interface avec Nextcloud. 

### Système d'exploitation pour Smartphone

![](/images/gafam_lineage.png)

Côté téléphone, m'étant débarrassé récemment de mon iPhone, je suis en train de changer mon Android classique vers la solution [LineageOS][31], anciennement Cyanogen.

LineageOS est un système d'exploitation Open Source basé sur Android publié en décembre 2016. Son code source est disponible sur Github. Un des avantages de cette solution est qu'elle n'installe pas par défaut tous les éléments de la suite Google. LineageOS est aujourd'hui un OS très répandu et compatible avec la plupart des téléphones sous Android. 

Le [système français « /e/ »][32] créé par Gaël Duval est également disponible depuis peu. Ce système se veut respectueux de la vie privée en intégrant aucun service de Google. C'est un fork de LineageOS. 

### Système de virtualisation

![](/images/gafam_proxmox.png)

Vu que j'utilise un serveur chez moi pour héberger certains services, j'utilise [Proxmox][33] à la place de VMWare ou de Microsoft HyperV.

Proxmox est une solution libre et gratuite, il reste cependant possible de souscrire à un support, ce que je vous conseille si vous l'utiliser dans un environnement professionnel.

Cet outil à plusieurs avantages, tout d'abord il s'installe très facilement sur la plupart des OS Linux pour serveur (Debian, Ubuntu, etc.). Il permet de gérer des clusters sans outil supplémentaire. Et pour finir en plus de pouvoir créer des VM classique avec KVM vous pouvez sur la même interface créer des conteneurs de type LXC. 

### Communication instantanée

Rien d'exceptionnel à ce niveau, j'ai simplement remplacé Messenger & tous les autres par [Signal][34]. Un service de messagerie Open Source & chiffré de bout en bout. Il est disponible sur toutes les plateformes (iOS, Android, Linux, etc.) 

Il existe également [Wire][35] qui a été présenté récemment par Snowden une nouvelle fois.

### Service vidéo

![](/images/gafam_video.png)

Le grand problème pour les vidéos, c'est que le monopole est énorme avec Youtube. J'ai testé [Peertube][36] assez rapidement, mais je ne trouve généralement pas ce que je veux, car la plupart des contenues sont publiés sur Youtube. 

Peertube est une alternative libre à Youtube décentralisée ou chacun peut héberger sa propre instance. Il est également possible de faire de la fédération et de proposer les vidéos de quelqu'un d'autres sur son instance sans pour autant les héberger. Ce service n'est pour moi pas adapté au grand public et c'est son principal inconvénient.

En attendant une vraie solution, pour le peu que j'utilise Youtube je passe par l'outil [Freetube][37]. C'est un client libre qui protège des trackeurs et qui respecte la vie privée des utilisateurs.

### Service de cartographie

![](/images/gafam_openstreetmap.png)

La aussi, entre Google Maps & Waze comme GPS, Google détient les pleins pouvoir. 

Il existe cependant des alternatives de qualité comme [OpenStreeMap][38] sur ordinateur et [OsmAnd][39] sur Android & iOS. Cette dernière est une application Open Source basée sur les données d'OpenStreeMap.

## Création de schéma

![](/images/gafam_schema.png)

Si vous êtes un administrateur système et réseau j'imagine que vous avez souvent besoin de créer des schémas réseau ou d'infrastructure.

De ce côté, j'utilisais principalement Visio de Microsoft, qui est pour moi l'un des meilleurs dans ce domaine. Et comme je suis adepte de LibreOffice j'ai tenté de le remplacer par LibreOffice Draw, mais ce n'était pas concluant. 

Je me suis donc tourné vers [draw.io][40], une solution libre auto-hébergeable pour créer ses schémas. Il est tout aussi facile à prendre en main que Visio et à plusieurs avantages : 

  * Disponible en Web (pas de client à installer) 
  * Beaucoup de stencil disponible par défaut
  * Lecture des formats Visio
  * Auto hébergeable (avantage important en entreprise) 

### Autres

Vu que l'article est déjà bien assez long, je vais rajouter plusieurs outils dans cette partie. 

Pour l'anonymisation des données il existe aujourd'hui beaucoup de service de VPN, mais est-ce que nous pouvons leur faire confiance ?

De mon côté, c'est non. Le seul en qui j'aurais confiance si j'en avais besoin est le service [ProtonVPN][41]. 

Il existe la aussi des alternatives comme le réseau [Tor][42] que j'utilise régulièrement. J'ai également ouvert un noeud de sortie pour y contribuer. Je reviendrais plus en détail là-dessus dans un article dédié. 

Si vous êtes un joueur occasionnel comme moi, vous aurez besoin d'un dual boot sous Windows pour faire tourner vos jeux. De mon côté, n'ayant qu'un PC portable j'ai souscris à un abonnement sur [Shadow][43]. Shadow est un service de cloud gaming français (hébergement chez OVH). 

Ils proposent depuis peu des prix vraiment attractifs. Vous avez accès via leur client à un PC Windows 10 complet avec des performances assez élevées pour faire tourner les derniers jeux. N'hésitez pas à me dire si un code de parrainage vous intéresse. 

Je sais, c'est sous Windows 10, mais dans ce milieu-là, pas vraiment le choix. Je l'utilise cependant seulement pour jouer et pas comme machine de tous les jours.

## Conclusion

Pour conclure sur cet article (trop?) long, je dirais qu'il est aujourd'hui possible de se passer de ces services. Je suis d'accord, ça demande une certaine gymnastique. 

De plus, il y a eu une prise de conscience depuis l'affaire Snowden, ce qui est une bonne chose. Il faudrait maintenant que cette prise de conscience se généralise. 

Sur mon approche, je sais qu'elle n'est pas parfaite et qu'il y a sûrement des choses à revoir. J'ai également pu dire une ou deux bêtises dans l'article, mais j'ai voulu retranscrire ma vision qui sera certes incomplète mais je pense que c'est un bon début. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://fr.wikipedia.org/wiki/GAFAM
 [2]: https://www.cultura.com/projet-y-9782021441048.html
 [3]: https://nextcloud.com/
 [4]: https://metrics.torproject.org/rs.html#details/D9E1A43F01048D2DE750282F984BFEECC9891D7D
 [5]: https://draw.rm-it.fr
 [6]: https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal
 [7]: https://framasoft.org/
 [8]: https://www.cultura.com/nulle-part-ou-se-cacher-9782709646154.html
 [9]: https://fr.wikipedia.org/wiki/Nothing_to_Hide
 [10]: https://fr.wikipedia.org/wiki/Citizenfour
 [11]: https://www.laquadrature.net/en/
 [12]: https://fr.wikipedia.org/wiki/Snowden_(film)
 [13]: https://www.qwant.com/
 [14]: https://searx.me/
 [15]: https://fr.wikipedia.org/wiki/M%C3%A9tamoteur
 [16]: https://duckduckgo.com/
 [17]: https://protonmail.com/
 [18]: https://tutanota.com/
 [19]: https://net-security.fr/security/migration-vers-tutanota-mail/
 [20]: https://system76.com/pop
 [21]: https://net-security.fr/system/arch-linux-pourquoi-et-comment/
 [22]: https://mastodon.social/about
 [23]: https://fr.libreoffice.org/
 [24]: https://twitter.com/LibreOfficeFR
 [25]: https://www.mozilla.org/en-US/firefox/new/?redirect_source=firefox-com
 [26]: https://www.mozilla.org/fr/about/
 [27]: https://brave.com/
 [28]: https://www.chromium.org/
 [29]: https://owncloud.org/
 [30]: https://nextcloud.com/collaboraonline/
 [31]: https://lineageos.org/
 [32]: https://e.foundation/?lang=fr
 [33]: https://www.proxmox.com/en/
 [34]: https://signal.org/fr/
 [35]: https://wire.com/en/
 [36]: https://joinpeertube.org/
 [37]: https://freetubeapp.io/
 [38]: https://www.openstreetmap.org/
 [39]: http://osmand.net/
 [40]: https://www.draw.io/
 [41]: https://protonvpn.com/
 [42]: https://www.torproject.org/
 [43]: https://shadow.tech/frfr/