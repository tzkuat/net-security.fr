---
title: Les commandes LVM
author: Fabio Pace
type: post
date: 2019-01-14T14:15:29+00:00
url: /system/les-commandes-lvm/
thumbnail: /images/Lvm/lvm_1.jpg
featureImage: images/Lvm/lvm_1.jpg
shareImage: /images/Lvm/lvm_1.jpg
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "LVM",
    "stockage"
]
categories: [
    "systeme"
]

---
Bonjour à tous, aujourd'hui nous allons voir ensemble brièvement ce qu'est LVM (avec ses avantages et ses inconvénients) et également la liste des commandes nécessaires pour gérer les volumes LVM.

## Définition

Tout d'abord, il faut savoir que LVM (Logical Volume Manager) permet de remplacer le partitionnement que l'on a l'habitude d'utiliser. La différence majeur par rapport au partitionnement simple, c'est que nous nous préoccupons pas de l&#8217;emplacement de ces volumes. Comme son nom l'indique, nous créons des volumes logiques.

|||
|--- |--- |
|AVANTAGES|INCONVÉNIENTS|
|Pas de limitation (primaire, étendue…)|Si un volume physique est HS, les volumes rattachés seront HS également.|
|Non connaissance de l’emplacement||
|Manipulation aisée du stockage||
|Aucun risque dû au redimensionnement(contrairement au partitionnement)||


Nous allons définir les termes suivant afin de mieux en comprendre :

  * **PV** ou **Volume Physique** : Lié à une partition, à un disque physique ou virtuel (dans le cas de la virtualisation), d'un disque RAID&#8230;
  * **VG** ou **Groupe de Volume** : Le regroupement des disques physiques forme un groupe de volume.
  * **LV** ou **Volume Logique** : Les volumes logiques correspondent à une partition que nous allons monter sur la machine.
  * **FS** ou **Système de Fichier** : C'est grâce au système de fichier que nous allons pouvoir organiser notre point de montage (contient également le type du FS comme ext4 sous linux)

Et comme M.Bonaparte a dit :  
> Un bon croquis vaut mieux qu'un long discourt.

![](/images/Lvm/lvm_1.jpg)

## Les commandes

Pour lister tous les périphériques visibles :

```
lvmdiskscan
```

Pour afficher les PVs créés :

```
pvdisplay
pvs
```

Pour créer un disque physique :

```
pvcreate /dev/sdb
```

Pour agrandir le PV à la totalité du disque physique `/dev/sdb` :

```
pvresize /dev/sdb
```

Pour savoir quels PVs sont utilisés et par qui :

```
pvs -o+pv_used
```

Pour déplacer les données d'un PV vers un autre du même VG :

```
pvmove /dev/sdb /dev/sdc
```

Pour supprimer un PV non utilisé d'un VG :

```
vgreduce vg01 /dev/sdb
```

Pour supprimer un PV de LVM : 

```
pvremove /dev/sdb
```

Pour créer un VG (vg01) dans le PV /dev/sdb :

```
vgcreate vg01 /dev/sdb
```

Pour afficher les VGs créés :

```
vgdisplay
vgs
```

Pour renommer un VG : 

```
vgrename vg01 Newvg01
```

Pour ajouter un PV à un VG :

```
vgextend vg01 /dev/sdcvgextend vg01 /dev/sdc
```

Pour créer un LV de 5GB (lv01) dans le VG vg01 :

```
lvcreate -L 5G -n lv01 vg01
```

Pour créer un LV en prenant la totalité de l'espace disponible :

```
lvcreate -l 100%FREE -n lv01 vg01
```

Pour créer un LV en prenant seulement 5% du VG :

```
lvcreate -l 5%VG -n lv01 vg01
```

Pour afficher les LV créés :

```
lvdisplay
lvs
```

Pour étendre un LV de 50GB :

```
lvresize -L +50G /dev/vg01/lv01
```

Pour réduire un LV de 5% de sa taille : 

```
lvresize -l -5%LV /dev/vg01/lv01
```

**Attention :** Lorsque vous souhaitez agrandir un LV, un VG ou un PV, il faut bien évidement vérifier la couche au dessus de l'élément. Par exemple si vous voulez agrandir de 10GB le LV, il faut vérifier que le VG possède un espace non attribué de 10GB.

Pour renommer un LV : 

```
lvrename lv01 Newlv01
```

Pour créer un FS dans le LV :

```
mkfs.ext4 /dev/vg01/lv01
```

Pour redimensionner le FS dû au redimensionnement du LV :

```
resize2fs /dev/vg01/lv01
```

Pour monter un répertoire dans le LV :

```
mkdir /data01
mount /dev/vg01/lv01 /data01
```

L'article est terminé, vous avez les commandes essentielles pour gérer l'espace de stockage de votre machine.  

J'espère que l'article vous sera utile.  
A bientôt.

Fabio Pace