---
title: Commandes GNU/Linux en vrac – Partie 1
author: Mickael Rigonnaux
type: post
date: 2020-05-27T12:59:10+00:00
url: /system/commandes-gnu-linux-en-vrac-partie-1/
thumbnail: /images/logo-3.png
featured: true
tags: [
    "logiciellibre",
    "GNU/Linux"
]
categories: [
    "systeme",
    "logiciellibre"
]

---

## Introduction

Bonjour à tous ! Aujourd'hui un article différent, il est le 1er d'une série concernant les commandes GNU/Linux que je découvre avec l'utilisation quotidienne de mon Arch Linux et de l'administration des serveurs.

Le but est de présenter et de vous faire découvrir des commandes et des outils qui pourraient être utiles pour vous.

## Commandes chattr & lsattr

`chattr` est une commande GNU/Linux qui permet à un utilisateur de définir des attributs sur un fichier présent dans le système.

Il permet notamment de verrouiller des fichiers, c'est à dire de le protéger de la suppression et de la modification, même de l'utilisateur `root`.

La commande `lsattr` de son côté permet de lister les attributs rattachés aux fichiers.

Par exemple, si nous créons 2 fichiers, file1 et file2 et que nous lançons la commande suivante :


```
sudo chattr +i file1
```


Le fichier file1 sera protégé alors que file2 non. Résultat d'un test en essayant de supprimer les deux fichiers :

![](/images/chattr1.png)

Le fichier est également protégé contre la modification, il est impossible de l'éditer avec `vim` ou `nano`.

Pour déverrouiller le fichier vous pouvez utiliser la commande suivante :


```
chattr -i file1
```

D'autres options sont disponibles avec cette commande mais le principe reste le même :


```
# Protéger un dossier de manière récursive
chattr -R +i /path

# Ajouter une sécurité pour la suppression 
chattr +s file1

# Verrouiller le fichier seulement contre la suppression 
chattr +u file1
```

Il faut faire la même opération avec `-` pour désactiver l'attribut.

Pour terminer, la commande `lsattr` permet de lister les attributs, par exemple avec `file1` :

![](/images/chattr2.png)

L'attribut `i` est bien présent ce qui montre que le fichier est protégé.

## Commandes last & lastb

Les commandes `last` et `lastb` permettent de récupérer des informations sur les connexions effectuées sur la machine. Elles sont très importantes si vous voulez faire du forensic sur votre machine par exemple.

`last` permet de lister toutes les connexions, par exemple :

![](/images/last1.png)

Alors que `lastb` permet de lister les connexions manquées, par exemple :

![](/images/last2.png)

Cette commande est très efficace et permet de fermer rapidement les processus bloqués.

## Commande xkill

La commande `xkill` est une commande GNU/Linux qui permet, comme la commande `kill` de tuer un processus. Sa particularité est qu'il est possible de le faire de façon graphique. Elle fonctionnera donc seulement si vous utilisez un environnement graphique.

Pour l'utiliser il suffit de taper xkill dans un terminal, à ce moment la votre pointeur de souris se transformera en croix (ou autres selon votre distribution et votre thème) et il vous suffira de cliquer sur la fenêtre à fermer pour tuer le processus.

![](/images/xkill1-1.png)

## Commande yes

La commande `yes` est une commande GNU/Linux qui renvoi simplement des `y` après son lancement. Elle continue tant que l'utilisateur ne l'arrête pas ou qu'un autre processus le stop. 

En voici un exemple :

![](/images/yes1.png)

J'ai rajouté la commande more pour vous montrer le fonctionnement. Cette commande, même si elle semble inutile permet de valider automatiquement certaines interactions. Elle peut être utilisée dans les cas suivants :


```
sudo yes | apt install vim

yes | rm file1
```


Il est aussi possible de répéter des chaînes avec cette commande, par exemple :

`yes net-security`

La chaîne net-security sera alors répétée :

![](/images/yes2.png)

Cela peut être pratique pour générer des fichiers volumineux pour des tests par exemple.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N'hésitez pas à me dire également si ce genre d'article vous plaît !

Merci pour votre lecture et à bientôt !