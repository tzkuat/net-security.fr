---
title: 'Arch Linux : Pourquoi et comment ?'
author: Mickael Rigonnaux
type: post
date: 2020-01-06T08:04:24+00:00
url: /system/arch-linux-pourquoi-et-comment/
thumbnail: /images/arch_logo-1.png
tags: [
    "GNU/Linux",
    "Arch"
]
categories: [
    "systeme",
    "logiciellibre"
]
---
![](/images/arch_banner-1.png)


Bonjour à tous ! Pour ce premier article de 2020 nous allons parler du très connu [Arch Linux][1]. 

## Introduction

J'ai entrepris depuis maintenant un an un changement sur ma manière de fonctionner et d'utiliser les différents services et systèmes informatique. J'essaie d'utiliser essentiellement des outils/ressources respectueux de la vie privée et plus généralement des logiciels libres. 

Par exemple [Tutanota][2] à la place de Gmail, [LibreOffice][3] à la place d'Office, Linux à la place de Windows, etc. Je peux faire un article sur ce sujet si ça vous intéresse (même s'il en existe déjà des milliers). 

Au niveau de mon OS principal, j'utilisais jusqu'à présent [PopOS][4], ce dernier est un système basé sur Ubuntu proposé par l'entreprise américaine System76. Il est très proche d'Ubuntu il intègre des outils en plus et une interface Gnome un peu plus plaisante. La version que j'utilise est basée sur la 18.04 LTS d'Ubuntu, une version très stable.

Avant d'utiliser cette solution j'ai utilisé plusieurs distributions : Kali (personnellement impossible à utiliser tous les jours), Debian, Ubuntu, BackBox...

J'ai donc installé pas mal de distribution durant l'année écoulée, généralement à grand coup de « Suivant, Suivant, Suivant » comme une grande partie des utilisateurs. 

PopOS me convient parfaitement, simple, rapide et stable. Mais me considérant comme un utilisateur de Linux plutôt « avancé » j'avais également envie d'utiliser un OS dans ce style, qui me permettrait d'installer et d'utiliser le strict nécessaire sur ma machine et de comprendre réellement son fonctionnement. 

C'est donc naturellement que je me suis tourné vers Arch Linux.

## Arch Linux ?

Arch Linux est une distribution libre qui se veut rapide et légère, elle s'articule autour de la philosophie « KISS » ou « Keep It Simple, Stupid ». Il faut comprendre dans le sens « Garde ça simple ».

Il est prévu pour les utilisateurs « avancés » de Linux & même si vous n'êtes pas avancés je vous conseille de l'installer, c'est un exercice parfait pour apprendre.

Une autre particularité est que ce logiciel est en « [Rolling Release][5]« , c'est à dire qu'il est en développement constant et qu'il évolue très souvent. Il n'a pas de version majeure comme sous Ubuntu par exemple avec 18.04, 18.10, etc. D'autres OS utilisent ce système comme Gentoo par exemple. Ce système comporte des avantages et des inconvénients, vous utiliserez les dernières versions des paquets par exemple, ce qui est une bonne chose, mais vous serez également les premiers à rencontrer des bugs ou incompatibilités.

Pour finir, la communauté autour de ce système est énorme tout comme le wiki & le forum qui sont une sorte de bible pour les utilisateurs de Linux. Personnellement, m'est arrivé de trouver des solutions sur le forum ou le wiki d'Arch alors que mon problème concernait Debian.

## Installation

### Introduction

Nous allons maintenant voir comment installer Arch Linux (et vous allez voir rien à voir avec Debian ou Ubuntu) avec l'environnement graphique KDE. 

Nous verrons également comment réaliser les actions de base comme installer un paquet, faire des mises à jour, etc.

Dans mon cas je vais utiliser une machine virtuelle car Arch Linux est déjà installé sur ma machine. Je vais cependant reproduire l'installation que j'ai réalisée sur mon poste, c'est à dire une installation en BIOS/Legacy avec une seule partition & sans Swap, l'installation la plus simple possible. 

Cette installation concerne la version 2020.01.01 de l'OS.

Voici les caractéristiques de la machine :

* RAM : 4Go
* CPU : 1vCPU
* Disque : 40Gb
* Version : 2020.01.01

Si vous n'utilisez pas de VM vous pouvez créer une clé USB bootable avec la commande « dd » suivante : 

```dd if=/chemin/archlinux-2020.01.01-x86_64.iso of=/dev/xxx bs=4M status=progress```

Il faudra remplacer « xxx » par votre clé USB.

Pour l'installation, vous pouvez également suivre la très complète [documentation d'Arch Linux.][6]

### Lancement & gestion des disques

Vous arrivez maintenant sur l'interface de démarrage d'Arch : 

![](/images/arch_1.png)

Afin de poursuivre l'installation choisissez « Boot Arch Linux ». Nous voici maintenant avec un shell et l'utilisateur « root ». Dans un premier temps, si vous utilisez un clavier azerty il faut changer la disposition des touches : 

```loadkeys fr```

![](/images/arch_2.png)

Au niveau du partitionnement du disque, si vous avez peur de faire une bêtise vous pouvez utiliser un liveCD avec GParted. De mon côté j'ai utilisé la commande fdisk.

Je précise une nouvelle fois que dans mon cas il s'agit d'une utilisation en BIOS et non en UEFI. Les commandes suivantes ne sont pas correctes pour de l'UEFI. 

Vérifier le nom de votre disque : 

```fdisk -l```

![](/images/arch_3.png)

Dans mon cas c'est le disque « /dev/sda » de 40Go. Pour la configuration il faut lancer les commandes suivantes : 

```fdisk /dev/sda```

Après cette commande vous entrez de l'invit de commande de l'outil fdisk. Pour lister les partitions vous pouvez utiliser « p ». 

Pour créer une partition il faut utiliser les commandes suivantes : 

![](/images/arch_4.png)


Nous pouvons maintenant formater la partition en ext4 avec la commande : 

```mkfs.ext4 /dev/sda ```

![](/images/arch_5.png)


Et monter le système de fichier : 

```mount /dev/sda /mnt```

Nous pouvons maintenant passer à l'installation de base de notre machine Arch. 

### Installation de base

Tout d'abord nous allons configurer le réseau. Vous pouvez vérifier que votre interface est bien présente : 

```ip link```

Pour la configuration en statique vous pouvez lancer ces commandes : 

```
ip link set <interface> up
ip address add <votre_adresse/masque> broadcast + dev <interface>
ip route add 0.0.0.0/0 via <gateway> dev <interface>
```

Par exemple dans mon cas : 

![](/images/arch_6.png)

Pensez également à renseigner le serveur de nom dans le fichier /etc/resolv.conf : 

```nano /etc/resolv.conf```

![](/images/arch_7.png)

Vous avez maintenant un accès au réseau. Si vous disposez d'un serveur DHCP vous pouvez également lancer le démon avec la commande : 

```dhcpcd```

Maintenant, passons à la configuration des miroirs, pour cela il faut se rendre dans le fichier /etc/pacman.d/mirrorlist et ne garder qu'un miroir, dans notre cas ça sera un miroir français. 

```nano /etc/pacman.d/mirrorlist```

![](/images/arch_8.png)

**TIPS** : Vous pouvez supprimer des lignes dans nano avec les touches CTRL + k

Nous pouvons maintenant passer à l'installation de base d'Arch : 

```pacstrap /mnt base linux linux-firmware```

Vous pouvez également installer plusieurs utilitaires qui seront pratiques pour la suite : 

```pacstrap /mnt zip unzip vim mc syslog-ng lsb-release bash-completion base-devel```

Après l'installation des outils de base, il faut générer le fichier fstab pour la gestion des partitions : 

```genfstab -U -p /mnt >> /mnt/etc/fstab ```

Nous pouvons maintenant passer à la configuration de l'OS, pour cela il faut se rendre dans ce dernier avec la commande suivante : 

```arch-chroot /mnt```

Votre shell à normalement changé. 

Pour la configuration de la zone géographique : 

```ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime```

La création du fichier /etc/adjtime : 

```hwclock --systohc```

Au niveau des locale, il faut dé-commenter « fr_FR.UTF-8 UTF-8 » dans le fichier /etc/locale.gen et lancer la commande : 

![](/images/arch_9.png)

```locale-gen```

Il faut ensuite créer le fichier « /etc/locale.conf » et configurer la variable LANG : 

```
vim /etc/locale.conf
LANG=fr_FR.UTF-8
```

![](/images/arch_10.png)

Même principe pour la gestion du clavier avec le fichier « /etc/vconsole.conf » : 

```
vim /etc/vconsole.conf
KEYMAP=fr-latin9
```

Nous devons maintenant configurer le nom d'hôte de la machine dans les fichiers « /etc/hostname » & « /etc/hosts » : 

```
vim /etc/hostname
arch-linux
```

```
vim /etc/hosts
127.0.0.1 localhost
::1       localhost
127.0.1.1 arch-linux
```

Il faut maintenant ajouter un mot de passe à l'utilisateur root : 

```passwd```

Et pour finir, installer un bootloader, dans mon cas ça sera Grub2 : 

```pacman -S grub os-prober```

![](/images/arch_11.png)

Le paquet os-prober est indispensable dans le cas d'un dual-boot. 

Pour terminer l'installation de grub vous devez lancer les commandes suivantes : 

```
grub-install --target=i386-pc /dev/sdX 
grub-mkconfig -o /boot/grub/grub.cfg
```

Dans le cas où vous avez un système déjà installé avec Grub, vous pouvez la lancer et lancer la commande : 

```update-grub```

Avant de redémarrer, vous pouvez installer le network manager pour éviter de refaire la configuration à la main :

```pacman -Syy networkmanager```

Pour terminer, il faut sortir du chroot, démonter le /mnt et reboot le système : 

```
exit
umount -R /mnt
reboot
```

Vous avez maintenant Arch Linux installé. Après le redémarrage vous devriez avoir l'interface de grub : 

![](/images/arch_12.png)

### Post installation

Nous allons maintenant passer à l'installation des différents outils de base et de l'interface graphique KDE. 

Tout d'abord ntp pour la synchronisation de l'heure :

```pacman -Syy ntp```

Puis Xorg qui permet de gérer l'affichage (comme Wayland) ainsi que les paquets pour gérer les périphériques (clavier, souris, trakcpad) : 

```pacman -S xorg-{server,xinit,apps} xf86-input-libinput xdg-user-dirs```

Il faut maintenant installer les drivers de la carte graphique. Pour cela il faut d'abord l'identifier avec la commande suivante : 

```lspci | grep -e VGA -e 3D```

Dans mon cas il s'agit d'une carte VMWare : 

![](/images/arch_13.png)

Généralement il s'agit d'Intel, AMD ou Nvidia. Les noms des drivers à installer sont [disponibles ici][7]. 

Les drivers libres sont : 

  * AMD : xf86-video-ati
  * Intel : xf86-video-intel
  * Nvidia : xf86-video-nouveau
  * VMWare : xf86-video-vmware

La commande pour installer le driver : 

```pacman -S xf86-video-vmware```

Nous pouvons maintenant passer à l'installation de quelques outils comme Gimp ou encore LibreOffice : 

```pacman -S gimp libreoffice-still-fr hunspell-fr firefox-i18n-fr firefox-ublock-origin sudo```

Il faut maintenant créer votre utilisateur et lui ajouter un mot de passe : 

```
useradd -m -g wheel -c 'Mickael Rigonnaux' -s /bin/bash mrigonnaux
passwd mrigonnaux
```

Et pour terminer il faut dé-commenter la ligne suivante dans le fichier /etc/sudoers : 

```
visudo
##Uncomment to allow members of group wheel to execute any command
%wheel ALL=(ALL) ALL
```

Nous pouvons maintenant passer à l'installation de l'interface KDE. 

Il faut utiliser l'utilisateur précédemment créé pour installer l'environnement.

Tout d'abord il faut installer KDE et ses différentes applications : 

```sudo pacman -S plasma kde-applications digikam elisa kdeconnect packagekit-qt5```

Pour avoir le bon clavier lors du lancement il faut ensuite lancer :

```sudo localectl set-x11-keymap fr```

Nous avons maintenant installé KDE comme environnement de bureau, Xorg comme gestionnaire d'affichage et de fenêtre et pour finir SDDM comme « display manager », ce dernier permet de lancer l'environnement graphique et de gérer les connexions. SDDM s'est installé automatiquement avec KDE. 

SDDM se lance avec la commande suivante : 

```sudo systemctl start sddm```

Vous devriez maintenant avoir accès à l'interface de KDE : 

![](/images/arch_15.png)

![](/images/arch_16.png)

Et pour finir vous pouvez activer SDDM au démarrage de la machine : 

```sudo systemctl enable sddm```

Vous avez maintenant Arch Linux installé et fonctionnel !

### Supplément

Comme vous avez pu le voir, le gestionnaire de paquet est pacman sur Arch Linux, voici les commandes principales : 

```
## Installer un paquet 
pacman -S <pkg>

## Installer et mettre à jour la liste des paquets
pacman -Syu <pkg>

## Désinstaller un paquet 
pacman -Rsc <pkg>

## Chercher un paquet 
pacman -Ss <query>

## Faire une upgrade 
pacman -Syu 

## Lister les paquets installés 
pacman -Qe

## Chercher un paquet installé 
pacman -Qs <query>

## Lister les paquets inutiles
pacman -Qdt

## Désinstaller les paquets inutiles
pacman -Rns $(pacman -Qdtq)
```

En plus de pacman, vous pouvez ajouter l'utilitaire yay qui permet d'installer des paquets issus des repo AUR (Arch User Repository) : 

```
sudo pacman -S git
git clone https://aur.archlinux.org/yay
cd yay
makepkg -sri
```

De mon côté mon installation ressemble maintenant à ça : 

![](/images/arch_14.png)

J'utilise maintenant quotidiennement Arch mais je garde toujours mon dualboot avec Pop au cas où. Si vous voulez installer un autre environnement graphique vous avez différentes procédures sur le turoriel de Frederic Bezies. 

Sources : 

  * [Wiki Arch Linux][8]
  * [Tutoriel de Frederic Bezies][9]

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.archlinux.org/
 [2]: https://tutanota.com/fr/
 [3]: https://fr.libreoffice.org/
 [4]: https://system76.com/pop
 [5]: https://fr.wikipedia.org/wiki/Rolling_release
 [6]: https://wiki.archlinux.org/index.php/Installation_guide
 [7]: https://wiki.archlinux.org/index.php/Xorg#Driver_installation
 [8]: https://wiki.archlinux.org/
 [9]: https://github.com/FredBezies/arch-tuto-installation/blob/master/install.md