---
title: Raspberry – Thermomètre connecté avec une sonde DHT22
author: Mickael Rigonnaux
type: post
date: 2019-08-31T16:04:02+00:00
excerpt: "Découvrez dans cet article comment réaliser un thermomètre connecté à partir d'un Raspberry Pi et d'une sonde DHT22 avec des alertes et un dashboard."
url: /system/raspberry-thermometre-connecte-avec-une-sonde-dht22/
thumbnail: /images/DHT22/DHT22_Logo.png
featureImage: images/DHT22/DHT22_Logo.png
shareImage: /images/DHT22/DHT22_Logo.png
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "libre",
    "domotique"
]
categories: [
    "systeme"
]

---
![](/images/DHT22/DHT22_Banner.png)

Bonjour à tous ! Aujourd'hui nous allons aborder un projet qui traîne depuis un certain temps pour moi, la réalisation d'un thermomètre connecté avec un Raspberry Pi et une sonde DHT22. 

Cette sonde nous permettra de récupérer la température et l'humidité

Ce type de thermomètre peut-être utile si vous voulez monitorer la température de votre maison par exemple et exécuter des actions en cas de température élevée ou faible pour allumer le chauffage ou la climatisation. 

Il peut également permettre de surveiller une salle serveur par exemple, en envoyant des alertes en cas de température élevée comme dans le cas que nous verrons. 

## Matériel

Tout d'abord le matériel utilisé pour ce thermomètre : 

  * Raspberry Pi 3B sous Raspbian 9
  * Une sonde DHT22 ([Lien ici][1].)
  * Des câbles dupont femelle/femelle

Attention, si vous achetez une sonde DHT22 il en existe plusieurs modèles, certaines sont pré-assemblées comme celle que j'ai choisie. Les branchements et les matériaux peuvent être différents.

Je présenterais les branchements pour les deux cas que j'ai vu, avec la sonde du lien et avec une sonde « nue ». 

Dans le cas d'une sonde nue il faut rajouter : 

  * Une plaque d'essai 
  * Des résistances 4.7 ohms ([Lien ici][2].)
  * Des câbles dupont mâle/femelle

## Branchement

Comme indiqué au-dessus, voici les deux sondes DHT22 : 

![](/images/DHT22/DHT22_2.jpeg)

### Avec une sonde pré-assemblée 

Nous avons donc seulement 3 pin sur ce modèle qu'il faut brancher de la manière suivante (ne faite pas attention à la sonde sur l'illustration, nous utilisons bien une DHT22 et pas une 11) : 

![](/images/DHT22/DHT22_1.png)

Le PIN 1 de la sonde est relié au PIN 2 du rapsberry qui correspond à l'alimentation 5V. 

Le PIN 2 de la sonde est relié à la masse sur le PIN 6 du raspberry.

Le PIN 3 de la sonde est relié au PIN 7 du raspberry afin de permettre les échanges d'informations.

Dans mon cas je n'utilise pas de plaque d'essai, je branche directement la sonde au raspberry : 

![](/images/DHT22/DHT22_3.jpg)

### Avec une sonde nue

Avec une sonde nue il faut donc rajouter une résistance et une plaque d'essai pour réaliser un branchement comme celui-là : 

![](/images/DHT22/DHT22_4.jpg) 

Le 3ème PIN de la sonde n'est pas utilisé, sinon les autres sont reliés aux mêmes PIN sur le raspberry, l'alimentation, la masse et le PIN 7 pour les données. 

## Fonctionnement

Pour récupérer les informations de la sonde une librairie a été développée par Adafruit, personnellement j'utilise [la librairie Python][6] compatible avec 2.7 et Python 3. 

Pour installer cette librairie, plusieurs possibilités sont possibles, de mon côté j'ai procédé de la façon suivante : 


```
git clone https://github.com/adafruit/Adafruit_Python_DHT.git
cd Adafruit_Python_DHT
sudo python setup.py install

#### Avec Python 3.x ####
sudo python3 setup.py install
```

Vous pouvez ensuite vérifier le fonctionnement de votre sonde avec les commandes suivantes : 

```
cd examples
python AdafruitDHT.py 22 4
```

Le script doit normalement vous renvoyer la température et l'humidité de la sonde. Les paramètres 22 et 4 correspondent respectivement à la sonde utilisée (DHT22) et au PIN utilisé (GPIO4, le 7 du raspberry). 

![](/images/DHT22/DHT22_5.png)

La sonde est maintenant installée et fonctionnelle. 

## Pour aller plus loin

De mon côté ce que je voulais faire c'était réaliser un dashboard Web pour consulter la température et l'humidité d'une pièce avec éventuellement des alertes si la température est trop élevée. 

Pour cela j'ai repris la librairie fournie par Adafruit et j'ai développé un script Python qui renvoi la température et l'humidité dans deux fichiers CSV différents avec une date au format timestamp. Pour automatiser le script j'utilise simplement un cron. 

Pour l'affichage j'ai utilisé la librairie CanvasJS qui permet de réaliser des graphiques facilement depuis les données d'un fichier CSV. 

Une option a également été rajoutée pour être alerté en cas de température trop élevée. 

Voici le script en question : 
```
#!/usr/bin/python

import sys
import datetime
import Adafruit_DHT 
import time
import calendar
import smtplib

#Configuration du type de sonde et du PIN
sensor = 22
pin = 4

humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

#Récupération du temps
ts = str(calendar.timegm(time.gmtime())) + "000"

if str(sys.argv[1]) == 'temperature':
    if temperature is not None:
        print(ts + ',' '{0:0.1f}'.format(temperature))
    else:
        print('Err.')
        sys.exit(1)

elif str(sys.argv[1]) == 'humidity':
    if humidity is not None:
        print(ts + ',' + '{0:0.1f}'.format(humidity))
    else:
        print('Err.')
        sys.exit(1)

elif str(sys.argv[1]) == 'alert':
        if temperature > 20 :
            from email.MIMEMultipart import MIMEMultipart
            from email.MIMEText import MIMEText
            fromaddr = "exemple@gmail.com"
            toaddr = "exemple@exemple.com"
            msg = MIMEMultipart()
            msg['From'] = fromaddr
            msg['To'] = toaddr
            msg['Subject'] = "Temperature is high !"
            body = "Temperature is high ! Temp : " + str(temperature)
            msg.attach(MIMEText(body, 'plain'))
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(fromaddr, "xxxx")
            text = msg.as_string()
            server.sendmail(fromaddr, toaddr, text)
            server.quit()                                                                                                                                                                                                 
```

Les paramètres possibles sont donc : 

  * temperature pour récupérer la date et la température
  * humidity pour récupérer l'humidité et la température
  * alert pour vérifier la température et être alerté si trop élevée
  
![](/images/DHT22/DHT22_6.png)

J'ai ensuite rajouté tout ça dans le fichier cron pour que le script soit exécuté toutes les heures :

```
0 * * * * python /chemin_vers_le_script/DHT22.py temperature >> /var/www/html/temperature.csv
0 * * * * python /chemin_vers_le_script/DHT22.py humidity >> /var/www/html/humidity.csv
0 * * * * python /chemin_vers_le_script/DHT22.py alert
```

Voici un exemple de mail reçu : 

![](/images/DHT22/DHT22_7-1.png)

Dans mon cas l'adresse d'envoi utilisée est une adresse Gmail. 

Et pour finir voici le dashboard réalisé avec CanvasJS :

![](/images/DHT22/DHT22_8.png)

Le code du dashboard n'est pas présent ici car il rendrait l'article illisible. Il est présent sur [le Fork Github du projet][9] que j'ai réalisé pour stocker l'ensemble des fichiers. Il est à placer à la racine du serveur Web.

Pour récupérer les informations depuis la page Web je renvoie l'ensemble des résultats du script avec le cron à la racine du serveur Web dans deux fichiers, temperature.csv et humidity.csv.

Le serveur utilisé dans mon cas est Apache2.

CanvasJS est une librairie avec une licence commerciale, il reste tout de même possible de l'utiliser gratuitement si c'est dans un cadre scolaire, personnel. Tout est [expliqué ici.][10] 

[Lien du projet complet sur Github.][11] 

Voilà, nous avons maintenant notre dashboard pour suivre l'évolution de la température et les alertes par mail. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.ebay.com/itm/AM2302-DHT22-Digital-Temperature-And-Humidity-Sensor-Module-Replace-SHT15-SHT11/222681366115?hash=item33d8d7fe63:g:jPEAAOSwm3pZ5ePC
 [2]: https://www.ebay.com/itm/RESISTANCE-1-2W-4K7-4-7Kohms-4700ohms-5/322197345835?ssPageName=STRK%3AMEBIDX%3AIT&var=511103094603&_trksid=p2060353.m2749.l2649
 [6]: https://github.com/adafruit/Adafruit_Python_DHT
 [9]: https://github.com/mrigonnaux/Adafruit_Python_DHT/blob/master/Add_mrigonnaux/CanvasJS_Dashboard/index.html
 [10]: https://canvasjs.com/faq/
 [11]: https://github.com/mrigonnaux/Adafruit_Python_DHT