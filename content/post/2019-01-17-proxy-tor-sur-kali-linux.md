---
title: Proxy Tor sur Kali Linux
author: Mickael Rigonnaux
type: post
date: 2019-01-17T15:39:34+00:00
url: /security/proxy-tor-sur-kali-linux/
thumbnail: /images/Kalitorify/kalitor_present.jpg
featureImage: images/Kalitorify/kalitor_present.jpg
shareImage: /images/Kalitorify/kalitor_present.jpg
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "Kali",
    "Tor",
    "Privacy"
]
categories: [
    "securite",
    "systeme"
]

---
Bonjour à tous ! 

Aujourd'hui nous allons découvrir un outil que j'ai découvert récemment, [Kalitorify][1]. Ce dernier permet de rediriger l'ensemble du traffic de votre machine Kali Linux vers [Tor][2] en créant un proxy. Ce dernier utilise donc directement Tor et iptables pour fonctionner.

Pour ceux qui ne connaissent pas Tor, c'est un réseau décentralisé. Il est composé de plusieurs noeuds et permet d'anonymiser l'origine de connexions. Je vous invite à vous rendre [ici][4] pour plus d'informations.

Voici un schéma simplifié de son fonctionnement : 

![](/images/Kalitorify/kalitor_illu.png)

Grâce à cet outil vous pourrez devenir un peu plus « anonyme » sur internet. Même si je vous conseille d'utiliser en plus de cet outil un VPN pour vous connecter au réseau Tor. Cependant l'utilisation d'outil comme celui-ci ralentira la connexion internet de votre machine.

## Installation

Il faut dans un premier temps récupérer le code de l'outil sur Github et installer Tor : 

```
apt-get update && apt-get install tor
git clone https://github.com/brainfucksec/kalitorify.git
```

Il faut ensuite installer l'outil : 

```
cd kalitorify/
sudo make install
sudo reboot
```

## Utilisation

Une fois ces étapes effectuées vous pourrez lancer directement les commandes avec « kalitorify ». Tout d'abord vérifions notre IP Publique avec l'outil en ligne ifconfig.io (l'IP est volontairement masquée) : 

![](/images/Kalitorify/kalitor_1-1.png)

L'IP en question est l'IP que j'utilise habituellement, lançont maintenant kalitorify avec la commande suivante :


```
kalitorify --tor
```


Cette commande va initialiser notre connexion au réseau Tor :  


![](/images/Kalitorify/kalitor_2.png)

On peut maintenant tester une nouvelle fois notre IP Publique avec ifconfig.io et nous voyons bien que l'IP n'est plus la même et que nous passons maintenant à travers des noeuds Tor : 

![](/images/Kalitorify/kalitor_3.png)

Une fois vos opérations terminées vous pouvez arrêter ce proxy avec la commande suivante :

```
kalitorify --clearnet
```

![](/images/Kalitorify/kalitor_4.png)

D'autres options sont également disponibles : 


```
kalitorify --status #Connaître le statut de l'outil (lancé, arrêté, etc.)
kalitorify --ipinfo #Affiche l'IP publique
kalitorify --restart #Redémarre Tor et change l'IP publique
```

Si vous souhaitez avoir plus d'informations je vous invite à vous rendre sur la page Github du projet : [github.com/BrainfuckSec/kalitorify][4]

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://github.com/brainfucksec/kalitorify
 [2]: https://www.torproject.org/
 [4]: https://github.com/BrainfuckSec/kalitorify