---
title: Installation des Drivers Nvidia sur Kali Linux
author: Fabio Pace
type: post
date: 2019-01-03T17:00:09+00:00
url: /system/installation-des-drivers-nvidia-sur-kali-linux/
thumbnail: /images/Nvidia-Kali/Nvidia-Kali5.png
featureImage: images/Nvidia-Kali/Nvidia-Kali5.png
shareImage: /images/Nvidia-Kali/Nvidia-Kali5.png
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "Kali"
]
categories: [
    "systeme"
]

---
Bonjour à tous, pour ce début d'année, nous allons voir comment installer sa carte graphique NVIDIA sur la distrib de Kali Linux.

Attention, en installant les pilotes NVIDIA, il se peut que vous n'ayez plus d'interface graphique (seul le mode sans echec), Net-Security ne sera pas tenu responsable en cas de dommage matériel.

La plupart des commandes doivent être éxécutées en root. Je n'ai pas rajouté le « sudo » pour éviter des erreurs de copier-coller si vous êtes déjà en root.

Avant toute chose, il est préférable de mettre à jour Kali soit par le gestionnaire de paquets « Logiciel », soit directement via le terminal : 

```
apt update && apt dist-upgrade -y && reboot
```

Vérifier tout d'abord votre configuation de votre (ou vos) cartes graphiques (généralement chipset graphique du processeur + carte graphique Nvidia) :

```
lspci | grep -E "VGA|3D"
```

![](/images/Nvidia-Kali/Nvidia-Kali1.png)

Ici, vous pouvez voir le chispet graphique d'Intel (HD Graphics 530) et la carte graphique Nvidia (GeForce GTX 960M)

Nous allons maintenant blacklisté le module « Nouveau » afin de garder seulement les pilotes officiel NVIDIA :

```
echo -e "blacklist nouveau\noptions nouveau modeset=0\nalias nouveau off" > /etc/modprobe.d/blacklist-nouveau.conf
```

On met à jour l'image système de fichier racine et on redémarre :

```
update-initramfs -u && reboot
```

Après le redémarage, si nous vérifions les modules chargés par le système, nous nous appercevons que le module « Nouveau » n'y figure pas :

```
lsmod | grep -i nouveau
```

Vous pouvez dès à présent installer les paquets nvidia suivants :

```
apt-get install nvidia-driver nvidia-xconfig
```

*NB* Une liste de paquets complémentaires devra être installée également (se fait automatiquement) en répondant « yes »

En lançant la commande suivante, vous devrez récupérer l'ID PCI du BUS où est branché votre GPU : 

```
nvidia-xconfig --query-gpu-info
```

Et en version rapide :

```
nvidia-xconfig --query-gpu-info | grep 'BusID : ' | cut -d ' ' -f6
```

Dans mon cas, j'obtiens `PCI:1:0:0`

Puis, nous allons créer un fichier qui contiendra les infos de la carte graphique à utiliser (remplacer seulement l'ID récupéré précédement) `/etc/X11/xorg.conf` :


```
Section "ServerLayout"
     Identifier "layout"
     Screen 0 "nvidia"
     Inactive "intel"
 EndSection
 Section "Device"
     Identifier "nvidia"
     Driver "nvidia"
     BusID "PCI:10:0:0"
 EndSection
 Section "Screen"
     Identifier "nvidia"
     Device "nvidia"
     Option "AllowEmptyInitialConfiguration"
 EndSection
 Section "Device"
     Identifier "intel"
     Driver "modesetting"
 EndSection
 Section "Screen"
     Identifier "intel"
     Device "intel"
 EndSection
```


* Pour plus d'informations, vous pouvez visiter le manuel officiel Nvidia à cette [page](http://us.download.nvidia.com/XFree86/Linux-x86/375.39/README/randr14.html)

Maintenant, nous allons configurer le gestionnaire de sessions GDM (Gnome Display Manager) car c'est le gestionnaire par défaut sur la distrib Kali. Pour cela, il faut créer deux fichiers qui contiendront éxactement la même chose : 

* `/usr/share/gdm/greeter/autostart/optimus.desktop`
* `/etc/xdg/autostart/optimus.desktop`

Et insérer le contenu suivant :

```
[Desktop Entry]
 Type=Application
 Name=Optimus
 Exec=sh -c "xrandr --setprovideroutputsource modesetting NVIDIA-0; xrandr --auto"
 NoDisplay=true
 X-GNOME-Autostart-Phase=DisplayServer
```


* Si vous avez un autre gestionnaire de sessions, vous pouvez vous référer à ce tutorial : https://wiki.archlinux.org/index.php/NVIDIA\_Optimus#Display\_Managers

Enfin, il va falloir modifier une ligne du grub, afin qu'il puisse démarrer avec la carte graphique NVIDIA, pour cela ouvrez en édition le fichier `/etc/default/grub` par l'éditeur de votre choix :

```
vi /etc/default/grub
```

Commentez la ligne GRUB\_CMDLINE\_LINUX_DEFAULT en ajoutant un dièse (#) et rajouter la ligne suivante :

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet nvidia-drm.modeset=1"
```

Vous devez donc obtenir : 

![](/images/Nvidia-Kali/Nvidia-Kali2.png)

Enfin, mettez à jour votre grub : 

```
update-grub
```

Rédémarrer votre machine, et installer les outils « cuda » afin d'utiliser votre GPU à la place de votre CPU pour tous les calculs généraux :

```
apt-get install ocl-icd-libopencl1 nvidia-cuda-toolkit
```

Après l'installation, vous pouvez vérifier si votre carte graphique est bien reconnu :

```
nvidia-settings
```

![](/images/Nvidia-Kali/Nvidia-Kali3.png)

Ou simplement en utilisant l'utilitaire hashcat avec l'argument -I : 

```
hashcat -I
```

![](/mages/Nvidia-Kali/Nvidia-Kali4.png)

Voilà, votre GPU Nvidia est maintenant reconnu par Kali, à bientôt pour un nouvel article.

Fabio Pace

 [1]: images/Nvidia-Kali/Nvidia-Kali1.png
 [2]: https://wiki.archlinux.org/index.php/NVIDIA_Optimus#Display_Managers
 [3]: images/Nvidia-Kali/Nvidia-Kali2.png
 [4]: images/Nvidia-Kali/Nvidia-Kali3.png
 [5]: images/Nvidia-Kali/Nvidia-Kali4.png