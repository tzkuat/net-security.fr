---
title: 'BlueKeep (CVE-2019-0708) & Metasploit'
author: Mickael Rigonnaux
type: post
date: 2019-09-10T09:17:16+00:00
url: /security/bluekeep-metasploit/
thumbnail: /images/BlueKeep/bluekeep_logo.png
featureImage: images/BlueKeep/bluekeep_logo.png
shareImage: /images/BlueKeep/bluekeep_logo.png
tags: [
    "hack",
    "metasploit",
    "windows"
]
categories: [
    "systeme",
    "securite"
]

---
Bonjour à tous, aujourd'hui nous allons aborder un sujet dont vous avez sûrement entendu parler ces dernières semaines, la [CVE-2019-0708][1] également appelée BlueKeep. 

## BlueKeep ? 

BlueKeep est une vulnérabilité découverte en mai 2019 par le [National Cyber Security Centre][2] du Royaume-Uni et qui touche le protocole RDP (Remote Desktop Protocol) principalement utilisé dans les environnements Windows. Le protocole RDP utilise par défaut le port 3389. 

Les versions de Windows vulnérables sont les suivantes : 

  * Windows XP 
  * Windows Vista
  * Windows 7 
  * Windows 2003
  * Windows 2008 / 2008 R2

Les versions 8, 10 et 2012 server et plus ne sont pas affectées.

Cette vulnérabilité est de type RCE (Remote Code Execution), c'est à dire qu'elle permet d'exécuter du code à distance sur une machine vulnérable. 

Le score [CVSS][3] attribué à cette CVE est de 10 car elle permet de devenir administrateur d'une machine vulnérable facilement sans avoir d'authentification. Il est donc possible de compromettre complètement la machine de manière simple (impact fort, pas d'authentification et complexité faible).

La vulnérabilité est connue depuis mai 2019, mais l'exploit qui permet de réaliser une attaque afin d'obtenir un shell n'est disponible que depuis peu. Jusqu'à maintenant, certains groupes ont développé des exploits mais ne les ont pas publiés. 

De plus, la faille est « Wormable », c'est à dire qu'il est possible d'automatiser les attaques sans aucune action humaine. Ce qui permet au « worm » de se propager à travers les réseaux. 

## Fonctionnement 

Comme vu plus haut, cette faille provient du protocole RDP, et plus précisément au niveau des canaux utilisés. N'étant pas expert, ce que je comprends en simplifié :

  * RDP utilise des canaux virtuels pour la connexion
  * RDP utilise toujours le même canal, SVC31
  * Les canaux sont sélectionnés avant le « Security Commencement », ce qui le rend « Wormable »
  * Les SVC sont utilisés à la connexion, les DVC sont créés et détruits à la demande
  * Lors d'une connexion, le DVC « MS_T120 » est utilisé avec le SVC31

![](/images/BlueKeep/bluekeep_4.png)

La faille vient donc si on utilise un DVC « MS_T120 » sur un autre canal que le SVC31. Cela créé une corruption de la mémoire et permet d'exécuter du code à distance. 

N'hésitez pas à revenir vers moi si vous avez des choses à ajouter ou à corriger dans ces explications. 

Plus de détail sur la faille dans les liens suivants : 

  * <https://www.malwaretech.com/2019/05/analysis-of-cve-2019-0708-bluekeep.html>
  * <https://www.malwaretech.com/2019/09/bluekeep-a-journey-from-dos-to-rce-cve-2019-0708.html>
  * <https://wazehell.io/2019/05/22/cve-2019-0708-technical-analysis-rdp-rce/>

## État des lieux

Aujourd'hui, même si la faille est connue depuis mai 2019 il existe encore un grand nombre de machine utilisant RDP exposée sur Internet. 

Il est possible de retrouver ces machines sur Shodan : 

![](/images/BlueKeep/bluekeep_1.png)


Plus de 5 millions de RDP exposé sur Internet. Il est également possible de filtrer ces résultats par OS avec la commande suivante : 


```
port:"3389" os:"Windows 7 or 8"
```


![](/images/BlueKeep/bluekeep_2.png)

Avec un filtre par pays en plus : 


```
port:"3389" country:"FR" os:"Windows 7 or 8"
```

![](/images/BlueKeep/bluekeep_3.png)

L'utilisation du filtre « Windows 7 or 8 » nous permet de récupérer les machines Windows 7, 8, 2008 et 2008 R2. 

Un article sur le même sujet est disponible sur [Wired.][4] 

## Metasploit 

Cet article est écrit aujourd'hui car depuis peu Metasploit a publié un module pour l'exploitation de cette faille. 

Dans un premier temps plusieurs exploits ont été publiés mais ils permettaient seulement de faire crasher la machine (BSOD) et donc de créer un DOS (Deny Of Service) : 

  * <https://github.com/Ekultek/BlueKeep> 
  * <https://www.exploit-db.com/exploits/46946> 

Mais Metasploit a publié récemment un module pour cette faille qui fonctionne avec Windows 7 et Windows 2008/2008 R2. Le module en question est [disponible ici][5]. Il est également intégré dans les dernières versions de l'outil. 

Une vidéo de l'exploit est disponible : 

Ce module permet d'exécuter des commandes avec les privilèges SYSTEM. 

Attention, n'utilisez ce module ou ces scripts seulement sur des machines qui vous appartiennent. 

## Correction

Pour la correction/contournement de cette faille il y a plusieurs possibilités : 

  * Désactiver le protocole RDP
  * Patcher les systèmes

Microsoft propose un patch pour toutes les versions vulnérables, même pour les versions qui ne sont plus officiellement maintenues. Si les mises à jours sont automatiques, vos systèmes sont probablement déjà protégés de BlueKeep. 

Voici les KB (Patch) à appliquer sur vos systèmes pour les protéger : 

  * [Windows 7 & 2008/2008 R2][6]
  * [Windows XP, Vista & 2003][7]

Plusieurs grandes campagnes ont été menées, notamment par l'ANSSI, le CERT-FR ou certaines grandes entreprises de la sécurité pour la correction de cette faille.

## Conclusion

Pour faire une rapide conclusion sur cette faille, il faut s'attendre à des attaques similaires à WannaCry, surtout lorsque l'on regarde le nombre de service exposé sur internet. 

Avec l'arrivée d'un module Metasploit « clé en main », il devient très facile de réaliser des attaques de masse sur l'ensemble des systèmes disponible sur internet. 

Pensez à patcher vos systèmes et à fermer les ports qui ne sont pas utiles. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.cvedetails.com/cve/CVE-2019-0708/
 [2]: https://www.ncsc.gov.uk
 [3]: https://fr.wikipedia.org/wiki/Common_Vulnerability_Scoring_System
 [4]: https://www.wired.com/story/microsoft-bluekeep-patched-too-slow/?utm_source=JeromeVosgienFR&utm_medium=SophosFranceLink
 [5]: https://github.com/rapid7/metasploit-framework/blob/edb7e20221e2088497d1f61132db3a56f81b8ce9/modules/exploits/windows/rdp/cve_2019_0708_bluekeep_rce.rb
 [6]: https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-0708
 [7]: https://support.microsoft.com/en-us/help/4500705/customer-guidance-for-cve-2019-0708