---
title: "Installation d'un nœud de sortie Tor sur Debian 10"
author: Mickael Rigonnaux
type: post
date: 2020-04-25T11:03:03+00:00
url: /security/privacy/installation-dun-noeud-de-sortie-tor-sur-debian-10/
thumbnail: /images/tor_logo-1.png
tags: [
    "cryptographie",
    "GNU/Linux",
    "privacy",
    "tor"
]
categories: [
    "systeme",
    "securite",
    "logiciellibre"
]

---

Bonjour à tous ! Aujourd'hui un article qui traîne depuis un moment dans les brouillons, l'installation d'un nœud [Tor][1] sur un VPS avec le système [Debian][2] 10. C'est également le moment de présenter rapidement le projet et de faire un retour d'expérience sur ma modeste aventure dans ce projet.

## Tor ? 

Ne vous inquiétez pas, je ne vais pas vous présenter Tor dans les détails, ce n'est pas le but et l'intérêt n'est pas la vu que le sujet a déjà été traité plusieurs fois, nous allons simplement revoir les basiques. Si vous voulez vraiment des détails sur le sujet je vous invite à vous rendre sur le site officiel.

Le réseau Tor, issu du « [Tor Project][1] » est un réseau décentralisé et superposé. Superposé veut dire qu'il est basé sur un autre réseau. Ce réseau est composé de nœuds, qui sont principalement des serveurs GNU/Linux hébergé par la communauté. Les outils qui permettent d'utiliser ce réseau sont des logiciels libres proposés sous [licence BSD][3].

Cet ensemble de serveurs permet de gérer les connexions TCP (UDP n'est pas supporté) du réseau Tor afin de les rendre anonymes. Cela permet de naviguer sur le web par exemple ou encore d'utiliser des outils de communication instantané.

Le réseau Tor n'est cependant pas accessible avec les outils que nous utilisons habituellement. Généralement, l'accès à ce réseau se fait via [Tor Browser][4]. C'est un navigateur basé sur Firefox qui permet de se connecter à ce réseau.

Pour anonymiser les flux Tor utilise le « routage en oignon », c'est-à-dire que la connexion effectue un certain nombre de rebond sur des serveurs du réseau avant d'arriver à sa destination. Un exemple simplifié en image : 

![](/images/Kalitorify/kalitor_illu.png)

Les machines utilisées pour effectuer des rebonds vont former un circuit, ce qui va permettre de transférer les informations de notre poste client vers le serveur en utilisant ce réseau.

L'anonymisation, comme dans la majorité des cas est assurée par la cryptographie. Dans le cadre de Tor, l'idée est de distribuer des clés secrètes entre les nœuds du circuit en utilisant de la cryptographie asymétrique. L'ensemble des flux entre les nœuds donc chiffrés par Tor, à l'exception des flux entre le nœud de sortie et le serveur cible.

Un schéma permettant de visualiser ce fonctionnement :

![](/images/tor_2.png)


## Pourquoi ? 

Lorsque les médias parlent de Tor, c'est souvent pour parler de la vente d'armes, de drogues, ou d'autres affaires illégales. 

Mais là n'est pas le but de ce réseau, oui comme tous les outils permettant de protéger l'anonymat il a son lot d'utilisation abusive.

Tor c'est avant tout un système qui permet de se protéger de la surveillance sur Internet, il a notamment été utilisé par des lanceurs d'alertes comme Edward Snowden ou Julian Assange pour ne citer qu'eux. C'est également un outil utilisé contre la censure sur Internet, notamment dans les pays ou naviguer sur Internet n'est pas aussi libre que chez nous.

On entend également parler de « Deep Web » ou de « Dark Net » lorsque le sujet de Tor est abordé. Pour moi, ces termes, très souvent mal utilisés peuvent être définis de cette façon : 

  * Le « Deep Web » concerne tout ce qui n'est pas indexé sur Internet, donc tout ce qui n'est pas accessible via les moteurs de recherche classique.
  * Les « Dark Nets » eux sont des réseaux qui ne sont pas accessibles via nos outils classiques, comme Tor et les autres superposés.

## Les nœuds 

Comme déjà abordé, Tor fonctionne grâce à des nœuds, qui sont pour la plupart hébergé chez des cloud provider comme OVH, Scaleway, etc. par des personnes désireuses de contribuer à ce projet. 

La liste de ces nœuds est publique, ce qui permet le partage des clés publiques ainsi que la création du circuit. 

Il existe 3 sortes de nœuds :

  * Entrée (Entry/Guard Relay) : ces nœuds sont les points d'entrées du réseau Tor. Vos postes clients vont initier des connexions vers ces relais là pour entrer sur le réseau Tor, ils verront donc votre véritable IP publique.
  * Intermédiaire (Middle Relay) : sont les nœuds les plus communément hébergés par la communauté. Ils permettent d'effectuer les différents rebonds au sein du réseau Tor et donc de faire la liaison entre le nœud d'entrée et celui de sortie. L'ensemble des flux entre ces nœuds sont chiffrés.
  * Sortie (Exit Relay) : ils sont les plus importants du réseau, déjà car ils ne sont pas nombreux et car ce sont eux qui renvoient votre trafic directement vers les serveurs cibles. C'est également sur ces nœuds que le chiffrement effectué par Tor s'arrête, les flux renvoyés par le nœud ne sont pas chiffrés par le réseau. 

Une illustration qui permet de mieux appréhender leurs fonctions :

![](/images/tor_3.png)

Il existe un quatrième type de relai, qui lui est différent. Vous pouvez penser que si la liste de l'ensemble des nœuds est publiques, il est très facile pour certains états/groupes de bloquer l'ensemble des nœuds et du coup empêcher l'accès au réseau. Pour contrer ce problème des relais de type « bridge » sont disponibles sur le réseau Tor mais ne sont pas publiques. Ils sont des nœuds d'entrées, qui permettent, sur demande à des personnes n'ayant pas accès au réseau d'y accéder tout de même.

Ce système est principalement basé sur la confiance, c'est-à-dire que ce n'est pas parce que vous ajoutez un relai au réseau qu'il sera utilisé immédiatement. Plusieurs critères seront pris en compte avant de router des flux vers votre instance, comme l'uptime, la bande passante allouée, etc.

Il faut également savoir qu'il existe des risques à héberger des nœuds Tor, et principalement des nœuds de sorties car vous êtes responsables du trafic qui est effectué par votre machine. Même si nous allons le voir, des paramètres sont possibles pour afficher clairement que c'est un nœud Tor.

Il existe cependant des hébergeurs qui sont « Tor friendly », la liste est disponible sur ce lien : 

  * <https://trac.torproject.org/projects/tor/wiki/doc/GoodBadISPs>

OVH par exemple tolère les nœuds Tor dans la limite ou il ne cause pas de problème, Nextinpact a dédié [un article][5] sur ce sujet.

## Mon retour d'expérience

Avant de réaliser mon instance sur un VPS d'OVH j'ai eu la (très) mauvaise idée d'installer un nœud intermédiaire chez moi physiquement sur un Raspberry Pi et de l'exposer sur Internet. Autant vous dire qu'au bout de 3 jours mon IP était reconnue comme malicieuse par beaucoup de service, et la navigation était un enfer. Je ne vous conseille donc pas d'héberger un relai Tor directement chez vous à moins d'avoir une IP dédiée pour ce relai. 

Il est plus convenable d'héberger ce relai sur une machine externe, dans mon cas il s'agit d'un VPS OVH à 3€ par mois. Il faut cependant faire attention au choix du fournisseur, car aujourd'hui une grande partie des relais sont répartis chez les mêmes provider, ce qui centralise et fragilise ce système. Vous pouvez trouver des recommandations sur ce sujet sur le site officiel. OVH pour la France par exemple n'est pas recommandé.

J'ai donc ouvert depuis Octobre 2019 un nœud de sortie sur un VPS OVH, vous pouvez suivre son évolution et ses paramètres sur ce lien : 

  * <https://metrics.torproject.org/rs.html#details/D9E1A43F01048D2DE750282F984BFEECC9891D7D>

Durant ces 6 derniers mois je n'ai pas reçu de plainte de la part d'OVH ou d'autres personnes. Seulement quelques mails pour m'avertir que le VPS réalisait du SPAM sur des ports liés à des services mail par exemple. Pour l'instant je n'ai pas rencontré de problème important avec l'hébergement de mon relai de sortie.

Il m'a fallu quelques modifications pour que le service tourne correctement, notamment au niveau de la bande passante. Le réseau Tor utilisait une partie trop importante, ce qui mettait le VPS en défaut.

Aujourd'hui le service tourne bien et je n'y touche que très rarement, ça tourne en fond. C'est pour moi un aspect important, j'ai été et je suis toujours utilisateur de ce réseau, c'est la moindre des choses de mettre une machine à disposition des autres.

## L'installation

Pour l'installation je pars du principe que vous avez déjà réalisé les actions de sécurité de base sur votre machine comme l'authentification par clé SSH uniquement, le filtrage des ports avec un firewall, etc.

Les actions sont à réaliser avec l'utilisateur root et sont faites sur l'OS Debian 10. Dans mon cas je vais présenter l'installation d'un relai de sortie.

Tout d'abord il faut mettre le système à jour et installer les dépendances : 


```
apt update && apt upgrade
apt install apt-transport-https wget gnupg
```


Maintenant il faut ajouter le repo de Tor à notre distribution. Dans le fichier /etc/apt/source.list 


```
#### TOR REPO
deb https://deb.torproject.org/torproject.org buster main
deb-src https://deb.torproject.org/torproject.org buster main
```


Et ajouter la clé GPG : 


```
wget -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --import
gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | apt-key add -
```


Pour terminer l'installation du paquet spécifique à Debian ainsi que de Tor :


```
apt update
apt install tor deb.torproject.org-keyring
```


Nous pouvons maintenant passer à l'étape la plus importante, la configuration de Tor avec le fichier /etc/tor/torrc. Dans mon cas la configuration est la suivante : 


```
# Relay Port
ORPort 443

# Name for the relay
Nickname tzkuatnode

# Contact Info 
ContactInfo contact@rm-it.fr

# Policy for exit node
ExitPolicy accept *:*

ExitRelay 1

# Port and page for explication
DirPort 80
DirPortFrontPage /etc/tor/tor.html

Address tor-exit.net-security.fr

RelayBandwidthRate 5000 KB  # Throttle traffic to 100KB/s (800Kbps)
RelayBandwidthBurst 8000 KB # But allow bursts up to 200KB/s (1600Kbps)

```


Ma configuration est plutôt simple, elle est à adapter en fonction de vos besoins.

Pour les détails : 

  * ORPort correspond au port que vous allez utiliser pour exposer votre service Tor. L'utilisation du port 443 permet d'utiliser un port déjà connu et majoritairement ouvert. Le port utilisé par défaut est 9001.
  * Nickname correspond au nom de votre relai.
  * ContactInfo est l'adresse mail à mettre à disposition si une personne veut prendre contact avec vous
  * ExitPolicy permet de filtrer ce que l'on veut accepter ou non avec notre relai de sortie. Il est possible de filtrer les IPs ou les ports par exemple.
  * ExitRelay permet d'activer ou non le relai de sortie.
  * DirPort et DirPortFrontPage permettent de mettre à disposition une page explicative du réseau Tor et des nœuds de sorties. Cela permet montrer directement que l'IP est utilisée pour ce service. L'utilisation du port 80 est conseillée, celui utilisé par défaut est 9030.
  * Address permet fournir le nom DNS relié à ce noeud. Il est également recommandé de faire un enregistrement inversé afin de permettre de retrouver ce nom avec l'IP. Cela permet d'identifier le service plus facilement.
  * Les deux dernières lignes sont relatives à bande passante attribuée à votre relai. 

L'ensemble des paramètres sont à adapter en fonction de vos besoins et de ce que vous voulez mettre en place. 

Une fois que la configuration est en place vous pouvez relancer le service Tor :


```
systemctl restart tor
```


Vous pouvez vérifier dans les logs, vous devriez avoir la ligne suivante dans /var/log/tor/log, signe que votre instance est bien lancée :


```
[Notice] Self-testing indicates your DirPort is reachable from the outside. Excellent.
```


Je me permets également de vous partager un outil très pratique, un générateur de fichier de configuration. Cela permet de réaliser simplement des configurations pour les autres types de relais :

  * <https://tor-relay.co/>

Vous pouvez en suite retrouver votre nœud sur l'outil qui permet de monitorer les relais Tor : 

  * <https://metrics.torproject.org/rs.html>

Voici des exemples avec mon relai : 

![](/images/tor_1.png)



L'installation que j'ai faite est assez basique et peut-être incomplète, mais cela permet de répondre à mon besoin. Vous n'avez plus qu'à attendre que votre relai soit reconnu comme de confiance, pour qu'il puisse être utilisé par la communauté.

## Sources

  * <https://community.torproject.org/relay/>
  * <https://support.torproject.org/apt/tor-deb-repo/>
  * <https://trac.torproject.org/projects/tor/wiki/TorRelayGuide/DebianUbuntu>
  * <https://medium.com/coinmonks/tor-nodes-explained-580808c29e2d>

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Je vous invite également à faire des [dons pour soutenir ce projet][6] et à ouvrir des relais si vous en avez la possibilité !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://www.torproject.org/
 [2]: https://www.debian.org/
 [3]: https://fr.wikipedia.org/wiki/Licence_BSD
 [4]: https://www.torproject.org/download/
 [5]: https://www.nextinpact.com/news/81507-ovh-proxy-irc-vpn-et-tor-ne-sont-plus-interdits-sauf-si.htm
 [6]: https://donate.torproject.org/