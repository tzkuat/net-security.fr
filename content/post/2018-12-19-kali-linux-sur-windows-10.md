---
title: Kali Linux sur Windows 10
author: Mickael Rigonnaux
type: post
date: 2018-12-19T12:57:46+00:00
url: /system/kali-linux-sur-windows-10/
thumbnail: /images/Kali-Linux_Windows10/kali_w10_present.jpg
featureImage: images/Kali-Linux_Windows10/kali_w10_present.jpg
shareImage: /images/Kali-Linux_Windows10/kali_w10_present.jpg
featured: true
tags: [
    "GNU/Linux",
    "Linux",
    "Kali Linux"
]
categories: [
    "securite",
    "systeme"
]

---

Bonjour à tous ! Aujourd’hui nous allons voir ensemble comment installer [Kali Linux](https://www.kali.org/) sur son poste Windows 10 avec WSL !

Vous le savez surement, depuis un petit moment maintenant il est possible d’installer des sous-systèmes Linux sur sa machine Windows 10. La fonctionnalité qui permet cela est WSL (Windows Subsystem Linux) et est proposée depuis Windows 10 Redstone (1607). 

Différents systèmes sont disponibles, comme Debian, Ubuntu, Suse ou encore celle qui nous intéresse dans cet article, Kali Linux. Pour moi l’avantage est que je n’ai plus besoin de machines virtuelle pour réaliser des tests ou autres. Attention par contre les OS proposés sont « light », ils ne comprennent généralement pas les outils et paquets de base. 

Vous trouverez plus de renseignements sur ce système d’exploitation spécifique aux tests d’intrusion offensif sur [ce lien.](https://www.kali.org/)

Pour activer WSL sur votre poste il est nécessaire d'activer la fonctionnalité depuis Powershell (en administrateur) sur votre poste :


```
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```

L’ensemble des systèmes cités plus haut sont disponibles directement sur le Windows Store, il faut donc simplement réaliser l’installation. 

Vous pouvez directement installer le sous-système Kali :

![](/images/Kali-Linux_Windows10/win1.png)

![](/images/Kali-Linux_Windows10/win2.png)

Une fois téléchargé, un courte configuration sera nécessaire :

![](/images/Kali-Linux_Windows10/win3.png)

Vous voilà en possession d’un sous-système Kali Linux sur votre machine Windows, mais sans interface graphique.

Jusqu’à ce niveau, l’installation est la même pour les différents systèmes Linux.

**EDIT** L'installation via le script ne fonctionne plus. Merci de suivre les commandes suivants pour installer XFCE.

```
sudo apt update
sudo apt upgrade
sudo apt-get install xfce4
sudo apt-get install xrdp
sudo /etc/init.d/xrdp start
```

Vous pouvez maintenant vous connecter sur le port 3390 de votre poste :

![](/images/Kali-Linux_Windows10/win6.png)

## Ancienne installation (non fonctionnelle)

Pour procéder à l’installation de l’interface graphique il faut récupérer et exécuter le script suivant :


```
wget https://kali.sh/xfce4.sh 
chmod +x xfce4.sh 
./xfce4.sh
```


![](/images/Kali-Linux_Windows10/Win4.png)

Une fois le script lancé, vous pouvez attendre jusqu’à 45 minutes, l’installation est longue. Vous aurez ensuite à configurer votre clavier :

![](/images/Kali-Linux_Windows10/Capture-1.png)

Une fois ces tâches effectuées, lancez le service xrdp avec la commande suivante :


```
service xrdp start
```

![](/images/Kali-Linux_Windows10/win5.png)

Vous n’avez plus qu’à vous connecter à votre machine Kali en RDP (Remote Desktop Protocol) sur le port 3390 avec l’utilitaire fourni de base par Windows :

![](/images/Kali-Linux_Windows10/win6.png)

![](/images/Kali-Linux_Windows10/win7.png)

Connectez-vous et profitez de votre système Kali sans avoir eu à créer de machines virtuelle ou de dual-boot.

![](/images/Kali-Linux_Windows10/win8.png)

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat