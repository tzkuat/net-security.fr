---
title: 'Nagstamon : un client pour vos instances de supervision'
author: Mickael Rigonnaux
type: post
date: 2021-05-05T17:10:43+00:00
url: /logiciel-libre/nagstamon-un-client-pour-vos-instances-de-supervision/
thumbnail: /images/nagstamon.png
featured: true
featuredImage: images/nagstamon.png
toc: true
tags: [
    "logiciellibre",
    "GNU/Linux",
    "supervision",
    "Windows"
]
categories: [
    "systeme",
    "logiciellibre"
]
---

Bonjour à tous ! Aujourd'hui (et après un long moment d'absence) une rapide présentation d'un outil que j'ai découvert il y a peu et que je trouve très pratique à utiliser tous les jours : [Nagstamon](1).

## Nagstamon ?

Pour faire simple, Nagstamon (ou Nagios Status Monitor) c'est un client que vous allez pouvoir installer sur vos PC que ça soit Mac, GNU/Linux, Unix ou encore Windows. Ce client permet tout simplement de vous alerter en cas de problème dans votre supervision.

Je l'ai découvert car il est très utilisé dans mon entreprise actuelle, il est proposé sous licence libre GPLv2 et le code est disponible sur Github à [ce lien](2). Il est développé en Python et est proposé aujourd'hui en version 3.6.

Plus concrètement, il va vous permettre d'agréger et de consulter très simplement vos différentes solutions de supervision, voici les solutions prises en charge par l'outil :

  * [Nagios](3) 1.x, 2.x, 3.x and 4.x
  * [Icinga](4) 1.2+ and 2.3+
  * [Opsview](5) 5+
  * [Centreon](6) 2.3+
  * [Op5 Monitor/Ninja](7) 7+
  * [Checkmk](8) 1.1.10+
  * [Thruk](9) 1.5.0+
  * [Zabbix](10) 2.2+
  * [Monitos](11) 4.4+

Il y a également d'autres solutions qui sont toujours en mode bêta comme Promotheus ou Sensu, pour toutes les informations n'hésitez pas à vous rendre sur le site officiel.

## Pourquoi ?

Vous allez me dire, oui c'est un client et à quoi ça sert à part ne pas passer par l'interface Web ? Déjà de mon côté je trouve ça très utile car il permet d'agréger les sources, ce qui peut vite devenir fastidieux quand on a plusieurs interfaces. Cela permet également de savoir en un coup d’œil si l'ensemble du SI est fonctionnel, de plus vu qu'il est toujours activé sur mon PC je suis toujours alerté en cas de problème, même lorsque je n'ai pas mes mails ou les interfaces ouvertes.

J'imagine également que l'on est nombreux à avoir des solutions de supervision basées sur Nagios/Centreon ou encore Zabbix. Je pense que cet outil pourra être utile pour pas mal de monde.

Plus concrètement, cela se matérialise par un petit pop-up qui reste toujours en avant sur votre bureau, par exemple :
![Nagstamon Pop-Up](/images/image-49.png)

Et lorsque vous le survolez, vous obtenez toutes les informations :

![Nagstamon Pop-Up](/images/nag-1.png)

Cette image vient directement du site de Nagstamon pour ne pas vous montrer les données de ma supervision.


Vous avez également un système de notification qui permet d'être alerté, cela se passe avec des sons définis par Nagstamon (attention cela surprend au début !) et des clignotements du pop-up.

De mon côté, j'ai configuré cela pour être alerté par des sons seulement en cas d'alertes critiques ou de serveur « down », car sinon, ça devient vite gênant au quotidien. Vous avez également la possibilité d’acquitter directement les alertes depuis le client, ainsi que d'ignorer les serveurs en maintenant par exemple.

Pour connaître toutes les fonctionnalités, je vous invite à vous rendre sur [le site officiel](1) ou à le tester directement !

## Installation & Configuration

Pour l'installation, rien de plus simple vous pouvez récupérer les fichiers d'installation pour votre OS préféré sur ce lien :

  * <https://github.com/HenriWahl/Nagstamon/releases/tag/v3.6.0>

Vous pouvez également installer l'outil directement depuis votre gestionnaire de paquet selon votre distribution :

```
# Ubuntu/Debian
apt install nagstamon

# RHEL/Fedora
wget https://nagstamon.de/repo/fedora/nagstamon.repo --output-document=/etc/yum.repos.d/nagstamon.repo
dnf install nagstamon
```

Une fois l'outil installé, la configuration est très simple, il suffit de renseigner les interfaces que le client doit utiliser :

![Configuration 1](/images/image-50.png)

Dans cette interface vous avez également la possibilité de choisir la solution de supervision que vous utilisez.

Une fois ajouté, vous pourrez enfin profiter de Nagstamon. Comme je l'ai dit, de mon côté j'ai masqué les hôtes en maintenance ainsi que les alertes acquittées : 

![Configuration 2](/images/image-51.png)

J'ai aussi modifié les alertes pour avoir seulement les critiques ainsi que les hôtes non disponibles : 

![Configuration 3](/images/image-52.png)

Il existe bien d'autres configurations et d'options à découvrir sur cet outil, n'hésitez pas à chercher par vous même et à consulter le [site officiel](14).

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît ! Ici le but n'était pas de faire un article technique ou autre, mais simplement de présenter cet outil que je trouve incroyable par sa fonctionnalité et sa simplicité.

Merci pour votre lecture et à bientôt !

 [1]: https://nagstamon.de/
 [2]: https://github.com/HenriWahl/Nagstamon
 [3]: https://www.nagios.org/
 [4]: https://www.icinga.org/
 [5]: https://www.opsview.org/
 [6]: https://www.centreon.com/
 [7]: https://www.op5.com/
 [8]: https://checkmk.de/
 [9]: https://www.thruk.org/
 [10]: https://www.zabbix.com
 [11]: https://www.monitos.de/
 [13]: https://nagstamon.de/assets/images/index_2.png
 [14]: https://nagstamon.de