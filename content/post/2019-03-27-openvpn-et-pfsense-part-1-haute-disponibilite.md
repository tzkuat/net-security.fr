---
title: 'OpenVPN & PFSense – Part. 1 : Haute Disponibilité'
author: Fabio Pace
type: post
date: 2019-03-27T09:00:10+00:00
url: /system/openvpn-et-pfsense-part-1-haute-disponibilite/
thumbnail: /images/pfsense-ha/ha_pfsense0.png
shareImage: /images/pfsense-ha/ha_pfsense0.png
featured: true
tags: [
    "pfsense",
    "openvpn"
]
categories: [
    "systeme",
    "reseau"
]

---

Bonjour à tous, aujourd'hui nous allons voir comment mettre en place rapidement une infrastructure redondante offrant le service VPN (client to site) avec une authentification via l'Active Directory.

Pour cela, nous allons découper l'article en 4 parties : 

  * La mise en place de la Haute Disponibilité
  * L'authentification LDAP (via Active Directory)
  * La gestion des certificats (pour le serveur VPN)
  * La mise en place d'OpenVPN comme serveur VPN

## Introduction

Pfsense est un OS tournant sur FreeBSD, et a la particularité d’être léger et très performant.

C'est un routeur virtuel, et permet donc de réaliser (à moindre coût) un réseau redondant et sécurisé.

## Architecture

Avant de commencer, voici le schéma de l'architecture que nous allons mettre en place :

![](/images/pfsense-ha/openvpn_pfsense1.png)

Afin de simplifier les flux, notre réseau WAN où se trouve les deux Pfsense est aussi le réseau LAN où se trouve les serveurs à administrer.  
Les Pfsenses n'effectuent donc pas de routage entre les interfaces (mis à part entre l'interface WAN et OpenVPN qui est virtuelle).

Les deux sous réseaux 172.0.0.0/24 et 172.0.2.0/23 sont les sous-réseaux du tunnel VPN. Les clients obtiendront une IP en fonction de leur OU Active Directory : 

  * L'équipe IT (STAFF) seront en 172.0.0.0/24 avec 172.0.0.1 la pâte VIP des Pfsenses
  * Les collaborateurs (personnelle de l'entreprise) seront en 172.0.2.0/23 avec 172.0.2.1 la pâte VIP des Pfsenses

## La Haute Disponibilité

La haute disponibilité nous permet d’alléger la charge sur les hôtes (si les deux hôtes sont actif / actif) ou juste de permettre une continuité d’activité (actif / passif). 

Nous allons donc utiliser les fonctionnalités CARP, Pfsync et XML-RPC de Pfsense.

**CARP** (_Common Address Redundancy Protocol_) est un protocole qui permet de faire de la haute disponibilité et de partager au sein d’un cluster une adresse IP virtuelle (VIP). Il est une alternative à VRRP (_Virtual Router Redundancy Protocol_), HSRP (_Hot Standby Router Protocol_) … La valeur ajoutée de ce protocole est qu’il est totalement sécurisé (les échanges entre les hôtes du cluster sont chiffrés) et libre de droits. 

**Pfsync** est un protocole qui permet de synchroniser la table mémoire du firewall Packet Filter (Pare-feu de Pfsense) entre tous les membres du cluster afin que les états de connexions soient synchronisés sur les différents hôtes. 

NB : Les messages Pfsync sont envoyés en multicast (envoie d’un message à un groupe d’hôtes, plus restreint que anycast). Il est donc recommandé d’utiliser une interface dédiée afin de sécuriser les différents flux. Par contrainte de gestion, nous utiliserons seulement une carte réseau dans notre exemple.

**XML-RPC** est un protocole qui permet de répliquer la configuration d’un serveur à un autre. 

Tout d'abord, il faut créer une adresse IP virtuelle (VIP) dans chaque nœud du cluster :

![](/images/pfsense-ha/ha_pfsense1.png)

  1. Se rendre dans l'onglet « Firewall », puis « Virtual IPs » et ajouter une VIP.
  2. Choisir « CARP »
  3. L'interface est celle où vous allez faire parler vos deux Pfsense (il est recommandé d'utiliser une interface dédiée)
  4. Renseigner l'adresse IP virtuelle ainsi que son masque de sous réseau
  5. Saisir le mot de passe de l'adresse VIP (il faudra renseigner le même mot de passe pour la configuration de la VIP du second Pfsense)
  6. Laisser par défaut à 1 le VHID Group. Attention si vous utilisez plusieurs VIP (plusieurs interfaces) modifier le VHID Group afin qu'il soit unique pour chaque VIP
  7. Renseigner une description afin de rendre l'entrée plus explicite

Nous devons effectuer la même configuration sur le Pfsense02 (le backup).

NB : Il faut activer le mode « promiscious » afin d’autoriser les adresses IP virtuelles si vous utilisez une plateforme de virtualisation (configuration sur le portgroup d’un vSwitch sur Vmware par exemple).

Puis, il faut configurer la Haute Disponibilité.  

![](/images/pfsense-ha/ha_pfsense2.png)

  1. Rendez-vous dans l'onglet « System » et « High Availability Sync »
  2. Cocher la case « Synchronisation States » afin de synchroniser les tables mémoires des firewall (protocole Pfsync)
  3. Choisissez l'interface d'écoute du cluster : WAN dans notre cas.
  4. Renseigner l'adresse IP du second noeud du cluster : x.x.x.3

Il faut également configurer la synchronisation XML-RPC afin de répliquer la configuration du Pfsense01 vers le Pfsense02 :

![](/images/pfsense-ha/ha_pfsense3.png)

  1. Saisir le second nœud du cluster (le backup) : x.x.x.3
  2. Entrer le nom d'utilisateur de la Web GUI du Pfsense02. En effet, la réplication se fait à l'aide du port 80 ou 443 (via l'interface graphique)
  3. Entrer le mot de passe lié au compte utilisateur précédemment saisie
  4. Cocher tout pour tout répliquer

Veuillez configurer Pfsync également sur le Pfsense02 (backup) : 

![](/images/pfsense-ha/ha_pfsense4.png)

NB : Saisissez l'adresse IP du Master (Pfsense01) en Peer IP.  
Attention, ne configurer pas la Synchronisation XML-RPC. Seul le Master réplique sa configuration sur le Slave.

La haute disponibilité est maintenant en place, nous pouvons désormais configurer à partir du Pfsense01 car toutes les configurations que nous allons réaliser (Certificats, OpenVPN…) seront dupliquées sur le deuxième Pfsense (Pfsense02). 

Si vous avez des problèmes de réplication, attention de bien ouvrir les flux entre les deux Pfsense.  
Je vous recommande d’utiliser des alias et des séparateurs afin de bien comprendre les différentes règles. 

Voici un exemple de règles mis en place. 

![](/images/pfsense-ha/ha_pfsense5.png)

Je vous remercie pour votre lecture.  
Vous pouvez continuer l'article concernant l'authentification LDAP [ici][7].

A bientôt.

Fabio Pace.

 [1]: https://net-security.fr/wp-content/uploads/pfsense-ha/openvpn_pfsense1.png
 [2]: https://net-security.fr/wp-content/uploads/pfsense-ha/ha_pfsense1.png
 [3]: https://net-security.fr/wp-content/uploads/pfsense-ha/ha_pfsense2.png
 [4]: https://net-security.fr/wp-content/uploads/pfsense-ha/ha_pfsense3.png
 [5]: https://net-security.fr/wp-content/uploads/pfsense-ha/ha_pfsense4.png
 [6]: https://net-security.fr/wp-content/uploads/pfsense-ha/ha_pfsense5.png
 [7]: https://net-security.fr/system/openvpn-pfsense-part-2-lauthentification-ldap/