---
title: Gestionnaire de mots de passe auto-hébergé avec Bitwarden
author: Mickael Rigonnaux
type: post
date: 2019-07-10T08:43:11+00:00
url: /system/gestionnaire-de-mots-de-passe-auto-heberge-avec-bitwarden/
thumbnail: /images/Bitwarden_onprem/bitwarden-logo.png
featureImage: images/Bitwarden_onprem/bitwarden-logo.png
shareImage: /images/Bitwarden_onprem/bitwarden-logo.png
tags: [
    "GNU/Linux",
    "Docker",
    "Conteneur"
]
categories: [
    "systeme",
    "logiciellibre",
    "securite"
]


---
![](/images/Bitwarden_onprem/bitwarden_banner.png)

Bonjour à tous, aujourd'hui nous allons aborder ensemble le gestionnaire de mots de passe que j'utilise maintenant depuis plus d'un an et voir comment le déployer sur votre infrastructure. 

## Bitwarden ? 

### Présentation

Je ne vais pas revenir sur pourquoi utiliser un gestionnaire de mots de passe, ce sujet a déjà été traité plusieurs fois sur différents blogs comme celui de Korben. [Lien ici.][2] 

J'imagine donc que si vous êtes ici c'est que vous comprenez l'intérêt d'un tel outil ! 

**[Bitwarden][1]**, c'est un logiciel gratuit, Open-Source ([Repo Git][3]) et multi-plateforme (MacOS, Windows, Linux, iOS & Androïd). Comme tous les gestionnaires de mots de passe il permet de stocker ses identifiants, de générer des mots de passe aléatoire, etc. La seule chose que vous aurez à retenir est votre mot de passe maître. Vous pouvez également enregistrer d'autres informations comme, votre identité, vos cartes bancaires, etc. 

Bitwarden dispose également de plugin Web pour gérer l'auto complétion par exemple, personnellement c'est cette version que j'utilise tous les jours.
![](/images/Bitwarden_onprem/Bitwarden-8.png)

En plus des clients lourds, des plugins Web et des applications mobiles, le logiciel est également disponible directement en CLI et en Web ! Vous aurez donc normalement toujours accès à vos mots de passe, peu importe ou vous êtes.

![](/images/Bitwarden_onprem/Bitwarden-9.png)

Comparé aux autres logiciels du même type que j'ai pu tester, je le trouve plus agréable à utiliser, beaucoup plus rapide et facile que KeePass par exemple où moins intrusif que Dashlane qui essai toujours de remplir les champs (même si ça se désactive je suis d'accord). C'est simplement mon retour sur les différents logiciels. 

Un des gros avantages de ce logiciel est que dans sa version gratuite il propose une synchronisation des mots de passe dans le cloud, et cela sans limite d'utilisation ou de nombre. Vous pouvez donc accéder à vos mots de passe de partout du moment que vous avez un accès internet.

Sur la partie sécurité, toutes les données que vous envoyez à Bitwarden sont chiffrées et hachées avant de quitter votre ordinateur. Le seul moyen de déchiffrer les données est d'utiliser le mot de passe maître que vous êtes le seul à connaître. 

Plus techniquement, les données sont protégées avec AES-256. Au niveau du mot de passe maître, PBKDF2 est utilisé avec HMAC SHA256 et les échanges se font avec l'aide de TLS. Vous avez plus d'informations sur ce sujet [ici][6]. 

Dans le même sujet, Bitwarden propose un programme de [Bug Bounty sur Hacker One][7] et à aussi publier un rapport d'audit de son code ([disponible ici][8]) pour être le plus transparent possible. 

Bitwarden pour fournir ce service à ses utilisateurs est basé sur le Cloud d'Azure. Si vous ne voulez pas utiliser cette configuration, vous pouvez héberger vous-même votre serveur Bitwarden, nous verrons comment faire un peu plus tard. 

### Les offres

Il existe aussi des versions payantes de ce logiciel que ça soit pour des utilisations personnelles ou professionnelles :

![](/images/Bitwarden_onprem/Bitwarden-10.png)

La plupart des offres sont accessibles, pour les entreprises un des avantages de la solution est le partage des mots de passe, qui est souvent compliqué à mettre en œuvre. De plus il est possible de segmenter les utilisateurs avec des groupes pour limiter l'accès aux mots de passe. Vous avez également dans toutes les versions la possibilité de faire de la 2FA. La possibilité de l'auto hébergement est également possible dans toutes les options.

### Fonctionnalités

Sur l'aspect utilisation de tous les jours Bitwarden intègre plusieurs outils sympathiques comme vérifier si votre mot de passe n'a pas déjà été compromis (basé sur [HaveIBeenPwnd][11]) : 

![](/images/Bitwarden_onprem/Bitwarden-12.png)

Ou l'auto complétion dans un formulaire, si vous avez un ou plusieurs mots de passe enregistrés pour un site, un petit nombre s'affiche au-dessus du logo Bitwarden et vous n'avez qu'à sélectionner l'identifiant pour compléter le formulaire. 

![](/images/Bitwarden_onprem/Bitwarden-13.png)

Si vous utilisez déjà un gestionnaire, vous pouvez facilement faire un export et l'intégrer à Bitwarden , tout est expliqué [ici][12].

## Auto Hébergement

Si vous n'avez pas confiance en Azure ou si vous voulez tout simplement avoir complètement la main sur vos données vous avez la possibilité d'héberger votre propre serveur Bitwarden. Ce service est basé sur Docker, pour se faire il faudra donc tout d'abord installer Docker ( >= 1.8) et Docker-Compose ( >= 1.17.1).

Dans mon cas l'installation se fait sur un VPS sous Ubuntu Server 18.04 LTS. Pour accéder à la machine depuis internet j'ai également créé une redirection DNS, ça permettra également de créer un certificat Let's Encrypt. Vous pouvez si vous le voulez le déployer en local et utiliser localhost par exemple. 

Au préalable il faut avoir au moins 2Go de RAM et un processeur d'au moins 1.4 GHz et 10 Go de disque. Les ports 80 & 443 doivent également être disponibles. 

Installation de Docker :

```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt-get update
sudo apt-get install docker-ce docker-compose
```

Vérifiez ensuite si docker est bien lancé avec la commande suivante : 


```
systemctl status docker
```


Téléchargement du script d'installation de Bitwarden, le logiciel sera installé dans le dossier du lancement du script dans le répertoire ./bwdata : 

```curl -Lso bitwarden.sh https://go.btwrdn.co/bw-sh && chmod +x bitwarden.sh```

Vous pouvez maintenant lancer l'installation : 


```
./bitwarden.sh install
```

![](/images/Bitwarden_onprem/Bitwarden-1.png)

Plusieurs questions vont être posées pendant l'installation, comme votre domaine, si vous voulez un certificat Let's Encrypt et votre adresse mail éventuellement. 

Des informations comme votre clé et votre ID seront également nécessaires, pour avoir ces informations il faut se rendre ici : 

![](/images/Bitwarden_onprem/Bitwarden-2.png)

Une fois ces étapes passées, l'installation est terminée et vous pouvez lancer votre serveur avec la commande suivante : 


```
./bitwarden.sh start
```
![](/images/Bitwarden_onprem/Bitwarden-3.png)

L'installation se terminera et vous pourrez vous connecter à votre domaine : 

![](/images/Bitwarden_onprem/Bitwarden-4.png)

Avec la commande suivante vous pourrez également observer les différents conteneurs lancés par le script Bitwarden : 

```
docker container ls
```

![](/images/Bitwarden_onprem/Bitwarden-7.png)

On voit bien les différents conteneurs, et notamment la base de données MSSQL. 

Vous pouvez maintenant vous connecter à votre serveur Bitwarden avec votre navigateur : 

![](/images/Bitwarden_onprem/Bitwarden-5.png)

Et créer un compte avec l'adresse que vous avez renseignée au préalable. Une fois le compte créé, vous pouvez utiliser ce dernier avec les applications et les plugin de la façon suivante, avant de vous connecter cliquez en haut à gauche sur la roue crantée et renseignez votre domaine : 

![](/images/Bitwarden_onprem/Bitwarden-6.png)

Voilà, vous venez de déployer votre serveur Bitwarden. Si vous voulez plus d'information sur comment est déployé le serveur, vous pouvez vous rendre sur le [repo Git][14] et sur la [documentation officielle.][15] Il faut également prendre en compte que dans une infrastructure de ce type c'est du coup à vous de vous occuper des backups des données et de la disponibilité. 

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat

 [1]: https://bitwarden.com/
 [2]: https://korben.info/bien-choisir-memoriser-mots-de-passe.html
 [3]: https://github.com/bitwarden
 [4]: images/Bitwarden_onprem/Bitwarden-8.png
 [5]: images/Bitwarden_onprem/Bitwarden-9.png
 [6]: https://proprivacy.com/privacy-news/bitwarden-review
 [7]: https://hackerone.com/bitwarden
 [8]: https://blog.bitwarden.com/bitwarden-completes-third-party-security-audit-c1cc81b6d33
 [9]: images/Bitwarden_onprem/Bitwarden-10.png
 [10]: images/Bitwarden_onprem/Bitwarden-11.png
 [11]: https://haveibeenpwned.com/
 [12]: https://help.bitwarden.com/article/import-data/
 [13]: https://bitwarden.com/host/
 [14]: https://github.com/bitwarden/server
 [15]: https://help.bitwarden.com/article/install-on-premise/