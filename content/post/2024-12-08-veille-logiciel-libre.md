---
title: "Faire sa veille avec des logiciels libres"
author: Mickael Rigonnaux
type: post
date: 2024-12-17T08:00:44+00:00
url: /systeme/veille-logiciel-libre
thumbnail: /images/veille-libre/icon-veille.png
shareImage: /images/veille-libre/icon-veille.png
featured: true
toc: true
tags: [
    "veille",
    "libre"
]
categories: [
    "systeme"
]

---

Bonjour à tous, aujourd'hui un article après un (long) moment d'absence, c'est un article qui se veut simple et rapide car il a déjà été traité plusieurs fois par d'autres personnes, on va parler de veille technologique.

Mais plutôt que de parler essentiellement des sources, qui diffèrent selon les moments, les spécialités, on va parler des outils qui permettent de faire de la veille et plus personnellement ceux que j'utilise qui sont majoritairement libres et auto-hébergeable.

## De la veille ?

On parlera + spécifiquement de veille technologique mais la veille s'applique à tous les domaines donc les outils seront également utilisables.

La veille, je ne vous apprends rien, fait partie intégrante de nos métiers. Particulièrement dans la sécurité et dans l'IT au sens large, c'est très important de voir ce qui se fait en termes d'outils, de mouvements, de groupes d'attaquants et de nouvelles vulnérabilités.

Durant ma (courte) carrière c'est quelque chose qui m'a toujours servi et qui m'a aidé à me mettre en avant dans des entretiens, c'est toujours apprécié de connaitre des outils par exemple et d'être au courant des dernières actualités.

## Le fonctionnement

De mon côté je réalise de la veille assez basique, j'utilise un outil pour suivre des flux RSS, si un article m'intéresse je l'enregistre dans un second outil qui lui me permet de les organiser via des tags et de rajouter des commentaires. J'utilise d'autres outils qui peuvent être spécifiques, notamment sur le suivi des CVE.

### Les outils

#### FreshRSS

[FreshRSS](https://freshrss.org/) est un outil très connu, qui permet de suivre des flux RSS depuis une page Web, je l'utilise depuis maintenant plusieurs années en alternative à Netvibes que j'utilisais avant ou encore Feedly.

Il dispose d'une large communauté, il est développé en PHP et est distribué sous licence AGPL.

Je ne vous ferai pas de tuto pour l'héberger, ce n'est pas le but de l'article, cela viendra peut-être dans un prochain article, si cela vous intéresse, je vous renvoie vers la documentation officielle :
* https://github.com/FreshRSS/FreshRSS#installation

De manière très simple vous pouvez catégoriser des flux RSS dans des dossiers et les organiser, voici un exemple avec mon utilisation de FreshRSS :
![](/images/veille-libre/veille-libre-1.png)

En sachant que vous pouvez trouver en ligne des instances publiques pour vous inscrire ou tester l'outil (personnellement je n'ai pas encore pris la peine de l'héberger moi même, honte à moi).

#### Shaarli

Shaarli de son côté est un outil qui a déjà fait l'objet d'un article sur ce blog.
* https://net-security.fr/logiciel-libre/shaarli-un-outil-pour-sauvegarder-organiser-vos-liens/

Shaarli est un outil qui permet de sauvegarder, gérer, d'organiser et de partager ses marques pages. Il se veut minimaliste, simple et sans base de données pour fonctionner. Il est bien entendu proposé sous licence libre GPLv3 et a été créé par Seb Sauvage. Le logiciel est développé principalement en PHP/Javascript.

L'intérêt de Shaarli c'est de pouvoir partager cette veille avec tout le monde, je suis régulièrement celui de Sebsauvage par exemple.
  * <https://links.tzku.at>
  * <https://sebsauvage.net/links/>
  * <https://links.pofilo.fr/>

Depuis un navigateur, il suffit d'installer une extension et en un clic vous pouvez enregistrer des liens, rajouter des commentaires ainsi que des "tags"
 pour les organiser.
![](/images/veille-libre/veille-libre-2.png)

#### OpenCVE (**Sous Licence BSL**)

OpenCVE de son côté est un logiciel au code accessible mais qui n'est pas libre/ni open-source car proposé sous licence "[Business Source Licence](https://github.com/opencve/opencve/blob/master/LICENSE)".

Il devait passer sous licence Apache en 2024 mais cela a été repoussé.

Si cela vous intéresse, un article complet est disponible sur le blog :
* https://net-security.fr/security/veille-opencveio/

OpenCVE est un outil de veille spécifique qui me permet de suivre et de mieux gérer les vulnérabilités. Nous avons tous aujourd'hui des dizaines de serveurs, de logiciels, de versions, etc.

OpenCVE permet d'en assurer le suivi, ce qui n'est pas un luxe quand on voit le nombre de vulnérabilité qui sont sorties en 2024.

Plus concrètement, c'est un outil Web qui permet de s'abonner à des vendeurs et/ou des logiciels : Apache2, Ubuntu, PHP, Microsoft Windows... Et d'être alerté en cas de vulnérabilité sur ces catégories. Il permet en plus de ça d'accéder à la liste complète des CVE et de les filtrer par vendeur, produit, etc.

#### Wallabag

[Wallabag](https://wallabag.org/) de son côté est une alternative à Pocket (qui est intégré nativement dans Firefox). Il permet de classer, enregistrer et de lire des articles. Je l'ai utilisé un temps, mais je n'avais pas forcément besoin d'un lecteur intégré et je voulais que ma veille soit publique (d'où l'utilisation de Shaarli.).

L'outil est développé en PHP et est disponible sous licence MIT. Une application mobile est également disponible pour suivre et lire les articles.

Il permet d'avoir sur une seule interface tous les articles enregistrés, de les trier avec des tags/catégories, d'avoir le temps de lecture... C'est quand même bien plus visuel que Shaarli si le but n'est pas de partager l'information.

![Source : https://wallabag.it](/images/veille-libre/veille-libre-3.png)

Si cela vous intéresse, vous trouverez la documentation pour l'installation ici :
* https://doc.wallabag.org/en/admin/installation/installation.html#installation

En sachant qu'il existe une version commerciale hébergée ici également :
* https://www.wallabag.it/en
#### Autres outils

J'ai présenté rapidement les articles que j'ai utilisés ou que j'utilise encore tous les jours. J'ai également avec le temps enregistré plusieurs outils que je testerais peut-être à l'avenir.

Voici donc une liste non exhaustive de logiciels libres pour faire de la veille :
* **Tiny Tiny RSS**, proposé sous licence GPLv3 et développé en PHP : https://tt-rss.org/
* **Miniflux**, une application minimaliste pour suivre des flux RSS, développé en Go et sous licence Apache 2.0 : https://miniflux.app/
* **Flus**, par le créateur de FreshRSS, qui voulait un outil beaucoup plus simple et épuré. Il permet notamment de partager des sections/collections. (merci à @Booteille du [Journal du Hacker](https://www.journalduhacker.net/) pour le partage !) : https://flus.fr/
  * Flus est développé en PHP et est proposé sous licence AGPL.

## Conclusion

Et voilà, vous connaissez maintenant les outils que j'utilise pour réaliser quotidiennement ma veille. Je n'ai pas présenté les sources mais n'hésitez pas à me demander si besoin.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat