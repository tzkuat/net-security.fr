---
title: 'OpenVPN & PFSense – Part. 4 : OpenVPN Client to Site'
author: Fabio Pace
type: post
date: 2019-03-30T14:45:41+00:00
url: /system/openvpn-pfsense-part-4-openvpn-client-to-site/
thumbnail: /images/pfsense-openvpn/openvpn_pfsense0-e1553957350886.png
shareImage: /images/pfsense-openvpn/openvpn_pfsense0-e1553957350886.png
tags: [
    "pfsense",
    "openvpn"
]
categories: [
    "systeme",
    "reseau"
]

---
![](/images/openvpn_pfsense0.png)

Bonjour à tous, pour finaliser l'article sur OpenVPN et PFSense, nous allons désormais voir la configuration d'OpenVPN en « Client to Site ».

OpenVPN est un logiciel qui permet de créer un réseau privé virtuel (VPN). Le package OpenVPN est installé de base dans Pfsense. 

L'utilisation d'OpenVPN repose sur des certificats, veuillez bien respecter les différentes étapes de la [Partie 3 - Les certificats][2].

## Configuration coté serveur

Tout d'abord, veuillez vous rendre dans la partie VPN, puis choisir OpenVPN. Nous allons commencer par configurer la partie serveur :

![](/images/pfsense-openvpn/openvpn_pfsense1-1.png)
![](/images/images/pfsense-openvpn/openvpn_pfsense2.png)

  1. Choisir « Remote Access (User Auth) » car nous avons un seul certificat client pour tous les clients (pour faciliter la gestion et l'article). De plus, il est recommandé par Netgate d'utiliser « Remote Access » pour une connexion LDAP / Radius.
  2. Choisir le serveur d'authentification pour ce VPN : ici nous choisissons « Active Directory - STAFF »
  3. Choisir UDP pour gagner en performance : il n'y aura pas de contrôle d'erreur si la ligne internet du client est instable, mais consommera moins de bande passante
  4. Choisir l'interface VIP créée précédement
  5. Choisir le port d'écoute du VPN : nous choisissons 1195 car le 1194 sera réservé aux collaborateurs
  6. Donner une description pour rendre la configuration plus explicite
  
![](/images/pfsense-openvpn/openvpn_pfsense3.png)

  1. Cocher la case « Use a TLS Key » pour effectuer une authentification TLS afin de créer une demande de connexion (il faut que les deux clés correspondent, coté serveur comme coté client)
  2. OpenVPN génère une clé automatiquement lorsque vous sauvegardez votre configuration, ou vous pouvez importer une clé déjà existante
  3. Choisir l'authentification TLS pour utiliser la clé seulement pour authentifier la personne (et non chiffrer le flux)
  4. Choisir la CA précédement créée
  5. Choisir le certificat serveur créé précédement
  6. Le chiffrement asymétrique (RSA) utilisera une clé de 2048 bits (recommander)
  
![](/images/images/pfsense-openvpn/openvpn_pfsense4.png)

NB : ECDH Curve correspond aux courbes éliptique qui offrent une plus grande sécurité et performance que le protocole DH (Diffie-Hellman). Cependant, dû à la complexiter, nous verrons plus tard comment intégrer les courbes eliptique.

  1. Choisir une méthode de chiffrement sécurisé, AES-256 par exemple. A noter que dans notre cas nous aurions pu choisir AES-128 pour améliorer les performances car la longueur de la clé va ralentir le chiffrement des données. CBC est le mode de chiffrement par bloc qui reste un des modes le plus sûr. 
  2. L'algorithme d'authentification choisie est SHA256 pour sa robustesse et sa sécurité (éviter SHA1)

NB : La partie « NCP Algorithms » classe les différents algortihmes que le client va pouvoir négocier. Nous indiquons que le client pourra négocier la méthode de chiffrement « AES-128-GCM » car GCM (Galois / Counter Mode) est une méthode perfomante et sécurisée.

![](/images/images/pfsense-openvpn/openvpn_pfsense6.png)

  1. Choisir un réseau pour les utilisateurs du VPN : 172.0.0.0/24 par exemple
  2. Choisir le ou les sous-réseaux dont les utilisateurs auront le droit d'y accéder
  3. Choisir un nombre maximum de connexion simultanée au VPN
  
![](/images/images/pfsense-openvpn/openvpn_pfsense7.png)

  1. Cocher la case afin de maintenir la connexion VPN même si l'utilisateur change d'IP (par exemple un utilisateur en 4G : IP public est suceptible de changer s'il se déplace en étant connecté)
  2. Si vous choisissez « Subnet », cela signifie que les utilisateurs se véront dans le réseau VPN
  3. Cocher la case pour forcer le domaine par défaut du client pour les recherches DNS
  4. Entrer votre domaine (lab.dom par exemple)
  5. Cocher la case pour donner l'adresse IP des serveurs DNS pour la résolution de nom dans le VPN 
  
![](/images/images/pfsense-openvpn/openvpn_pfsense8.png)

  1. Renseigner une option dans le champs « Custom Option » pour éviter que l'authentification reste en cache sur le poste du client (recommandation d'OpenVPN)
  2. Augmenter le niveau de verbosité si vous souhaitez avoir plus de log

La configuration d'OpenVPN coté serveur est maintenant terminée. Nous allons voir comment exporter la configuration et l'installer sur le poste.

## Configuration coté client

Afin de faciliter l’intégration sur les postes clients, nous allons installer un package sur Pfsense afin d’exporter la configuration OpenVPN pour les clients.

![](/images/images/pfsense-openvpn/openvpn_pfsense9.png)

  1. Se rendre dans « System » -> « Package Manager » 
  2. Choisir « Available Packages » pour installer des packages
  3. Choisir « openvpn-client-export »

![](/images/pfsense-openvpn/openvpn_pfsense10-1.png)

  1. Dans la rubrique « OpenVPN » -> « Client Export Utility »
  2. Choisir quelle configuration vous souhaitez générer (STAFF pour notre part)
  3. Choisir « Other » si vous souhaitez que votre utilisateur se connecte via un nom de domaine
  4. Le nom domaine pour accéder au VPN : vpn.lab.dom par exemple

![](/images/pfsense-openvpn/openvpn_pfsense11.png)

  1. Renseigner également « auth-nocache » pour éviter de recevoir une erreur au moment de la connexion avec OpenVPN
  2. Sauvegarder pour garder la configuration actuelle par défaut

![](/images/pfsense-openvpn/openvpn_pfsense12.png)

OpenVPN vous propose différents mode d'installation et de configuraiton pour le client : 

  1. Un seul fichier de configuration contenant : 
      * Les lignes de configuration (protocole de chiffrement, port utilisé&#8230;)
      * La CA du serveur
      * La clé TLS pour l'authentification
  2. Une archivre contenant séparement : 
      * Un fichier .ovpn avec la configuraiton du VPN
      * Un fichier .crt contenant la CA du serveur
      * Un fichier .key contenant la clé TLS
  3. Le package d'installation (.exe) afin que l'utilisateur puisse lancer simplement l'installation et la configuration de la connexion VPN

NB : Si vous choisissez la méthode 1 ou 2, cela signifie qu vous avez déjà télécharger en amont le client OpenVPN, ou que vous n'êtes pas sur une distribution Windows.

Attention, le client OpenVPN GUI (interface graphique) n'est pas disponible sur la plateforme MAC et LINUX, il suffit de télécharger openvpn-client depuis vore gestionnaire de paquet et d'ajouter manuellement la configuration dans votre gestionnaire réseau.

J'espère que l'article vous aura plu, n'hésitez pas à le commenter si vous avez des questions et/ou améliorations à apporter.

A bientôt.

Fabio Pace.

 [1]: images/openvpn_pfsense0.png
 [2]: https://net-security.fr/system/openvpn-pfsense-part-3-les-certificats/
 [3]: images/pfsense-openvpn/openvpn_pfsense2.png
 [4]: images/pfsense-openvpn/openvpn_pfsense3.png
 [5]: images/pfsense-openvpn/openvpn_pfsense4.png
 [6]: images/pfsense-openvpn/openvpn_pfsense6.png
 [7]: images/pfsense-openvpn/openvpn_pfsense7.png
 [8]: images/pfsense-openvpn/openvpn_pfsense8.png
 [9]: images/pfsense-openvpn/openvpn_pfsense9.png