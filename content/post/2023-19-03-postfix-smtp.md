---
title: Un relais SMTP postfix avec TLS, SASL, DKIM, DMARC & SPF
author: Mickael Rigonnaux
type: post
date: 2023-03-28T10:05:44+00:00
url: /securite/postfix-secure
thumbnail: /images/postfix-secure/postfix-logo.png
featureImage: images/postfix-secure/postfix-logo.png
shareImage: /images/postfix-secure/postfix-logo.png
featured: true
toc: true
tags: [
    "openssl",
    "cryptographie",
    "postfix",
    "certificat",
    "mail"
]
categories: [
    "securite"
]

---

Bonjour à tous ! Aujourd'hui un article qui va parler de mail et de sécurité. Nous allons voir comment installer un serveur de mail SMTP postfix avec toutes les couches de sécurité, c'est à dire :
* Authentification SASL
* Chiffrement TLS
* DMARC, SPF & DKIM

Le but étant de pouvoir envoyer des mails qui n'arrivent pas dans les spams.

## Pré-requis

Voici les pré-requis pour réaliser cette configuration :
* Utiliser une machine GNU/Linux (Debian Like)
* Disposer d'une IPv4 publique
* Disposer d'un domaine
* Avoir la possibilité de faire un enregistrement reverse sur l'IP
* Ouvrir le port 25/TCP (SMTP) dans le sens sortant
* Ouvrir le port 465/TCP (SMTPS) dans le sens entrant (ou faire du filtrage)

De mon côté j'ai utilisé un VPS Scaleway pour l'exemple avec Ubuntu 22.04 LTS. J'ai utilisé le domaine `tzku.at` et le sous domaine `smtp-test.tzku.at` pour faire la configuration.

Il faut obligatoirement faire pointer votre sous-domaine vers l'IP publique et configurer un enregistrement reverse afin de renvoyer le même sous-domaine.

Vérification des enregistrements DNS :
```
tzkuat@pop-os ~> dig smtp-test.tzku.at
[...]
;; ANSWER SECTION:
smtp-test.tzku.at.	3190	IN	A	163.172.142.29
[...]

tzkuat@pop-os ~> dig -x 163.172.142.29
[...]
;; ANSWER SECTION:
29.142.172.163.in-addr.arpa. 38	IN	PTR	smtp-test.tzku.at.
[...]
```

On voit bien que les enregistrements DNS sont présents que ça soit l'enregistrement A ou PTR.

## SASL ? SPF ? DKIM ? DMARC ?

Quand nous parlons de sécurité des mails, c'est très souvent compliqué. Du moins, cela l'était pour moi il y a peu. Il existe aujourd'hui plusieurs moyens d'améliorer la sécurité des échanges de mail et d'empêcher que les mails n'arrivent dans les spams. Les principaux protocoles/mécanismes de sécurité spécifique aux mails sont :
* DMARC
* DKIM
* SPF

Ces 3 mécanismes sont liés au DNS car ils nécessitent tous des enregistrements spécifiques.

D'autres mécanismes de sécurité seront configurés dans cet article : SASL pour l'authentification et TLS pour chiffrer les flux avec le relais de mail.

### SPF : Sender Policy Framework

SPF est la méthode la plus répandue car la plus simple, via un enregistrement DNS vous allez simplement indiquer qui peut expédier des mails avec ce domaine. Elle est définie dans la RFC 7208. SPF se matérialise par un enregistrement DNS.

Pour plus d'informations :
* https://www.bortzmeyer.org/7208.html
* https://fr.wikipedia.org/wiki/Sender_Policy_Framework

Voici un exemple d'enregistrement SPF :
```
tzkuat@WS-MRIGONNAUX ~> dig net-security.fr txt | grep spf
net-security.fr.        600     IN      TXT     "v=spf1 include:mx.ovh.com -all"
tzkuat@WS-MRIGONNAUX ~>
```

Dans cet exemple nous pouvons voir que seul `mx.ovh.com` est autorisé à expédier des mails pour le domaine `net-security.fr`.

### DKIM : DomainKeys Identified Mail

Définit dans la RFC 6376 DKIM permet d'authentifier l'expéditeur d'un mail grâce à la cryptographie asymétrique. DKIM va permettre de signer un mail envoyé grâce à une clé privée, le destinataire pourra alors retrouver la clé publique dans un enregistrement DNS afin de vérifier la signature du mail et donc l'identité de l'expéditeur. DKIM se matérialise par un enregistrement DNS en plus d'une configuration sur le serveur expéditeur.

Pour plus d'informations :
* https://www.bortzmeyer.org/4871.html
* https://fr.wikipedia.org/wiki/DomainKeys_Identified_Mail

### DMARC : Domain-based Message Authentication, Reporting, and Conformance

DMARC est un mécanisme qui permet de créer une politique pour un domaine grâce à un enregistrement DNS. Il permet de dire quoi faire en fonction des protocoles DKIM et SPF. Par exemple, si vous les configurez de manière stricte, vous pouvez dire dans la politique de rejeter tous les mails ne respectant pas SPF & DKIM. De plus, DMARC permet également de recevoir des rapports venant de plusieurs entités (Google, Microsoft, etc.) avec les résultats des authentifications réalisées avec le domaine en question. DMARC se matérialise par un enregistrement DNS.

Les enregistrements DMARC se font sur forme suivante `_dmarc.example.com` avec un enregistrement TXT pour ajouter les configurations du mécanisme.

Voici un exemple d'enregistrement pour `microsoft.com` :
```
tzkuat@WS-MRIGONNAUX ~/.ssh> dig _dmarc.microsoft.fr TXT
[...]
;; ANSWER SECTION:
_dmarc.microsoft.fr.    3600    IN      TXT     "v=DMARC1; p=reject; pct=100; rua=mailto:rua@dmarc.microsoft; ruf=mailto:ruf@dmarc.microsoft; fo=1"
[...]
```

Tous les paramètres sont disponibles ici :
* https://fr.wikipedia.org/wiki/DMARC#Param%C3%A8tres

Pour plus d'informations :
* https://www.bortzmeyer.org/7960.html
* https://fr.wikipedia.org/wiki/DMARC


## Configuration

### Génération du certificat SSL/TLS

Vu que nous allons utiliser le protocole SMTPS (SMTP avec TLS) il faut d'abord générer un certificat SSL. Dans mon cas je vais utiliser un certificat Let's Encrypt généré avec `certbot`. Vous pouvez également utiliser un certificat auto-signé ou un certificat issu d'une autre autorité.

Installation de `certbot` :
```
sudo apt update
sudo apt install certbot
```

Puis une demande de certificat avec `certbot` (dans mon cas le domaine est smtp-test.tzku.at):
* `certbot certonly --standalone -d smtp-test.tzku.at`

Si l'enregistrement DNS pour votre domaine est bien créé, vous devriez avoir une sortie de ce type :
```
root@scw-upbeat-blackwell:~# certbot certonly --standalone -d smtp-test.tzku.at
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Enter email address (used for urgent renewal and security notices)
 (Enter 'c' to cancel): xxx@example.com

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.3-September-21-2022.pdf. You must
agree in order to register with the ACME server. Do you agree?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: Y

[...]
Account registered.
Requesting a certificate for smtp-test.tzku.at

Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/smtp-test.tzku.at/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/smtp-test.tzku.at/privkey.pem
This certificate expires on 2023-06-20.
These files will be updated when the certificate renews.
Certbot has set up a scheduled task to automatically renew this certificate in the background.
[...]
```

Nous avons bien notre certificat et notre clé privée :
* Clé privée : `/etc/letsencrypt/live/smtp-test.tzku.at/privkey.pem`
* Certificat : `/etc/letsencrypt/live/smtp-test.tzku.at/fullchain.pem`

### Installation & configuration de postfix / SASL

Il faut commencer par mettre à jour les paquets et installer `postfix` ainsi que `sasl2-bin` :
```
sudo apt update
sudo apt install postfix sasl2-bin
```

Pour l'installation de `postfix` il faut selectionner "Internet Site". Dans l'écran suivant j'ai entré `tzku.at` puisque j'utilise ce domaine pour la configuration.

Nous allons maintenant pouvoir modifier notre configuration `postfix` afin de prendre en compte SASL & TLS.

Voici ma configuration dans `/etc/postfix/main.cf` après modification :
```
smtpd_banner = $myhostname ESMTP $mail_name (Ubuntu)
biff = no

# appending .domain is the MUA's job.
append_dot_mydomain = no

# Uncomment the next line to generate "delayed mail" warnings
#delay_warning_time = 4h

readme_directory = no

# See http://www.postfix.org/COMPATIBILITY_README.html -- default to 3.6 on
# fresh installs.
compatibility_level = 3.6

# TLS parameters
smtpd_tls_cert_file=/etc/letsencrypt/live/smtp-test.tzku.at/fullchain.pem
smtpd_tls_key_file=/etc/letsencrypt/live/smtp-test.tzku.at/privkey.pem
smtpd_tls_security_level=may
smtpd_tls_auth_only = yes
smtpd_tls_mandatory_protocols = !SSLv2, !SSLv3, !TLSv1, !TLSv1.1, !TLSv1.2
smtpd_tls_protocols = !SSLv2, !SSLv3, !TLSv1, !TLSv1.1, !TLSv1.2
tls_preempt_cipherlist = no

smtp_tls_CApath=/etc/ssl/certs
smtp_tls_security_level=may
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache

# Postfix config
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
myhostname = smtp-test
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = /etc/mailname
mydestination = $myhostname, tzku.at, localhost.localdomain, localhost
relayhost =
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = all
inet_protocols = all

# SASL Config

cyrus_sasl_config_path = /etc/postfix/sasl
smtpd_sasl_local_domain = $myhostname
smtpd_sasl_security_options = noanonymous
```

Une grande partie des configurations ont été laissées par défaut. La configuration TLS est la plus restrictive possible, elle ne permet d'utiliser que TLSv1.3 ainsi que notre certificat généré un peu plus haut.


Une fois fait, il faut également éditer le fichier `/etc/postfix/master.cf` afin de décommenter les lignes suivantes sur la partie SMTPS seulement :
```
smtps     inet  n       -       y       -       -       smtpd
  -o syslog_name=postfix/smtps
  -o smtpd_tls_wrappermode=yes
  -o smtpd_sasl_auth_enable=yes
  -o smtpd_reject_unlisted_recipient=no
#  -o smtpd_client_restrictions=$mua_client_restrictions
#  -o smtpd_helo_restrictions=$mua_helo_restrictions
#  -o smtpd_sender_restrictions=$mua_sender_restrictions
  -o smtpd_recipient_restrictions=
  -o smtpd_relay_restrictions=permit_sasl_authenticated,reject
  -o milter_macro_daemon_name=ORIGINATING
```

Une fois fait nous pouvons continuer la configuration de SASL en créant le fichier `/etc/postfix/sasl/smtpd.conf` avec la configuration suivante :
```
vi /etc/postfix/sasl/smtpd.conf
pwcheck_method: auxprop
auxprop_plugin: sasldb
mech_list: CRAM-MD5 DIGEST-MD5 LOGIN PLAIN
```

Ce fichier permet à `postfix` d'utiliser `sasldb` ainsi que les différentes méthodes d'authentification pour SASL.

Il faut en suite copier le fichier de configuration `/etc/default/saslauthd` vers `/etc/default/saslauthd-postfix` :
* `cp /etc/default/saslauthd /etc/default/saslauthd-postfix`

Et appliquer la configuration suivante :
```
vi /etc/default/saslauthd
START=yes
DESC="SASL Auth. Daemon for Postfix"
NAME="saslauthd-postf"
# Option -m sets working dir for saslauthd (contains socket)
MECHANISMS="sasldb"
OPTIONS="-c -m /var/spool/postfix/var/run/saslauthd"
```

Il est important d'ajouter le `sasldb` dans `MECHANISMS`, cela permet d'utiliser la base de données de `sasl` pour gérer les connexions. La dernière ligne `OPTIONS` est à remplacer car dans notre installation `postfix` tourne dans un jail chroot.

Il faut en suite créer ce dossier dans le dossier `chroot` de `postfix` : 
* `dpkg-statoverride --add root sasl 710 /var/spool/postfix/var/run/saslauthd`

Et ajouter l'utilisateur `postfix` dans le groupe `sasl` :
* `adduser postfix sasl`

Ensuite il faut modifier le script de démarrage de `postfix` pour qu'il prenne en compte `sasl` dans son environnement `chroot` :
```
vi /usr/lib/postfix/configure-instance.sh
FILES="etc/localtime etc/services etc/resolv.conf etc/hosts \
            etc/host.conf etc/nsswitch.conf etc/nss_mdns.config etc/sasldb2"
```

Après tout ça, la configuration de `postfix` et `SASL` est terminée ! Il ne reste qu'à relancer/activer les services :
```
systemctl enable saslauthd
systemctl enable postfix
systemctl restart saslauthd
systemctl restart postfix
```

Nous avons donc postfix installé avec SASL pour l'authentification et TLS pour le chiffrement. Le port 465 doit normalement être exposé :
```
root@scw-upbeat-blackwell:~# ss -lntup | grep 465
tcp   LISTEN 0      100              0.0.0.0:465       0.0.0.0:*    users:(("master",pid=6523,fd=18))        
tcp   LISTEN 0      100                 [::]:465          [::]:*    users:(("master",pid=6523,fd=19))        
root@scw-upbeat-blackwell:~# 
```

#### Gestion des utilisateurs SASL

Les utilisateurs sont stockés et gérés par la base de données de SASL.

Pour ajouter un utilisateur :
* `saslpasswd2 -c -u domain user`

Pour les lister :
* `sasldblistusers2`

Pour les supprimer :
* `saslpasswd2 -d user@domain`

Pour changer un mot de passe :
* `saslpasswd2 user@domain`


Nous allons maintenant pouvoir passer à DKIM.

### Configuration de DKIM

Pour DKIM, je me suis fortement inspiré de la documentation suivante (merci Ikoula!) :
* https://fr-wiki.ikoula.com/fr/Installer_DKIM_sur_Postfix_sous_Debian

Le but ici va être d'installer et de configurer OpenDKIM de sorte à générer notre bi-clé afin de signer les mails sortants.

Il faut commencer par installer OpenDKIM :
```
sudo apt install opendkim opendkim-tools
```

Une fois fait, voici la configuration que j'ai ajoutée dans `/etc/opendkim.conf` :
```
AutoRestart             yes
AutoRestartRate         10/1h
Syslog                  yes
SyslogSuccess           yes
LogWhy                  yes

Canonicalization        relaxed/simple
Mode                    sv
#SubDomains             no
OversignHeaders         From

UserID                  opendkim:opendkim
UMask                   002

Socket                  inet:8891@localhost

PidFile                 /var/run/opendkim/opendkim.pid
ExternalIgnoreList      refile:/etc/opendkim/TrustedHosts
InternalHosts           refile:/etc/opendkim/TrustedHosts
KeyTable                refile:/etc/opendkim/KeyTable
SigningTable            refile:/etc/opendkim/SigningTable
SignatureAlgorithm      rsa-sha256
TrustAnchorFile         /usr/share/dns/root.key
```

Tous les détails sont disponibles dans la documentation citée plus haut. Ces lignes permettent simplement de définir les fichiers qui vont être utilisés et les algorithmes cryptographiques.

Puis il faut rajouter la ligne `SOCKET="inet:8891@localhost"` au fichier `/etc/default/opendkim` :
```
vi /etc/default/opendkim

SOCKET="inet:8891@localhost"
```

Cette ligne permet de dire à OpenDKIM de faire écouter le service sur le port 8891 de `localhost`.

#### Configuration de Postfix & DKIM

Il faut également rajouter ces configurations dans la configuration de postfix `/etc/postfix/main.cf` :
```
vi /etc/postfix/main.cf

# DKIM Config
milter_protocol = 2
milter_default_action = accept
smtpd_milters = inet:localhost:8891
non_smtpd_milters = inet:localhost:8891
```

Il faut maintenant créer les dossiers pour l'arborescence de DKIM :
```
mkdir /etc/opendkim
mkdir /etc/opendkim/keys
```

Et créer le fichier des hôtes autorisés à signer des messages avec DKIM (vous pouvez rajouter vos réseaux ici) :
```
vi /etc/opendkim/TrustedHosts

127.0.0.1
localhost
*.tzku.at
```

Le fichier `/etc/opendkim/KeyTable` doit également être créé, il correspond aux différents domaines/enregistrements avec les clés correspondantes :
```
vi /etc/opendkim/KeyTable

mail._domainkey.tzku.at tzku.at:mail:/etc/opendkim/keys/tzku.at/mail.private
```

Dans cet exemple, le domaine tzku.at devra aller chercher sa clé dans `/etc/opendkim/keys/tzku.at/mail.private`.

Avant de de générer les clés, il faut créer un dernier fichier, qui fera le lien entre les adresses mails et l'enregistrement DNS à utiliser :
```
vi /etc/opendkim/SigningTable

*@tzku.at mail._domainkey.tzku.at
```

#### Génération des clés pour DKIM

Il faut tout d'abord créer le dossier permettant de stocker les clés, dans notre cas :
* `mkdir /etc/opendkim/keys/tzku.at`

Une fois fait, il faut se rendre dans le dossier et générer la clé :
```
cd /etc/opendkim/keys/tzku.at

opendkim-genkey -s mail -d tzku.at
chown opendkim:opendkim mail.private
```

Deux fichiers ont été générés :
* `mail.private` : correspond à la clé privée
* `mail.txt` : correspond à la clé publique

Il reste maintenant à relancer le service avant de créer l'enregistrement DNS :
```
systemctl enable opendkim
systemctl restart postfix
systemctl restart opendkim
```

Nous pouvons également vérifier qu'OpenDKIM tourne bien sur notre machine :
```
root@scw-upbeat-blackwell:/etc/opendkim/keys/tzku.at# ss -lntup | grep opendkim
tcp   LISTEN 0      4096           127.0.0.1:8891      0.0.0.0:*    users:(("opendkim",pid=5175,fd=3))       
```

OpenDKIM est maintenant configuré, il reste à configurer les 3 protocoles de sécurité au niveau du DNS en créant plusieurs enregistrements.

### Enregistrement DNS : DKIM & DMARC

Toutes les configurations sont le plus strictes possibles.

#### DKIM

Pour fonctionner, DKIM nécessite un enregistrement DNS. Cet enregistrement sert aux entités recevant les mails, elles vont récupérer la clé publique sur un enregistrement DNS TXT, cela permet de vérifier la signature du mail.

La clé publique est à récupérer dans le fichier `/etc/opendkim/keys/tzku.at/mail.txt` généré plus haut. Plus globalement, tout l'enregistrement est disponible dans le fichier.

Dans notre cas, l'enregistrement est le suivant :
```
mail._domainkey	IN	TXT	( "v=DKIM1; h=sha256; k=rsa; "
	  "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwWScUbxCDN0Gak6nJuYaAR+vwgUNqf9yUkHybhQVhSjiSoUiPczv1gcrKHh9p/2O4BEfy0bzmwo0Jmv01QNRpX2ARn0N4Va10S46d7N9CQVl9VstCOVrHz7htppZ0qjjlkeFoSMymDrATUW8UenWVSg09Ifoi+QcvEZfBw2zf5eM8bPFdcOkGKwehKJYq9ieE0ErPT+9oOdiJl"
	  "n8a8V9yGHwarqwapHI0tWm0u0+rzVC/Yg9DLTPIhVe8QFNZu/JHz0W1NyxM63vCdjOYbvBZQpEWrFHdFTmb0FSLGHsWUwZFZOotSyK9MZrfA9sWaqbnFLhaOEpZLdmQlTOAKoPLQIDAQAB" )  ;
```

Vous remarquez que la clé est découpée en deux parties car le serveur DNS dispose d'une limitation au niveau du nombre de caractères.

#### DMARC

On peut également en profiter pour mettre en place un enregistrement DMARC, ce dernier permet de définir la marche à suivre lorsque SPF/DKIM ne sont pas réunis en recevant des rapports en cas de problème.

Pour notre cas :
```
_dmarc IN TXT "v=DMARC1; p=reject; rua=mailto:dmarc@tzku.at; ruf=mailto:dmarc@tzku.at"
```

L'adresse `dmarc@tzku.at` dans mon cas est un simple alias, vous pouvez utiliser ce que vous voulez. Cela vous permettra de recevoir

Dans cet enregistrement nous voyons que :
* `p=reject` : Procédure en cas d'échec DKIM/DMARC, reject dans notre cas (le plus strict)
* `rua=mailto:dmarc@tzku.at` : Mail pour recevoir les rapports simplifiés
* `ruf=mailto:dmarc@tzku.at` : Mail pour recevoir les rapports détaillés

Il existe un grand nombre de configuration pour DMARC, dans mon cas j'ai été au plus simple.

#### SPF

Pour SPF, il existe plusieurs configurations possible. Dans mon cas, je souhaite simplement envoyer des mails depuis l'IP rattachée au domaine `smtp-test.tzku.at`, je rajoute donc la ligne suivante :
* `@ IN TXT    "v=spf1 a:smtp-test.tzku.at -all"`


### Tests et autres informations

#### Propagation DNS

Nous avons donc toutes les configurations en places ainsi que les enregistrements DNS.

Pour les vérifier vous pouvez utiliser la commande `dig` de cette façon :
```
## SPF

tzkuat@pop-os ~> dig tzku.at TXT | grep spf
tzku.at.		3600	IN	TXT	"v=spf1 a:smtp-test.tzku.at -all"

## DKIM

tzkuat@pop-os ~ [0|1]> dig mail._domainkey.tzku.at TXT 
[...]
;; ANSWER SECTION:
mail._domainkey.tzku.at. 3596	IN	TXT	"v=DKIM1; h=sha256; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwWScUbxCDN0Gak6nJuYaAR+vwgUNqf9yUkHybhQVhSjiSoUiPczv1gcrKHh9p/2O4BEfy0bzmwo0Jmv01QNRpX2ARn0N4Va10S46d7N9CQVl9VstCOVrHz7htppZ0qjjlkeFoSMymDrATUW8UenWVSg09Ifoi+QcvEZfBw2zf5eM8bPFdc" "OkGKwehKJYq9ieE0ErPT+9oOdiJln8a8V9yGHwarqwapHI0tWm0u0+rzVC/Yg9DLTPIhVe8QFNZu/JHz0W1NyxM63vCdjOYbvBZQpEWrFHdFTmb0FSLGHsWUwZFZOotSyK9MZrfA9sWaqbnFLhaOEpZLdmQlTOAKoPLQIDAQAB"

## DMARC
tzkuat@pop-os ~> dig _dmarc.tzku.at TXT
[...]
;; ANSWER SECTION:
_dmarc.tzku.at.		3583	IN	TXT	"v=DMARC1; p=reject; rua=mailto:dmarc@tzku.at; ruf=mailto:dmarc@tzku.at"
```
Il est aussi possible de vérifier la propagation avec un outil du type : https://dnschecker.org/

#### Envoi d'un mail

Le moment tant attendu... Après avoir créé un compte SASL avec la commande suivante :
* `saslpasswd2 -c -u tzku.at tzkuat`

Nous allons enfin pouvoir essayer d'envoyer un mail !

J'ai au préalable autorisé mon IP à envoyer des mails dans la configuration de Postfix.

Pour les tests, j'utilise directement `openssl` vu qu'un simple `telnet` ne fonctionne pas sur une connexion chiffrée.

La commande que j'utilise est la suivante : `openssl s_client -connect smtp-test.tzku.at:465`. Une fois connecté, j'utilise les commandes de mail classique :
```
tzkuat@pop-os ~/.ssh > openssl s_client -connect smtp-test.tzku.at:465
---
read R BLOCK
220 smtp-test.tzku.at ESMTP Postfix (Ubuntu)
ehlo tzku.at
250-smtp-test.tzku.at
250-PIPELINING
250-SIZE 10240000
250-VRFY
250-ETRN
250-AUTH CRAM-MD5 DIGEST-MD5 LOGIN PLAIN
250-ENHANCEDSTATUSCODES
250-8BITMIME
250-DSN
250-SMTPUTF8
250 CHUNKING
auth login 
334 VXNlcm5hbWU6
dHprdWF0QHR6a3UuYXQ=
334 UGFzc3dvcmQ6
xxxx
235 2.7.0 Authentication successful
mail from: tzkuat@tzku.at 
250 2.1.0 Ok
rcpt to: test-cf2brn6q2@srv1.mail-tester.com
250 2.1.5 Ok
data
354 End data with <CR><LF>.<CR><LF>
Subject: Ceci est un mail avec SPF, SKIM & DMARC 
Ceci est un mail 
. 
250 2.0.0 Ok: queued as 9D8EE43E9C
421 4.4.2 smtp-test.tzku.at Error: timeout exceeded
closed
```

Le mail en question a été envoyé a une adresse mail qui permet de tester sa configuration (et donc de valider les mécanismes mis en place). L'outil utilisé est celui-ci :
* https://www.mail-tester.com/

Vous pouvez également envoyer des mails à votre adresse personnelle et vérifier manuellement les résultats.

De mon côté, voilà ce que me renvoie mail-tester :
![](/images/postfix-secure/postfix-secure-1.png)

On peut voir que les points DKIM, DMARC & SPF sont validés !

Pour le reste j'ai utilisé la méthode `LOGIN` pour m'authentifier, c'est pour cela que le login est encodé en base 64.

Voici le résultat détaillé de mail-tester :
![](/images/postfix-secure/postfix-secure-2.png)

#### Fichiers de configuration

Je profite également pour vous déposer directement ici tous les fichiers de configuration que j'ai pu présenter dans l'article.
* [`/etc/postfix/main.cf`](/files/postfix-main.txt)
* [`/etc/postfix/master.cf`](/files/postfix-master.txt)
* [`/etc/default/saslauthd`](/files/postfix-saslauthd.txt)
* [`/etc/postfix/sasl/smtpd.conf`](/files/postfix-smtpd.txt)
* [`/etc/opendkim.conf`](/files/postfix-opendkim.txt)
* [`/etc/default/opendkim`](/files/postfix-opendkim-default.txt)

## Conclusion

Et voilà, vous avez tout maintenant pour configurer votre serveur SMTP. De mon côté je vais continuer de creuser pour voir comment on peut mutualiser le relais en segmentant les droits, chose que je n'ai pas réussi à faire jusqu'à maintenant.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat