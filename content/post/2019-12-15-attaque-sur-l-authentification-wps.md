---
title: "Attaque sur l'authentification WPS"
author: Fabio Pace
type: post
date: 2019-12-15T09:40:59+00:00
url: /security/attaque-sur-l-authentification-wps/
thumbnail: /images/WPS/wps-1.png
tags: [
    "wifi",
    "WPS"
]
categories: [
    "reseau",
    "securite"
]
---
![](/images/WPS/wps-1.png)

Bonjour à tous, aujourd'hui nous allons voir comment attaquer l'authentification WPS qui est généralement activé sur les box opérateur et bornes WIFI.

Avant de commencer, j'insiste sur le fait que cet article a pour **but de sensibiliser sur l'authentification WPS** et **non à inciter à utiliser la faiblesse de la technologie sur des réseaux où vous n'avez pas l'autorisation.**

## Presentation

Tout d’abord, le **WPS** (**W**i-Fi **P**rotected **S**etup) est une technologie qui simplifie le processus de connexion d’un équipement Wi-Fi au réseau. En effet, il faut l’intervention physique de la personne pour qu’elle s’authentifie.

Au départ, pour obtenir la certification WPS, il fallait appairer les équipements entre eux en saisissant manuellement un code PIN composé de 8 chiffres.

Aujourd’hui, l’appairage se fait plus automatiquement, il suffit de presser le bouton WPS du routeur et le client s’authentifie. La méthode utilisée se nomme **PBC** : « **P**ush **B**utton **C**onnect ».

Néanmoins, cette méthode d’authentification n’est pas du tout sûre. Il est très facile de compromettre la sécurité sur ce type d’authentification. Nous allons le voir tout de suite.

## L'attaque

Le code PIN étant composé de 8 chiffres, cela fait donc 10<sup>8</sup> soit 100 000 000 possibilités. Il faut savoir que le 8ème chiffre est une somme de contrôle qui peut être facilement calculé : on obtient donc 10<sup>7</sup> soit 10 000 000 possibilités.

De plus, le grand problème du WPS est que l’équipement vérifie le code en deux parties : il audite les quatre premiers chiffres, puis les quatre derniers. On a donc 10<sup>4</sup> possibilités pour la première partie et 10<sup>4</sup> pour la dernière partie, soit 10 000 et 10 000. Pour être plus précis, si on prend en compte les deux problèmes, on a 10<sup>4</sup> possibilités pour la première partie, et 10<sup>3</sup> possibilités pour la dernière partie, puisqu’il y a seulement 7 chiffres significatifs, soit 10 000 et 1 000 possibilités. D’après des recherches, il est possible de tester 46 codes en moyenne par minute, soit environ 239 minutes ou 4 heures.

Nous utilisons l'outil Bully. C'est un outil écrit en C et spécialement créé pour l'attaque par bruteforce pour le WPS. 

![](/images/WPS/image.png)>

Nous devons donc choisir le SSID WIFI avec l'adresse MAC de l'équipement (BSSID) : 


```
bully wlan0mon -b XX:XX:XX:XX:XX:XX -e SSID_WIFI
```


NB : _Il faut bien évidement une carte WIFI en mode « monitoring » pour effectuer cette action._

Au bout de quelques heures, vous aurez donc le code PIN pour appairer un équipement via le WPS. 

Il est donc primordial de désactiver l’authentification via WPS, ou de limiter le nombre de tentatives erronées (en bloquant l’accès) pour augmenter le temps de réalisation de l’attaque. Il serait judicieux de coupler le système de blocage à un système de supervision, afin d’être alerté de la tentative d’attaque. Il est conseillé de mettre en place un IDS (Intrusion Detection System) pour détecter le bruteforce réalisé.

J’espère que l’article vous aura plu. N’hésitez pas à commenter si vous avez la moindre remarque.

A bientôt, 

Fabio Pace.