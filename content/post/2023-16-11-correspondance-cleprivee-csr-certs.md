---
title: "OpenSSL : Vérifier la correspondance entre clé privée, certificat & CSR"
author: Mickael Rigonnaux
type: post
date: 2023-11-16T14:00:44+00:00
url: /securite/openssl-cert-csr-key
thumbnail: /images/ssl-png.png
shareImage: /images/ssl-png.png
featured: true
toc: true
tags: [
    "openssl",
    "cryptographie",
    "certificat",
    "csr"
]
categories: [
    "securite"
]

---

Bonjour à tous ! Aujourd'hui un rapide article sur OpenSSL qui va vous expliquer comment retrouver la correspondance entre les certificats, les clés privées et les CSR.

## Introduction

Nous avons tous connu des erreurs du style : 
* `Certificate and private key net-security.test:443:0 from /etc/cert/domain_name.crt and /etc/cert/domain_name.key do not match`

C'est une erreur très courante et très explicite : le certificat en question ne correspond pas à la clé privée utilisée.

## Clé privée ? Certificat ? CSR ?

Tous les éléments ont déjà été abordés dans plusieurs articles :
* https://net-security.fr/securite/creer-javakeystore/
* https://net-security.fr/security/creer-une-csr-avec-des-san-sur-openssl/
* https://net-security.fr/security/openssl-formats-cheat-sheet/
* https://net-security.fr/security/openssl-ecc/

Les certificats et les clés privées, qu'ils utilisent le système RSA ou ECC sont tous basés sur des mathématiques. Il est donc logique/facile de retrouver le lien entre tous les éléments.

Lors de la génération d'un certificat, les étapes sont toujours les mêmes (sauf auto-signé) :
1. Génération de la clé privée
2. Génération de la CSR (certificate signing request) à partir de la clé privée
3. Signature de notre CSR par l'autorité voulue (publique ou interne, peu importe)

Ces 3 fichiers sont donc liés entre eux. Pour RSA, le point commun va être le modulus qui sera commun à tous les fichiers, pour les courbes elliptiques, nous pourrons extraire une clé publique. Pour chaque fichier, les modulus ou les clés publiques doivent être identiques.

Si vous ne trouvez pas le même résultat entre votre certificat, votre clé privée ou votre CSR, cela ne peut pas fonctionner.

## Vérification & commandes

Pour cet article je vais utiliser OpenSSL 3.0.11 sur une VM Debian 11.

### RSA

Pour vérifier tout ça, j'ai d'abord généré une clé privée, une CSR & un certificat auto-signé :
```
tzkuat@tzkuat-pc:~/test-cert/rsa$ openssl genrsa -out test-netsec-rsa.key 2048
tzkuat@tzkuat-pc:~/test-cert/rsa$ openssl req -new -sha256 -key test-netsec-rsa.key -out test-netsec-rsa.csr
[...]
tzkuat@tzkuat-pc:~/test-cert/rsa$ openssl x509 -req -days 365 -in test-netsec-rsa.csr -signkey test-netsec-rsa.key -sha256 -out test-netsec-rsa.crt
Certificate request self-signature ok
```

Une fois fait, nous pouvons vérifier le lien entre les différents fichiers (le "modulus" dans ce cas précis). Cela se matérialise par une chaîne de caractères unique.

#### Les commandes

Pour extraire/vérifier le modulus sur une clé privée :
* `openssl rsa -noout -modulus -in votre-cle.key`

Pour extraire/vérifier le modulus sur une CSR :
* `openssl req -noout -modulus -in votre-csr.csr`

Pour extraire/vérifier le modulus sur un certificat :
* `openssl x509 -noout -modulus -in votre-cert.crt`

Avec mes fichiers, cela donne :
```
tzkuat@tzkuat-pc:~/test-cert/rsa$ openssl rsa -noout -modulus -in test-netsec-rsa.key
Modulus=A48B33CC7F51E644022E9315E1C444A234F023E6894586CB6AB9AE0386D2B4CDBDE99CF197487198D177D2A0C147ADEA8DE9DE09E8CE35D8D62D1D4F8D79E8F99ABA2FB928EF9ABEEB6069050EF2F336271FEF11ED86D55C22AD3C5221A05E2494F54EDE84CE3BF8C6C8AA4DFA7E566937CF82FA7DB97BF8EFCA0668529E1EAD3B916ECAC5DDC666C48513AC541A981C84244F06C8C3D48736D6C66FA91E894D767A0F6B73ADA64D37C862E0EC3343C6A1934D49BACBA0EA4EABF20EE7D364A3705738A1B8383C5F884413617C5E74C3B6615C66A8914DD4221B3E43BAE8FB81E3C500B0D38E6F5C88815B60F1C08B8C2523A023F1AC97BBCB6615E0C33DBD59

tzkuat@tzkuat-pc:~/test-cert/rsa$ openssl req -noout -modulus -in test-netsec-rsa.csr
Modulus=A48B33CC7F51E644022E9315E1C444A234F023E6894586CB6AB9AE0386D2B4CDBDE99CF197487198D177D2A0C147ADEA8DE9DE09E8CE35D8D62D1D4F8D79E8F99ABA2FB928EF9ABEEB6069050EF2F336271FEF11ED86D55C22AD3C5221A05E2494F54EDE84CE3BF8C6C8AA4DFA7E566937CF82FA7DB97BF8EFCA0668529E1EAD3B916ECAC5DDC666C48513AC541A981C84244F06C8C3D48736D6C66FA91E894D767A0F6B73ADA64D37C862E0EC3343C6A1934D49BACBA0EA4EABF20EE7D364A3705738A1B8383C5F884413617C5E74C3B6615C66A8914DD4221B3E43BAE8FB81E3C500B0D38E6F5C88815B60F1C08B8C2523A023F1AC97BBCB6615E0C33DBD59

tzkuat@tzkuat-pc:~/test-cert/rsa$ openssl x509 -noout -modulus -in test-netsec-rsa.crt
Modulus=A48B33CC7F51E644022E9315E1C444A234F023E6894586CB6AB9AE0386D2B4CDBDE99CF197487198D177D2A0C147ADEA8DE9DE09E8CE35D8D62D1D4F8D79E8F99ABA2FB928EF9ABEEB6069050EF2F336271FEF11ED86D55C22AD3C5221A05E2494F54EDE84CE3BF8C6C8AA4DFA7E566937CF82FA7DB97BF8EFCA0668529E1EAD3B916ECAC5DDC666C48513AC541A981C84244F06C8C3D48736D6C66FA91E894D767A0F6B73ADA64D37C862E0EC3343C6A1934D49BACBA0EA4EABF20EE7D364A3705738A1B8383C5F884413617C5E74C3B6615C66A8914DD4221B3E43BAE8FB81E3C500B0D38E6F5C88815B60F1C08B8C2523A023F1AC97BBCB6615E0C33DBD59
```

On retrouve bien la même chaîne de caractères pour les 3 fichiers. Si ce n'était pas le cas, il y aurait un problème, le certificat ne correspondrait pas à la clé ou inversement.

Vous pouvez coupler les commandes avec la commande `sha256sum` pour que les résultats soient plus lisibles :
```
openssl x509 -noout -modulus -in votre-cert.crt | openssl sha256sum
openssl req -noout -modulus -in votre-csr.csr | openssl sha256sum
openssl rsa -noout -modulus -in votre-cle.key | openssl sha256sum
```

Un exemple avec `sha256sum` : 
```
tzkuat@tzkuat-pc:~/test-cert/rsa$ openssl x509 -noout -modulus -in test-netsec-rsa.crt | sha256sum
148e7e9af166a36fbea12f76bb491ea8299dc445c79b0e19f446d84974a574ee  -

tzkuat@tzkuat-pc:~/test-cert/rsa$ openssl req -noout -modulus -in test-netsec-rsa.csr | sha256sum
148e7e9af166a36fbea12f76bb491ea8299dc445c79b0e19f446d84974a574ee  -

tzkuat@tzkuat-pc:~/test-cert/rsa$ openssl rsa -noout -modulus -in test-netsec-rsa.key | sha256sum
148e7e9af166a36fbea12f76bb491ea8299dc445c79b0e19f446d84974a574ee  -
```

On retrouve bien le même condensat pour tous les fichiers, ce qui prouve qu'il n'y a pas de problème entre les fichiers.

### ECC

J'ai aussi généré clé privée, CSR & certificat avec ECC :
```
tzkuat@tzkuat-pc:~/test-cert/ecc$ openssl ecparam -genkey -name prime256v1 -out test-net-sec-ecc.key
tzkuat@tzkuat-pc:~/test-cert/ecc$ openssl req -new -sha256 -key test-net-sec-ecc.key -nodes -out test-net-sec-ecc.csr
[...]
tzkuat@tzkuat-pc:~/test-cert/ecc$ openssl req -x509 -sha256 -days 365 -key test-net-sec-ecc.key -in test-net-sec-ecc.csr -out test-net-sec-ecc.crt
```

Une fois fait, nous pouvons vérifier le lien entre les différents fichiers (la clé publique dans ce cas précis). Cela se matérialise par une chaîne de caractères unique.

#### Les commandes

Pour extraire/vérifier le modulus sur une clé privée :
* `openssl pkey -pubout -in test-net-sec-ecc.key`

Pour extraire/vérifier le modulus sur une CSR :
* `openssl req -in votre-csr.csr -pubkey -noout`

Pour extraire/vérifier le modulus sur un certificat :
* `openssl x509 -in votre-cert.crt -pubkey -noout`

Avec mes fichiers, cela donne :
```
tzkuat@tzkuat-pc:~/test-cert/ecc$ openssl pkey -pubout -in test-net-sec-ecc.key
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAECImBg+44s35Iw8v4FdC8w+dBgXtd
UrSfqlYWbflbG4k7S4S8zLg18dXhTY5n+qkvRrPjLk7lFKhgWYqDy9u/9Q==
-----END PUBLIC KEY-----

tzkuat@tzkuat-pc:~/test-cert/ecc$ openssl req -in test-net-sec-ecc.csr -pubkey -noout
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAECImBg+44s35Iw8v4FdC8w+dBgXtd
UrSfqlYWbflbG4k7S4S8zLg18dXhTY5n+qkvRrPjLk7lFKhgWYqDy9u/9Q==
-----END PUBLIC KEY-----

tzkuat@tzkuat-pc:~/test-cert/ecc$ openssl x509 -in test-net-sec-ecc.crt -pubkey -noout
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAECImBg+44s35Iw8v4FdC8w+dBgXtd
UrSfqlYWbflbG4k7S4S8zLg18dXhTY5n+qkvRrPjLk7lFKhgWYqDy9u/9Q==
-----END PUBLIC KEY-----
```

On retrouve bien la même clé publique pour les 3 fichiers. Si ce n'était pas le cas, il y aurait un problème, le certificat ne correspondrait pas à la clé ou inversement.

Comme pour RSA, vous pouvez coupler ça avec `sha256sum` :

```
openssl pkey -pubout -in test-net-sec-ecc.key | sha256sum
openssl req -in votre-csr.csr -pubkey -noout | sha256sum
openssl x509 -in votre-cert.crt -pubkey -noout | sha256sum

tzkuat@tzkuat-pc:~/test-cert/ecc$ openssl pkey -pubout -in test-net-sec-ecc.key | sha256sum
b126b539083ce034dc9cfca6f6b39a5c60db728e260ec61c8570d9652dc05fe4  -

tzkuat@tzkuat-pc:~/test-cert/ecc$ openssl req -in test-net-sec-ecc.csr -pubkey -noout | sha256sum
b126b539083ce034dc9cfca6f6b39a5c60db728e260ec61c8570d9652dc05fe4  -

tzkuat@tzkuat-pc:~/test-cert/ecc$ openssl x509 -in test-net-sec-ecc.crt -pubkey -noout | sha256sum
b126b539083ce034dc9cfca6f6b39a5c60db728e260ec61c8570d9652dc05fe4  -
```

On retrouve bien le même condensat pour tous les fichiers, ce qui prouve qu'il n'y a pas de problème entre les fichiers.

## Conclusion

Et voilà, nous avons maintenant vu comment vérifier les informations qui permettent de valider l'appartenance d'un certificat à une clé privée, que ça soit en ECC ou en RSA.

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat