---
title: "Mémoire de fin d'études : Cryptographie & Monétique"
author: Mickael Rigonnaux
type: post
date: 2020-10-05T15:49:15+00:00
url: /security/memoire-de-fin-detudes-cryptographie-monetique/
thumbnail: /images/clipart1104832-removebg-preview.png
featured: true
tags: [
    "cryptographie"
]
categories: [
    "securite"
]

---
Bonjour à tous ! Aujourd'hui (encore) un article différent car je vais vous parler rapidement de mon mémoire de fin d'études.

Je viens d'arriver au bout de mes études et pour conclure j'ai dû, comme tout le monde, rédiger un mémoire. De mon côté j'ai pu réaliser ce diplôme en alternance à Aix-en-Provence au sein de la société [Monext][1] et du [campus Ynov][2].

Les sujets abordés dans ce mémoire sont la cryptographie et la monétique, et plus précisément comment ils fonctionnent ensemble. Ces sujets sont revenus très régulièrement lors de mon alternance et de ma scolarité. Il me tenait donc à cœur de partager mes travaux. 

Les documents sont mis à disposition sous licence libre [CC BY-SA 4.0][3] et sont disponibles dans grâce aux liens suivants :

  * [Mémoire PDF][4]
  * [Annexes PDF][5]
  * [Mémoire ODT][6]
  * [Annexes ODT][7]

Je tiens à préciser que l'ensemble du document a été rédigé grâce au logiciel [LibreOffice][8].

Pour faire un rapide aperçu du contenu, une introduction à la cryptographie ainsi qu'à la monétique sont réalisées. L'application de la cryptographie dans la monétique est en suite expliquée avec les éventuelles limites. En dernière partie du document les évolutions projetées sont abordées avec une nouvelle fois les différents limites.

Je pense que ce document est très intéressant pour comprendre un peu plus techniquement comment fonctionne un paiement par carte. Il peut être une très bonne introduction à la cryptographie, j'ai essayé au maximum de vulgariser pour rendre le document accessible à tous.

Pour terminer cet article je tenais à remercier la société [Monext][1] d'avoir rendu ces travaux possibles ainsi que le campus [Ynov d'Aix-en-Provence][2].

N'hésitez pas à revenir vers moi si vous avez des questions sur ce document 

Mickael Rigonnaux @tzkuat

 [1]: https://www.monext.fr/
 [2]: https://www.ynov-aix.com/
 [3]: https://creativecommons.org/licenses/by-sa/4.0/
 [4]: https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-M%c3%a9moire.pdf
 [5]: https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-Annexes.pdf
 [6]: https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-M%c3%a9moire.odt
 [7]: https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-Annexes.odt
 [8]: https://fr.libreoffice.org/