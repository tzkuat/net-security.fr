
# List of packages to check
packages=("git" "hugo")

# Loop through the list of packages
for package in "${packages[@]}"; do
    # Check if the package is installed
    dpkg -l | grep -qw $package
    if [ $? -eq 0 ]; then
        echo "The package $package is installed."
    else
        echo "The package $package is NOT installed."
    fi
done

## Variables

### Site name
SITE_NAME="NET-SECURITY"
### Deployment directory
SITE_DIRECTORY="/var/www/net-security/"
### URL of gitlab source
GITLAB_URL="https://gitlab.com/tzkuat/net-security.fr"
### URL of gitlab theme
THEME_URL="https://github.com/chipzoller/hugo-clarity"
### Path to work hugo directory
DIR="/data/net-security.fr/"
### Path to theme dir
THEME_DIR="/data/net-security.fr/themes"


## Deploy hugo new site


if [ -d "$DIR" ]; then
   echo "'$DIR' exist"
   git -C $DIR pull
else
   echo "'$DIR' not exist"
   git -C /data/ clone $GITLAB_URL
fi

### Pull last theme


if [ -d "$THEME_DIR" ]; then
   echo "'$THEME_DIR' exist"
   git -C $THEME_DIR pull
else
   echo "'$THEME_DIR' not exist"
   mkdir $THEME_DIR
   echo $THEME_URL
   git -C $THEME_DIR clone $THEME_URL
fi

### Backup lastversion of website

cp -r $SITE_DIRECTORY /tmp/$SITE_NAME-$(date -d "today" +"%Y%m%d%H%M")/

### Build website new version

cd $DIR

rm -rf $SITE_DIRECTORY/*

hugo --noBuildLock -d $SITE_DIRECTORY --noTimes
