---
title: "OSINT dans le but de préparer un audit, première partie : les mails"
author: Mickael Rigonnaux
type: post
date: 2023-02-20T10:05:44+00:00
url: /securite/osint-mail
thumbnail: /images/osint-mail/osint-mail-logo.png
featureImage: images/osint-mail/osint-mail-logo.png
shareImage: /images/osint-mail/osint-mail-logo.png
featured: true
toc: true
draft: true
tags: [
    "osint",
    "mail",
    "googledork",
    "dns"
]
categories: [
    "securite"
]

---

Bonjour à tous ! Aujourd'hui un article qui va parler d'un sujet très en vogue en ce moment : l'OSINT. Cet article est le 1er d'une série d'article qui permettra de récupérer des informations disponibles publiquement dans le but de préparer un audit de sécurité (ou pas?) pour une société/entité X.

## Diclaimer

Cet article est proposé à titre pédagogique/professionnel uniquement. Je ne pourrais en aucun cas être tenu responsable des mauvaises utilisations des différents logiciels.

Dans tous les cas, ne débutez une campagne de phishing qu'avec l'accord de votre direction ou de votre responsable.

De plus, il n'explique pas comment préparer au mieux son "opsec" et protéger au mieux son identité lors d'une investigation.

## OSINT ?

> OSINT (Open Source Intelligence) ou ROSO (Renseignement d’origine sources ouvertes) en français, est un ensemble de disciplines visant à collecter et analyser des informations extraites de sources librement accessibles (sites Web, comptes sur les réseaux ou médias sociaux, imageries satellitaires, journaux papier, etc.). 

> Plus qu’un assemblage d’outils et de méthodes, il est d’usage de parler d’un « état d’esprit », tourné vers l’investigation, fait de rigueur et d’éthique.

* [Source : osintfr.com](https://osintfr.com/fr/comment-commencer-faq/)

Cette discipline s'applique très bien pour notre cas : préparer un audit/pentest. C'est une étape incournable si vous faite de la sécurité informatique.

Je vous invite d'ailleurs à rejoindre la communauté OSINT-FR si cela vous intéresse. J'y suis très peu actif ces derniers temps, mais elle compte parmis ces membres des gens très compétents et permet d'en apprendre tous les jours sur tous les sujets liés à l'OSINT.

Lien : https://osintfr.com

## Récupérer des informations sur les mails

Les adresses mails et plus largement les mails sont aujourd'hui des éléments essentiels pour un système d'information. Ils permettent de faire un grand nombre de chose : des connexions, échanger sur des projets, planifier... Tout est lié à une adresse mail (ou presque). C'est pour cela qu'ils sont la cible préférée des attaquants.

Nous allons voir aujourd'hui comment récupérer simplement des informations comme :
* Les solutions de mails utilisées
* Le format des mails
* Comment récupérer une liste d'adresses mails d'une société
* Que faire avec (pour aller plus loin)

### Les domaines

Tout d'abord il faut savoir quel domaine est utilisé pour envoyer des mails. Il n'est pas rare qu'une société utilise un domaine différent de son site commercial pour envoyer ses mails.

Pour cela, il suffit de trouver le site commercial/vitrine et de chercher un mail de contact. Cela nous donnera la 1ère information afin de commencer les recherches.

Par exemple, si nous prenons la société Sopra Steria, nous retrouvons assez facilement l'information dans les conditions générales :
* https://www.soprasteria.fr/bas-de-page/conditions-generales

![](/images/osint-mail/osint-mail-1.png)

Il est aussi possible de récupérer l'information via une Google Dork, par exemple :
* `intext:mail sopra steria`

Le 1er résultat est le bon pour cette requête.

Récupérer le domaine peut permettre également de se procurer un domaine au plus proche du domaine cible.

### Récupérer les informations de l'infrastructure de mail

Maintenant que nous avons le domaine, nous allons essayer de trouver quel est la solution de mail utilisée. Google Workspace, Office 365 ? Cela pourrait nous aider par exemple si l'on voulait faire du phishing ciblé ([cf article phishing](https://net-security.fr/securite/rex-phishing/))

Globalement les mails et les différents protocoles de sécurité rattachés ont tous un point commun : ils utilisent des enregistrements DNS pour fonctionner.

Nous allons donc pouvoir utiliser de simples requêtes DNS pour trouver ces informations.

Personnellement, j'utilise la commande `dig`.

**Attention, ces tests permettent de récupérer des informations pour un domaine spécifique et non pour tous les sous domaines. Par exemple mail.net-security.fr peut avoir une configuration complètement différente de net-security.fr.**

La 1ère chose à faire est de récupérer les enregistrements MX (Mail eXcganger), qui permettent de désigner le ou les serveurs de mail.

Dans plusieurs cas, cela permet d'identifier directement le fournisseur (Office 365 presque tout le temps) :
```
tzkuat@pop-os ~> dig soprasteria.com MX

; <<>> DiG 9.18.1-1ubuntu1.2-Ubuntu <<>> soprasteria.com MX
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8443
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;soprasteria.com.		IN	MX

;; ANSWER SECTION:
soprasteria.com.	3572	IN	MX	5 soprasteria-com.mail.protection.outlook.com.
...

tzkuat@pop-os ~> dig qwant.com MX

; <<>> DiG 9.18.1-1ubuntu1.2-Ubuntu <<>> qwant.com MX
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27882
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;qwant.com.			IN	MX

;; ANSWER SECTION:
qwant.com.		5335	IN	MX	0 qwant-com.mail.protection.outlook.com.

;; Query time: 0 msec
;; SERVER: 127.0.0.53#53(127.0.0.53) (UDP)
;; WHEN: Mon Feb 20 21:52:24 CET 2023
;; MSG SIZE  rcvd: 88
...
```

Pour les deux tests `qwant.com` et `soprasteria.com` on trouve facilement l'information voulue :
* Qwant : `qwant.com. 5335 IN	MX	0 qwant-com.mail.protection.outlook.com.`
* Sopra : `soprasteria.com.	3572 IN	MX	5 soprasteria-com.mail.protection.outlook.com.`

On voit bien que ces deux domaines sont liés à Office 365.

Ces enregistrements changent selon les fournisseurs, voici le résultat avec mon domaine personnel (utilisant Tutanota) :

```
;; ANSWER SECTION:
rm-it.fr.		3600	IN	MX	100 mail.tutanota.de.
```

Et un autre exemple pour un domaine utilisant Google Workspace :
```
;; ANSWER SECTION:
example.com.		3600	IN	MX	1 aspmx.l.google.com.
example.com.	3600	IN	MX	5 alt2.aspmx.l.google.com.
example.com.	3600	IN	MX	10 alt3.aspmx.l.google.com.
example.com.	3600	IN	MX	10 alt4.aspmx.l.google.com.
example.com.	3600	IN	MX	5 alt1.aspmx.l.google.com.
```

Toujours avec cette même commande, il est possible de récupérer les enregistrements SPF (qui sont de type TXT), ils permettent de spécifier qui peut envoyer des mails en utilisant ce domaine.

Par exemple :
```
tzkuat@pop-os ~> dig qwant.com TXT | grep spf
qwant.com.		83674	IN	TXT	"v=spf1 ip4:194.187.168.35 ip4:194.187.168.37 ip4:194.187.168.38 ip4:94.23.181.132 ip4:52.28.158.135 ip4:52.29.12.146 include:spf.protection.outlook.com -all"
tzkuat@pop-os ~> dig google.com TXT | grep spf
google.com.		3502	IN	TXT	"v=spf1 include:_spf.google.com ~all"
tzkuat@pop-os ~> dig net-security.fr TXT | grep spf
net-security.fr.	3364	IN	TXT	"v=spf1 include:mx.ovh.com ~all"
tzkuat@pop-os ~> 
```

On peut voir que pour le domaine `qwant.com` plusieurs IP sont autorisées à envoyer des mails. Pour les enregistrement liés à Office 365 et Google, ils utilisent des enregisrements qui pointent vers des listes de noms et d'IP.

```
tzkuat@pop-os ~> dig spf.protection.outlook.com TXT | grep spf
; <<>> DiG 9.18.1-1ubuntu1.2-Ubuntu <<>> spf.protection.outlook.com TXT
;spf.protection.outlook.com.	IN	TXT
spf.protection.outlook.com. 513	IN	TXT	"v=spf1 ip4:40.92.0.0/15 ip4:40.107.0.0/16 ip4:52.100.0.0/14 ip4:104.47.0.0/17 ip6:2a01:111:f400::/48 ip6:2a01:111:f403::/49 ip6:2a01:111:f403:8000::/50 ip6:2a01:111:f403:c000::/51 ip6:2a01:111:f403:f000::/52 include:spfd.protection.outlook.com -all"
tzkuat@pop-os ~> dig _spf.google.com TXT | grep spf
; <<>> DiG 9.18.1-1ubuntu1.2-Ubuntu <<>> _spf.google.com TXT
;_spf.google.com.		IN	TXT
_spf.google.com.	300	IN	TXT	"v=spf1 include:_netblocks.google.com include:_netblocks2.google.com include:_netblocks3.google.com ~all"
```

Cela permet de simplifier les enregistrements.

En deux commandes nous avons pu récupérer des informations importantes comme : qui peut envoyer des mails pour un domaine et ou sont envoyés ces mails.

Toujours sur la même idée, il est possible d

## Conclusion


J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !

Mickael Rigonnaux @tzkuat