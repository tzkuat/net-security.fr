---
title: Commandes GNU/Linux en vrac – Partie 4
author: Mickael Rigonnaux
type: post
date: 2022-06-13T20:05:44+00:00
url: /system/commandes-gnu-linux-en-vrac-partie-4/
thumbnail: /images/logo-3.png
featureImage: images/logo-3.png
shareImage: /images/logo-3.png
featured: true
tags: [
    "logiciellibre",
    "GNU/Linux"
]
categories: [
    "systeme",
    "logiciellibre"
]

---

Bonjour à tous ! Aujourd’hui le 3ème article de la série « Commandes GNU/Linux en vrac ». Les autres sont disponibles ici :

  * 1er : https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-1
  * 2ème : https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-2
  * 3ème : https://net-security.fr/system/commandes-gnu-linux-en-vrac-partie-3

Le but est de présenter et de vous faire découvrir des commandes et des outils qui pourraient être utiles pour vous.

## Commande time

La commande `time` va permettre d'évaluer la durée d'exécution d'un script ou d'une commande. Très intéressant pour voir les performances de votre dernier script par exemple.

La forme est la suivante :
* `time options commande`

Prenons un exemple avec la commande `ls` :
* `time ls`

Le retour donne :
```
$ time ls
real    0m0.005s
user    0m0.001s
sys     0m0.004s
```

* `real` : indique le temps réel que la commande a demandé pour s'exécuter
* `user` : indique le temps de l'utilisateur (CPU utilisé par le programme)
* `sys` : indique le temps système (temps utilisé par le système pour la tâche)

Pour utiliser un format POSIX plus lisible :
```
tzkuat@tzkuat-pc:~$ time -p sudo apt update
[sudo] password for tzkuat:
Hit:1 http://archive.ubuntu.com/ubuntu jammy InRelease
Get:2 http://security.ubuntu.com/ubuntu jammy-security InRelease [110 kB]
[...]
Get:40 http://archive.ubuntu.com/ubuntu jammy-backports/multiverse amd64 c-n-f Metadata [116 B]
Fetched 21.9 MB in 34s (643 kB/s)
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
51 packages can be upgraded. Run 'apt list --upgradable' to see them.

real 45.50
user 0.06
sys 0.15
```
La commande a donc pris 45 secondes pour s'exécuter.

Comme pour les autres commandes, il est possible d'utiliser des options, elles sont toutes disponibles avec la commande `man time`.


## Commande timeout

La commande `timeout`, sert, comme son nom l'indique à rajouter un `timeout` à une commande ou à un script.

La forme est la suivante :
* `timeout options durée commande`

Prenons l'exemple d'une commande `nmap`, elles peuvent durer jusqu'à plusieurs heures/jours. Si vous voulez qu'elle s'arrête après 2h si la commande n'est pas terminée, il suffit de lancer la commande suivante :
* `timeout 2h nmap <cible>`

Pour les durées vous pouvez utiliser :
* `s` : (qui est la valeur par défaut) pour les secondes
* `m` : pour les minutes
* `h` : pour les heurs
* `d` : pour les jours

Exemple avec un simple ping :
```
tzkuat@tzkuat-pc:~$ timeout 5 ping archive.org
PING archive.org (207.241.224.2) 56(84) bytes of data.
64 bytes from www.archive.org (207.241.224.2): icmp_seq=1 ttl=47 time=139 ms
64 bytes from www.archive.org (207.241.224.2): icmp_seq=2 ttl=47 time=139 ms
64 bytes from www.archive.org (207.241.224.2): icmp_seq=3 ttl=47 time=139 ms
64 bytes from www.archive.org (207.241.224.2): icmp_seq=4 ttl=47 time=139 ms
64 bytes from www.archive.org (207.241.224.2): icmp_seq=5 ttl=47 time=139 ms
tzkut@tzkuat-pc:~$
```

Il est aussi possible de spécifier le signal à envoyer pour terminer l'opération. Je suis loin d'être un expert, mais il est possible d'envoyer tous les signaux disponibles via la commande `kill -l` :
```
tzkuat@tzkuat-pc:~$ kill -l
 1) SIGHUP       2) SIGINT       3) SIGQUIT      4) SIGILL       5) SIGTRAP
 6) SIGABRT      7) SIGBUS       8) SIGFPE       9) SIGKILL     10) SIGUSR1
11) SIGSEGV     12) SIGUSR2     13) SIGPIPE     14) SIGALRM     15) SIGTERM
16) SIGSTKFLT   17) SIGCHLD     18) SIGCONT     19) SIGSTOP     20) SIGTSTP
21) SIGTTIN     22) SIGTTOU     23) SIGURG      24) SIGXCPU     25) SIGXFSZ
26) SIGVTALRM   27) SIGPROF     28) SIGWINCH    29) SIGIO       30) SIGPWR
31) SIGSYS      34) SIGRTMIN    35) SIGRTMIN+1  36) SIGRTMIN+2  37) SIGRTMIN+3
38) SIGRTMIN+4  39) SIGRTMIN+5  40) SIGRTMIN+6  41) SIGRTMIN+7  42) SIGRTMIN+8
43) SIGRTMIN+9  44) SIGRTMIN+10 45) SIGRTMIN+11 46) SIGRTMIN+12 47) SIGRTMIN+13
48) SIGRTMIN+14 49) SIGRTMIN+15 50) SIGRTMAX-14 51) SIGRTMAX-13 52) SIGRTMAX-12
53) SIGRTMAX-11 54) SIGRTMAX-10 55) SIGRTMAX-9  56) SIGRTMAX-8  57) SIGRTMAX-7
58) SIGRTMAX-6  59) SIGRTMAX-5  60) SIGRTMAX-4  61) SIGRTMAX-3  62) SIGRTMAX-2
63) SIGRTMAX-1  64) SIGRTMAX
```

Par défaut, le signal `SIGTERM` est utilisé mais il peut être ignoré par certains programmes.

Un exemple en utilisant `SIGKILL` (aussi possible via le numéro) :
* `timeout -s SIGKILL 2s ping archive.org`
* `timeout -s 9 2s ping archive.org`

Résultat :
```
tzkuat@tzkuat-pc:~$ timeout -s SIGKILL 2 ping archive.org
PING archive.org (207.241.224.2) 56(84) bytes of data.
64 bytes from www.archive.org (207.241.224.2): icmp_seq=1 ttl=47 time=139 ms
64 bytes from www.archive.org (207.241.224.2): icmp_seq=2 ttl=47 time=139 ms
Killed
```

Encore une fois, si cela vous intéresse, vous trouverez toutes les options avec la commande `man timeout`.


## Commande visudo

La gestion des privilèges est la base de la sécurité pour les systèmes GNU/Linux. Pour gérer ces privilèges il faut modifier le fichier `/etc/sudoers` que vous connaissez déjà probablement.

Il est très facile de faire une bêtise en modifiant ce fichier, rendant la configuration inutilisable. Pour modifier ce fichier, j'ai longtemps utilisé des éditeurs comme `vi` ou encore `nano`, ce qui est en soi une très mauvaise pratique.

C'est là qu'intervient `visudo`. Cette commande ouvre un éditeur de texte comme `vi` ou encore `nano`, mais au moment d'enregistrer l'outil va valider la syntaxe du fichier, ce qui n'est pas le cas en utilisant directement les éditeurs classiques.

Même si vous tapez `visudo`, il est possible que l'outil utilise `nano` pour ouvrir le fichier. Cela va dépendre de l'éditeur par défaut de votre système, à vous de l'adapter en fonction.

## Commande mtr

J'ai découvert récemment l'outil `mtr` via le site [Tecmint](https://www.tecmint.com) :
* https://www.tecmint.com/mtr-a-network-diagnostic-tool-for-linux/

Cet outil en ligne de commande est un utilitaire de diagnostique réseau (c'est toujours la faute du réseau de toute façon...), il conbine `ping` et `traceroute` en une seule interface (l'outil existe aussi en interface graphique avec mtr-gtk). C'est à dire que le traceroute se fait via l'ICMP et non l'UDP.

Cela permet d'avoir en un coup d'oeil, l'ensemble des sauts entre deux machines avec la latence de chaque saut. Il peut être alors intéressant de lancer l'outil afin de vérifier la connectivité entre deux machines, afin de voir à quel niveau la latence est la plus élevée, c'est un outil très pratique pour détecter des problèmes sur des équipements.

Il est aussi possible de lancer la commande pour une longue durée, notamment après des migrations afin d'évaluer les évolutions par exemple.

Pour l'utiliser, il suffit de lancer la commande :
* `mtr <domaine ou IP>`

Voici un exemple pour `mtr archive.org` depuis un VPS de chez Gandi :
![](/images/mtr-1.png)

L'outil est complet et il est possible d'utiliser plusieurs options et affichages :
![](/images/mtr-2.png)

Une fois la commande lancée, le menu via le CLI est interactif, il suffit d'appuyer sur `r` pour remettre les compteurs à zéro,  `d` pour changer l'affichage...

Si cela vous intéresse, toutes les options sont disponibles via la commande `man mtr`.


J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !