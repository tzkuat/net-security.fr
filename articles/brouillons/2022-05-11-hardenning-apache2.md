---
title: Veille sécurité avec OpenCVE.io
author: Mickael Rigonnaux
type: post
date: 2021-xx-xxT10:05:44+00:00
url: /securite/veille-opencveio
thumbnail: /images/logo-3.png
featureImage: images/logo-3.png
shareImage: /images/logo-3.png
featured: true
tags: [
    "opensource",
    "cve",
    "veille"
]
categories: [
    "securite"
]

---
Bonjour à tous ! Aujourd’hui un article rapide

## Commande `chfn`

## Commande `wall`

## Commandes `zcat` & `zgrep`

## [Bonus] `/etc/skel` & rechercher dans l'historique

J’espère que cet article vous aura plu, si vous avez des questions ou des remarques sur ce que j’ai pu écrire n’hésitez pas à réagir avec moi par mail ou en commentaire ! N’hésitez pas à me dire également si ce genre d’article vous plaît !

Merci pour votre lecture et à bientôt !