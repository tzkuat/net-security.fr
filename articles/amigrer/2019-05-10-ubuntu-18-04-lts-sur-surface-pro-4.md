---
title: Ubuntu 18.04 LTS sur Surface Pro 4
author: Fabio Pace
type: post
date: 2019-05-10T11:00:39+00:00
url: /system/ubuntu-18-04-lts-sur-surface-pro-4/
thumbnail: /images/UbuntuOnSurface/surface_pro0-1.png

---

## Présentation

Bonjour à tous, aujourd'hui je vais vous expliquer mon envie de passer sur Ubuntu sur ma Surface Pro 4.

Pour vous mettre dans le contexte, voici les caractéristiques de la surface : 

  * CPU : Intel I5 7300
  * RAM : 4Go
  * Disque : SSD 128Go
  * Pas de carte graphique (chipset du processeur)

Comme vous pouvez le voir, la RAM n'étant pas élevée, et Windows n'est pas encore bien optimisé pour tourner sur un petit équipement de ce type (ce n'est que mon avis), surtout si vous utilisez un navigateur web avec plsuieurs onglets. En effet, le système d'exploitation prend déjà presque la moitier des ressources dès son démarage (avec une « fresh » installation).

L'ajout ou la modification niveau hardware est impossible car la RAM et le SSD sont soudés à la carte mère.

Ubuntu étant basé sur le noyeau Debian, et nous savons tous que les noyaux linux ont la particularité d'être léger en taille (peu d'espace sur le disque), peu d'allocation en RAM et très performant.

## Pourquoi Ubuntu

Ubuntu à la particularité d'être « user-friendly ». Son implémentation dans le monde du « GUI » est quasi parfaite. L'interface est fluide et soignée. Beaucoup de paquets sont développés pour les noyaux Debian (Donc Ubuntu). Je suis passé sur ArchLinux avec l'interface Manjaro. Le système est assez fluide mais je trouve non optimisée sur la Surface (problème d'écriture, taille de la police, taille de certaines fenêtres&#8230;).

Je suis donc revenu sur Ubuntu, et plus particulièrement sur la version 18.04 en LTS (pour garantir le support coté OS).

## L'instalation d'Ubuntu

Avant d'installer votre système d'exploitation, je vous conseil de faire une sauvegarde de tous vos fichiers se trouvant sur la partition Windows, car si vous souhaitez n'avoir que Ubuntu sur votre machine (car peu d'espace sur le disque et que vous en avez mare de Windows), la suppression du disque sera obligatoire. 

Puis, vous redémarrer votre Surface sur l'UEFI en restant appuyé sur les touches : 

```Power + Volume Haut```

Vous désactivez le secure boot qui risque de vous déranger pour l'installation de l'OS, ou plus particulièrement d'installer un kernel linux recompiler qui permet d'activer l'écran tactile.![](/images/)

[<img loading="lazy" width="737" height="470" src="images/UbuntuOnSurface/surface_pro.png" alt="" class="wp-image-916" srcset="images/UbuntuOnSurface/surface_pro.png 737w, images/UbuntuOnSurface/surface_pro-300x191.png 300w" sizes="(max-width: 737px) 100vw, 737px" />][1]</figure> 

Je vous conseil de désactiver complètement le secure boot car le kernel que nous allons récupérer n'est pas signé par Microsoft ni par un tiers.![](/images/)

[<img loading="lazy" width="629" height="433" src="images/UbuntuOnSurface/surface_pro2.png" alt="" class="wp-image-917" srcset="images/UbuntuOnSurface/surface_pro2.png 629w, images/UbuntuOnSurface/surface_pro2-300x207.png 300w" sizes="(max-width: 629px) 100vw, 629px" />][2]</figure> 

Puis, il faut booter sur la clef que vous avez préparé en amont en appuyant sur les boutons : 

```Power + Volume Bas```

L'installation d'Ubuntu va débuter. 

## Configuration de l'écran tactile  


Suite à l'installation de base d'Ubuntu, l'écran tactile ne fonctionne pas (et oui ca aurais été trop beau). 

Cependant, grâce au github de Jakeday <https://github.com/jakeday/linux-surface> , nous allons éviter de recompiler à la main le kernel d'Ubuntu pour intégrer les modules pour l'écran tactile.

Pour cela, vous devez installer les outils pour intéragir avec git et d'exécuter les différentes commandes du script : 


```

```
o apt install git curl wget sed

```


Puis, il faut cloner le repo : 


```

```
 clone --depth 1 https://github.com/jakeday/linux-surface.git ~/linux-surface

```


Et enfin, lancer le script :


```

```
o sh ~/linux-surface/setup.sh

```


Le script va vous poser différentes questions (les réponses suivantes sont préconisés par l'éditeur du script) :

  * **No** : Vous allez garder le module Suspend, si vous souhaitez utiliser Hibernate, veuillez vous référer au README du repo
  * **Yes** : Installer le package pour le stylet (libwacom)
  * **Yes** : Suppression du fichier de configuration par défaut de xorg
  * **Yes** : Suppression des fichiers de configuration par défaut de pulseaudio et installation des firmwares adéquat (ipts, i915&#8230;)
  * **Yes** : Changer l'heure de votre machine pour éviter des conflits avec le dualboot Windows. (dans le cas ou vous n'avez pas de dualboot, vous pouvez saisir « No »)
  * Yes : Installer le dernier Kernel qui a été recompilé (renseigner « No » si vous souhaitez le télécharger et l'installer à la main : voir dans le github)

Vous pouvez redémarrer votre surface, et profiter d'Ubuntu en version tactile.

J'espère que l'article vous aura plus, n'hésitez pas à le commenter si vous avez la moindre remarque.

Bonne journée,

Fabio Pace

 [1]: images/UbuntuOnSurface/surface_pro.png
 [2]: images/UbuntuOnSurface/surface_pro2.png