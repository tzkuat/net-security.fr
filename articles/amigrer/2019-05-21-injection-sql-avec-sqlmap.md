---
title: Injection SQL avec SQLMap
author: Fabio Pace
type: post
date: 2019-05-21T16:00:48+00:00
url: /security/injection-sql-avec-sqlmap/
thumbnail: /images/SqlMap/sqlmap0.png

---
![](/)
  <figure class="aligncenter is-resized"><a href="images/SqlMap/sqlmap0.png"><img loading="lazy" src="images/SqlMap/sqlmap0.png" alt="" class="wp-image-1012" width="545" height="230" srcset="images/SqlMap/sqlmap0.png 764w, images/SqlMap/sqlmap0-300x127.png 300w" sizes="(max-width: 545px) 100vw, 545px" /></a></figure>
</div>

Bonjour à tous, aujourd'hui nous allons voir un outil très simple à utiliser afin d'exécuter des injections SQL en masse : SQLMap.

## Présentation

Tout d'abord, une injection SQL c'est le faite d'attaquer une base de données en envoyant des requêtes SQL afin d'essayer de récupérer des informations de la base de données (structure, utilisateurs, mots de passe&#8230;).

Cet outil est open source et est inclut dans Kali.

Si vous utilisez un système autre que Kali, il vous faudra installer l'outil via le github officiel. Sur CentOS, on aura donc : 


```
yum install git python
mkdir -p /opt/tools && cd /opt/tools/
git clone https://github.com/sqlmapproject/sqlmap.git && cd sqlmap
```


Puis, vous pouvez lancer sqlmap via l'utilitaire python : 


```

```
hon sqlmap.py

```


Dans notre cas nous utiliserons directement sqlmap de Kali donc qui se lance via la commande _sqlmap_

## Préparation de la plateforme web

Il vous faudra préparer votre environnement afin de réaliser vos injections SQL. Pour cela, nous allons utiliser DVWA.

<u>Qu'est-ce que DVWA ?</u>

[DVWA][1] (**Damn Vulnerable Web App**) est une application web vulnérable écrite en PHP/SQL. Cette application permet de découvrir le « web pentesting ».

Vous pouvez récupérer un LiveCD (prêt à l&#8217;emplois) en ISO [ici][2] pour monter votre VM web vulnérable ou suivre l'article de Mickaël pour la déployer via Docker : <https://net-security.fr/system/commencez-le-pentest-avec-docker/>

Lorsque votre installation est terminée, récupérez l'adresse IP de votre machine et accédez à l'interface web. Dans mon cas ça sera **http://dvwa.lab.local**

## Configuration de DVWA

Si vous n'avez pas pris le LiveCD, il vous faudra configurer la base de données.

Si vous avez injecté des éléments dans la base de données, vous pouvez la recréer rapidement via l'onglet Setup :

![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap1.png"><img loading="lazy" width="870" height="496" src="images/SqlMap/sqlmap1.png" alt="" class="wp-image-994" srcset="images/SqlMap/sqlmap1.png 870w, images/SqlMap/sqlmap1-300x171.png 300w, images/SqlMap/sqlmap1-768x438.png 768w" sizes="(max-width: 870px) 100vw, 870px" /></a></figure>
</div>

Si vous commencez à faire du pentest, ou simplement tester des injections SQL simplement via SQLMap, il vous faudra mettre le niveau de sécurité à « faible » : 

![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap2.png"><img loading="lazy" width="835" height="655" src="images/SqlMap/sqlmap2.png" alt="" class="wp-image-995" srcset="images/SqlMap/sqlmap2.png 835w, images/SqlMap/sqlmap2-300x235.png 300w, images/SqlMap/sqlmap2-768x602.png 768w" sizes="(max-width: 835px) 100vw, 835px" /></a></figure>
</div>

## La mise en place de l'injection SQL

Tout est prêt pour faire notre injection SQL via l'outil SQLMap. C'est partie !

Tout d'abord, vous devez vous rendre dans l'onglet « SQL Injection » :

![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap3.png"><img loading="lazy" width="651" height="521" src="images/SqlMap/sqlmap3.png" alt="" class="wp-image-997" srcset="images/SqlMap/sqlmap3.png 651w, images/SqlMap/sqlmap3-300x240.png 300w" sizes="(max-width: 651px) 100vw, 651px" /></a></figure>
</div>

Nous allons tester le bouton « submit » pour vérifier si des paramètres sont envoyés au serveur : 

![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap4.png"><img loading="lazy" width="984" height="541" src="images/SqlMap/sqlmap4.png" alt="" class="wp-image-998" srcset="images/SqlMap/sqlmap4.png 984w, images/SqlMap/sqlmap4-300x165.png 300w, images/SqlMap/sqlmap4-768x422.png 768w" sizes="(max-width: 984px) 100vw, 984px" /></a></figure>
</div>

Nous pouvons voir que le serveur attend deux paramètres : 

  * Un identifiant via le paramètre id=
  * L'action du bouton : Submit=Submit

Nous allons donc tester directement via l'outil SQLMap si le champs « User ID » est injectable :


```

```
map -u "http://dvwa.lab.local/vulnerabilities/sqli/?id=&Submit=Submit"

```


<u style="color:red;font-weight:bold;">NB :</u> L'argument -u permet de spécifier une cible 

![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap5.png"><img loading="lazy" width="1024" height="248" src="images/SqlMap/sqlmap5-1024x248.png" alt="" class="wp-image-999" srcset="images/SqlMap/sqlmap5-1024x248.png 1024w, images/SqlMap/sqlmap5-300x73.png 300w, images/SqlMap/sqlmap5-768x186.png 768w, images/SqlMap/sqlmap5.png 1101w" sizes="(max-width: 1024px) 100vw, 1024px" /></a></figure>
</div>

Nous pouvons observer que le site nous redirige (code 302 pour redirection permanente) vers l'accueil où il y a le formulaire de connexion. 

Nous devons donc trouver le moyen de se connecter via l'outil. En recherchant dans la page web, nous trouvons des cookies de sessions : 

![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap6.png"><img loading="lazy" width="638" height="538" src="images/SqlMap/sqlmap6.png" alt="" class="wp-image-1000" srcset="images/SqlMap/sqlmap6.png 638w, images/SqlMap/sqlmap6-300x253.png 300w" sizes="(max-width: 638px) 100vw, 638px" /></a></figure>
</div>

Nous récupérons le PHPSESSID et également la valeur du niveau de sécurité que nous avons fixé précédement.

Nous allons donc ressaisir la commande en incluant le paramètre cookie : 


```

```
map -u "http://dvwa.lab.local/vulnerabilities/sqli/?id=&Submit=Submit" --cookie="PHPSESSID=hch0coigflcc0lakj4vect5v93;security=low"

```


![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap7.png"><img loading="lazy" width="1024" height="490" src="images/SqlMap/sqlmap7-1024x490.png" alt="" class="wp-image-1001" srcset="images/SqlMap/sqlmap7-1024x490.png 1024w, images/SqlMap/sqlmap7-300x144.png 300w, images/SqlMap/sqlmap7-768x368.png 768w, images/SqlMap/sqlmap7.png 1115w" sizes="(max-width: 1024px) 100vw, 1024px" /></a></figure>
</div>

Nous obtenons 3 informations importantes concernant l'injection SLQ : 

<u style="font-weight:bold;color:red">A :</u> Il y a 3 payload qui permettent d'injecter une commande, la plus simple est de type booléen « vide » : id= » OR NOT nombre=nombre&Submit=Submit ». Le nombre étant égal à lui même, la commande est injectée.

<u style="font-weight:bold;color:red">B :</u> L'outil a récupéré les informations de la base de données utilisées : MySQL, avec une estimation de la version.

<u style="font-weight:bold;color:red">C :</u> L&#8217;emplacement de sauvegarde des différentes données.

Nous avons trouvé la porte d'entrée, il nous reste plus qu'à récupérer les informations, comme par exemple les bases de données :


```

```
map -u "http://dvwa.lab.local/vulnerabilities/sqli/?id=&Submit=Submit" --cookie="PHPSESSID=hch0coigflcc0lakj4vect5v93;security=low" --dbs

```


![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap8.png"><img loading="lazy" width="516" height="255" src="images/SqlMap/sqlmap8.png" alt="" class="wp-image-1002" srcset="images/SqlMap/sqlmap8.png 516w, images/SqlMap/sqlmap8-300x148.png 300w" sizes="(max-width: 516px) 100vw, 516px" /></a></figure>
</div>

Nous récupérons le nom de 6 bases de données.

Nous pouvons faire la même chose avec l'argument tables pour lister toutes les tables de chaque base de données :


```

```
map -u "http://dvwa.lab.local/vulnerabilities/sqli/?id=&Submit=Submit" --cookie="PHPSESSID=hch0coigflcc0lakj4vect5v93;security=low" --tables

```


![](/)
  <figure class="aligncenter is-resized"><a href="images/SqlMap/sqlmap9.png"><img loading="lazy" src="images/SqlMap/sqlmap9.png" alt="" class="wp-image-1003" width="275" height="524" srcset="images/SqlMap/sqlmap9.png 333w, images/SqlMap/sqlmap9-158x300.png 158w" sizes="(max-width: 275px) 100vw, 275px" /></a></figure>
</div>

Mainteant que vous avez les différentes bases de données et tables existantes, vous pouvez récupérer les colonnes d'une table spécifique. Nous prendrons l'exemple sur la table « users » de la base « dvwa » :


```

```
map -u "http://dvwa.lab.local/vulnerabilities/sqli/?id=1&Submit=Submit" --cookie="PHPSESSID=hch0coigflcc0lakj4vect5v93;security=low" -D dvwa -T users --columns

```


<u style="color:red;font-weight:bold">NB</u>  
-D : base de données  
-T : table  
-columns : liste les colonnes de la table  


![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap10-1.png"><img loading="lazy" width="279" height="209" src="images/SqlMap/sqlmap10-1.png" alt="" class="wp-image-1005" /></a></figure>
</div>

Grâce à ces informations, nous allons pouvoir essayer de faire un dump des utilisateurs et de leur mot de passe : 


```

```
map -u "http://dvwa.lab.local/vulnerabilities/sqli/?id=1&Submit=Submit" --cookie="PHPSESSID=hch0coigflcc0lakj4vect5v93;security=low" -D dvwa -T users -C user,password --dump

```


<u style="color:red;font-weight:bold">NB</u>  
-C : sélectionne les colonnes  
-dump : récupère les informations  


![](/)
  <figure class="aligncenter"><a href="images/SqlMap/sqlmap11.png"><img loading="lazy" width="909" height="693" src="images/SqlMap/sqlmap11.png" alt="" class="wp-image-1006" srcset="images/SqlMap/sqlmap11.png 909w, images/SqlMap/sqlmap11-300x229.png 300w, images/SqlMap/sqlmap11-768x586.png 768w" sizes="(max-width: 909px) 100vw, 909px" /></a></figure>
</div>

SQLMap vous demande si vous souhaitez utiliser un dictionnaire de mot de passe pour qu'il trouve la correspondance des hashs : répondez oui.

On obtient donc les mots de passe de chaque utilisateur.

Vous savez maintenant utiliser l'outil SQLMap pour réaliser des injections SQL rapidement. N'hésitez pas à regarder les différents arguments possibles, car vous pouvez faire beacuoup de chose (comme faire apparaître un prompt SQL par exemple).

J'espère que l'article vous aura plu. N'hésitez pas à commenter si vous avez la moindre remarque.

A bientôt, 

Fabio Pace.

 [1]: http://www.dvwa.co.uk/
 [2]: http://www.dvwa.co.uk/DVWA-1.0.7.iso