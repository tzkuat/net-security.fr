---
title: Configuration OpenVPN sur EdgeRouter-X
author: Fabio Pace
type: post
date: 2019-10-30T12:58:25+00:00
url: /system/configuration-openvpn-sur-edgerouter-x/
thumbnail: /images/OpenVPN_Ubi/erx_openvpn0.png

---
![](/images/OpenVPN_Ubi/erx_openvpn0.png)

Bonjour à tous, aujourd'hui nous allons voir comment configurer un tunnel VPN client to site via OpenVPN sur le routeur EdgeRouter-X de Ubiquiti.

Ce petit routeur d'une cinquantaine d'euros permet de gérer son (ou plutôt ses) LAN(s) de manière optimale. Vous pouvez le trouver sur [amazon][1].

## Création utilisateur OpenVPN

Tout d'abord, il faut créer le compte utilisateur que vous allez utiliser pour vous connecter au VPN. Pour cela, direction d'interface CLI du routeur : 

```
ssh admin@192.168.5.1
```

Puis, il faut entrer en mode configuration :

```
configure
set system login user utilisateurOpenVPN authentication plaintext-password MotDePasse_UtilisateurOpenVPN
set system login user utilisateurOpenVPN level operator
commit
```

Votre utilisateur est désormais créé, vous pouvez vérifier en saisissant la commande suivante : 

```

```
w system login user

```
![](/images/)

[<img loading="lazy" width="729" height="211" src="images/OpenVPN_Ubi/erx_openvpn1.png" alt="" class="wp-image-1323" srcset="images/OpenVPN_Ubi/erx_openvpn1.png 729w, images/OpenVPN_Ubi/erx_openvpn1-300x87.png 300w" sizes="(max-width: 729px) 100vw, 729px" />][2]</figure> 

## Création des certificats

Nous pouvons passer désormais à la création des ceritifcats. Pour ce faire, vous devez vous connecter en root sur le routeur : 


```

```
o -i

```


Puis, nous allons dans le dossier de configuration OpenVPN que nous créons au préalable : 


```
mkdir /config/openvpn
cd /config/openvpn
```


Nous commençons par créer l'autorité de certification qui nous permettra d'approuver nos certificats serveur et utilisateur.


```

```
r/lib/ssl/misc/CA.sh -newca

```


Vous devez répondre aux questions posées.

Si vous avez laissé le nom par défaut de la CA, le certificat se trouve dans le répertoire /config/openvpn/demoCA et se nomme **cacert.pem**

Puis, il faut créer le certificat serveur pour OpenVPN. Pour cela, nous allons créer un CSR (une demande de certificat) : 


```

```
r/lib/ssl/misc/CA.sh -newreq

```


Vous devez là encore répondre aux questions.

Vous avez normalement deux fichiers qui ont été générés : 

  * **newkey.pem** : clé privé du CSR
  * **newreq.pem** : CSR généré

Nous devons désormais signer le certificat serveur via la CA générée précédement : 


```

```
r/lib/ssl/misc/CA.sh -sign

```


Après avoir saisie le mot de passe de la CA, votre certificat est signé et un nouveau fichier a été créé : 

  * **newcert.pem** : certificat du serveur signé par la CA

Pour une meilleure gestion des certificats, je vous recommande de renommer vos certificats : 


```
mv newcert.pem server.pem
mv newkey.pem server.key
```


Et de supprimer votre CSR qui ne sert plus à rien : 


```

```
newreq.pem

```


Nous allons désormais utiliser openssl qui va déchiffrer la clé privée afin qu'OpenVPN puisse utiliser la clé sans saisir la passphrase :


```

```
nssl rsa -in server.key -out server-decrypted.key

```


Pour effectuer les échanges des clés de manière sécurisé, nous utilisons Diffie Hellman avec la commande suivante : 


```

```
nssl dhparam -out dh2048.pem -2 2048

```


La génération des paramètres va prendre environs 30 minutes.

Nous allons maintenant créer le certificat de l'utilisateur qui va se connecter au VPN : 


```

```
r/lib/ssl/misc/CA.sh -newreq

```


**<span style="text-decoration: underline;">NB :</span>** _Dans la partie « Common Name », saisissez le nom de l'utilisateur._


```

```
r/lib/ssl/misc/CA.sh -sign

```



```
mv newcert.pem utilisateurOpenVPN.pem
mv newkey.pem utilisateurOpenVPN.key
rm newreq.pem
openssl rsa -in utilisateurOpenVPN.key -out utilisateurOpenVPN-decrypted.key
```


## Configuration routeur 

Nous allons maintenant pouvoir faire la configuration du routeur pour initialiser OpenVPN.

Tout d'abord, vous devez revenir en mode « admin » pour commencer la configuration : 


```
exit
configure
```


Puis, nous commençons par déclarer l'interface vtun0 en lui ajoutant une description, le mode de chiffrement : 


```
set interfaces openvpn vtun0
set interfaces openvpn vtun0 description "OpenVPN server"
set interfaces openvpn vtun0 mode server
set interfaces openvpn vtun0 encryption aes256
set interfaces openvpn vtun0 hash sha256
```


On crée un réseau pour le VPN, on ajoute les routes si on souhaite inter-connecter le réseau du VPN et d'autres réseaux, et le dns du réseau :


```
set interfaces openvpn vtun0 server subnet 192.168.200.0/24
set interfaces openvpn vtun0 server push-route 192.168.10.0/24
set interfaces openvpn vtun0 server name-server 192.168.200.1
```


Les différents fichiers de configuration pour les certificats : 


```
set interfaces openvpn vtun0 tls ca-cert-file /config/openvpn/demoCA/cacert.pem
set interfaces openvpn vtun0 tls cert-file /config/openvpn/server.pem
set interfaces openvpn vtun0 tls key-file /config/openvpn/server-decrypted.key
set interfaces openvpn vtun0 tls dh-file /config/openvpn/dh2048.pem
```


Enfin, les options d'OpenVPN pour son bon fonctionnement : 


```
set interfaces openvpn vtun0 openvpn-option "--port 1194"
set interfaces openvpn vtun0 openvpn-option --tls-server
set interfaces openvpn vtun0 openvpn-option "--comp-lzo yes"
set interfaces openvpn vtun0 openvpn-option --persist-key
set interfaces openvpn vtun0 openvpn-option --persist-tun
set interfaces openvpn vtun0 openvpn-option "--keepalive 10 120"
set interfaces openvpn vtun0 openvpn-option "--user nobody"
set interfaces openvpn vtun0 openvpn-option "--group nogroup"
```


Il faut autoriser les flux dans le firewall, pour cela on ajoute les commandes suivantes : 


```
set firewall name WAN_LOCAL rule 20 action accept
set firewall name WAN_LOCAL rule 20 description 'Allow OpenVPN'
set firewall name WAN_LOCAL rule 20 destination port 1194
set firewall name WAN_LOCAL rule 20 protocol udp
```


On active l'écoute des requêtes DNS sur l'interface OpenVPN : 


```

```
 service dns forwarding listen-on vtun0

```


## Le fichier OVPN pour le client

Pour la connexion du client, il suffit de créer le fichier .ovpn avec les infos ci-dessous : 

```client
dev tun
proto udp
remote <IP PUBLIC ou FQDN> 1194
cipher AES-256-CBC
auth SHA256
resolv-retry infinite
redirect-gateway def1
nobind
comp-lzo yes
persist-key
persist-tun
user nobody
group nogroup
verb 3
setenv ALLOW_PASSWORD_SAVE 0 
auth-user-pass

<ca>
-----BEGIN CERTIFICATE-----

-----END CERTIFICATE-----
</ca>

<cert>
-----BEGIN CERTIFICATE-----

-----END CERTIFICATE-----
</cert>

<key>
-----BEGIN RSA PRIVATE KEY-----

-----END RSA PRIVATE KEY-----
</key>```

Pour rappel, les différents fichier que vous avez besoin pour compléter votre fichier .ovpn : 

  * /config/openvpn/demoCA/**cacert.pem**
  * /config/openvpn/**utilisateurOpenVPN.pem**
  * /config/openvpn/**utilisateurOpenVPN-decrypted.key**

Si vous souahitez récupérer votre IP public, vous pouvez éxécuter la commade suivante : 


```

```
l http://ipconfig.io/ip

```


Vous pouvez vérifier votre configuration en éxécutant la commande suivante : 


```

```
w interfaces openvpn

```


![](/)
  <figure class="aligncenter"><a href="images/OpenVPN_Ubi/erx_openvpn2.png"><img loading="lazy" width="468" height="517" src="images/OpenVPN_Ubi/erx_openvpn2.png" alt="" class="wp-image-1328" srcset="images/OpenVPN_Ubi/erx_openvpn2.png 468w, images/OpenVPN_Ubi/erx_openvpn2-272x300.png 272w" sizes="(max-width: 468px) 100vw, 468px" /></a></figure>
</div>

Vous pouvez désormais importer le fichier .ovpn dans votre client OpenVPN, et tester la connexion. 

J’espère que l’article vous aura plu. N’hésitez pas à commenter si vous avez la moindre remarque.

A bientôt, 

Fabio Pace.

 [1]: https://www.amazon.fr/Ubiquiti-Networks-ER-X-Ethernet-connect%C3%A9/dp/B011N1IT2A/ref=pd_sbs_147_t_0/258-2454265-7490337?_encoding=UTF8&pd_rd_i=B011N1IT2A&pd_rd_r=03817bf2-787d-41c0-b54d-b441cc965766&pd_rd_w=0dGOI&pd_rd_wg=09HVc&pf_rd_p=9b28d941-c13a-4c2b-b935-36854aa20020&pf_rd_r=MN4AG3VK475ZX0ZXMQSW&psc=1&refRID=MN4AG3VK475ZX0ZXMQSW
 [2]: images/OpenVPN_Ubi/erx_openvpn1.png