#!/bin/bash
mkdir /opt/themes

git clone $HUGO_THEME_URL /opt/themes/hugo-clarity

hugo --themesDir /opt/themes -d /output
